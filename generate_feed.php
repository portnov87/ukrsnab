<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 07.07.2018
 * Time: 21:49
 */

Class SimpleXMLElementExtended extends SimpleXMLElement
{
    /**
     * Adds a child with $value inside CDATA
     * @param unknown $name
     * @param unknown $value
     */
    public function addChildWithCDATA($name, $value = NULL)
    {
        $new_child = $this->addChild($name);

        if ($new_child !== NULL) {
            $node = dom_import_simplexml($new_child);
            $no = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($value));
        }

        return $new_child;
    }
}

umask(0);
ini_set("display_errors", 1);
error_reporting(E_ALL);
//libxml_disable_entity_loader(false);
/*ob_implicit_flush(1);

echo str_pad('', 1024);
@ob_flush();
flush();
*/
date_default_timezone_set("Europe/Kiev");
//header('Content-Type: text/xml; charset=utf-8', true); //set document header content type to be XML

$title = 'Ukrsnab - интернет-гипермаркет строительной и промышленной техники';
$link = 'https://ukrsnab.com.ua';

$shippingcountry = 'UA';
$shippingcountrylong = ' UAH';
$shippingservice = 'Standard';
$shippingprice = '5.95' . $shippingcountrylong;


function xsanatise($var)
{
    $var = strip_tags($var);
    $var = htmlspecialchars($var, ENT_XML1, 'UTF-8');
    $var = htmlspecialchars($var, ENT_QUOTES, 'UTF-8');
    $var = preg_replace('/[\x00-\x1f]/', '', $var);
    $var = str_ireplace(array('<', '>', '&', '\'', '"'), array('&lt;', '&gt;', '&amp;', '&apos;', '&quot;'), $var);
    $var = str_replace('&nbsp;', ' ', $var);
    $var = str_replace('&ndash;', ' ', $var);

    return $var;
}

function xsanatisegoogle($var)
{

    $var = htmlspecialchars($var, ENT_XML1, 'UTF-8');
    $var = str_replace('&amp;gt;', '>', $var);
    $var = str_replace('&amp;gt,', '>', $var);
    $var = str_replace('&gt,', '>', $var);
    $var = str_replace('&gt;', '>', $var);

    $var = str_replace('&amp;', '&', $var);


    return $var;
}


function xcheck($var)
{

    $var = trim($var);

    if (strlen($var) >= 1 && preg_match('/[A-Z]+[a-z]+[0-9]+/', $var) !== false) {
        return true;
    } else {
        return false;
    }

}


function xcheckgoogle($var)
{

    $var = trim($var);

    if (strlen($var) >= 1 && preg_match('/^[1-9][0-9]*$/', $var) !== false) {
        return true;
    } else {
        return false;
    }

}


require_once dirname(__FILE__) . '/app/Mage.php';
Mage::app()->getCacheInstance()->flush();

Mage::app('admin')->setUseSessionInUrl(false);
Mage::app('default');



function getPath($product)
{
    $path = array();


    if ($product) {
        //$categoriesid = $this->getProduct()->getCategoryIds();
        $categoriesid = [];
        //$level=
        foreach ($product->getCategoryCollection() as $cat) {
            $categoriesid[] = $cat->getId();
        }
        if (count($categoriesid) > 0) {
            $category_id = $categoriesid[count($categoriesid) - 1];
            $category = Mage::getModel('catalog/category')->load($category_id);
        }


    }
    /*} else {
        $category = $this->getCategory();
    }*/


    if ($category) {
        $pathInStore = $category->getPathInStore();
        $pathIds = array_reverse(explode(',', $pathInStore));

        $categories = $category->getParentCategories();

        // add category path breadcrumb
        $c = count($pathIds);// - 1;
        $i = 1;
        if ($product) {
            $current_product = $product->getId();
        } else $current_product = false;

        foreach ($pathIds as $categoryId) {
            if (isset($categories[$categoryId])) {


                if ($categories[$categoryId]->getName() != '') {
                    $path['category' . $categoryId] = $categories[$categoryId]->getName();
                }

                $i++;

                // }
            }
        }

        if ($category->getName() != '') {
            $path['category' . $category->getId()] = $category->getName();

        }
    }


    return $path;

}


function getCategories()
{
    $collection = Mage::getModel('catalog/category')
        ->getCollection()
        ->addAttributeToSelect('*');
        //->addUrlRewrite()
        //->addAttributeToFilter('status', array('eq' => 1));

    return $collection;

}
function generate_feed($file)
{
    $siteUrl='https://ukrsnab.com.ua/';
    $siteName=$companyName='УкрСнаб';

    $yml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
    $yml .= "<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n";
    $yml .= "<yml_catalog date=\"".date('Y-m-d H:i')."\">\n";
    $yml .= "	<shop>\n";
    $yml .= "		<name>$siteName</name>\n";
    $yml .= "		<company>$companyName</company>\n";
    $yml .= "		<url>$siteUrl</url>\n";


    $yml .= "		<currencies>\n";
    $yml .= "			<currency id=\"UAH\" rate=\"1\"/>\n";
    //$yml .= "			<currency id=\"USD\" rate=\"CBRF\"/>\n";
    //$yml .= "			<currency id=\"EUR\" rate=\"CBRF\"/>\n";
    $yml .= "		</currencies>\n";

    $yml .= "		<categories>\n";

    $categories=getCategories();
    foreach ($categories as $category){
        $parent=$category->getParentId();
        $idcategory=$category->getId();
        $name=$category->getName();
        if ($parent==3){
            $yml .= '<category id="' . $idcategory . '">' . $name . '</category>' . "\n";
        }else {
            $yml .= '<category id="' . $idcategory . '" parentId="' . $parent . '">' . $name . '</category>' . "\n";
        }
    }
    $yml .= "		</categories>\n";


    $yml .= "		<offers>\n";

    $collection_product = Mage::getModel('catalog/product')
        ->getCollection()
        ->addAttributeToSelect('*');
        //->addUrlRewrite()
        //->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)); //STATUS_DISABLED
    $resource = Mage::getSingleton('core/resource');

    /**
     * Установка соединения для чтения
     */
    $readConnection = $resource->getConnection('core_read');

    foreach ($collection_product as $product){

        //echo $product->getAttributeSetId();
        //die();
        $product_id=$product->getId();
        $stockItem = $product->getStockItem();
        if ($stockItem->getIsInStock())
            $available='true';
        else
            $available='false';


        $yml .= "		<offer id='".$product_id."' available='".$available."'>\n";
        $name_product=$product->getName();
        $code=$product->getSku();

        $price=$product->getPrice();

        $_price=$product->getPrice();
        $special_price=false;
        if (empty($product->getSpecialPrice())) {
            $price = Mage::helper('core')->currency($product->getFinalPrice(), true, false);
            $price = str_replace('грн.', '', $price);

        } else {
            $price = Mage::helper('core')->currency($product->getFinalPrice(), true, false);
            $price = str_replace('грн.', '', $price);

            $special_price = Mage::helper('core')->currency($product->getSpecialPrice(), true, false);
            $special_price = str_replace('грн.', '', $special_price);

        }

        $query = "SELECT * FROM core_url_rewrite WHERE product_id='" . $id . "' AND category_id IS NULL";
        $results = $readConnection->fetchAll($query);
        $urllink = $product->getProductUrl();
        foreach ($results as $res) {
            $urllink = 'https://ukrsnab.com.ua/' . $res['request_path'];
        }

        $image=Mage::getBaseUrl('media') . 'catalog/product' . $product->getImage();
        //$description=$product->getDescription();
        $description = htmlspecialchars($product->getDescription());
        $paramCurrency='UAH';
        $yml .= "<name>".htmlspecialchars($name_product)."</name>\n";
        $yml .= "<code>".$code."</code>\n";
        if ($special_price)  $yml .= "<oldprice>".$special_price."</oldprice>\n";
        $yml .= "<priceRUAH>".$price."</priceRUAH>\n";
        $yml .= "<price>".$_price."</price>\n";
        //$yml .= "<stock></stock>\n";
        $cats = $product->getCategoryIds();
        foreach ($cats as $cat) {
            $yml .= "<categoryId>" . $cat . "</categoryId>\n";
            break;
        }
        $image=
        $yml .= "<image>".$image."</image>\n";
        $yml .= "<description>".$description."</description>\n";
        $yml .= "<url>".$urllink."</url>\n";
        $yml .= "<currencyId>".$paramCurrency."</currencyId>\n";
        $vendorname=false;
        if ($product->getProizvoditel() !== false) {
            $vendorname=$product->getProizvoditel();

        }
        if ($vendorname)
        $yml .= "<vendor>". $vendorname."</vendor>\n";

        $entityTypeId           = Mage::getResourceModel('catalog/product')->getTypeId();
        $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter($entityTypeId);

        $attributes = $product->getAttributes();
         $attributeSetId = $product->getAttributeSetId();

        foreach ($attributes as $attribute) {

            if (($attribute->getAttributeGroupId()=='215')&&($attribute->getAttributeSetId()==$attributeSetId)){


                $attributeLabel = $attribute->getFrontendLabel();
                $value = $attribute->getFrontend()->getValue($product);
                if (($value!='No')&&($value!='')) {
                    //$params[]=['name']
                    echo $attributeLabel . '-' . $value;
                    echo "<br />";//' . $label . '-
                    $yml .= "<param name=\"$attributeLabel\">". $value."</param>\n";
                    die();
                }
            }
            //die();
        }



        $yml .= "		</offer>\n";
    }

    $yml .= "		</offers>\n";
    $yml .= "	</shop>\n";
    $yml .= "</yml_catalog>\n";

    /*
    <name>
code
stock
priceRUAH
guarantee
image*/


    /*
    $rss = new SimpleXMLElementExtended('<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0"></rss>');

    $channel = $rss->addChild('channel'); //add item node

    $channel->addChild('title', $title); //title of the feed
    $channel->addChild('link', $link); //title of the feed
    $channel->addChild('description', 'Ukrsnab.com.ua - первый интернет-магазин, где представлен весь ассортимент профессионального инструмента, строительного оборудования, техники для сада, огорода, энергоснабжения. Тел: +38(067)230-60-90');


    $root_category = 3;
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    $collection = Mage::getModel('catalog/product')
        ->getCollection()
        ->addAttributeToSelect('*')
        ->addUrlRewrite()
        ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)); //STATUS_DISABLED
    $resource = Mage::getSingleton('core/resource');


    $readConnection = $resource->getConnection('core_read');


    foreach ($collection as $product) {


        $upcmpn = 0;
        $productId = $product->getId();
        $product = Mage::getModel('catalog/product')->load($productId);
        $stockItem = $product->getStockItem();
        if ($stockItem->getIsInStock()) {
            $stock = 'in stock';
        } else {
            $stock = 'out of stock';
        }


        $item = $channel->addChild('item'); //add item node


        $item_group_id = $item->addChild('xmlns:g:id', xsanatise($product->getSku()));


        $item->addChild('xmlns:g:title', xsanatise($product->getName())); //add title node under item


        $urllink = '';

        $urllink = '';
        $id = $product->getId();
        $query = "SELECT * FROM core_url_rewrite WHERE product_id='" . $id . "' AND category_id IS NULL";
        $results = $readConnection->fetchAll($query);
        $urllink = $product->getProductUrl();
        foreach ($results as $res) {
            $urllink = 'https://ukrsnab.com.ua/' . $res['request_path'];
        }


        $link = $item->addChild('xmlns:g:link', $urllink); //add link node under item


        if (xcheck($product->getProizvoditel()) !== false) {
            $item->addChild('xmlns:g:brand', xsanatise($product->getProizvoditel()));
        } else {
            //    $item->addChild('xmlns:g:brand')->addChild('xmlns:g:identifier_exists','false');
        }

        if (xcheck($product->getSku()) !== false) {
            $item->addChild('xmlns:g:mpn', xsanatise($product->getSku()));
        } else {

        }

        $new = $item->addChild('xmlns:g:condition', 'new');

        $item->addChildWithCDATA('xmlns:g:description', strip_tags($product->getDescription()));

        $product_type = getPath($product);
        $_ex_product_type = '';
        if (count($product_type) > 0) {
            $_ex_product_type = 'Главная > ' . implode(' > ', $product_type);
        }
        $item->addChildWithCDATA('xmlns:g:product_type', strip_tags($_ex_product_type));


        $image_link = $item->addChild('xmlns:g:image_link', Mage::getBaseUrl('media') . 'catalog/product' . $product->getImage());

        $cats = $product->getCategoryIds();

        foreach ($cats as $category_id) {
            // $_cat = Mage::getModel('catalog/category')->load($category_id) ;
            switch ($category_id) {
                case '42': //Виброплиты
                    $item->addChild('xmlns:g:custom_label_0', 'Квадрокоптеры');
                    break;
                case '44': //Вибротрамбовки
                    $item->addChild('xmlns:g:custom_label_1', 'Автомобили');
                    break;
                case '45': //Затирочные машины
                    $item->addChild('xmlns:g:custom_label_2', 'Самолеты');
                    break;
                case '49': //Швонарезчики
                    $item->addChild('xmlns:g:custom_label_3', 'Вертолеты');
                    break;
                case '41': //Швонарезчики
                    $item->addChild('xmlns:g:custom_label_4', 'Комплектующие');
                    break;

            }
            // echo $_cat->getName();
        }


        if (($product->getSku() == '') && ($product->getProizvoditel())) {
            $item->addChild('xmlns:g:identifier_exists', ' no');
        }

        if (empty($product->getSpecialPrice())) {
            $price = Mage::helper('core')->currency($product->getFinalPrice(), true, false);
            $price = str_replace('грн.', 'UAH', $price);
            $item->addChild('xmlns:g:price', $price);// . $shippingcountrylong);
        } else {
            $price = Mage::helper('core')->currency($product->getFinalPrice(), true, false);
            $price = str_replace('грн.', 'UAH', $price);
            $item->addChild('xmlns:g:price', $price);// . $shippingcountrylong);
            $special_price = Mage::helper('core')->currency($product->getSpecialPrice(), true, false);
            $special_price = str_replace('грн.', 'UAH', $special_price);
            $item->addChild('xmlns:g:sale_price', $special_price);//. $shippingcountrylong );
        }

        $item->addChild('xmlns:g:availability', $stock);


        // }

        @ob_flush();
        flush();
    }

    $content_feed = $rss->asXML();
    $content_feed = html_entity_decode($content_feed, ENT_NOQUOTES, 'UTF-8');
*/
    //$file = 'google-feed.xml';


    $handle = fopen( '/home/ukrsnab_u/web/www/ukrsnab.com.ua/'.$file, "w+");

    fwrite($handle, header('Content-Type: text/xml; charset=utf8'));
    fwrite($handle, $yml);
    fclose($handle);
}


generate_feed('promoua.xml');