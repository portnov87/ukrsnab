<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 07.07.2018
 * Time: 21:49
 */

ini_set('memory_limit', '1048M');

Class SimpleXMLElementExtended extends SimpleXMLElement
{

    /**
     * Adds a child with $value inside CDATA
     * @param unknown $name
     * @param unknown $value
     */
    public function addChildWithCDATA($name, $value = NULL)
    {
        $new_child = $this->addChild($name);

        if ($new_child !== NULL) {
            $node = dom_import_simplexml($new_child);
            $no = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($value));
        }

        return $new_child;
    }
}


ini_set("display_errors", 1);
error_reporting(E_ALL);
//libxml_disable_entity_loader(false);
ob_implicit_flush(1);

echo str_pad('', 1024);
@ob_flush();
flush();


date_default_timezone_set("Europe/Kiev");
header('Content-Type: text/xml; charset=utf-8', true); //set document header content type to be XML

$title = 'Ukrsnab - интернет-гипермаркет строительной и промышленной техники';
$link = 'https://ukrsnab.com.ua';

$shippingcountry = 'UA';
$shippingcountrylong = ' UAH';
$shippingservice = 'Standard';
$shippingprice = '5.95' . $shippingcountrylong;


function xsanatise($var)
{
    $var = strip_tags($var);
    $var = htmlspecialchars($var, ENT_XML1, 'UTF-8');
    $var = htmlspecialchars($var, ENT_QUOTES, 'UTF-8');
    $var = preg_replace('/[\x00-\x1f]/', '', $var);
    $var = str_ireplace(array('<', '>', '&', '\'', '"'), array('&lt;', '&gt;', '&amp;', '&apos;', '&quot;'), $var);
    $var = str_replace('&nbsp;', ' ', $var);
    $var = str_replace('&ndash;', ' ', $var);

    return $var;
}

function xsanatisegoogle($var)
{

    $var = htmlspecialchars($var, ENT_XML1, 'UTF-8');
    $var = str_replace('&amp;gt;', '>', $var);
    $var = str_replace('&amp;gt,', '>', $var);
    $var = str_replace('&gt,', '>', $var);
    $var = str_replace('&gt;', '>', $var);

    $var = str_replace('&amp;', '&', $var);


    return $var;
}


function xcheck($var)
{

    $var = trim($var);

    if (strlen($var) >= 1 && preg_match('/[A-Z]+[a-z]+[0-9]+/', $var) !== false) {
        return true;
    } else {
        return false;
    }

}


function xcheckgoogle($var)
{

    $var = trim($var);

    if (strlen($var) >= 1 && preg_match('/^[1-9][0-9]*$/', $var) !== false) {
        return true;
    } else {
        return false;
    }

}

$rss = new SimpleXMLElementExtended('<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0"></rss>');

/*
<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">

<channel>
		<title>Planeta Hobby</title>
		<link>https://modelistam.com.ua</link>
		<description>Радиоуправляемые модели и аксессуары в интернет магазине modelistam.com.ua. Розничная торговля радиоуправляемыми моделями вертолетов, самолетов.</description>
<item>*/
//$channel = $rss->addChild('xmlns:g:channel'); //add channel node
//$atom = $rss->addChild('xmlns:g:atom:atom:link'); //add atom node
//$atom->addAttribute('href', 'http://www.mysite.com/google_atom.php'); //add atom node attribute
//$atom->addAttribute('rel', 'self');
//$atom->addAttribute('type', 'application/rss+xml');
$channel = $rss->addChild('channel'); //add item node

$channel->addChild('title', $title); //title of the feed
$channel->addChild('link', $link); //title of the feed
$channel->addChild('description', 'Ukrsnab.com.ua - первый интернет-магазин, где представлен весь ассортимент профессионального инструмента, строительного оборудования, техники для сада, огорода, энергоснабжения. Тел: +38(067)230-60-90');

//$description = $rss->addChild('xmlns:g:description','description line goes here'); //feed description
//$link = $rss->addChild('xmlns:g:link','http://www.mysite.com'); //feed site
//$language = $rss->addChild('xmlns:g:language','en-US'); //language

/*
$updated = $rss->addChild('updated',date("D, d M Y H:i:s T", time())); //language

//Create RFC822 Date format to comply with RFC822
$date_f = date("D, d M Y H:i:s T", time());
$build_date = gmdate(DATE_RFC2822, strtotime($date_f));
$lastBuildDate = $rss->addChild('lastBuildDate',$date_f); //feed last build date
*/


require_once dirname(__FILE__) . '/app/Mage.php';
Mage::app()->getCacheInstance()->flush();


/*
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'include_google_feed', array(
    'group' => 'General Information',
    'input' => 'checkbox',
    'type' => 'int',
    'label' => 'Выводить в фиде гугла',
    'backend' => '',
    'visible' => true,
    'required' => false,
    'visible_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$installer->endSetup();
*/

Mage::app('admin')->setUseSessionInUrl(false);
Mage::app('default');
$root_category = 3;
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
/*$collection = Mage::getModel('catalog/product')
    ->getCollection()
    ->addAttributeToSelect('*')
    ->addUrlRewrite()
    ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
    ->addAttributeToFilter('visibility', array('neq' => 1));


*/

$collectionProducts = Mage::getModel('catalog/product')->getCollection()
    ->addAttributeToFilter('include_feed_promo', 1)
    ->addAttributeToSelect('*')
    ->addUrlRewrite();
    //->addAttributeToFilter('visibility', array('neq' => 1))
    //->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));

$i=1;

foreach ($collectionProducts as $_product)
{
    $productId = $_product->getId();
    $product = Mage::getModel('catalog/product')->load($productId);
    $product->setIncludeFeedPromo(0);
    $product->save();
    echo $i.' '.$productId."\r\n";
    $i++;

}


echo 'count $collectionProducts = '.count($collectionProducts)."\r\n\r\n";


$collectionProducts = Mage::getModel('catalog/product')->getCollection()
    ->addAttributeToFilter('include_feed_promo', 0)
    ->addAttributeToSelect('*')
    ->addUrlRewrite()
    ->addAttributeToFilter('visibility', array('neq' => 1))
    ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));

$i=1;

foreach ($collectionProducts as $_product)
{
    $productId = $_product->getId();
    $product = Mage::getModel('catalog/product')->load($productId);
    $product->setIncludeFeedPromo(1);
    $product->save();
    echo $i.' '.$productId."\r\n";
    $i++;

}


die();

$include_category=[];

$categories_include = Mage::getModel('catalog/category')
    ->getCollection()
    ->addAttributeToFilter('is_active', 1)
    ->addAttributeToFilter('include_google_feed', 1);

foreach ($categories_include as $cat)
{
    $include_category[]=$cat->getId();
}


echo '<pre>$include_category';
print_r($include_category);
echo '</pre>';


$collection = Mage::getModel('catalog/product')->getCollection()
    ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
    //->addAttributeToFilter('category_id', array('in' => $include_category))
    ->addAttributeToFilter('include_feed_google', 1)
    ->addAttributeToSelect('*')
    ->addUrlRewrite()
    ->addAttributeToFilter('visibility', array('neq' => 1))
    ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));



$collection->getSelect()->columns('entity_id')
    ->group('entity_id');


Mage::getSingleton('cataloginventory/stock')
    ->addInStockFilterToCollection($collection);


$resource = Mage::getSingleton('core/resource');

/**
 * Установка соединения для чтения
 */
$readConnection = $resource->getConnection('core_read');

$count_products = $no_sklad_count_products = 0;
$cats_show = [];
foreach ($collection as $product) {


    $upcmpn = 0;

    $productId = $product->getId();
    $product = Mage::getModel('catalog/product')->load($productId);
    $stockItem = $product->getStockItem();

    $cats = $product->getCategoryIds();

    $google_product_category = false;

    $show = true;
    foreach ($cats as $category_id) {

        switch ($category_id) {
            case '45': //Затирочные машины
            case '75':
            case '14':
            case '15':
            case '16':
            case '76':
                $google_product_category = '4132';//219';

                break;
            case '23': //Выбираторы для бетона
            case '56':
            case '57':
            case '58':
            case '59':
            case '60':
            case '61':
            case '396':
            case '17':
            case '18':
            case '19':
            case '20':
            case '107':
            case '24':
            case '25':
            case '26':
            case '27':
            case '88':
            case '31':
            case '32':
            case '33':
            case '34':
                $google_product_category = '2456';
                //$item->addChild('xmlns:g:google_product_category', '2456');
                break;
            default:
                $google_product_category = '1167';
                //$item->addChild('xmlns:g:google_product_category', '1167');//xsanatise(
                break;

        }

//        Вибраторы для бетона, виброплиты, вибротрамбовки - 2456
//Затирочные машины - 4132


    }


    if ($show && ($stockItem->getIsInStock() == 1)) {
        $stock = 'in stock';


        $product_type = getPath($product);
        $_ex_product_type = '';
        if (count($product_type) > 0) {
            $_ex_product_type = 'Главная > ' . implode(' > ', $product_type);


            $item = $channel->addChild('item'); //add item node

            if ($google_product_category)
                $item->addChild('xmlns:g:google_product_category', $google_product_category);

            $item_group_id = $item->addChild('xmlns:g:id', xsanatise($product->getSku()));


            $item->addChild('xmlns:g:title', xsanatise($product->getName())); //add title node under item


            $urllink = '';

            $urllink = '';
            $id = $product->getId();
            $query = "SELECT * FROM core_url_rewrite WHERE product_id='" . $id . "' AND category_id IS NULL";
            $results = $readConnection->fetchAll($query);
            $urllink = $product->getProductUrl();
            foreach ($results as $res) {
                $urllink = 'https://ukrsnab.com.ua/' . $res['request_path'];
            }


            $link = $item->addChild('xmlns:g:link', $urllink); //add link node under item


            if (xcheck($product->getProizvoditel()) !== false) {
                $brand = $product->getAttributeText('proizvoditel');
                $item->addChild('xmlns:g:brand', xsanatise($brand));//$product->getProizvoditel()
            } else {
                //    $item->addChild('xmlns:g:brand')->addChild('xmlns:g:identifier_exists','false');
            }

            if (xcheck($product->getSku()) !== false) {
                $item->addChild('xmlns:g:mpn', xsanatise($product->getSku()));
            } else {
                //$item->addChild('xmlns:g:mpn')->addChild('xmlns:g:identifier_exists','false');
                /*

                            if(xcheck($product->getUpc())!==false && strlen($product->getUpc())>=12 && strlen($product->getUpc())<=13){


                                $item->addChild('g:gtin', xsanatise($product->getUpc()));

                            }else{
                                //$item->addChild('xmlns:g:gtin')->addChild('xmlns:g:identifier_exists','false');
                            }
                */
            }

            //echo strip_tags($product->getDescription());die();

            $new = $item->addChild('xmlns:g:condition', 'new');
            //$new =
            // $item->addChildWithCDATA('xmlns:g:description', strip_tags($product->getDescription()));
            //$description=$item->addChild('xmlns:g:description');//, strip_tags($product->getDescription()));
            //$description->addCData(strip_tags($product->getDescription()));

            $item->addChildWithCDATA('xmlns:g:description', strip_tags($product->getDescription()));


            $item->addChildWithCDATA('xmlns:g:product_type', strip_tags($_ex_product_type));

            if ($product->getImage() == 'no_selection') {

            } else {

                //Mage::getBaseUrl('media') . 'catalog/product' . $product->getImage());
            }
            $image_link = $item->addChild('xmlns:g:image_link', $product->getImageUrl());




            if (($product->getSku() == '') && ($product->getProizvoditel())) {
                $item->addChild('xmlns:g:identifier_exists', ' no');
            }

            if (empty($product->getSpecialPrice())) {
                $price = Mage::helper('core')->currency($product->getFinalPrice(), true, false);
                $price = str_replace('грн.', 'UAH', $price);
                $item->addChild('xmlns:g:price', $price);// . $shippingcountrylong);
            } else {
                $price = Mage::helper('core')->currency($product->getPrice(), true, false);
                $price = str_replace('грн.', 'UAH', $price);
                $item->addChild('xmlns:g:price', $price);// . $shippingcountrylong);
                $special_price = Mage::helper('core')->currency($product->getSpecialPrice(), true, false);
                $special_price = str_replace('грн.', 'UAH', $special_price);
                $item->addChild('xmlns:g:sale_price', $special_price);//. $shippingcountrylong );
            }

            $item->addChild('xmlns:g:availability', $stock);

            $count_products++;
        }
    } else {
        $stock = 'out of stock';
        $no_sklad_count_products++;
    }


// }

    @ob_flush();
    flush();
}

echo "\r\n count_products " . $count_products . "\r\n";
echo "\r\n no_sklad_count_products " . $no_sklad_count_products . "\r\n";

echo "\r\n cats_show " . implode("\r\n", $cats_show) . "\r\n";


function getPath($product)
{
    $path = array();
    $excludes = [26, 671];


    if ($product) {
        //$categoriesid = $this->getProduct()->getCategoryIds();
        $categoriesid = [];
        //$level=
        foreach ($product->getCategoryCollection() as $cat) {
            $categoriesid[] = $cat->getId();
        }
        if (count($categoriesid) > 0) {
            $category_id = $categoriesid[count($categoriesid) - 1];
            $category = Mage::getModel('catalog/category')->load($category_id);
        }


    }
    /*} else {
        $category = $this->getCategory();
    }*/


    if ($category) {
        if (in_array($category->getId(), $excludes)) return [];

        $pathInStore = $category->getPathInStore();
        $pathIds = array_reverse(explode(',', $pathInStore));

        $categories = $category->getParentCategories();

        // add category path breadcrumb
        $c = count($pathIds);// - 1;
        $i = 1;
        if ($product) {
            $current_product = $product->getId();
        } else $current_product = false;

        foreach ($pathIds as $categoryId) {
            if (isset($categories[$categoryId])) {


                if (in_array($categoryId, $excludes)) return [];

                if ($categories[$categoryId]->getName() != '') {
                    $path['category' . $categoryId] = $categories[$categoryId]->getName();
                }
                /*array(
                    'label' => $categories[$categoryId]->getName(),
                    'category_id' => $categoryId,
                    'type' => 'category',
                    //'lastcategory' => $lastcategory,
                    //'show_other_category' => $show_other_category,
                    'current_product_id' => $current_product,
                    'link' => $this->_isCategoryLink($categoryId) ? $categories[$categoryId]->getUrl() : ''
                );*/
                $i++;


                // }
            }
        }

        if ($category->getName() != '') {
            $path['category' . $category->getId()] = $category->getName();
            /*array(
            'label' => $category->getName(),
            'category_id' => $category->getId(),
            'type' => 'category',
            'current_product_id' => $current_product,
            'link' => $this->_isCategoryLink($category->getId()) ? $category->getUrl() : ''
        );*/
        }
    }


    return $path;

}

//$conn->close();
$content_feed = $rss->asXML();
$content_feed = html_entity_decode($content_feed, ENT_NOQUOTES, 'UTF-8');

$file = '/var/www/ukrsnab.com.ua/htdocs/google-feed.xml';


$handle = fopen($file, "w+");

fwrite($handle, header('Content-Type: text/xml; charset=utf8'));
fwrite($handle, $content_feed);
fclose($handle);





$fileCsv = '/var/www/ukrsnab.com.ua/htdocs/media/googleProducts.txt';

$handle = fopen($fileCsv, "w+");
$contentCsv='';

foreach ($collection as $product) {


    $urllink = '';
    $id = $product->getId();
    $query = "SELECT * FROM core_url_rewrite WHERE product_id='" . $id . "' AND category_id IS NULL";
    $results = $readConnection->fetchAll($query);
    $urllink = $product->getProductUrl();
    foreach ($results as $res) {
        $urllink = 'https://ukrsnab.com.ua/' . $res['request_path'];
    }

    if ($urllink!='')
        $contentCsv .= $urllink."\n";

    @ob_flush();
    flush();
}



if ($contentCsv!='') {
    //fwrite($handle, header('Content-Type: text/xml; charset=utf8'));
    fwrite($handle, $contentCsv);
    fclose($handle);
}
