<?php

/**
 * Catalin Ciobanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License (MIT)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/MIT
 *
 * @package     Catalin_Seo
 * @copyright   Copyright (c) 2016 Catalin Ciobanu
 * @license     https://opensource.org/licenses/MIT  MIT License (MIT)
 */
class Catalin_SEO_Model_Catalog_Layer extends Mage_Catalog_Model_Layer
{

    /**
     * Get collection of all filterable attributes for layer products set
     *
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Attribute_Collection
     */
    public function getFilterableAttributes()
    {
        $collection = parent::getFilterableAttributes();

        if ($collection instanceof Mage_Catalog_Model_Resource_Product_Attribute_Collection) {
            // Pre-loads all needed attributes at once
            $attrUrlKeyModel = Mage::getResourceModel('catalin_seo/attribute_urlkey');
            $attrUrlKeyModel->preloadAttributesOptions($collection);
        }

        return $collection;
    }



    /**
     * Initialize product collection
     *
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
     * @return Mage_Catalog_Model_Layer
     */
    public function prepareProductCollection($collection)
    {
        $collection
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite($this->getCurrentCategory()->getId());

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        $this->collectionProductByCategory($collection);
        return $this;
    }
    public function collectionProductByCategory($collection)
    {

        $categoryId=$this->getCurrentCategory()->getId();

        $select=$collection->getSelect();

        $showproductsparentcat = Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Category::ENTITY, 'show_products_parentcat');
        $showProdParentAttributeId = $showproductsparentcat->getId();

        $resource = Mage::getSingleton('core/resource');

        $readConnection = $resource->getConnection('core_read');
        $_query = "SELECT cce.entity_id FROM catalog_category_entity_int ccei
RIGHT JOIN catalog_category_entity cce ON (ccei.entity_id=cce.entity_id and ccei.attribute_id=$showProdParentAttributeId) 
WHERE cce.parent_id='$categoryId' AND (ccei.value=0)
GROUP BY cce.entity_id";
// OR ccei.value IS NULL)
        $catsForShow=[];

        $results_attr_gr = $readConnection->fetchAll($_query);
        foreach ($results_attr_gr as $entity)
            $catsForShow[]=$entity['entity_id'];



        if (count($catsForShow)>0) {

            $collection->getSelect()->where(
                'cat_index.product_id NOT IN (SELECT `cat_prod_`.`product_id` FROM `catalog_category_product` AS `cat_prod_` WHERE `cat_prod_`.category_id IN (' . implode(',', $catsForShow) . ') )'
            );

        }



        return $collection;

    }


}
