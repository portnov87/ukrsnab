<?php

/**
 * Catalin Ciobanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License (MIT)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/MIT
 *
 * @package     Catalin_Seo
 * @copyright   Copyright (c) 2016 Catalin Ciobanu
 * @license     https://opensource.org/licenses/MIT  MIT License (MIT)
 */
class Catalin_SEO_Model_Catalog_Resource_Layer_Filter_Attribute extends Mage_Catalog_Model_Resource_Layer_Filter_Attribute
{

    /**
     * Apply attribute filter to product collection
     *
     * @param Mage_Catalog_Model_Layer_Filter_Attribute $filter
     * @param int $value
     * @return Mage_Catalog_Model_Resource_Layer_Filter_Attribute
     */
    public function applyFilterToCollection($filter, $value)
    {
        /*echo '<pre>$filter';
        print_r($filter);
        echo '</pre>';
*/
       /* echo '<pre>value';
        print_r($value);
        echo '</pre>';*/
        if (!Mage::helper('catalin_seo')->isEnabled()) {
            return parent::applyFilterToCollection($filter, $value);
        }

        $collection = $filter->getLayer()->getProductCollection();
        $attribute = $filter->getAttributeModel();
        $connection = $this->_getReadAdapter();
        $tableAlias = $attribute->getAttributeCode() . '_idx' . uniqid();
        $conditions = array(
            "{$tableAlias}.entity_id = e.entity_id",
            $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
            $connection->quoteInto("{$tableAlias}.store_id = ?", $collection->getStoreId()),
        );

        $attrUrlKeyModel = Mage::getResourceModel('catalin_seo/attribute_urlkey');
        if (!is_array($value)) {
            $options = $attribute->getSource()->getAllOptions(false);
            foreach ($options as $option) {
                if ($option['label'] == $value) {
                    $value = $option['value'];
                }
            }
            $conditions[] = $connection->quoteInto("{$tableAlias}.value = ?", $value);
        } else {
            $conditions[] = "{$tableAlias}.value in ( ";
            foreach ($value as $v) {
                $v = $attrUrlKeyModel->getOptionId($attribute->getId(), $v);
                $conditions[count($conditions) - 1] .= $connection->quoteInto("?", $v) . ' ,';
            }
            $conditions[count($conditions) - 1] = rtrim($conditions[count($conditions) - 1], ',');
            $conditions[count($conditions) - 1] .= ')';
        }

     /*   echo '<pre>conditions '.$value;
        print_r($conditions);
        echo '</pre>';
*/
        $collection->getSelect()->join(
            array($tableAlias => $this->getMainTable()), implode(' AND ', $conditions), array()
        );
        //echo $collection->getSelect();
        //die();
        $collection->getSelect()->distinct();

        return $this;
    }

    /**
     * Retrieve array with products counts per attribute option
     *
     * @param Mage_Catalog_Model_Layer_Filter_Attribute $filter
     * @return array
     */
    public function getCount($filter)
    {
/*
        // clone select from collection with filters
        $select = clone $filter->getLayer()->getProductCollection()->getSelect();
        // reset columns, order and limitation conditions
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);

        $connection = $this->_getReadAdapter();
        $attribute  = $filter->getAttributeModel();
        $tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());
        $conditions = array(
            "{$tableAlias}.entity_id = e.entity_id",
            $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
            $connection->quoteInto("{$tableAlias}.store_id = ?", $filter->getStoreId()),
        );

        $select
            ->join(
                array($tableAlias => $this->getMainTable()),
                join(' AND ', $conditions),
                array('value', 'count' => new Zend_Db_Expr("COUNT({$tableAlias}.entity_id)")))
            ->group("{$tableAlias}.value");
        $categoryId=$filter->getLayer()->getCategoryId();

        $select=$this->collectionProductByCategory($select, $categoryId);
        echo '<pre>'.$this->getMainTable().' '.$select;
        print_r($connection->fetchPairs($select));
        echo '</pre>';die();
        return $connection->fetchPairs($select);

*/


        if (!Mage::helper('catalin_seo')->isEnabled()) {
            return parent::getCount($filter);
        }

        // clone select from collection with filters
        $select = clone $filter->getLayer()->getProductCollection()->getSelect();
        // reset columns, order and limitation conditions
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);


        $connection = $this->_getReadAdapter();
        $attribute = $filter->getAttributeModel();
        $tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());
        $conditions = array(
            "{$tableAlias}.entity_id = e.entity_id",
            $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
            $connection->quoteInto("{$tableAlias}.store_id = ?", $filter->getStoreId())
        );

        // start removing all filters for current attribute - we need correct count
        $parts = $select->getPart(Zend_Db_Select::FROM);
        $from = array();
        foreach ($parts as $key => $part) {
            if (stripos($key, $tableAlias) === false) {
                $from[$key] = $part;
            }
        }
        //$select->setPart(Zend_Db_Select::FROM, $from);
        // end of removing

        $select
            ->join(
                array($tableAlias => $this->getMainTable()), join(' AND ', $conditions), array('value', 'count' => new Zend_Db_Expr("COUNT({$tableAlias}.entity_id)")))
            ->group("{$tableAlias}.value");


        $categoryId=$filter->getLayer()->getCategoryId();

       // $select=$this->collectionProductByCategory($select, $categoryId);


        return $connection->fetchPairs($select);
    }

    public function collectionProductByCategory($select,$categoryId)
    {




        $showproductsparentcat = Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Category::ENTITY, 'show_products_parentcat');
        $showProdParentAttributeId = $showproductsparentcat->getId();

        $resource = Mage::getSingleton('core/resource');

        $readConnection = $resource->getConnection('core_read');
        $_query = "SELECT cce.entity_id FROM catalog_category_entity_int ccei
RIGHT JOIN catalog_category_entity cce ON (ccei.entity_id=cce.entity_id and ccei.attribute_id=$showProdParentAttributeId) 
WHERE cce.parent_id='$categoryId' AND (ccei.value=0)
GROUP BY cce.entity_id";
// OR ccei.value IS NULL)
        $catsForShow=[];

        $results_attr_gr = $readConnection->fetchAll($_query);
        foreach ($results_attr_gr as $entity)
            $catsForShow[]=$entity['entity_id'];



        if (count($catsForShow)>0) {

            $select->where(
                'cat_index.product_id NOT IN (SELECT `cat_prod_`.`product_id` FROM `catalog_category_product` AS `cat_prod_` WHERE `cat_prod_`.category_id IN (' . implode(',', $catsForShow) . ') )'
            );

        }

        return $select;

    }


}
