<?php

class AW_Blog_Helper_Cat extends Mage_Core_Helper_Abstract
{
    /**
     * Renders CMS page
     * Call from controller action
     *
     * @param Mage_Core_Controller_Front_Action $action
     * @param integer                           $identifier
     *
     * @return bool
     */
    public function renderPage(Mage_Core_Controller_Front_Action $action, $identifier = null)
    {
        if (!$catId = Mage::getSingleton('blog/cat')->load($identifier)->getCatId()) {
            return false;
        }

        $pageTitle = Mage::getSingleton('blog/cat')->load($identifier)->getTitle();
        $blogTitle = Mage::getStoreConfig('blog/blog/title') . " - " . $pageTitle;

        $action->loadLayout();
        if ($storage = Mage::getSingleton('customer/session')) {
            $action->getLayout()->getMessagesBlock()->addMessages($storage->getMessages(true));
        }
        $title='Читайте статьи про '.$pageTitle.' в блоге интернет-гипермаркета строительной и промышленной техники Ukrsnab';

        $action->getLayout()->getBlock('head')->setTitle($title);
        $description='Самые актуальные и полезные статьи о '.$pageTitle.'. Мы пишем только о том что поможет вам при выборе строительной техники и о том как ухаживать за ней.';
        $action->getLayout()->getBlock('head')->setDescription($description);

        $action->getLayout()->getBlock('root')->setTemplate(Mage::getStoreConfig('blog/blog/layout'));
        $action->renderLayout();

        return true;
    }
}