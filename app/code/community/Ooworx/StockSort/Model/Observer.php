<?php

/**
 * Copyright (C) 2015 - Ooworx
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Ooworx_StockSort_Model_Observer
{

    public function catalogProductCollectionLoadBefore($observer)
    {
        // Check is activated
        if (Mage::getStoreConfig('cataloginventory/options/stocksort') == false) {
            //return $this;
        }

//        echo '<pre>getHandles';
//        print_R(Mage::getSingleton('core/layout')->getUpdate()->getHandles());
//        echo '</pre>';

        if ((strpos($_SERVER['REQUEST_URI'],'prokat')!==false)||(strpos($_SERVER['REQUEST_URI'],'remont')!==false))
        {

        }
        elseif (in_array("catalog_category_view", Mage::getSingleton('core/layout')->getUpdate()->getHandles())
            || in_array("catalog_category_layered_ajax_layer", Mage::getSingleton('core/layout')->getUpdate()->getHandles())
            || in_array("attributesplash_page_view", Mage::getSingleton('core/layout')->getUpdate()->getHandles())
            || in_array("alphabet_index_index", Mage::getSingleton('core/layout')->getUpdate()->getHandles())
            || in_array("catalogsearch_result_index", Mage::getSingleton('core/layout')->getUpdate()->getHandles())) {
        //elseif (!in_array("catalog_product_view", Mage::getSingleton('core/layout')->getUpdate()->getHandles())) {
            $collection = $observer->getCollection();
            // Join inventory table to get product state of stock
            $collection->getSelect()
                ->joinLeft(
                    array('_inventory_table' => $collection->getTable('cataloginventory/stock_item')),
                    "_inventory_table.product_id = e.entity_id",
                    array('is_in_stock', 'manage_stock') // Select attribute for product
                );
            $fromPart = $collection->getSelect()->getPart(Zend_Db_Select::FROM);
           if (!isset($fromPart['cat_index'])) {

               $collection->getSelect()
                   ->order('at_category_id.position DESC')
                   ->order('price_index.price ASC')
                   ->order('_inventory_table.is_in_stock DESC');

            }
            else {

                $collection->getSelect()
                    ->order('cat_index.position DESC')
                    ->order('price_index.price ASC')
                    ->order('_inventory_table.is_in_stock DESC');

            }

            $order = $collection->getSelect()->getPart('order');


            array_unshift($order, array_pop($order));
            $new_order=[];
            if (!in_array("catalogsearch_result_index", Mage::getSingleton('core/layout')->getUpdate()->getHandles())) {

                foreach ($order as $_or) {

                    if ($_or[0] == '_inventory_table.is_in_stock') {
                        $new_order[0] = $_or;
                    } elseif (($_or[0] == 'cat_index.position')||($_or[0] == 'at_category_id.position')) {
                        $new_order[1] = $_or;
                    } elseif ($_or[0] == 'price_index.price') {
                        $new_order[2] = $_or;
                    } else {
                        $new_order[3] = $_or;
                    }
                }

                $order = $new_order;
            }
            ksort($order);

//            echo '<pre>$new_order';
//            print_r($new_order);
//            echo '</pre>';
            $collection->getSelect()->setPart('order', $order);

//
            $limit=Mage::getStoreConfig('catalog/frontend/list_per_page');
            $p=(isset($_GET['p'])?$_GET['p']:1);
            $offset=($p-1)*$limit;

            $collection->getSelect()->limit($limit,$offset);

//            echo $collection->getSelect();
//            die();


            //echo '<div style="display:none"><pre>'.$collection->getSelect().'</pre></div>';

        }
        return $this;
        // End
    }

}
