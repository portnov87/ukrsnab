<?php
/**
 * @category    Fishpig
 * @package     Fishpig_AttributeSplash
 * @license     http://fishpig.co.uk/license.txt
 * @author      Ben Tideswell <help@fishpig.co.uk>
 */
	
	$this->startSetup();

	try {
		$conn = $this->getConnection();
        $this->getConnection()->addColumn($this->getTable('attributesplash_page'), 'letter', " VARCHAR(255) NOT NULL default ''");

		Mage::getResourceModel('attributeSplash/group')->reindexAll();
	}
	catch (Exception $e) {
		Mage::logException($e);
	}
	
	$this->endSetup();
	