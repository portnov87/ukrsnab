<?php
/**
 * @category    Fishpig
 * @package     Fishpig_AttributeSplash
 * @license     http://fishpig.co.uk/license.txt
 * @author      Ben Tideswell <help@fishpig.co.uk>
 */

class Fishpig_AttributeSplash_Model_Resource_Letter extends Fishpig_AttributeSplash_Model_Resource_Abstract
{
	/**
	 * Fields to be serialized before saving
	 * This applies to the filter fields
	 *
	 * @var array
	 */
     protected $_serializableFields = array(
     	'other' => array('a:0:{}', array()),
     );
    /**
     * Get the index table for pags
     *
     * @return string
     */
    public function getIndexTable()
    {
        return $this->getTable('attributeSplash/letter_index');
    }
    /**
     * Retrieve the store table name
     *
     * @return string
     */
    public function getStoreTable()
    {
        //echo $this->getTable('attributeSplash/letter_store');die();
        return $this->getTable('attributeSplash/letter_store');
    }
    public function _construct()
	{
		$this->_init('attributeSplash/letter', 'id');
	}

	/**
	 * Retrieve select object for load object data
	 * This gets the default select, plus the attribute id and code
	 *
	 * @param   string $field
	 * @param   mixed $value
	 * @return  Zend_Db_Select
	*/
	protected function _getLoadSelect($field, $value, $object)
	{
		$select = parent::_getLoadSelect($field, $value, $object)
			->join(array('_option_table' => $this->getTable('eav/attribute_option')), '`_option_table`.`option_id` = `main_table`.`option_id`', '')
			->join(array('_attribute_table' => $this->getTable('eav/attribute')), '`_attribute_table`.`attribute_id`=`_option_table`.`attribute_id`', array('attribute_id', 'attribute_code', 'frontend_label'));
		
		return $select;
	}


	/**
	 * Retrieve the name of the unique field
	 *
	 * @return string
	 */
	public function getUniqueFieldName()
	{
		return 'id';
	}
 
	/**
	 * Retrieve a collection of products associated with the splash page
	 *
	 * @return Mage_Catalog_Model_Resource_Eav_Resource_Product_Collection
	 */	
	public function getProductCollection(Fishpig_AttributeSplash_Model_Page $page)
	{	
		$collection = Mage::getResourceModel('catalog/product_collection')
			->setStoreId(1);

		$alias = $page->getAttributeCode() . '_index';
        $alias1 = $page->getAttributeCode() .'_optionvalue' ;

        $letter = $page->getLetter();

        if ($letter!='') {
            $letter = strtoupper($letter);
            //$collection->addAttributeToFilter('trademark', array('like' => 'Н%'));
            $collection->getSelect()
                ->join(
                    array($alias => $this->getTable('catalog/product_index_eav')),
                    "`{$alias}`.`entity_id` = `e`.`entity_id`"
                    . $this->_getReadAdapter()->quoteInto(" AND `{$alias}`.`attribute_id` = ? ", $page->getAttributeId())
                    . $this->_getReadAdapter()->quoteInto(" AND `{$alias}`.`store_id` = ? ", $page->getStoreId())
                    ,
                    ''
                )
                /*->join(
                    array($alias1 => 'eav_attribute_option_value'),
                    "`{$alias1}`.`option_id` = `{$alias}`.`value`"
                    . " AND `{$alias1}`.`value` LIKE '".$letter."%' ",
                    ''
                )*/;
            //echo $collection->getSelect();
        }

		//echo $collection->getSelect();

		return $collection;
	}
	
	/**
	 * Set required fields before saving model
	 *
	 * @param Mage_Core_Model_Abstract $object
	 * @return $this
	 */
	protected function _beforeSave(Mage_Core_Model_Abstract $object)
	{
		if (!$object->getData('store_ids')) {
			$object->setData('store_ids', array(Mage::app()->getStore(true)->getId()));
		}
/*
		if ($object->getId()) {
			$object->getAttributeModel();

			$object->unsetData('attribute_id');
		}

		if (!$this->_pageIsUniqueToStores($object)) {
			throw new Exception('A page already exists for this attribute and store combination.');
		}*/

		return parent::_beforeSave($object);
	}
	
	/**
	 * Determine whether [ages scope if unique to store
	 *
	 * @param Mage_Core_Model_Abstract $object
	 * @return bool
	 */
/*	protected function _pageIsUniqueToStores(Mage_Core_Model_Abstract $object)
	{
		if (Mage::app()->isSingleStoreMode() || !$object->hasStoreIds()) {
			$stores = array(Mage_Core_Model_App::ADMIN_STORE_ID);
		}
		else {
			$stores = (array)$object->getData('store_ids');
		}

		$select = $this->_getReadAdapter()
			->select()
			->from(array('main_table' => $this->getMainTable()), 'page_id')
			->join(array('_store' => $this->getStoreTable()), 'main_table.page_id = _store.page_id', '')
			->where('option_id=?', $object->getOptionId())
			->limit(1);
			
		if (count($stores) === 1) {
			$select->where('_store.store_id = ?', array_shift($stores));
		}
		else {
			$select->where('_store.store_id IN (?)', $stores);
		}

		if ($object->getId()) {
			$select->where('main_table.page_id <> ?', $object->getId());
		}

		return $this->_getWriteAdapter()->fetchOne($select) === false;
	}
	*/
	/**
	 * Auto-update splash group
	 *
	 * @param Mage_Core_Model_Abstract $object
	 * @return $this
	 */
	protected function _afterSave(Mage_Core_Model_Abstract $object)
	{
		parent::_afterSave($object);
		
		/*if (!$object->getSkipAutoCreateGroup()) {
			$this->updateSplashGroup($object);
		}*/
		
		return $this;
	}
	
	/**
	 * Check whether the attribute group exists
	 * If not, create the group
	 *
	 * @param Fishpig_AttributeSPlash_Model_Page $page
	 */
	public function updateSplashGroup(Fishpig_AttributeSplash_Model_Page $page)
	{
		/*if (!$page->getSplashGroup()) {
			$group = Mage::getModel('attributeSplash/group')
				->setAttributeId($page->getAttributeModel()->getId())
				->setDisplayName($page->getAttributeModel()->getFrontendLabel())
				->setStoreId(0)
				->setIsEnabled(1);

			try {
				$group->save();
			}
			catch (Exception $e) {
				Mage::helper('attributeSplash')->log($e->getMessage());
			}
		}*/

		return $this;
	}



}
