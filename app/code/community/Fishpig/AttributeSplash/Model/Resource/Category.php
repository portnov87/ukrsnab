<?php
/**
 * @category    Fishpig
 * @package     Fishpig_AttributeSplash
 * @license     http://fishpig.co.uk/license.txt
 * @author      Ben Tideswell <help@fishpig.co.uk>
 */

class Fishpig_AttributeSplash_Model_Resource_Category extends Fishpig_AttributeSplash_Model_Resource_Abstract
{

     
	public function _construct()
	{
		$this->_init('attributeSplash/page_category', 'page_id');
	}


}
