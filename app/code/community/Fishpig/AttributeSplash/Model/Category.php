<?php
/**
 * @category    Fishpig
 * @package     Fishpig_AttributeSplash
 * @license     http://fishpig.co.uk/license.txt
 * @author      Ben Tideswell <help@fishpig.co.uk>
 */

class Fishpig_AttributeSplash_Model_Category extends Fishpig_AttributeSplash_Model_Abstract
{
	/**
	 * Setup the model's resource
	 *
	 * @return void
	 */
	public function _construct()
	{
		$this->_init('attributeSplash/page_category');
	}

}
