<?php
/**
 * @category    Fishpig
 * @package     Fishpig_AttributeSplash
 * @license     http://fishpig.co.uk/license.txt
 * @author      Ben Tideswell <help@fishpig.co.uk>
 */

class Fishpig_AttributeSplash_PageController extends Mage_Core_Controller_Front_Action
{
	/**
	 * Display the splash page
	 *
	 * @return void
	 */
	public function viewAction()
	{
		if (($splashPage = $this->_initSplashPage()) === false) {
			return $this->_forward('noRoute');
		}
        //echo '$ajax';
		// Register the splash layer model
		Mage::register('current_layer', Mage::getSingleton('attributeSplash/layer'));
		
		$this->_applyCustomViewLayout($splashPage);

		if ($rootBlock = $this->getLayout()->getBlock('root')) {
			$rootBlock->addBodyClass('splash-page-' . $splashPage->getId());
			$rootBlock->addBodyClass('splash-page-' . $splashPage->getAttributeCode());
		}



						
		if ($headBlock = $this->getLayout()->getBlock('head')) {

            $currentCategory = Mage::registry('current_category');


            $page = Mage::app()->getFrontController()->getRequest()->getParam('p');
			if ($title = $splashPage->getPageTitle()) {

                if ($page) {
                    if ($page > 1) {
                        $title = 'Страница ' . $page.' | '.$title;// . ' | '.$title;

                    }
                }

				$headBlock->setTitle($title);
			}
			else {
				$this->_title($splashPage->getName());
			}

			if ($description = $splashPage->getMetaDescription()) {
			    if ($page) {
                    if ($page > 1) {
                        $description = $description . ' - Страница ' . $page;// . ' | '.$title;

                    }
                }
				$headBlock->setDescription($description);
			}
			
			if ($keywords = $splashPage->getMetaKeywords()) {
				$headBlock->setKeywords($keywords);
			}

            if (isset($_REQUEST['dir'])||(isset($_REQUEST['order'])))
            {
                $headBlock->setRobots('noindex,nofollow');
            }
            $linkPrev = false;
            $linkNext = false;



            /*if ($h1 = $splashPage->getH1()) {
                //echo '$h1'.$h1;
             //   Mage::register('h1', $h1);
            }
            Mage::unregister('h1');
            Mage::register('h1', '$h1');*/

            /*
            if ($currentCategory) {
                $text = '';
                $title = $currentCategory->getName();


                $manufacture = $splashPage->getDisplayName();
                $text = $manufacture;
                $description = '➤➤➤Лекарства ❱❱'.$title.'❰❰ ✔Препараты для лечения и профилактики в интернет-аптеке ➥ Пани Аптека ☎ +38 (044) 353 81 81 ✔Доставка по Украине ';

                //htmlspecialchars('В нашей интернет-аптеке, Вы можете купить ' . mb_strtolower($title, 'utf8') . ' ' . $text . ' по приемлимой стоимости. Звоните прямо сейчас! +38 (044) 353 81 81');

                $title = 'Препараты и лекартсва '.$text.' | Цены на Paniapteka.ua';
                //$title . ' ' . $text . ', купить в Украине: Киев, Одесса, Харьков - paniapteka.ua';
                $h1 = $title . ' ' . $text;
                if ($_SERVER['REQUEST_URI'] == '/bioactive-supplements.html?manufacturer=677') {
                    $description = 'Каталог продукции Экоменд от Пани Аптека. Купить препараты экоменд по низким ценам в Киеве. Только сертифицированная продукция';

                    $title = 'Экомед - купить препараты компании Экомед по лучшим ценам  | Вся продукция Экомед от Пани аптека';
                    $h1 = 'Препараты Экомед';
                }
                //echo 'title'.$title;
                $page = Mage::app()->getFrontController()->getRequest()->getParam('p');

                if ($page > 1) {
                    $title='Страница ' . $page . ' | '.$title;

                }

                $headBlock->setTitle($title);
                $headBlock->setDescription($description);


                // Mage::register('h1', $h1);


                //$headBlock->addItem('link_rel', $splashPage->getUrl(), 'rel="canonical"');


                $category = Mage::registry('current_category');
                $prodCol = $splashPage->getProductCollection()->addAttributeToFilter('status', 1)
                    ->addAttributeToFilter('visibility', array('in' => array(Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG, Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)));
                $tool = $this->getLayout()->createBlock('page/html_pager')
                    ->setLimit($this->getLayout()->createBlock('catalog/product_list_toolbar')->getLimit())->setCollection($prodCol);

                if ($tool->getCollection()->getSelectCountSql()) {
                    if ($tool->getLastPageNum() > 1) {
                        if (!$tool->isFirstPage()) {
                            $linkPrev = true;
                            if ($tool->getCurrentPage() == 2) {
                                $url = explode('?', $tool->getPreviousPageUrl());
                                $prevUrl = @$url[0];
                            } else {
                                $prevUrl = $tool->getPreviousPageUrl();
                            }
                        }
                        if (!$tool->isLastPage()) {
                            $linkNext = true;
                            $nextUrl = $tool->getNextPageUrl();
                        }
                    }
                }
            }

            */
            //echo $splashPage->getUrl();die();
           // $headBlock->addLinkRel('canonical', $splashPage->getUrl());
            //$headBlock->addItem('link_rel', $splashPage->getUrl(), 'rel="canonical"');
            if ($linkPrev)
                $headBlock->addItem('link_rel', $prevUrl, 'rel="prev"');

            //echo '<link rel="prev" href="' . $prevUrl . '" />';
            if ($linkNext)
                $headBlock->addItem('link_rel', $nextUrl, 'rel="next"');
                //echo '<link rel="next" href="' . $nextUrl . '" />';



		}








		
		if ($breadBlock = $this->getLayout()->getBlock('breadcrumbs')) {
			if (!$breadBlock->getSkipSplashPageHomeCrumb()) {
				$breadBlock->addCrumb('home', array('label' => $this->__('Главная'), 'title' => $this->__('Главная'), 'link' => Mage::getUrl()));
			}
			
			/*if (!$breadBlock->getSkipSplashPageGroupCrumb()) {
				if ($splashGroup = $splashPage->getSplashGroup()) {
					$breadBlock->addCrumb('splash_group', array('label' => $splashGroup->getName(), 'title' => $splashGroup->getName(), 'link' => $splashGroup->getUrl()));
				}
			}*/

			$current_category_id=$splashPage->getCategoryId();
			if ($current_category_id!='') {
                $current_category = Mage::getModel('catalog/category')->load($current_category_id);
                if ($current_category){
                    $breadBlock->addCrumb('splash_group', array('label' => $current_category->getName(), 'title' => $current_category->getName(), 'link' => $current_category->getUrl()));
                }
			}else{
                $breadBlock->addCrumb('brands', array('label' => 'Бренды', 'title' => 'Бренды', 'link' => '/brands.html'));
            }

			if (!$breadBlock->getSkipSplashPageCrumb()) {
				$breadBlock->addCrumb('splash_page', array('label' => $splashPage->getName(), 'title' => $splashPage->getName()));
			}
		}


       /* $update = $this->getLayout()->getUpdate();
        $update->addHandle('default');

        if (!$category->hasChildren()) {
            $update->addHandle('catalog_category_layered_nochildren');
        }

        $this->addActionLayoutHandles();
       // $update->addHandle($category->getLayoutUpdateHandle());
        //$update->addHandle('CATEGORY_' . $category->getId());
// apply custom ajax layout
        if (Mage::helper('catalin_seo')->isAjaxEnabled() && Mage::helper('catalin_seo')->isAjaxRequest()) {
            $update->addHandle('catalog_category_layered_ajax_layer');
        }*/
		//$this->renderLayout();
        // return json formatted response for ajax
        $ajax=$this->getRequest()->getParam('isLayerAjax', false);


        if (Mage::helper('catalin_seo')->isAjaxEnabled() && Mage::helper('catalin_seo')->isAjaxRequest()) {

            if(Mage::getEdition() == Mage::EDITION_ENTERPRISE){
                $block = $this->getLayout()->getBlock('enterprisecatalog.leftnav');
            } else {
                $block = $this->getLayout()->getBlock('catalog.leftnav');
            }

            $listing = $this->getLayout()->getBlock('product_list')->toHtml();
            $layer = $block->toHtml();

            // Fix urls that contain '___SID=U'
            $urlModel = Mage::getSingleton('core/url');
            $listing = $urlModel->sessionUrlVar($listing);
            $layer = $urlModel->sessionUrlVar($layer);
            $sort= $this->getLayout()->getBlock('product_list_toolbar')->toHtml();
            $response = array(
                'listing' => $listing,
                'sort'=>$sort,
                'layer' => $layer
            );

            //echo 'dfdfdfdfdfd';

            $this->getResponse()->setHeader('Content-Type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($response));
        } else {
            $this->renderLayout();
        }
	}
	
	/**
	 * Apply custom layout handles to the splash page
	 *
	 * @param Fishpig_AttribtueSplash_Model_Page $splashPage
	 * @return Fishpig_AttribtueSplash_PageController
	 */
	protected function _applyCustomViewLayout(Fishpig_AttributeSplash_Model_Page $splashPage)
	{
		$update = $this->getLayout()->getUpdate();
		
		$update->addHandle('default');
		
		$this->addActionLayoutHandles();

		$update->addHandle('attributesplash_page_view_' . $splashPage->getId());
		$update->addHandle('attributesplash_page_view_' . $splashPage->getAttributeModel()->getAttributeCode());

		$this->loadLayoutUpdates();

		$update->addUpdate($splashPage->getLayoutUpdateXml());

		$this->generateLayoutXml()->generateLayoutBlocks();
		
		if ($splashPage->getPageLayout()) {
			$this->getLayout()->helper('page/layout')->applyTemplate($splashPage->getPageLayout());
		}
		else if ($pageLayout = Mage::getStoreConfig('attributeSplash/page/template')) {
			$this->getLayout()->helper('page/layout')->applyTemplate($pageLayout);
		}

		$this->_isLayoutLoaded = true;

		return $this;	
	}
	
	/**
	 * Initialise the Splash Page model
	 *
	 * @return false|Fishpig_AttributeSplash_Model_Page
	 */
	protected function _initSplashPage()
	{
		if (($page = Mage::registry('splash_page')) !== null) {
			return $page;
		}

		$splashPage = Mage::getModel('attributeSplash/page')
			->setStoreId(Mage::app()->getStore()->getId())
			->load((int) $this->getRequest()->getParam('id', false));

		if (!$splashPage->getIsEnabled() || !$splashPage->getSplashGroup()) {
			return false;
		}
		
		Mage::register('splash_page', $splashPage);
		Mage::register('current_category', $splashPage);
		Mage::register('category', $splashPage);
		
		if ($group = $splashPage->getSplashGroup()) {
			Mage::register('splash_group', $group);	
		}

		return $splashPage;
	}
}
