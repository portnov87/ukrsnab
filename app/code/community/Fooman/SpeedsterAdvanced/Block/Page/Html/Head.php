<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @copyright  Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com) (original implementation)
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz) (use of Minify Library)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/*
 * @author     Kristof Ringleff
 * @package    Fooman_SpeedsterAdvanced
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 */

class Fooman_SpeedsterAdvanced_Block_Page_Html_Head extends Mage_Page_Block_Html_Head
{

    protected $_speedsterMergeBlacklist = array();

    public function __construct()
    {
        parent::__construct();
        $blacklist = array_merge(
            explode(',', Mage::getStoreConfig('dev/js/speedster_merge_blacklist')),
            explode(',', Mage::getStoreConfig('dev/css/speedster_merge_blacklist'))
        );

        foreach ($blacklist as $listItem) {
            $listItem = Mage::helper('speedsterAdvanced')->normaliseUrl($listItem);
            if ($listItem) {
                $this->_speedsterMergeBlacklist[$listItem] = true;
            }
        }
    }

    /**
     * Merge static and skin files of the same format into 1 set of HEAD directives or even into 1 directive
     *
     * Will attempt to merge into 1 directive, if merging callback is provided. In this case it will generate
     * filenames, rather than render urls.
     * The merger callback is responsible for checking whether files exist, merging them and giving result URL
     *
     * @param string   $format - HTML element format for sprintf('<element src="%s"%s />', $src, $params)
     * @param array    $staticItems - array of relative names of static items to be grabbed from js/ folder
     * @param array    $skinItems - array of relative names of skin items to be found in skins according to design config
     * @param callback $mergeCallback
     *
     * @return string
     */
    protected function &_prepareStaticAndSkinElements(
        $format, array $staticItems, array $skinItems, $mergeCallback = null
    ) {
        $designPackage = Mage::getDesign();
        $baseJsUrl = Mage::getBaseUrl('js');
        $items = array();
        $blacklisted = array();
        if ($mergeCallback && !is_callable($mergeCallback)) {
            $mergeCallback = null;
        }

        // get static files from the js folder, no need to lookup design package
        foreach ($staticItems as $params => $rows) {
            foreach ($rows as $name) {
                if (!$this->isMergeBlacklisted($name)) {
                    $items['static'][$params][] = $mergeCallback
                        ? Mage::getBaseDir() . DS . 'js' . DS . $name
                        : $baseJsUrl . $name;
                } else {
                    $blacklisted['static'][$params][] = $baseJsUrl . $name;
                }
            }
        }

        // lookup each file basing on current theme configuration
        foreach ($skinItems as $params => $rows) {
            foreach ($rows as $name) {
                $relativeUrl = Mage_Core_Model_Store::URL_TYPE_SKIN . DS . str_replace(
                        Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN), '',
                        $designPackage->getSkinUrl($name, array())
                    );
                if (!$this->isMergeBlacklisted($relativeUrl)) {
                    $items['skin'][$params][] = $mergeCallback
                        ? $designPackage->getFilename($name, array('_type' => 'skin'))
                        : $designPackage->getSkinUrl($name, array());
                } else {
                    $blacklisted['skin'][$params][] = $designPackage->getSkinUrl($name, array());
                }
            }
        }

        $html = '';
        foreach ($items as $type) {
            foreach ($type as $params => $rows) {
                // attempt to merge
                $mergedUrl = false;
                if ($mergeCallback) {
                    $mergedUrl = call_user_func($mergeCallback, $rows);
                }
                // render elements
                $params = trim($params);
                $params = $params ? ' ' . $params : '';
                if ($mergedUrl) {
                    $html .= sprintf($format, $mergedUrl, $params);
                } else {
                    foreach ($rows as $src) {
                        $html .= sprintf($format, $src, $params);
                    }
                }
            }
        }
        //add blacklisted items last
        foreach ($blacklisted as $type) {
            foreach ($type as $params => $rows) {
                // render elements
                $params = trim($params);
                $params = $params ? ' ' . $params : '';
                foreach ($rows as $src) {
                    $html .= sprintf($format, $src, $params);

                }
            }
        }
        return $html;
    }


    /**
     * Get HEAD HTML with CSS/JS/RSS definitions
     * (actually it also renders other elements, TODO: fix it up or rename this method)
     *
     * @return string
     */
    public function getCssJsHtml()
    {
        // separate items by types
        $lines  = array();
        foreach ($this->_data['items'] as $item) {
            if (!is_null($item['cond']) && !$this->getData($item['cond']) || !isset($item['name'])) {
                continue;
            }
            $if     = !empty($item['if']) ? $item['if'] : '';
            $params = !empty($item['params']) ? $item['params'] : '';
            switch ($item['type']) {
                case 'js':        // js/*.js
                case 'skin_js':   // skin/*/*.js
                case 'js_css':    // js/*.css
                case 'skin_css':  // skin/*/*.css
                    $lines[$if][$item['type']][$params][$item['name']] = $item['name'];
                    break;
                default:
                    $this->_separateOtherHtmlHeadElements($lines, $if, $item['type'], $params, $item['name'], $item);
                    break;
            }
        }

        // prepare HTML
        $shouldMergeJs = Mage::getStoreConfigFlag('dev/js/merge_files');
        $shouldMergeCss = Mage::getStoreConfigFlag('dev/css/merge_css_files');
        $html   = '';
        foreach ($lines as $if => $items) {
            if (empty($items)) {
                continue;
            }
            if (!empty($if)) {
                // open !IE conditional using raw value
                if (strpos($if, "><!-->") !== false) {
                    $html .= $if . "\n";
                } else {
                    $html .= '<!--[if '.$if.']>' . "\n";
                }
            }

            // static and skin css
            //rel="stylesheet"
//            <link rel="preload" href="styles.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
//<noscript><link rel="stylesheet" href="styles.css"></noscript>
//                $html .= $this->_prepareStaticAndSkinElements('<link rel="stylesheet" type="text/css" href="%s"%s />'."\n",
            $html .= $this->_prepareStaticAndSkinElements('<link rel="preload" as="style" type="text/css"  onload="this.onload=null;this.rel=\'stylesheet\'" href="%s"%s />'."\n",
                empty($items['js_css']) ? array() : $items['js_css'],
                empty($items['skin_css']) ? array() : $items['skin_css'],
                $shouldMergeCss ? array(Mage::getDesign(), 'getMergedCssUrl') : null
            );

            if (Mage::app()->getStore()->isAdmin()) {

                // static and skin javascripts
                $html .= $this->_prepareStaticAndSkinElements('<script type="text/javascript"  src="%s"%s></script>' . "\n",
                    empty($items['js']) ? array() : $items['js'],
                    empty($items['skin_js']) ? array() : $items['skin_js'],
                    $shouldMergeJs ? array(Mage::getDesign(), 'getMergedJsUrl') : null
                );
            }else{
                // static and skin javascripts
                $html .= $this->_prepareStaticAndSkinElements('<script type="text/javascript" defer src="%s"%s></script>' . "\n",
                    empty($items['js']) ? array() : $items['js'],
                    empty($items['skin_js']) ? array() : $items['skin_js'],
                    $shouldMergeJs ? array(Mage::getDesign(), 'getMergedJsUrl') : null
                );
            }

            // other stuff
            if (!empty($items['other'])) {
                $html .= $this->_prepareOtherHtmlHeadElements($items['other']) . "\n";
            }

            if (!empty($if)) {
                // close !IE conditional comments correctly
                if (strpos($if, "><!-->") !== false) {
                    $html .= '<!--<![endif]-->' . "\n";
                } else {
                    $html .= '<![endif]-->' . "\n";
                }
            }
        }
        return $html;
    }


    public function isMergeBlacklisted($input)
    {
        if (isset($this->_speedsterMergeBlacklist['js' . DS . $input])
            || isset($this->_speedsterMergeBlacklist[$input])
        ) {
            return true;
        }
        return false;
    }



    public function getChunkedItems($items, $prefix = '', $maxLen = 2000)
    {
        if (!Mage::getStoreConfigFlag('foomanspeedster/settings/enabled')) {
            return parent::getChunkedItems($items, $prefix, 450);
        }

        $chunks = array();
        $chunk = $prefix;

        foreach ($items as $item) {
            if (strlen($chunk . ',' . $item) > $maxLen) {
                $chunks[] = $chunk;
                $chunk = $prefix;
            }
            //this is the first item
            if ($chunk === $prefix) {
                //remove first slash, only needed to create double slash for minify shortcut to document root
                $chunk .= substr($item, 1);
            } else {
                //remove first slash, only needed to create double slash for minify shortcut to document root
                $chunk .= ',' . substr($item, 1);
            }
        }

        $chunks[] = $chunk;
        return $chunks;
    }





    protected function _construct()
    {
        parent::_construct();
        $this->setupCanonicalUrl();
        $this->setMeta();
    }

    public function getConfig()
    {
        return Mage::getSingleton('seo/config');
    }

    public function getRobots()
    {

        $fullAction = $this->getAction()->getFullActionName();

        $productActions = [
            'catalog_product_view',
            'review_product_list',
            'review_product_view',
            'productquestions_show_index',
        ];

        if (in_array($fullAction, $productActions)) {
            $product = Mage::registry('current_product');
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addFieldToFilter('entity_id', $product->getId())
                ->addStoreFilter()
                ->addUrlRewrite();
            //$collection->getSelect()->group('entity_id');
            $product = $collection->getFirstItem();
            if ($product) $canonicalUrl = $product->getProductUrl();
        } elseif ($fullAction == 'catalog_category_view') {
            $category = Mage::registry('current_category');
            $canonicalUrl = $category->getUrl();
        } else {
            $canonicalUrl = Mage::helper('seo')->getBaseUri();
            $canonicalUrl = Mage::getUrl('', ['_direct' => ltrim($canonicalUrl, '/')]);
        }

        $robots = parent::getRobots();
        $layer = Mage::getSingleton('catalog/layer');
        $state = $layer->getState();

        $robots = 'INDEX,FOLLOW';
        $remove = true;
        foreach ($state->getFilters() as $key => $fil) {
            if (count($fil->getFilter()->getValues()) > 1) {
                $robots = 'NOINDEX,FOLLOW';
                $this->addLinkRel('canonical', $canonicalUrl);

                $remove = false;
                break;
            }
        }


        $canonicalUrl = Mage::helper('seo')->getBaseUri();
        switch ($canonicalUrl)
        {
            case '/dorozhnaja-tehnika/vibroplity-2':
                $canonicalUrl='/stroitelnoe-oborudovanie/vibroplity';
                $remove = false;
                break;
        }

        if (strpos($canonicalUrl,'?p=')!==false){
            //$canonicalUrl = substr($canonicalUrl, 0, strpos($canonicalUrl, "filter"));
            $canonicalUrl = substr($canonicalUrl, 0,strpos($canonicalUrl, "?p="));
            $canonicalUrl = substr($canonicalUrl, 0, strpos($canonicalUrl, "filter"));

//            echo '$canonicalUrl'.$canonicalUrl;
//            die();

            //$canonicalUrl=$canonical_url = substr($url, 0, strpos($url, "filter"));str-replac'/stroitelnoe-oborudovanie/vibroplity';
            $remove = true;

        }
//        echo '$canonicalUrl'.$canonicalUrl;
//        die();
        //$this->addLinkRel('canonical', $canonicalUrl);
        if ($remove) {
            $this->removeItem('link_rel', $canonicalUrl);
            $this->addItem('link_rel', $canonicalUrl);
        }





        return $robots;
    }

    public function setupCanonicalUrl()
    {
        if (!$this->getAction()) {
            return;
        }
        $fullAction = $this->getAction()->getFullActionName();

        $productActions = [
            'catalog_product_view',
            'review_product_list',
            'review_product_view',
            'productquestions_show_index',
        ];

        if (in_array($fullAction, $productActions)) {
            $product = Mage::registry('current_product');
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addFieldToFilter('entity_id', $product->getId())
                ->addStoreFilter()
                ->addUrlRewrite();
            //$collection->getSelect()->group('entity_id');
            $product = $collection->getFirstItem();
            if ($product) $canonicalUrl = $product->getProductUrl();
        } elseif ($fullAction == 'catalog_category_view') {
            $category = Mage::registry('current_category');
            $canonicalUrl = $category->getUrl();
        } else {
            $canonicalUrl = Mage::helper('seo')->getBaseUri();
            $canonicalUrl = Mage::getUrl('', ['_direct' => ltrim($canonicalUrl, '/')]);
        }

        //$canonicalUrl=str_replace('https://m.ukrsnab.com.ua','https://ukrsnab.com.ua',$canonicalUrl);
        //$canonicalUrl=str_replace('index.php/','',$canonicalUrl);
        //$canonicalUrl=str_replace('http://newukrsnab.test/','',$canonicalUrl);

        if ((Mage::app()->getRequest()->getParam('p') && Mage::app()->getRequest()->getParam('p') != 1) || Mage::app()->getRequest()->getParam('limit') || Mage::app()->getRequest()->getParam('order') || Mage::app()->getRequest()->getParam('mode')) {

            $this->addLinkRel('canonical', $canonicalUrl);
        } else {

            $layer = Mage::getSingleton('catalog/layer');
            $state = $layer->getState();

            $remove = true;
            foreach ($state->getFilters() as $key => $fil) {
                if (count($fil->getFilter()->getValues()) > 1) {
                    //$robots= 'NOINDEX,FOLLOW';
                    $this->addLinkRel('canonical', $canonicalUrl);

                    $remove = false;
                    break;
                }
            }

            if ($remove) {
                $this->removeItem('link_rel', $canonicalUrl);
            }
            /*if (count($state->getFilters()) > 1) {
                $this->addLinkRel('canonical', $canonicalUrl);
            }else
            $this->removeItem('link_rel', $canonicalUrl);*/
            //echo '$canonicalUrl '.$canonicalUrl;
        }


        $url = $_SERVER['REQUEST_URI'];

        $array_canonical = ['/energosnab/generatory/filter/vykhodnoie-napriazhieniie/220-v',
            '/energosnab/generatory/filter/proizvoditiel/tagred',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/rostiekh',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/stal',
            '/stroitelnoe-oborudovanie/parketoshlifovalnye-mashiny/filter/oblast-primienieniia/dlia-parkieta',
            '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/bert',
            '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/odwerk',
            '/stroitelnoe-oborudovanie/vibroplity/benzinovye-vibroplity/filter/proizvoditiel/avant',
            '/energosnab/generatory/filter/proizvoditiel/enierghomash',
            '/energosnab/generatory/benzinovye-generatory/filter/proizvoditiel/forte',
            '/energosnab/generatory/benzinovye-generatory/filter/proizvoditiel/gude',
            '/energosnab/generatory/generatory-bolee-50-kvt/filter/moshchnost-ghienieratora/101-kvt-200-kvt',
            '/energosnab/generatory/gazovye-generatory/filter/moshchnost-ghienieratora/3-kvt-5-kvt',
            '/energosnab/generatory/gazovye-generatory/filter/proizvoditiel/kipor',
            '/energosnab/generatory/invertornye-generatory/filter/moshchnost-ghienieratora/5-kvt-10-kvt',
            '/energosnab/generatory/svarochnye-generatory/filter/moshchnost-ghienieratora/5-kvt-10-kvt',
            '/instrument/filter/proizvoditiel/intierskol',
            '/instrument/filter/proizvoditiel/kress',
            '/mototehnika/motobloki-i-motokultivatory/filter/tip-dvighatielia/bienzinovyi',
            '/otbojnye-molotki/filter/tip-otboinogho-molotka/pnievmatichieskii',
            '/sad-ogorod/sadovaya-tekhnika/electropily/filter/proizvoditiel/ryobi',
            '/selkhoztekhnika/minitraktora/filter/proizvoditiel/kipor',
            '/stroitelnoe-oborudovanie/almaznaja-tehnika/ustanovki-dlja-almaznogo-burenija/filter/proizvoditiel/cayken',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/obiem-bietonomieshalki/220-l-500-l',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/obiem-bietonomieshalki/150-l-220-l',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/obiem-bietonomieshalki/70-l-150-l',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/obiem-bietonomieshalki/220-l-500-l',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/altrad-liv',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/limex',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/orange',
            '/stroitelnoe-oborudovanie/filter/proizvoditiel/kunzle-tasin',
            '/stroitelnoe-oborudovanie/parketoshlifovalnye-mashiny/filter/proizvoditiel/som',
            '/stroitelnoe-oborudovanie/parketoshlifovalnye-mashiny/filter/tip-shlifoval-noi-mashiny/diskovaia',
            '/stroitelnoe-oborudovanie/shvonarezchiki/elektricheskie-shvonarezchiki/filter/proizvoditiel/avant',
            '/stroitelnoe-oborudovanie/shvonarezchiki/filter/proizvoditiel-dvighatielia/loncin',
            '/stroitelnoe-oborudovanie/shvonarezchiki/filter/proizvoditiel/ntp-1',
            '/stroitelnoe-oborudovanie/vibratory/filter/proizvoditiel/avant',
            '/stroitelnoe-oborudovanie/vibratory/filter/proizvoditiel/enar',
            //'/stroitelnoe-oborudovanie/vibratory/filter/tip-vibratora/ghlubinnyie-vibratory',
            '/stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/filter/proizvoditiel/krasnyi-maiak',
            '/stroitelnoe-oborudovanie/vibratory/ploshchadochnye-vibratory/filter/proizvoditiel/enar',
            '/stroitelnoe-oborudovanie/vibratory/ploshchadochnye-vibratory/filter/proizvoditiel/krasnyi-maiak',
            '/stroitelnoe-oborudovanie/vibroplity/filter/ispol-zuietsia-dlia/ukladki-trotuarnoi-plitki',
            '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel-dvighatielia/subaru',
            '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/baumax',
            '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/scheppach',
            '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/swepac',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-120-299-kg/filter/proizvoditiel/biedronka',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-90-119-kg/filter/proizvoditiel/biedronka',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-90-119-kg/filter/proizvoditiel/honker',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-ot-40-do-89-kg/filter/proizvoditiel/biedronka',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-ot-40-do-89-kg/filter/proizvoditiel/honker',
            '/stroitelnoe-oborudovanie/vibrorejki/filter/proizvoditiel/avant',
            '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel-dvighatielia/kientavr',
            '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel/baumax',
            '/stroitelnoe-oborudovanie/vibrotrambovki/vibrotrambovki-s-dvigatelem-honda/filter/proizvoditiel/honker',
            '/stroitelnoe-oborudovanie/zatirochnye-mashiny/filter/proizvoditiel/masalta',
            '/stroitelnoe-oborudovanie/zatirochnye-mashiny/filter/proizvoditiel/spm',
            '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/filter/tip-dvighatielia/gazovyi',
        ];


        if (in_array($url, $array_canonical)) {
            //$canonical_url=$url;
            $canonical_url = substr($url, 0, strpos($url, "filter"));
            //$this->addLinkRel('canonical', $canonical_url);


        }

        /* if ($url=='/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-70-l-150-litrov/filter/obiem-bietonomieshalki/70-l-150-l'){
             $this->addLinkRel('canonical', '/stroitelnoe-oborudovanie/betonomeshalki');
         }*/
        if ($_SERVER['REMOTE_ADDR'] == '78.46.62.217') {
            // echo 'categoryurl'.$canonicalUrl;die();
            //$this->removeItem('link_rel', $category->getUrl());
        }
        //$this->removeItem('link_rel', $category->getUrl());



        $canonicalUrl = Mage::helper('seo')->getBaseUri();
        switch ($canonicalUrl)
        {
            case '/dorozhnaja-tehnika/vibroplity-2':
                $canonicalUrl='/stroitelnoe-oborudovanie/vibroplity';
                $this->addLinkRel('canonical', $canonicalUrl);
                $remove = false;
                break;
        }

        if (strpos($canonicalUrl,'?p=')!==false){

            $canonicalUrl = substr($canonicalUrl, 0,strpos($canonicalUrl, "?p="));
            $canonicalUrl = substr($canonicalUrl, 0, strpos($canonicalUrl, "filter"));
            $this->addLinkRel('canonical', $canonicalUrl);
            $remove = false;

        }

    }

    public function setMeta()
    {


        if ($product = Mage::registry('current_product')) {

            /* if ($_SERVER['REMOTE_ADDR'] == '78.46.62.217') {
                 echo '<pre>';
                 print_r($product);
                 echo '</pre>';
                 die();
             }*/

            $description = $product->getMetaDescription();
            $title=$product->getMetaTitle();
            $product_name = mb_strtolower($product->getName());
            if ($description==''){

                $description = 'Заказать ' . $product_name . ' ➤ По лучшей цене ➤ Официальный дилер в Украине... ➤Сегодня_выгодное_предложение... получи СКИДКУ от... +Доставку по Украине... ★ОТЗЫВЫ ★Акции ★СКИДКИ.... ☎️Звони сейчас +38(067)230-60-90';
                //$product->getName().' - Ukrsnab.com.ua | Первый интернет-магазин строительной техники и профессионального инструмента.';
                $this->setDescription($description);
            }


            if ($title=='') {
                $title = 'Купить ' . $product_name . ': продажа по низким ценам ' . $product_name . ' в ';
                $this->setTitle($title);//Description($description);
            }

        }
    }

}
