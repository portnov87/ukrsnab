<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com) (original implementation)
 * @copyright  Copyright (c) 2008 Fooman (http://www.fooman.co.nz) (use of Minify Library)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Speedster Html Head Block
 *
 * @package Fooman_Speedster
 * @author  Kristof Ringleff <kristof@fooman.co.nz>
 */
class Fooman_Speedster_Block_Page_Html_Head extends Mage_Page_Block_Html_Head
{

    public function getCssJsHtml()
    {
        if (!Mage::getStoreConfigFlag('foomanspeedster/settings/enabled')) {
            return parent::getCssJsHtml();
        }
        $webroot = "/";

        $lines = array();

        $baseJs = Mage::getBaseUrl('js');
        $baseJsFast = Mage::getBaseUrl('skin') . 'm/';
        Mage::getConfig()->getVarDir('minifycache');
        $html = '';
        //$html = "<!--".BP."-->\n";
        if (Mage::app()->getStore()->isAdmin()) {
            $script = '<script type="text/javascript" src="%s" %s></script>';
        }else{
            $script = '<script type="text/javascript" defer src="%s" %s></script>';
        }
        $stylesheet = '<link type="text/css" rel="stylesheet" href="%s" %s />';
        $alternate = '<link rel="alternate" type="%s" href="%s" %s />';

        foreach ($this->_data['items'] as $item) {
            if (is_null($item['name'])) {
                continue;
            }
            if (!is_null($item['cond']) && !$this->getData($item['cond'])) {
                continue;
            }
            $if = !empty($item['if']) ? $item['if'] : '';
            switch ($item['type']) {
                case 'js':
                    if (strpos($item['name'], 'packaging.js') !== false) {
                        $item['name'] = $baseJs . $item['name'];
                        $lines[$if]['script_direct'][] = $item;
                    } else {
                        $lines[$if]['script']['global'][] = "/" . $webroot . "js/" . $item['name'];
                    }
                    break;

                case 'script_direct':
                    $lines[$if]['script_direct'][] = $item;
                    break;

                case 'css_direct':
                    $lines[$if]['css_direct'][] = $item;
                    break;

                case 'js_css':
                    $lines[$if]['other'][] = sprintf($stylesheet, $baseJs . $item['name'], $item['params']);
                    break;

                case 'skin_js':
                    $chunks = explode('/skin', $this->getSkinUrl($item['name']), 2);
                    $skinJsURL = "/" . $webroot . "skin" . $chunks[1];

                    if (strpos($item['name'], '.min.js') !== false) {
                        $skinJsLoc = BP . DS . "skin" . $chunks[1];
                        $skinJsContent = file_get_contents($skinJsLoc);
                        if (preg_match('/@ sourceMappingURL=([^\s]*)/s', $skinJsContent, $matches)) {
                            //create a file without source map
                            $hash = md5($skinJsContent);
                            $skinJsLoc = str_replace('.min.js', '.' . $hash . '.min.js', $skinJsLoc);
                            $skinJsURL = str_replace('.min.js', '.' . $hash . '.min.js', $skinJsURL);
                            if (!file_exists($skinJsLoc)) {
                                file_put_contents(
                                    $skinJsLoc,
                                    str_replace(
                                        $matches[0], 'orig file with source map: ' . $this->getSkinUrl($item['name']),
                                        $skinJsContent
                                    )
                                );
                            }
                        }
                    }

                    $lines[$if]['script']['skin'][] = $skinJsURL;
                    break;

                case 'skin_css':
                    if ($item['params'] == 'media="all"') {
                        $chunks = explode('/skin', $this->getSkinUrl($item['name']), 2);
                        $lines[$if]['stylesheet'][] = "/" . $webroot . "skin" . $chunks[1];
                    } elseif ($item['params'] == 'media="print"') {
                        $chunks = explode('/skin', $this->getSkinUrl($item['name']), 2);
                        $lines[$if]['stylesheet_print'][] = "/" . $webroot . "skin" . $chunks[1];
                    } else {
                        $lines[$if]['other'][] = sprintf(
                            $stylesheet, $this->getSkinUrl($item['name']), $item['params']
                        );
                    }
                    break;

                case 'rss':
                    $lines[$if]['other'][] = sprintf(
                        $alternate, 'application/rss+xml' /*'text/xml' for IE?*/, $item['name'], $item['params']
                    );
                    break;

                case 'link_rel':
                    $lines[$if]['other'][] = sprintf('<link %s href="%s" />', $item['params'], $item['name']);
                    break;

                case 'ext_js':
                default:
                    $lines[$if]['other'][] = sprintf(
                        '<script type="text/javascript" defer src="%s"></script>', $item['name']
                    );
                    break;

            }
        }

        foreach ($lines as $if => $items) {
            if (!empty($if)) {
                // open !IE conditional using raw value
                if (strpos($if, "><!-->") !== false) {
                    $html .= $if . "\n";
                } else {
                    $html .= '<!--[if '.$if.']>' . "\n";
                }
            }
            if (!empty($items['stylesheet'])) {
                $cssBuild = Mage::getModel('speedster/buildSpeedster', array($items['stylesheet'], BP));
                foreach (
                    $this->getChunkedItems($items['stylesheet'], $baseJsFast . $cssBuild->getLastModified()) as $item
                ) {
                    $html .= sprintf($stylesheet, $item, 'media="all"') . "\n";
                }
            }
            if (!empty($items['script']['global']) || !empty($items['script']['skin'])) {
                if (!empty($items['script']['global']) && !empty($items['script']['skin'])) {
                    $mergedScriptItems = array_merge($items['script']['global'], $items['script']['skin']);
                } elseif (!empty($items['script']['global']) && empty($items['script']['skin'])) {
                    $mergedScriptItems = $items['script']['global'];
                } else {
                    $mergedScriptItems = $items['script']['skin'];
                }
                $jsBuild = Mage::getModel('speedster/buildSpeedster', array($mergedScriptItems, BP));
                $chunkedItems = $this->getChunkedItems($mergedScriptItems, $baseJsFast . $jsBuild->getLastModified());
                foreach ($chunkedItems as $item) {
                    $html .= sprintf($script, $item, '') . "\n";
                }
            }
            if (!empty($items['css_direct'])) {
                foreach ($items['css_direct'] as $item) {
                    $html .= sprintf($stylesheet, $item['name']) . "\n";
                }
            }
            if (!empty($items['script_direct'])) {
                foreach ($items['script_direct'] as $item) {
                    $html .= sprintf($script, $item['name'], '') . "\n";
                }
            }
            if (!empty($items['stylesheet_print'])) {
                $cssBuild = Mage::getModel('speedster/buildSpeedster', array($items['stylesheet_print'], BP));
                foreach (
                    $this->getChunkedItems($items['stylesheet_print'], $baseJsFast . $cssBuild->getLastModified()) as
                    $item
                ) {
                    $html .= sprintf($stylesheet, $item, 'media="print"') . "\n";
                }
            }
            if (!empty($items['other'])) {
                $html .= join("\n", $items['other']) . "\n";
            }
            if (!empty($if)) {
                // close !IE conditional comments correctly
                if (strpos($if, "><!-->") !== false) {
                    $html .= '<!--<![endif]-->' . "\n";
                } else {
                    $html .= '<![endif]-->' . "\n";
                }
            }
        }
        return $html;
    }

    public function getChunkedItems($items, $prefix = '', $maxLen = 2000)
    {
        if (!Mage::getStoreConfigFlag('foomanspeedster/settings/enabled')) {
            return parent::getChunkedItems($items, $prefix, 450);
        }

        $chunks = array();
        $chunk = $prefix;

        foreach ($items as $item) {
            if (strlen($chunk . ',' . $item) > $maxLen) {
                $chunks[] = $chunk;
                $chunk = $prefix;
            }
            //this is the first item
            if ($chunk === $prefix) {
                //remove first slash, only needed to create double slash for minify shortcut to document root
                $chunk .= substr($item, 1);
            } else {
                //remove first slash, only needed to create double slash for minify shortcut to document root
                $chunk .= ',' . substr($item, 1);
            }
        }

        $chunks[] = $chunk;
        return $chunks;
    }





    protected function _construct()
    {
        parent::_construct();
        $this->setupCanonicalUrl();
        $this->setMeta();
    }

    public function getConfig()
    {
        return Mage::getSingleton('seo/config');
    }

    public function getRobots()
    {

        $fullAction = $this->getAction()->getFullActionName();

        $productActions = [
            'catalog_product_view',
            'review_product_list',
            'review_product_view',
            'productquestions_show_index',
        ];

        if (in_array($fullAction, $productActions)) {
            $product = Mage::registry('current_product');
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addFieldToFilter('entity_id', $product->getId())
                ->addStoreFilter()
                ->addUrlRewrite();
            //$collection->getSelect()->group('entity_id');
            $product = $collection->getFirstItem();
            if ($product) $canonicalUrl = $product->getProductUrl();
        } elseif ($fullAction == 'catalog_category_view') {
            $category = Mage::registry('current_category');
            $canonicalUrl = $category->getUrl();
        } else {
            $canonicalUrl = Mage::helper('seo')->getBaseUri();
            $canonicalUrl = Mage::getUrl('', ['_direct' => ltrim($canonicalUrl, '/')]);
        }

        $robots = parent::getRobots();
        $layer = Mage::getSingleton('catalog/layer');
        $state = $layer->getState();

        $robots = 'INDEX,FOLLOW';
        $remove = true;
        foreach ($state->getFilters() as $key => $fil) {
            if (count($fil->getFilter()->getValues()) > 1) {
                $robots = 'NOINDEX,FOLLOW';
                $this->addLinkRel('canonical', $canonicalUrl);

                $remove = false;
                break;
            }
        }


        $canonicalUrl = Mage::helper('seo')->getBaseUri();
        switch ($canonicalUrl)
        {
            case '/dorozhnaja-tehnika/vibroplity-2':
                $canonicalUrl='/stroitelnoe-oborudovanie/vibroplity';
                $remove = false;
                break;
        }

        if (strpos($canonicalUrl,'?p=')!==false){
            //$canonicalUrl = substr($canonicalUrl, 0, strpos($canonicalUrl, "filter"));
            $canonicalUrl = substr($canonicalUrl, 0,strpos($canonicalUrl, "?p="));
            $canonicalUrl = substr($canonicalUrl, 0, strpos($canonicalUrl, "filter"));

//            echo '$canonicalUrl'.$canonicalUrl;
//            die();

            //$canonicalUrl=$canonical_url = substr($url, 0, strpos($url, "filter"));str-replac'/stroitelnoe-oborudovanie/vibroplity';
            $remove = true;

        }
//        echo '$canonicalUrl'.$canonicalUrl;
//        die();
        //$this->addLinkRel('canonical', $canonicalUrl);
        if ($remove) {
            $this->removeItem('link_rel', $canonicalUrl);
            $this->addItem('link_rel', $canonicalUrl);
        }





        return $robots;
    }

    public function setupCanonicalUrl()
    {
        if (!$this->getAction()) {
            return;
        }
        $fullAction = $this->getAction()->getFullActionName();

        $productActions = [
            'catalog_product_view',
            'review_product_list',
            'review_product_view',
            'productquestions_show_index',
        ];

        if (in_array($fullAction, $productActions)) {
            $product = Mage::registry('current_product');
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addFieldToFilter('entity_id', $product->getId())
                ->addStoreFilter()
                ->addUrlRewrite();
            //$collection->getSelect()->group('entity_id');
            $product = $collection->getFirstItem();
            if ($product) $canonicalUrl = $product->getProductUrl();
        } elseif ($fullAction == 'catalog_category_view') {
            $category = Mage::registry('current_category');
            $canonicalUrl = $category->getUrl();
        } else {
            $canonicalUrl = Mage::helper('seo')->getBaseUri();
            $canonicalUrl = Mage::getUrl('', ['_direct' => ltrim($canonicalUrl, '/')]);
        }

        //$canonicalUrl=str_replace('https://m.ukrsnab.com.ua','https://ukrsnab.com.ua',$canonicalUrl);
        //$canonicalUrl=str_replace('index.php/','',$canonicalUrl);
        //$canonicalUrl=str_replace('http://newukrsnab.test/','',$canonicalUrl);

        if ((Mage::app()->getRequest()->getParam('p') && Mage::app()->getRequest()->getParam('p') != 1) || Mage::app()->getRequest()->getParam('limit') || Mage::app()->getRequest()->getParam('order') || Mage::app()->getRequest()->getParam('mode')) {

            $this->addLinkRel('canonical', $canonicalUrl);
        } else {

            $layer = Mage::getSingleton('catalog/layer');
            $state = $layer->getState();

            $remove = true;
            foreach ($state->getFilters() as $key => $fil) {
                if (count($fil->getFilter()->getValues()) > 1) {
                    //$robots= 'NOINDEX,FOLLOW';
                    $this->addLinkRel('canonical', $canonicalUrl);

                    $remove = false;
                    break;
                }
            }

            if ($remove) {
                $this->removeItem('link_rel', $canonicalUrl);
            }
            /*if (count($state->getFilters()) > 1) {
                $this->addLinkRel('canonical', $canonicalUrl);
            }else
            $this->removeItem('link_rel', $canonicalUrl);*/
            //echo '$canonicalUrl '.$canonicalUrl;
        }


        $url = $_SERVER['REQUEST_URI'];

        $array_canonical = ['/energosnab/generatory/filter/vykhodnoie-napriazhieniie/220-v',
            '/energosnab/generatory/filter/proizvoditiel/tagred',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/rostiekh',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/stal',
            '/stroitelnoe-oborudovanie/parketoshlifovalnye-mashiny/filter/oblast-primienieniia/dlia-parkieta',
            '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/bert',
            '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/odwerk',
            '/stroitelnoe-oborudovanie/vibroplity/benzinovye-vibroplity/filter/proizvoditiel/avant',
            '/energosnab/generatory/filter/proizvoditiel/enierghomash',
            '/energosnab/generatory/benzinovye-generatory/filter/proizvoditiel/forte',
            '/energosnab/generatory/benzinovye-generatory/filter/proizvoditiel/gude',
            '/energosnab/generatory/generatory-bolee-50-kvt/filter/moshchnost-ghienieratora/101-kvt-200-kvt',
            '/energosnab/generatory/gazovye-generatory/filter/moshchnost-ghienieratora/3-kvt-5-kvt',
            '/energosnab/generatory/gazovye-generatory/filter/proizvoditiel/kipor',
            '/energosnab/generatory/invertornye-generatory/filter/moshchnost-ghienieratora/5-kvt-10-kvt',
            '/energosnab/generatory/svarochnye-generatory/filter/moshchnost-ghienieratora/5-kvt-10-kvt',
            '/instrument/filter/proizvoditiel/intierskol',
            '/instrument/filter/proizvoditiel/kress',
            '/mototehnika/motobloki-i-motokultivatory/filter/tip-dvighatielia/bienzinovyi',
            '/otbojnye-molotki/filter/tip-otboinogho-molotka/pnievmatichieskii',
            '/sad-ogorod/sadovaya-tekhnika/electropily/filter/proizvoditiel/ryobi',
            '/selkhoztekhnika/minitraktora/filter/proizvoditiel/kipor',
            '/stroitelnoe-oborudovanie/almaznaja-tehnika/ustanovki-dlja-almaznogo-burenija/filter/proizvoditiel/cayken',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/obiem-bietonomieshalki/220-l-500-l',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/obiem-bietonomieshalki/150-l-220-l',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/obiem-bietonomieshalki/70-l-150-l',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/obiem-bietonomieshalki/220-l-500-l',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/altrad-liv',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/limex',
            '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/orange',
            '/stroitelnoe-oborudovanie/filter/proizvoditiel/kunzle-tasin',
            '/stroitelnoe-oborudovanie/parketoshlifovalnye-mashiny/filter/proizvoditiel/som',
            '/stroitelnoe-oborudovanie/parketoshlifovalnye-mashiny/filter/tip-shlifoval-noi-mashiny/diskovaia',
            '/stroitelnoe-oborudovanie/shvonarezchiki/elektricheskie-shvonarezchiki/filter/proizvoditiel/avant',
            '/stroitelnoe-oborudovanie/shvonarezchiki/filter/proizvoditiel-dvighatielia/loncin',
            '/stroitelnoe-oborudovanie/shvonarezchiki/filter/proizvoditiel/ntp-1',
            '/stroitelnoe-oborudovanie/vibratory/filter/proizvoditiel/avant',
            '/stroitelnoe-oborudovanie/vibratory/filter/proizvoditiel/enar',
            //'/stroitelnoe-oborudovanie/vibratory/filter/tip-vibratora/ghlubinnyie-vibratory',
            '/stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/filter/proizvoditiel/krasnyi-maiak',
            '/stroitelnoe-oborudovanie/vibratory/ploshchadochnye-vibratory/filter/proizvoditiel/enar',
            '/stroitelnoe-oborudovanie/vibratory/ploshchadochnye-vibratory/filter/proizvoditiel/krasnyi-maiak',
            '/stroitelnoe-oborudovanie/vibroplity/filter/ispol-zuietsia-dlia/ukladki-trotuarnoi-plitki',
            '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel-dvighatielia/subaru',
            '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/baumax',
            '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/scheppach',
            '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/swepac',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-120-299-kg/filter/proizvoditiel/biedronka',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-90-119-kg/filter/proizvoditiel/biedronka',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-90-119-kg/filter/proizvoditiel/honker',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-ot-40-do-89-kg/filter/proizvoditiel/biedronka',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-ot-40-do-89-kg/filter/proizvoditiel/honker',
            '/stroitelnoe-oborudovanie/vibrorejki/filter/proizvoditiel/avant',
            '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel-dvighatielia/kientavr',
            '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel/baumax',
            '/stroitelnoe-oborudovanie/vibrotrambovki/vibrotrambovki-s-dvigatelem-honda/filter/proizvoditiel/honker',
            '/stroitelnoe-oborudovanie/zatirochnye-mashiny/filter/proizvoditiel/masalta',
            '/stroitelnoe-oborudovanie/zatirochnye-mashiny/filter/proizvoditiel/spm',
            '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/filter/tip-dvighatielia/gazovyi',
        ];


        if (in_array($url, $array_canonical)) {
            //$canonical_url=$url;
            $canonical_url = substr($url, 0, strpos($url, "filter"));
            //$this->addLinkRel('canonical', $canonical_url);


        }

        /* if ($url=='/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-70-l-150-litrov/filter/obiem-bietonomieshalki/70-l-150-l'){
             $this->addLinkRel('canonical', '/stroitelnoe-oborudovanie/betonomeshalki');
         }*/
        if ($_SERVER['REMOTE_ADDR'] == '78.46.62.217') {
            // echo 'categoryurl'.$canonicalUrl;die();
            //$this->removeItem('link_rel', $category->getUrl());
        }
        //$this->removeItem('link_rel', $category->getUrl());



        $canonicalUrl = Mage::helper('seo')->getBaseUri();
        switch ($canonicalUrl)
        {
            case '/dorozhnaja-tehnika/vibroplity-2':
                $canonicalUrl='/stroitelnoe-oborudovanie/vibroplity';
                $this->addLinkRel('canonical', $canonicalUrl);
                $remove = false;
                break;
        }

        if (strpos($canonicalUrl,'?p=')!==false){

            $canonicalUrl = substr($canonicalUrl, 0,strpos($canonicalUrl, "?p="));
            $canonicalUrl = substr($canonicalUrl, 0, strpos($canonicalUrl, "filter"));
            $this->addLinkRel('canonical', $canonicalUrl);
            $remove = false;

        }

    }

    public function setMeta()
    {


        if ($product = Mage::registry('current_product')) {

            /* if ($_SERVER['REMOTE_ADDR'] == '78.46.62.217') {
                 echo '<pre>';
                 print_r($product);
                 echo '</pre>';
                 die();
             }*/

            $description = $product->getMetaDescription();
            $title=$product->getMetaTitle();
            $product_name = mb_strtolower($product->getName());
            if ($description==''){

                $description = 'Заказать ' . $product_name . ' ➤ По лучшей цене ➤ Официальный дилер в Украине... ➤Сегодня_выгодное_предложение... получи СКИДКУ от... +Доставку по Украине... ★ОТЗЫВЫ ★Акции ★СКИДКИ.... ☎️Звони сейчас +38(067)230-60-90';
                //$product->getName().' - Ukrsnab.com.ua | Первый интернет-магазин строительной техники и профессионального инструмента.';
                $this->setDescription($description);
            }


            if ($title=='') {
                $title = 'Купить ' . $product_name . ': продажа по низким ценам ' . $product_name . ' в ';
                $this->setTitle($title);//Description($description);
            }

        }
    }

}
