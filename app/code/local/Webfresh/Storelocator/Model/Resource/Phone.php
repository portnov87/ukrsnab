<?php

class Webfresh_Storelocator_Model_Resource_Phone extends Mage_Core_Model_Resource_Db_Abstract {
    protected function _construct(){
        $this->_init('storelocator/phone', 'id');
    }
}