<?php

class Webfresh_Storelocator_Model_Resource_Storelocator extends Mage_Core_Model_Resource_Db_Abstract {
    protected function _construct(){
        $this->_init('storelocator/storelocator', 'storelocator_id');
    }
    protected $_store  = null;

    public function lookupStoreIds($pageId)
    {
        $adapter = $this->_getReadAdapter();

        $select  = $adapter->select()
            ->from($this->getTable('storelocator/store'), 'store_id')
            ->where('storelocator_id = ?',(int)$pageId);

        return $adapter->fetchCol($select);
    }

    protected function _afterLoad(Mage_Core_Model_Abstract $object) {
        if (!$object->getIsMassDelete()) {
            $object = $this->_loadStore($object);
        }

        if ($object->getId()) {
            $stores = $this->lookupStoreIds($object->getId());
            $object->setData('store_id', $stores);
        }

        return parent::_afterLoad($object);
    }

    protected function _getLoadSelect($field, $value, $object) {
        $select = parent::_getLoadSelect($field, $value, $object);

        if ($data = $object->getStoreId()) {
            $select->join(
                array('store' => $this->getTable('storelocator/store')), $this->getMainTable().'.storelocator_id = `store`.storelocator_id')
                ->where('`store`.store_id in (0, ?) ', $data);
        }

        $select->limit(1);

        return $select;
    }


    protected function _beforeSave(Mage_Core_Model_Abstract $object) {
      $date=now();

      //if ((isset($object->getId()))&&($object->getId()!=0)

        if ($object->getCreated == NULL) $object->setCreated($date);
        $object->setUpdated($date);

        return parent::_afterSave($object);
    }

    protected function _afterSave(Mage_Core_Model_Abstract $object) {
        if (!$object->getIsMassStatus()) {
            $this->_saveToStoreTable($object);
        }

        return parent::_afterSave($object);
    }

    protected function _beforeDelete(Mage_Core_Model_Abstract $object) {

        $adapter = $this->_getReadAdapter();
        $adapter->delete($this->getTable('storelocator/store'), 'storelocator_id='.$object->getId());

        return parent::_beforeDelete($object);
    }

    /**
     * Load stores
     */
    private function _loadStore(Mage_Core_Model_Abstract $object) {
        $select = $this->_getReadAdapter()->select()
            ->from($this->getTable('storelocator/store'))
            ->where('storelocator_id = ?', $object->getId());

        if ($data = $this->_getReadAdapter()->fetchAll($select)) {
            $array = array();
            foreach ($data as $row) {
                $array[] = $row['store_id'];
            }
            $object->setData('stores', $array);
        }
        return $object;
    }

    /**
     * Save stores
     */
    private function _saveToStoreTable(Mage_Core_Model_Abstract $object) {
        if (!$object->getData('stores')) {
            $condition = $this->_getWriteAdapter()->quoteInto('storelocator_id = ?', $object->getId());
            $this->_getWriteAdapter()->delete($this->getTable('storelocator/store'), $condition);

            $storeArray = array(
                'storelocator_id' => $object->getId(),
                'store_id' => '0');
            $this->_getWriteAdapter()->insert(
                $this->getTable('storelocator/store'), $storeArray);
            return true;
        }

        $condition = $this->_getWriteAdapter()->quoteInto('storelocator_id = ?', $object->getId());
        $this->_getWriteAdapter()->delete($this->getTable('storelocator/store'), $condition);
        foreach ((array)$object->getData('stores') as $store) {
            $storeArray = array();
            $storeArray['storelocator_id'] = $object->getId();
            $storeArray['store_id'] = $store;
            $this->_getWriteAdapter()->insert(
                $this->getTable('storelocator/store'), $storeArray);
        }
    }
}