<?php

class Webfresh_Storelocator_Model_Products_Api extends Mage_Api_Model_Resource_Abstract
{

    public function quantity($store_code,$data)
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $storelocator_products = $resource->getTableName('storelocator/products');

        $storelocator_par = Mage::getModel('storelocator/storelocator')->getCollection()
            ->addFieldToFilter('code',array('eq' => (string)$store_code))->getFirstItem();

        try {
            $data_insert=array();
            foreach ($data as $product) {

                $data_insert[]=array(
                    'sku'=>(string)$product->sku,
                    'quantity'=>(string)$product->quantity,
                    'delivery'=>(string)$product->delivery,
                    'st_id'=>$storelocator_par->getId()
                );

                $where = array(
                    'sku = ?'  => (string)$product->sku,
                    'st_id = ?' => $storelocator_par->getId()
                );

                $writeConnection->delete($storelocator_products, $where);

            }

            $writeConnection->insertMultiple($storelocator_products,  $data_insert);

        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
            // We cannot know all the possible exceptions,
            // so let's try to catch the ones that extend Mage_Core_Exception
        } catch (Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }
        return $storelocator_par->getId();
    }

}