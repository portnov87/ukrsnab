<?php

class Webfresh_Storelocator_Model_Products extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('storelocator/products');
    }

    public function getQtyProductInStorelocator($storelocator_id, $sku)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $select = $readConnection->select()
            ->from($resource->getTableName('storelocator/products'))
            ->join(array('storelocator' => 'storelocator'), 'storelocator.storelocator_id = storelocator_products.st_id', array('storelocator.title', 'storelocator.storelocator_id'));
        //->join(array('city' => 'storelocator_cities'), 'city.city_id = storelocator.city_id', array('city.city_name', 'city.city_id', 'city.kyrier'));


        $select->where('storelocator_products.sku = :product_sku AND storelocator.storelocator_id=:storelocator_id');
        $bind = array(
            'storelocator_id' => $storelocator_id,
            'product_sku' => $sku
        );
        //$select->group('entity_id');
        $results = $readConnection->fetchRow($select, $bind);
        return $results;
    }


    public function productsInStore($city_id, $productsSku, $type = 'only_available')
    {


        // $productsSku=$productsSku[0];
        if (is_array($productsSku))
            $productsSku_string = implode(',', $productsSku);
        else $productsSku_string = $productsSku;


        if ($productsSku_string != '') {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $profiler = $readConnection->getProfiler();
            /*$select = $readConnection->select()
                ->from($resource->getTableName('storelocator/products'))
                ->join(array('storelocator' => 'storelocator'), 'storelocator.storelocator_id = storelocator_products.st_id', array('storelocator.title', 'storelocator.latitude',
                    'storelocator.longitude', 'storelocator.address',
                    'storelocator.time_work',
                    'storelocator.title_stop',
                    'storelocator.metro',
                    'storelocator.minibuses',
                    'storelocator.phones',
                    'storelocator.logo',
                    'storelocator.district',
                    'storelocator.icon_map',
                    'storelocator.storelocator_id'))
                ->join(array('city' => 'storelocator_cities'), 'city.city_id = storelocator.city_id', array('city.city_name', 'city.city_id', 'city.kyrier'));
            */

            $storelocator_products=$resource->getTableName('storelocator/products');
            $select = $readConnection->select()
                ->from('storelocator')
                ->joinLeft(array('storelocator_pr' => $storelocator_products), 'storelocator.storelocator_id = storelocator_pr.st_id AND storelocator_pr.sku IN (' . $productsSku_string . ')', array(
                    'storelocator_pr.sku', 'storelocator_pr.quantity'))
                ->join(array('city' => 'storelocator_cities'), 'city.city_id = storelocator.city_id', array('city.city_name', 'city.city_id', 'city.kyrier'));


            if ($type == 'only_available') {
                if ($city_id) {
                    $select->where('storelocator_pr.sku IN (' . $productsSku_string . ') AND storelocator.city_id=:city_id AND storelocator.status=1');
                    $bind = array(
                        //'product_sku' => $productsSku,
                        'city_id' => $city_id
                    );
                } else {
                    $select->where('storelocator_pr.sku IN (' . $productsSku_string . ') AND storelocator.status=1');
                    $bind = array(//'product_sku' => $productsSku
                    );
                }
               // echo '$type'.$type;die();
            }else{
                $select->where('storelocator.city_id=:city_id AND storelocator.status=1');
                $bind = array(
                    //'product_sku' => $productsSku,
                    'city_id' => $city_id
                );
                //$select->where('storelocator.status=1');

            }
            $select->order('quantity desc');
            //$select->group('entity_id');
            //echo 'd';
            //echo $select->select();
            //die();
            $results = $readConnection->fetchAll($select, $bind);
            /*echo '<pre>type'.$type;
            print_R($results);
            echo '<pre>';die();
*/
            $returnresult = [];
            $allproductsInstore = [];
            foreach ($results as $item) {
                //$returnresult[$item['storelocator_id']]['products'][$item['sku']] = $item;
                //if (count($returnresult[$item['storelocator_id']]['products']) == count($productsSku)) {
                    //$item['storelocator_id']
                    $allproductsInstore[$item['storelocator_id']] = [
                        'id' => $item['storelocator_id'],
                        'latitude' => $item['latitude'],
                        'longitude' => $item['longitude'],
                        'address' => $item['address'],
                        'title' => $item['title'],
                        'city_id' => $item['city_id'],
                        'time_work' => $item['time_work'],
                        'title_stop' => $item['title_stop'],
                        'metro' => $item['metro'],
                        'minibuses' => $item['minibuses'],
                        'phones' => $item['phones'],
                        'logo' => $item['logo'],
                        'district' => $item['district'],
                        'icon_map' => $item['icon_map'],
                        'quantity' => $item['quantity'],
                    ];
                //}


            }
            return $allproductsInstore;//$returnresult;
        }
        return false;


    }
}