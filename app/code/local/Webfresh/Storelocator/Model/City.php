<?php

class Webfresh_Storelocator_Model_City extends Mage_Core_Model_Abstract
{
    protected $_options = null;

    public function _construct()
    {
        parent::_construct();
        $this->_init('storelocator/city');
    }

    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = array();
            $this->_options[''] = Mage::helper('storelocator')->__('Choose City...');
            $collection = $this->getCollection();
            foreach ($collection as $item) {
                $this->_options[$item->getId()] = $item->getCityName();
            }
        }

        return $this->_options;
    }

    public function getPhones()
    {
        $phones=Mage::getModel('storelocator/phone')->getCollection();
        $phones->addFieldToFilter('city_id',$this->getId());
        return $phones;
    }



    public function getDeletePhones()
    {

        $phones=Mage::getModel('storelocator/phone')->getCollection()->addFieldToFilter('city_id',$this->getId());
        foreach ($phones as $phone) $phone->delete();
    }
}