<?php

class Webfresh_Storelocator_Model_Storelocator extends Mage_Core_Model_Abstract
{
    public $default_logo='';
    public $default_icon_map='';
    public function _construct()
    {
        parent::_construct();
        $this->_init('storelocator/storelocator');
    }

    public function getNewFileName($destFile)
    {
        $fileInfo = pathinfo($destFile);
        if (file_exists($destFile)) {
            $index = time();
            $baseName = $fileInfo['filename'] . '.' . $fileInfo['extension'];
            while( file_exists($fileInfo['dirname'] . DIRECTORY_SEPARATOR . $baseName) ) {
                $baseName = time(). '.' . $fileInfo['extension'];
                //$fileInfo['filename']. '_' . $index .
                $index ++;
            }
            $destFileName = $baseName;
        } else {
            return $fileInfo['basename'];
        }

        return $destFileName;
    }


    public function getStorelocators($by_city=false)
    {
        if ($by_city)
        {
            $collection = $this->getCollection();
            $collection->addFieldToFilter('status',1);
            $collection->addFieldToFilter('city_id',$by_city);
            $collection->setOrder('title', 'ASC');
        }
        else{
            $collection = $this->getCollection();
            $collection->addFieldToFilter('status',1);
            $collection->setOrder('title', 'ASC');
        }

        return $collection;
    }
}