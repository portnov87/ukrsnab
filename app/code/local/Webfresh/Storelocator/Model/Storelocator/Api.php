<?php

class Webfresh_Storelocator_Model_Storelocator_Api extends Mage_Api_Model_Resource_Abstract
{

    public function create($storelocatorData)
    {
        $storelocator_par = Mage::getModel('storelocator/storelocator')->getCollection()->addFieldToFilter('code',array('eq' => $storelocatorData->code));
        try {
            if (count($storelocator_par)>0)
            {
                foreach ($storelocator_par as $st){
                    $storelocator = Mage::getModel('storelocator/storelocator')->load($st->getId());
                    break;
                }
            }else $storelocator = Mage::getModel('storelocator/storelocator');

            if (isset($storelocatorData->city)&&($storelocatorData->city!='')) {
                $city_col = Mage::getModel('storelocator/city')->getCollection()->addFieldToFilter('city_name',array('eq' => trim($storelocatorData->city)));
                if (count($city_col)>0) {
                    foreach ($city_col as $city_m) {
                        //$city_id = $city_m->getId();//CityId();
                        $city_id = $city_m->getCityId();//.$city_m->getId();
                        break;
                    }
                }else{
                    $city_col = Mage::getModel('storelocator/city');
                    $city_col->setCityName($storelocatorData->city);
                    $city_col->save();
                    $city_id = $city_col->getCityId();//.$city_col->getId();
                }

                $storelocator->setCityId($city_id);
                //Data(array('city_id'=>$city_id));//
            }
            if (isset($storelocatorData->status)) $storelocator->setStatus($storelocatorData->status);
            $storelocator->setCode($storelocatorData->code);
            $storelocator->setTitle($storelocatorData->title);
            $storelocator->setAddress($storelocatorData->address);

            $storelocator->save();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
            // We cannot know all the possible exceptions,
            // so let's try to catch the ones that extend Mage_Core_Exception
        } catch (Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }
        return $storelocator->getId();
    }

    public function info($storelocatorId)
    {
        $storelocator = Mage::getModel('storelocator/storelocator')->load($storelocatorId);
        if (!$storelocator->getId()) {
            $this->_fault('not_exists');
            // If customer not found.
        }
        return $storelocator->toArray();
        // We can use only simple PHP data types in webservices.
    }
    public function items() {
        $arr_products = array();
        $products =Mage::getModel('storelocator/storelocator')->getCollection();

        foreach ($products as $product) {
            $arr_products[] = $product->toArray(array('storelocator_id', 'title','address','phones','latitude','longitude','time_work','title_stop','metro','minibuses'));
        }
        return $arr_products;
    }


    public function update($storelocatorId, $storelocatorData)
    {
        $storelocator = Mage::getModel('storelocator/storelocator')->load($storelocatorId);

        if (!$storelocator->getId()) {
            $this->_fault('not_exists');
            // No customer found
        }

        $storelocator->addData($storelocatorData)->save();
        return true;
    }

    public function delete($storelocatorId)
    {

        $storelocator = Mage::getModel('storelocator/storelocator')->load($storelocatorId);

        if (!$storelocator->getId()) {
            $this->_fault('not_exists');
            // No customer found
        }

        try {
            $storelocator->delete();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('not_deleted', $e->getMessage());
            // Some errors while deleting.
        }

        return true;
    }
}