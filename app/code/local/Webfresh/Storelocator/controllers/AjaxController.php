<?php

class Webfresh_Storelocator_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function cityAction()
    {
        ini_set("display_errors", 1);
        error_reporting(E_ALL);
        $post = Mage::app()->getRequest()->getParams();

        $city_id = $post['city_id'];
        $html = '';

        if ($city_id != '') {
            $pharms = Mage::helper('storelocator')->getPharmas($city_id, 'array');

            $block = 'storelocator/pharmalist';
            $block_pharmalist = Mage::app()->getLayout()
                ->createBlock($block);

            if ($block_pharmalist) {
                $html = $block_pharmalist
                    ->setData('city_id', $city_id)
                    ->toHtml();

            }
            /*
                        $html = $this->getLayout()->createBlock($block)
                            ->setData('city_id', $city_id)
                            ->toHtml();
            */
        }
        $result = array();
        $result['html'] = $html;


        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    public function citymobileAction()
    {
        $post = Mage::app()->getRequest()->getParams();

        $city_id = $post['city_id'];
        $html = '';

        if ($city_id != '') {
            $pharms = Mage::helper('storelocator')->getPharmas($city_id, 'array');
            foreach ($pharms as $storelocator) {
                $html .= '<option data-longitude="' . $storelocator['longitude'] . '" data-latitude="' . $storelocator['latitude'] . '" 
 value="' . $storelocator['id'] . '">' . $storelocator['title'] . '</option>';

            }

        }
        $result = array();
        $result['html'] = $html;


        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    public function storecustomerAction()
    {

        $store_id = $this->getRequest()->getPost("store_id", "");
        $storelocator = Mage::getModel('storelocator/storelocator')->load($store_id);
        //$this->setStoreId($store_id);

        $html = '
        <div class="descr">
                <div class="l-prod">
                    <div class="image">';

        if ($storelocator->getLogo() != '') {
            $img = Mage::helper('storelocator')->getMedia() . $storelocator->getLogo();
            $html .= '<img src="' . $img . '" alt="' . $storelocator->getTitle() . '" />';
        }

        $html .= '</div>
        </div>
        <div class="r-prod">
            <div class="name">' . $storelocator->getTitle() . '</div>
            <div class="address">' . $storelocator->getAddress() . '</span></div>
        </div>
        <div class="clearfix"></div>';

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
            "html" => $html,
        )));
    }

    public function storeAction()
    {

        $store_id = $this->getRequest()->getPost("store_id", "");
        $storelocator = Mage::getModel('storelocator/storelocator')->load($store_id);
        //$this->setStoreId($store_id);

        $html = '
        <div class="descr">
                <div class="l-prod">
                    <div class="image">';

        if ($storelocator->getLogo() != '') {
            $img = Mage::helper('storelocator')->getMedia() . $storelocator->getLogo();
            $html .= '<img src="' . $img . '" alt="' . $storelocator->getTitle() . '" />';
        }

        $html .= '</div>
        </div>
        <div class="r-prod">
            <div class="name">' . $storelocator->getTitle() . '</div>
            <div class="address">' . $storelocator->getAddress() . '</span></div>
        </div>
        <div class="clearfix"></div>';

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
            "html" => $html,
        )));
    }


    public function storemobileAction()
    {

        $store_id = $this->getRequest()->getPost("store_id", "");
        $storelocator = Mage::getModel('storelocator/storelocator')->load($store_id);
        //$this->setStoreId($store_id);

        if ($storelocator) {

            $html = '<div class="pharma clearfix">
                        <div class="l-col">';

            if ($storelocator->getLogo() != '') {
                $img = Mage::helper('storelocator')->getMedia() . $storelocator->getLogo();
                $html .= '<img src="' . $img . '" style="width:55px;" alt="' . $storelocator->getTitle() . '" />';
            }
            $phone = $storelocator->getPhones();
            $html .= '</div>
                        <div class="r-col">
                            <!--<div class="name">' . $storelocator->getTitle() . '</div>-->
                            <div class="name">' . $storelocator->getAddress() . '</div>
                        </div>
                    </div>';
            if ($phone != '') {
                $html .= '<div class="title" > Телефоны аптеки:</div >
                    <ul class="list" > ';

                $html .= ' <li>
                            
                            <img src="' . Mage::getDesign()->getSkinUrl() . 'images/d-icon-phone.png" alt="Икокна" />' . $phone . '
                            
                        </li>';
                $html .= '</ul>';
            }
            $html .= '<div class="table clearfix">';


            if ($storelocator->getTimeWork() != ''):
                $html .= '
                        <div class="single">
                            <div class="text">Время</br>работы:</div>
                            <div class="descr">' . $storelocator->getTimeWork() . '</div>
                        </div>';
            endif;
            if ($storelocator->getTitleStop() != ''):
                $html .= '
                        <div class="single">
                            <div class="text">Название</br>остановки:</div>
                            <div class="descr">' . $storelocator->getTitleStop() . '</div>
                        </div>';
            endif;
            if ($storelocator->getMetro() != ''):
                $html .= '
                        <div class="single">
                            <div class="text">Станция</br>метро:</div>
                            <div class="descr">' . $storelocator->getMetro() . '</div>
                        </div>';
            endif;
            if ($storelocator->getMinibuses() != ''):
                $html .= '
                        <div class="single">
                            <div class="text">Маршрутки:</div>
                            <div class="descr">' . $storelocator->getMinibuses() . '</div>
                        </div>';
            endif;
            $html .= '
            
                    </div>';
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
            "html" => $html,
        )));
    }



    public function storelocatosproductAction()
    {

        /*$block = 'storelocator/pharmalist';
        $block_pharmalist = Mage::app()->getLayout()
            ->createBlock($block);

        if ($block_pharmalist) {
            $html = $block_pharmalist
                ->setData('city_id', $city_id)
                ->toHtml();

        }*/

        /*
        if ($product=Mage::registry('current_product')) {
            //Mage::unregister('current_product');
            //Mage::register('dosage_form',$dosage_value);
        }else{
            $product=Mage::getModel()->load();
        }*/
        $product_id = $this->getRequest()->getPost("product_id", "");
        $city_id = $this->getRequest()->getPost("city_id", "");

        $this->loadLayout(array('default', 'storelocator_ajax_storelocatosproduct'));
        return $this->getLayout()
            ->getBlock('shipping.pickup')
            ->setData('product_id', $product_id)
            ->setData('city_id', $city_id)
            ->toHtml();
    }

    public function pickupmapAction()
    {

        $city_id = $this->getRequest()->getPost("city_id", "");
        $product_id = $this->getRequest()->getPost("product_id", false);
        if ($product_id) {
            $product=Mage::getModel('catalog/product')->load($product_id);

            $this->loadLayout(array('default', 'storelocator_ajax_pickupmap'));
            $html=$this->getLayout()
                ->getBlock('storelocator.pickup_map')
                ->setData('typeblock', 'map')
                ->setData('city_id', $city_id)
                ->setData('product', $product)
                ->toHtml();
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
                "html" => $html,
            )));

        }

    }




}