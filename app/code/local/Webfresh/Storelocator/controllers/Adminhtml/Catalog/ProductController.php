<?php

require_once(Mage::getModuleDir('controllers','Mage_Adminhtml').DS.'Catalog'.DS.'ProductController.php');

class Webfresh_Storelocator_Adminhtml_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController
{
    public function saveAction()
    {

        parent::saveAction(); // TODO: Change the autogenerated stub
    }

    /**
     * Get custom products grid and serializer block
     */
    public function storelocatorAction()
    {

        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.storelocator');
            //->setProductsCustom($this->getRequest()->getPost('products_custom', null));
        $this->renderLayout();
    }

    /**
     * Get custom products grid
     */
    public function storelocatorGridAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.storelocator');
            //->setProductsRelated($this->getRequest()->getPost('products_custom', null));
        $this->renderLayout();
    }

    public function atcGridAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.atc')
            ->setAtcProducts($this->getRequest()->getPost('atc_products', null));
        //->setProductsRelated($this->getRequest()->getPost('products_custom', null));
        $this->renderLayout();
    }

    public function upsellcategoriesAction()
    {

        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.upsellcategories');
        //->setProductsCustom($this->getRequest()->getPost('products_custom', null));
        $this->renderLayout();
    }

    public function atcAction()
    {

        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.atc')
            ->setAtcProducts($this->getRequest()->getPost('atc_products', null));
        //->setProductsCustom($this->getRequest()->getPost('products_custom', null));
        $this->renderLayout();
    }

}