<?php
class Webfresh_Storelocator_Adminhtml_StorelocatorController  extends Mage_Adminhtml_Controller_action
{
    protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('storelocator/items')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Locations Manager'), Mage::helper('adminhtml')->__('Locations Manager'));

        return $this;
    }

    public function indexAction() {
        $grid_block_container = $this->getLayout()->createBlock('storelocator/adminhtml_storelocator_grid_container');//

        $this->loadLayout();
        $this->getLayout()->getBlock('content')->append($grid_block_container);
        $this->renderLayout();
    }

    public function editAction() {
        $id     = $this->getRequest()->getParam('id');
        $model  = Mage::getModel('storelocator/storelocator')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('storelocator_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('storelocator/items');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Locations Manager'), Mage::helper('adminhtml')->__('Location Manager'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('storelocator/adminhtml_storelocator_edit'))
                ->_addLeft($this->getLayout()->createBlock('storelocator/adminhtml_storelocator_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('storelocator')->__('Location does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {
            if (isset($data['new_city']) && $data['new_city']=='1' && $data['new_city_name']!='') {
                try {
                    $city_model = Mage::getModel('storelocator/city');
                    $city_exist = $city_model->getCollection()
                        //->addExpressionFieldToSelect('city_name1','LOWER(city_name)')//, array('city_name'))
                        ->addExpressionFieldToSelect('city_name', 'LOWER({{city_name}})', array('city_name'=>'city_name'))
                        ->addFieldToFilter('city_name',mb_strtolower($data['new_city_name'], 'UTF-8'));

                    if (count($city_exist)) {
                        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('storelocator')->__("City '%s' already exist in list", $data['new_city_name']));
                        Mage::getSingleton('adminhtml/session')->setFormData($data);
                        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                        return;
                    }
                    $city_data = array('city_name'=>$data['new_city_name']);
                    $city_model->setData($city_data);

                    $city_model->setCityCode(Mage::getHelper('storelocator/data')
                        ->translite($data['new_city_name']));

                    $data['city_id'] = $city_model->save()->getId();
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                    Mage::getSingleton('adminhtml/session')->setFormData($data);
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }
            }
            $model = Mage::getModel('storelocator/storelocator');

            if(isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '') {
                $imgName = $_FILES['logo']['name'];
                $imgName = str_replace(' ', '_',$imgName);

                $path = Mage::getBaseDir('media').DS ;
                $uploader = new Varien_File_Uploader('logo');
                $uploader->setAllowedExtensions(array('jpg','JPG','jpeg','gif','GIF','png','PNG'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);

                $destFile = $path.$imgName;
                $imgName  = $model->getNewFileName($destFile);
                $uploader->save($path,$imgName);

                //$uploader->save($path, $_FILES['logo']['name'] );

                $data['logo'] = $imgName;
            }
            elseif(isset($data['logo']['delete']) && $data['logo']['delete']) $data['logo']='';
            else unset($data['logo']);

            if(isset($_FILES['icon_map']['name']) && $_FILES['icon_map']['name'] != '') {
                $imgName = $_FILES['icon_map']['name'];
                $imgName = str_replace(' ', '_',$imgName);

                $path = Mage::getBaseDir('media').DS ;
                $uploader = new Varien_File_Uploader('icon_map');
                $uploader->setAllowedExtensions(array('jpg','JPG','jpeg','gif','GIF','png','PNG'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);

                $destFile = $path.$imgName;
                $imgName  = $model->getNewFileName($destFile);
                $uploader->save($path,$imgName);

               // $uploader->save($path, $_FILES['icon_map']['name'] );

                $data['icon_map'] = $imgName;
                //echo $imgName;die();
            }
            elseif(isset($data['icon_map']['delete']) && $data['icon_map']['delete']) $data['icon_map']='';
            else unset($data['icon_map']);



            $model->setData($data)
                ->setId($this->getRequest()->getParam('id'));
            try {
                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                    $model->setCreatedTime(now())
                        ->setUpdateTime(now());
                } else {
                    $model->setUpdateTime(now());
                }
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('storelocator')->__('Location was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('storelocator')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $model = Mage::getModel('storelocator/storelocator');

                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Location was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

}