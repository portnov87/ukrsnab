<?php
class Webfresh_Storelocator_Adminhtml_CityController  extends Mage_Adminhtml_Controller_action
{
    protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('storelocator/items')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Locations Manager'), Mage::helper('adminhtml')->__('Locations Manager'));

        return $this;
    }

    public function indexAction() {
        $grid_block_container = $this->getLayout()->createBlock('storelocator/adminhtml_city_grid_container');//

        $this->loadLayout();
        $this->getLayout()->getBlock('content')->append($grid_block_container);
        $this->renderLayout();
    }

    public function editAction() {
        $id     = $this->getRequest()->getParam('id');
        $model  = Mage::getModel('storelocator/city')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('city_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('storelocator/items');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('City Manager'), Mage::helper('adminhtml')->__('City Manager'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('storelocator/adminhtml_city_edit'))
                ->_addLeft($this->getLayout()->createBlock('storelocator/adminhtml_city_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('storelocator')->__('City does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {


            $model = Mage::getModel('storelocator/city');

            $model->setData($data)->setId($this->getRequest()->getParam('id'));


            try {
                /*if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                    $model->setCreatedTime(now())
                        ->setUpdateTime(now());
                } else {
                    $model->setUpdateTime(now());
                }*/
                if ($data['city_code']=='')
                    $model->setCityCode(Mage::helper('storelocator/data')->translite($data['city_name']));

                $model->save();
                $phones = $this->getRequest()->getParam('phones');
                $model->getDeletePhones();

                foreach ($phones['value'] as $key=>$si)
                {

                    if ($phones['delete'][$key]!=1) {

                        //if ($si['sinonum'] != '') {
                            if (isset($si['status'])) $status = 1; else $status = 0;
                            $phone=Mage::getModel('storelocator/phone');
                            $phone->setCityId($model->getId());
                            $phone->setType($si['type']);
                            $phone->setPhone($si['phone']);
                            $phone->setStatus($status);
                            $phone->setCode($si['code']);
                            $phone->save();
                            //$query = "INSERT INTO `brand_sinonim` SET `brand_id`='" . $brand_id . "',`search_tehnomir`='" . $search_tehnomir . "', `tehnomir`='" . $si['tehnomir'] . "',`sinonum`='" . $si['sinonum'] . "' ";
                            //$writeConnection->query($query);
                        //}
                    }


                }

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('storelocator')->__('City was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('storelocator')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $model = Mage::getModel('storelocator/city');

                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('City was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

}