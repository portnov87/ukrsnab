<?php

class Webfresh_Storelocator_AutocompleteController extends Mage_Core_Controller_Front_Action
{
    /*public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }*/

    public function cityAction()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $table_cities = 'spr_city';
        $table_regions = 'spr_region';

        $result = [];
        $result['result'] = false;
        $result['html'] = '';

        $post = Mage::app()->getRequest()->getParams();
        if (isset($post['name'])) {
            $city_name = $post['name'];
            $html = '';

            $currentUrl = $post['current_url'];
            if ($city_name != '') {
                $cities = $readConnection->fetchAll("SELECT `$table_cities`.`prefix`,`$table_cities`.`name`,`$table_regions`.`name` as `name_region` FROM `" . $table_cities . "` 
            LEFT JOIN `$table_regions` ON (`" . $table_cities . "`.`region_id`=`$table_regions`.`id`) 
            WHERE  `$table_cities`.`name` LIKE '$city_name%' LIMIT 0,10");
                $result['result'] = true;
                $htmlli = [];
                foreach ($cities as $city) {
                    $code = $city['prefix'];
                    $name = $city['name'];


                    $cityli = '<li><a  data-current-url="' . $currentUrl . '" data-code="' . $code . '" data-name="' . $name . '"  href="javascript:void(0)">' . $name;
                    if ($city['name_region'] != '') $cityli .= ', ' . $city['name_region'];
                    $cityli .= '</li>';
                    $htmlli[] = $cityli;

                }
                if (count($htmlli) == 0) $htmlli[] = '<li>' . Mage::helper('storelocator')->__('City not found') . '</li>';
                $result['html'] = implode('', $htmlli);

            }
        }


        $this->getResponse()->setBody(Zend_Json::encode($result));
    }


    public function cityjsonAction()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $table_cities = 'spr_city';
        $table_regions = 'spr_region';

        $result = [];

        $post = Mage::app()->getRequest()->getParams();
        if (isset($post['search'])) {
            $city_name = $post['search'];

            if ($city_name != '') {
                $cities = $readConnection->fetchAll("SELECT `$table_cities`.`prefix`,`$table_cities`.`id`,`$table_cities`.`name`,`$table_regions`.`name` as `name_region` FROM `" . $table_cities . "` 
            LEFT JOIN `$table_regions` ON (`" . $table_cities . "`.`region_id`=`$table_regions`.`id`) 
            WHERE  `$table_cities`.`name` LIKE '$city_name%' LIMIT 0,10");
                foreach ($cities as $city) {
                    //$code = $city['prefix'];
                    $name = $city['name'];
                    $id = $city['id'];

                    $text=$name;
                    if ($city['name_region'] != '') $text .= ', ' . $city['name_region'];
                    $result[]=['id'=>$id,'text'=>$text,'name'=>$name];//'prefix'=>$code,

                }
            }
        }

///echo Zend_Json::encode($result);
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }


}