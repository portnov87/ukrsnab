<?php
class Webfresh_Storelocator_Block_CurrentCity extends Mage_Core_Block_Template
{

	public function _prepareLayout()
	    {

		return parent::_prepareLayout();
	    }
    
	public function getStorelocator()     
	     { 
	        if (!$this->hasData('storelocator')) {
	            $this->setData('storelocator', Mage::registry('storelocator'));
	        }
	        return $this->getData('storelocator');
	        
	    }
    public function __construct() {


        parent::__construct();
        $this['cache_lifetime'] = null;
    }

    protected function _toHtml()
    {


        $this->setTemplate('webfresh/storelocator/current_city.phtml');
        return parent::_toHtml();
    }
}