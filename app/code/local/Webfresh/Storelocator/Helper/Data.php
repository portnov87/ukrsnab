<?php
if (!@class_exists('SxGeo')) {
    include Mage::getBaseDir() . DS . 'lib' . DS . 'SxGeo' . DS . 'sxgeo.php';
}

class Webfresh_Storelocator_Helper_Data extends Mage_Core_Helper_Abstract
{
    public $default_city_code = 'kiev';
    public $default_city_name = 'Киев';


    /*public function getCitiesForCourier()
    {
        $cities = Mage::getModel('storelocator/city')
            ->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('kyrier', 1)
            ->setOrder('city_name', 'ASC');
        $result = [];

        foreach ($cities as $city) {
            $result[] = [
                'id' => $city->getId(),
                'code' => $city->getCityCode(),
                'name' => $city->getCityName(),
                'free_courier' => $city->getCourierFree(),
                'courier_price' => $city->getCourierPrice(),
                'courier_amount_cart' => $city->getCourierAmountCart()
            ];
        }
        return $result;
    }

    public function getFreeshipping()
    {
        $city = $this->getCurrentCity();
        if (isset($city['free_shipping'])) {
            return $city['free_shipping'];
        }
        return $this->free_default;
    }*/

    public function getDefaultCity()
    {
        $city_name = $this->default_city_name;// Mage::helper('storelocator')->__('Default city');
        return ['name' => $city_name, 'code' => $this->default_city_code];

    }

    public function translite($value)
    {


        $converter = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        );
        $value = strtr($value, $converter);
        // в нижний регистр
        $str = strtolower($value);
        // заменям все ненужное нам на "-"
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        // удаляем начальные и конечные '-'
        $str = trim($str, "-");
        return $str;

    }

    public function getMedia()
    {
        return '/media/';
    }

    public function getCurrentCity()
    {
        $city_code = Mage::getSingleton('core/session')->getCity();
        $city_name = Mage::getSingleton('core/session')->getCityName();

        if ($city_code) {
                return [ 'name' => $city_name, 'code' => $city_code];
        }
        return $this->getDefaultCity();
    }

    public function setCurrentCity($city = [])
    {
        $name = $code = '';
        if (isset($city['name'])) {
            if ($city['name'] != '')
                $name = $city['name'];
        }
        if (isset($city['code'])) {
            if ($city['code'] != '')
                $code = $city['code'];
        }
        if (($code != '') && ($name != '')) {

            Mage::getSingleton('core/session')->setCityName($name);
            Mage::getSingleton('core/session')->setCity($code);
        }
    }


    public function setupcity()
    {
        $citykod = Mage::getSingleton('core/session')->getCity();
        // echo $citykod. 'cvv ' .Mage::getSingleton('core/session')->getCityName();
        // die();

        $city_location = $this->getGeoLocation();

        if (!$citykod) {
            $city_location = $this->getGeoLocation();
            if ($city_location) {

                if ($city_location) {
                    Mage::getSingleton('core/session')->setCityName($city_location['name']);
                    Mage::getSingleton('core/session')->setCity($city_location['key']);
                    return true;
                }
            }

            $city = $this->getDefaultCity();
            Mage::getSingleton('core/session')->setCityName($city['name']);
            Mage::getSingleton('core/session')->setCity($city['code']);
            return true;
        }
    }

    function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CF_CONNECTING_IP'))
            $ipaddress = getenv('HTTP_CF_CONNECTING_IP');
        elseif (getenv('HTTP_X_REAL_IP'))
            $ipaddress = getenv('HTTP_X_REAL_IP');
        elseif (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        elseif (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        elseif (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = '';

        return $ipaddress;
    }

    public function getGeoLocation()
    {
        $SxGeo = new SxGeo(Mage::getBaseDir() . DS . 'lib' . DS . 'SxGeo' . DS . 'SxGeoCity.dat');

        $ip = $this->get_client_ip();

        $city_find = $SxGeo->getCityFull($ip);

        if ($city_find) {


            if (isset($city_find['country']))
            {
                if ($city_find['country']['iso']!='UA'){
                    $result['name'] = Mage::helper('storelocator')->__('Default city');
                    $result['key'] = $this->default_city_code;
                    return $result;
                }
            }
            if (isset($city_find['city'])) {
                $result['name'] = $city_find['city']['name_ru'];
                $result['key'] = strtolower($city_find['city']['name_en']);
                return $result;
            }

        }
        return false;

    }
    /**
     * Retrieve current category model object
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCurrentCategory()
    {
        if ($category=Mage::registry('current_category'))
            return $category;

        return false;

    }
    public function getPhonemain()
    {
        $mainphone=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/phone_main');
        $mainphone='+38 (067) 402-40-46';//+38 (093) 170-69-91';//+38 (093) 552-28-29';//'+38 (067) 530-36-75';//+380675303675';
        if ($category=$this->getCurrentCategory())
        {

            //$categories=[176,8,44,125];//3,78,45];
            $categories=[7,43];
            $parentIdsCategory=$category->getParentIds();
            $categoryCurrentId=$category->getId();

            $mainphone='+38 (067) 402-40-46';//'+38 (093) 170-69-91';//'+38 (093) 552-28-29';//+38 (067) 530-36-75';//+380675303675';
            foreach ($categories as $catId)
            {
                if (in_array($catId,$parentIdsCategory)||($catId==$categoryCurrentId)){
                    $mainphone='+38 (093) 544-20-20';//+38 (067) 402-40-46';//'+38 (093) 170-69-91';//'+38 (093) 552-28-29';//'+38 (093) 552-28-29';
                    //$mainphone=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/phone_main');
                    break;
                }else{
                    //$mainphone='+380935522829';

                    $mainphone='+38 (067) 402-40-46';//'+38 (093) 170-69-91';//'+38 (093) 552-28-29';//'+38 (067) 530-36-75';
                    //$mainphone=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/phone_main');
                }
            }

        }




        return $mainphone;
    }

    public function getPhonerent()
    {

        $phone_arenda=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/phone_arenda');
        if ($phone_arenda=='')
            $phone_arenda=$this->getPhonemain();

        return $phone_arenda;

    }
    public function getPhones()
    {
        $phone_kievstar=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/phone_kievstar');
        $phone_life=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/phone_life');
        $phone_vodafon=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/phone_vodafon');
        return [
            'phone_kievstar'=>$phone_kievstar,
            'phone_life'=>$phone_life,
            'phone_vodafon'=>$phone_vodafon
        ];
    }



}
