<?php

class Webfresh_Ukrsnab_Model_Store extends Mage_Core_Model_Store
{


    public function formatPrice($price, $includeContainer = true)
    {
        if ($this->getCurrentCurrency()) {
            $priceReturn = $this->getCurrentCurrency()->format($price, array(), $includeContainer);

//Not the cleanest method but the fastest for now…

                return $this->getCurrentCurrency()->format($price, array('precision' => 0), $includeContainer);

        }

        return $price;
    }

}