<?php

class Webfresh_Ukrsnab_Model_Observer
{

    public function collectionProductByCategoryAfter(Varien_Event_Observer $observer)
    {
        $collection=$observer->getCollection();

        $select=$collection->getSelect();


    }
    public function collectionProductByCategory(Varien_Event_Observer $observer)
    {
        /*$collection=$observer->getCollection();
        $categoryId=$observer->getCategoryId();

        $select=$collection->getSelect();

        $showproductsparentcat = Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Category::ENTITY, 'show_products_parentcat');
        $showProdParentAttributeId = $showproductsparentcat->getId();

        $resource = Mage::getSingleton('core/resource');

        $readConnection = $resource->getConnection('core_read');
         $_query = "SELECT cce.entity_id FROM catalog_category_entity_int ccei
RIGHT JOIN catalog_category_entity cce ON (ccei.entity_id=cce.entity_id and ccei.attribute_id=$showProdParentAttributeId) 
WHERE cce.parent_id='$categoryId' AND (ccei.value=0)
GROUP BY cce.entity_id";
// OR ccei.value IS NULL)
         $catsForShow=[];

        $results_attr_gr = $readConnection->fetchAll($_query);
        foreach ($results_attr_gr as $entity)
            $catsForShow[]=$entity['entity_id'];



        if (count($catsForShow)>0) {
            $conditions = array(
                'cat_prod_.product_id=e.entity_id',
                'cat_prod_.category_id NOT IN (' . implode(',', $catsForShow) . ')'
            );
            $joinCond = join(' AND ', $conditions);
            $collection->getSelect()->join(
                array('cat_prod_' => 'catalog_category_product'),
                $joinCond
            );
        }
*/

    }

    public function raiting(Varien_Event_Observer $observer)
    {
        /*$currentUrl = Mage::helper('core/url')->getCurrentUrl();
        $url = Mage::getSingleton('core/url')->parseUrl($currentUrl);
        $path = $url->getPath();

        $raiting=Mage::getModel('ukrsnab/raiting')
            ->loadByUrl($path);
//            ->addFieldToFilter('url', $path)
//            ->getFirstItem();
            //->addFilterByUrl($path);


        if ($raiting->getId())
        {
//            echo '<pre>$raiting';
//            print_r($raiting);
//            echo '</pre>';
        }else
        {
            $raitingNew=Mage::getModel('ukrsnab/raiting');
            $raitingNew->setUrl($path);
            $raitingNew->setRaiting(0);
            $raitingNew->setVotes(0);

            $raitingNew->save();
            //echo 'rat id'.$raitingNew->getEntityId();


        }

*/


    }

    public function fullBreadcrumbCategoryPath(Varien_Event_Observer $observer)
    {
        $current_product = Mage::registry('current_product');

        if ($current_product) {
            $categories = $current_product->getCategoryCollection()->addAttributeToSelect('name');//->setPageSize(1);
            $i = 0;
            $d = count($categories) - 1;
            foreach ($categories as $category) {

                if ($i == $d) {
                    Mage::unregister('current_category');
                    Mage::register('current_category', $category);
                }
                $i++;
            }
        }
    }


    public function xsanatise($var)
    {
        $var = strip_tags($var);
        $var = htmlspecialchars($var, ENT_XML1, 'UTF-8');
        $var = htmlspecialchars($var, ENT_QUOTES, 'UTF-8');
        $var = preg_replace('/[\x00-\x1f]/', '', $var);
        $var = str_ireplace(array('<', '>', '&', '\'', '"'), array('&lt;', '&gt;', '&amp;', '&apos;', '&quot;'), $var);
        $var = str_replace('&nbsp;', ' ', $var);
        $var = str_replace('&ndash;', ' ', $var);

        return $var;
    }

    public function xsanatisegoogle($var)
    {

        $var = htmlspecialchars($var, ENT_XML1, 'UTF-8');
        $var = str_replace('&amp;gt;', '>', $var);
        $var = str_replace('&amp;gt,', '>', $var);
        $var = str_replace('&gt,', '>', $var);
        $var = str_replace('&gt;', '>', $var);

        $var = str_replace('&amp;', '&', $var);


        return $var;
    }


    public function xcheck($var)
    {

        $var = trim($var);

        if (strlen($var) >= 1 && preg_match('/[A-Z]+[a-z]+[0-9]+/', $var) !== false) {
            return true;
        } else {
            return false;
        }

    }


    public function xcheckgoogle($var)
    {

        $var = trim($var);

        if (strlen($var) >= 1 && preg_match('/^[1-9][0-9]*$/', $var) !== false) {
            return true;
        } else {
            return false;
        }

    }

    public function generatepromofeed()
    {

        $siteName = 'Ukrsnab - интернет-гипермаркет строительной и промышленной техники';
        $siteUrl = 'https://ukrsnab.com.ua';
        $companyName = 'Ukrsnab.com.ua';

        $yml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        $yml .= "<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n";
        $yml .= "<yml_catalog date=\"" . date('Y-m-d H:i') . "\">\n";
        $yml .= "	<shop>\n";
        $yml .= "		<name>$siteName</name>\n";
        $yml .= "		<company>$companyName</company>\n";
        $yml .= "		<url>$siteUrl</url>\n";

        $yml .= "		<currencies>\n";
        $yml .= "			<currency id=\"UAH\" rate=\"1\"/>\n";
        $yml .= "			<currency id=\"USD\" rate=\"CBRF\"/>\n";
        $yml .= "			<currency id=\"EUR\" rate=\"CBRF\"/>\n";
        $yml .= "		</currencies>\n";


        $categories_include = Mage::getModel('catalog/category')
            //->addAttributeToSelect('*')
            ->getCollection()
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToFilter('promo_feed', 1);


        //}
        $yml .= "<categories>\n";

        foreach ($categories_include as $cat) {
            $category = Mage::getModel('catalog/category')->load($cat->getId());
            $yml .= '<category id="' . $cat->getId() . '" parentId="' . $category->getParentId() . '">' . $category->getName() . '</category>' . "\n";
            $include_category[] = $cat->getId();
        }


        $yml .= "		</categories>\n";
        $yml .= "		<offers>\n";


        $collection = Mage::getModel('catalog/product')->getCollection()
            ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
            //->addAttributeToFilter('category_id', array('in' => $include_category))
            ->addAttributeToFilter('include_feed_promo', 1)
            ->addAttributeToSelect('*')
            ->addUrlRewrite()
            ->addAttributeToFilter('visibility', array('neq' => 1))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));


        $collection->getSelect()->columns('entity_id')
            ->group('entity_id');


        Mage::getSingleton('cataloginventory/stock')
            ->addInStockFilterToCollection($collection);




        $resource = Mage::getSingleton('core/resource');

        /**
         * Установка соединения для чтения
         */
        $readConnection = $resource->getConnection('core_read');

        $count_products = $no_sklad_count_products = 0;
        $cats_show = [];
        foreach ($collection as $product) {


            try {


                $productId = $product->getId();
                $product = Mage::getModel('catalog/product')->load($productId);
                $stockItem = $product->getStockItem();
                $_image = $product->getImageUrl();

            }  catch (Exception $e) {
                continue;
            }
                if ($stockItem->getIsInStock() == 1) {
                    $quantity_in_stock = 10;
                    $available = 'true';
                } else {
                    $available = 'false';
                    $quantity_in_stock = 0;
                }

                $yml .= "			<offer id=\"" . $product->getId() . "\" available=\"" . $available . "\" >\n";//bid=\"$bid\"

                $price = $product->getPrice();


                $price = $product->getPrice();
                $special_price = false;
                if (empty($product->getSpecialPrice())) {
                    $price = Mage::helper('core')->currency($product->getFinalPrice(), false, false);

                } else {
                    $price = Mage::helper('core')->currency($product->getFinalPrice(), false, false);
                    $price = $special_price = Mage::helper('core')->currency($product->getSpecialPrice(), false, false);
                }


                $yml .= "<price>" . $price . "</price>\n";//$v['price']

                /*            if (empty($product->getSpecialPrice())) {
                                $price=$product->getFinalPrice();
                                $yml .= "				<price>" . $price . "</price>\n";//$v['price']

                            } else {
                                $price=$product->getPrice();
                                $yml .= "				<price>" . $price . "</price>\n";//$v['price']

                            }
                */


                $price = round($price, 2);
                $paramCurrency = 'UAH';

                //$yml .= "				<price>" . $price . "</price>\n";//$v['price']
                $yml .= "				<currencyId>" . $paramCurrency . "</currencyId>\n";


                $cats = $product->getCategoryIds();

                $show = true;
                foreach ($cats as $category_id) {

                    $categoryIdMagento = $category_id;

                }

                $name = $product->getName();
                $_description = $product->getDescription();
                $manufacturerName = $product->getAttributeText('proizvoditel');


                $yml .= "				<categoryId>" . $categoryIdMagento . "</categoryId>\n";


                //$yml .= "<description><![CDATA[" . $description . "]]></description>\n";
                $yml .= "				<picture>" . $_image . "</picture>\n";
                $yml .= "				<quantity_in_stock>" . $quantity_in_stock . "</quantity_in_stock>\n";


                $entityTypeId = Mage::getResourceModel('catalog/product')->getTypeId();
                $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')
                    ->setEntityTypeFilter($entityTypeId);

                $attributes = $product->getAttributes();
                $attributeSetId = $product->getAttributeSetId();


                $skus[] = $product->getSku();

                foreach ($attributes as $attribute) {


                    $data_attr = $attribute->getData();

                    $attr_group = $data_attr['attribute_set_info'];
                    $attribute_group_id = $attribute->getAttributeGroupId();
                    foreach ($attr_group as $att) {
                        $attribute_group_id = $att['group_id'];
                    }

                    if ($attribute_group_id != '') {
                        $_query = "SELECT * FROM eav_attribute_group WHERE attribute_group_name='Особые характеристики'
AND attribute_group_id='" . $attribute_group_id . "' LIMIT 1";

                        $results_attr_gr = $readConnection->fetchAll($_query);
                        if (count($results_attr_gr) > 0) {
                            $attributeLabel = $attribute->getFrontendLabel();
                            $value = $attribute->getFrontend()->getValue($product);
                            if (($attributeLabel != '') && ($value != 'No') && ($value != '')) {//($attributeLabel!='Гарантия')&&

                                $attributeLabel=str_replace('"',"'",$attributeLabel);
                                $value=str_replace('"',"'",$value);
                                $yml .= "<param name=\"$attributeLabel\" unit=\"\"><![CDATA[" . $value . "]]></param>\n";
                                //die();
                            }
                            //}
                        }
                    }
                }


                //if ($params != '') $yml .= $params;

                $yml .= "<vendor><![CDATA[" . $manufacturerName . "]]></vendor>\n";

                $_description = $_description;
                //$yml .= "				<delivery>" . $delivery . "</delivery> \n";
                $yml .= "				<name><![CDATA[" . $name . "]]></name>\n";
                $yml .= "				<description><![CDATA[" . $_description . "]]></description>\n";
                $yml .= "			</offer>\n";


        }

        $yml .= "</offers>\n";
        $yml .= "</shop>\n";
        $yml .= "</yml_catalog>\n";

        $file = 'promo-feed.xml';
        $handle = fopen('/var/www/ukrsnab.com.ua/htdocs/' . $file, "w+");

        fwrite($handle, header('Content-Type: text/xml; charset=utf8'));
        fwrite($handle, $yml);
        fclose($handle);

        return $this;

    }


    public function generategooglefeed()
    {


        //date_default_timezone_set("Europe/Kiev");
        //header('Content-Type: text/xml; charset=utf-8', true); //set document header content type to be XML

        $title = 'Ukrsnab - интернет-гипермаркет строительной и промышленной техники';
        $link = 'https://ukrsnab.com.ua';

        $shippingcountry = 'UA';
        $shippingcountrylong = ' UAH';
        $shippingservice = 'Standard';
        $shippingprice = '5.95' . $shippingcountrylong;


        $rss = new SimpleXMLElementExtended('<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0"></rss>');

        $channel = $rss->addChild('channel'); //add item node

        $channel->addChild('title', $title); //title of the feed
        $channel->addChild('link', $link); //title of the feed
        $channel->addChild('description', 'Ukrsnab.com.ua - первый интернет-магазин, где представлен весь ассортимент профессионального инструмента, строительного оборудования, техники для сада, огорода, энергоснабжения. Тел: +38(067)230-60-90');

//        Mage::app()->getCacheInstance()->flush();
//
//
//        Mage::app('admin')->setUseSessionInUrl(false);
//        Mage::app('default');
        $root_category = 3;
        //Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);


        $include_category = [];

        $categories_include = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToFilter('include_google_feed', 1);

        foreach ($categories_include as $cat) {
            $include_category[] = $cat->getId();
        }


        $collection = Mage::getModel('catalog/product')->getCollection()
            ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
            //->addAttributeToFilter('category_id', array('in' => $include_category))
            ->addAttributeToFilter('include_feed_google', 1)
            ->addAttributeToSelect('*')
            ->addUrlRewrite()
            ->addAttributeToFilter('visibility', array('neq' => 1))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));


        $collection->getSelect()->columns('entity_id')
            ->group('entity_id');


        Mage::getSingleton('cataloginventory/stock')
            ->addInStockFilterToCollection($collection);


        $resource = Mage::getSingleton('core/resource');

        /**
         * Установка соединения для чтения
         */
        $readConnection = $resource->getConnection('core_read');

        $count_products = $no_sklad_count_products = 0;
        $cats_show = [];
        foreach ($collection as $product) {


            $upcmpn = 0;

            $productId = $product->getId();
            $product = Mage::getModel('catalog/product')->load($productId);
            $stockItem = $product->getStockItem();

            $cats = $product->getCategoryIds();

            $google_product_category = false;

            $show = true;
            foreach ($cats as $category_id) {

                switch ($category_id) {
                    case '45': //Затирочные машины
                    case '75':
                    case '14':
                    case '15':
                    case '16':
                    case '76':
                        $google_product_category = '4132';//219';

                        break;
                    case '23': //Выбираторы для бетона
                    case '56':
                    case '57':
                    case '58':
                    case '59':
                    case '60':
                    case '61':
                    case '396':
                    case '17':
                    case '18':
                    case '19':
                    case '20':
                    case '107':
                    case '24':
                    case '25':
                    case '26':
                    case '27':
                    case '88':
                    case '31':
                    case '32':
                    case '33':
                    case '34':
                        $google_product_category = '2456';
                        //$item->addChild('xmlns:g:google_product_category', '2456');
                        break;
                    default:
                        $google_product_category = '1167';
                        //$item->addChild('xmlns:g:google_product_category', '1167');//xsanatise(
                        break;

                }

//        Вибраторы для бетона, виброплиты, вибротрамбовки - 2456
//Затирочные машины - 4132


            }


            if ($show && ($stockItem->getIsInStock() == 1)) {
                $stock = 'in stock';


                $product_type = $this->getPath($product);
                $_ex_product_type = '';
                if (count($product_type) > 0) {
                    $_ex_product_type = 'Главная > ' . implode(' > ', $product_type);


                    $item = $channel->addChild('item'); //add item node

                    if ($google_product_category)
                        $item->addChild('xmlns:g:google_product_category', $google_product_category);

                    $item_group_id = $item->addChild('xmlns:g:id', $this->xsanatise($product->getSku()));


                    $item->addChild('xmlns:g:title', $this->xsanatise($product->getName())); //add title node under item


                    $urllink = '';

                    $urllink = '';
                    $id = $product->getId();
                    $query = "SELECT * FROM core_url_rewrite WHERE product_id='" . $id . "' AND category_id IS NULL";
                    $results = $readConnection->fetchAll($query);
                    $urllink = $product->getProductUrl();
                    foreach ($results as $res) {
                        $urllink = 'https://ukrsnab.com.ua/' . $res['request_path'];
                    }


                    $link = $item->addChild('xmlns:g:link', $urllink); //add link node under item


                    if ($this->xcheck($product->getProizvoditel()) !== false) {
                        $brand = $product->getAttributeText('proizvoditel');
                        $item->addChild('xmlns:g:brand', $this->xsanatise($brand));//$product->getProizvoditel()
                    } else {
                        //    $item->addChild('xmlns:g:brand')->addChild('xmlns:g:identifier_exists','false');
                    }

                    if ($this->xcheck($product->getSku()) !== false) {
                        $item->addChild('xmlns:g:mpn', $this->xsanatise($product->getSku()));
                    } else {

                    }

                    $new = $item->addChild('xmlns:g:condition', 'new');

                    $item->addChildWithCDATA('xmlns:g:description', strip_tags($product->getDescription()));


                    $item->addChildWithCDATA('xmlns:g:product_type', strip_tags($_ex_product_type));

                    if ($product->getImage() == 'no_selection') {

                    } else {

                    }
                    $image_link = $item->addChild('xmlns:g:image_link', $product->getImageUrl());


                    if (($product->getSku() == '') && ($product->getProizvoditel())) {
                        $item->addChild('xmlns:g:identifier_exists', ' no');
                    }

                    if (empty($product->getSpecialPrice())) {
                        $price = Mage::helper('core')->currency($product->getFinalPrice(), true, false);
                        $price = str_replace('грн.', 'UAH', $price);
                        $item->addChild('xmlns:g:price', $price);// . $shippingcountrylong);
                    } else {
                        $price = Mage::helper('core')->currency($product->getPrice(), true, false);
                        $price = str_replace('грн.', 'UAH', $price);
                        $item->addChild('xmlns:g:price', $price);// . $shippingcountrylong);
                        $special_price = Mage::helper('core')->currency($product->getSpecialPrice(), true, false);
                        $special_price = str_replace('грн.', 'UAH', $special_price);
                        $item->addChild('xmlns:g:sale_price', $special_price);//. $shippingcountrylong );
                    }

                    $item->addChild('xmlns:g:availability', $stock);

                    $count_products++;
                }
            } else {
                $stock = 'out of stock';
                $no_sklad_count_products++;
            }

//
//            @ob_flush();
//            flush();
        }

        $content_feed = $rss->asXML();
        $content_feed = html_entity_decode($content_feed, ENT_NOQUOTES, 'UTF-8');

        $file = '/var/www/ukrsnab.com.ua/htdocs/google-feed.xml';


        $handle = fopen($file, "w+");

        fwrite($handle, header('Content-Type: text/xml; charset=utf8'));
        fwrite($handle, $content_feed);
        fclose($handle);

    }


    protected function getPath($product)
    {
        $path = array();
        $excludes = [26, 671];


        if ($product) {
            //$categoriesid = $this->getProduct()->getCategoryIds();
            $categoriesid = [];
            //$level=
            foreach ($product->getCategoryCollection() as $cat) {
                $categoriesid[] = $cat->getId();
            }
            if (count($categoriesid) > 0) {
                $category_id = $categoriesid[count($categoriesid) - 1];
                $category = Mage::getModel('catalog/category')->load($category_id);
            }


        }
        /*} else {
            $category = $this->getCategory();
        }*/


        if ($category) {
            if (in_array($category->getId(), $excludes)) return [];

            $pathInStore = $category->getPathInStore();
            $pathIds = array_reverse(explode(',', $pathInStore));

            $categories = $category->getParentCategories();

            // add category path breadcrumb
            $c = count($pathIds);// - 1;
            $i = 1;
            if ($product) {
                $current_product = $product->getId();
            } else $current_product = false;

            foreach ($pathIds as $categoryId) {
                if (isset($categories[$categoryId])) {


                    if (in_array($categoryId, $excludes)) return [];

                    if ($categories[$categoryId]->getName() != '') {
                        $path['category' . $categoryId] = $categories[$categoryId]->getName();
                    }
                    /*array(
                        'label' => $categories[$categoryId]->getName(),
                        'category_id' => $categoryId,
                        'type' => 'category',
                        //'lastcategory' => $lastcategory,
                        //'show_other_category' => $show_other_category,
                        'current_product_id' => $current_product,
                        'link' => $this->_isCategoryLink($categoryId) ? $categories[$categoryId]->getUrl() : ''
                    );*/
                    $i++;


                    // }
                }
            }

            if ($category->getName() != '') {
                $path['category' . $category->getId()] = $category->getName();

            }
        }


        return $path;

    }


    public function generatefeed()
    {


        $skus = [];
        $siteUrl = 'https://ukrsnab.com.ua/';
        $siteName = $companyName = 'УкрСнаб';


        $yml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        $yml .= "<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n";
        $yml .= "<yml_catalog date=\"" . date('Y-m-d H:i') . "\">\n";
        $yml .= "	<shop>\n";
        $yml .= "		<name>$siteName</name>\n";
        $yml .= "		<company>$companyName</company>\n";
        $yml .= "		<url>$siteUrl</url>\n";


        $yml .= "		<currencies>\n";
        $yml .= "			<currency id=\"UAH\" rate=\"1\"/>\n";
        //$yml .= "			<currency id=\"USD\" rate=\"CBRF\"/>\n";
        //$yml .= "			<currency id=\"EUR\" rate=\"CBRF\"/>\n";
        $yml .= "		</currencies>\n";


        $collection_product = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('include_in_feed_rozetka', array('eq' => 1));
        $resource = Mage::getSingleton('core/resource');

        /**
         * Установка соединения для чтения
         */
        $readConnection = $resource->getConnection('core_read');


        if (count($collection_product)) {


            $cats_new = [];
            foreach ($collection_product as $pr) {
                //if ($pr->getData('include_in_feed_rozetka') == '1') {
                $cats = $pr->getCategoryIds();

                /*$collection_cats = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addAttributeToSelect('*');*/
                //$cats
                foreach ($cats as $c) {
                    $cat_model = Mage::getModel('catalog/category')->load($c);
                    if ($cat_model) {
                        if ($cat_model->getIsActive() == 1) {
                            $cats_new[$cat_model->getId()] = $cat_model->getName();
                        }
                    }
                    //$cats_new[$c->getId()]=$c->getName();

                }
                //}
            }

            if (count($cats_new) > 0) {
                $yml .= "		<categories>\n";
                foreach ($cats_new as $key => $cat_name) {
                    $yml .= '<category id="' . $key . '">' . $cat_name . '</category>' . "\n";
                }
                $yml .= "		</categories>\n";
            }
        }
        $yml .= "		<offers>\n";


        foreach ($collection_product as $product) {

            $product_id = $product->getId();

            $product = Mage::getModel('catalog/product')->load($product_id);
            $stockItem = $product->getStockItem();

            if ($stockItem->getIsInStock())
                $available = 'true';
            else
                $available = 'false';


            $old_magento_id = $product->getOldMagentoId();
            echo '$old_magento_id = ' . $old_magento_id . "\r\n";
            if ($old_magento_id != '')
                $id_roz = $old_magento_id;
            else $id_roz = $product_id;

            $yml .= '		<offer id="' . $id_roz . '" available="' . $available . '">';
            $yml .= "\n";
            $name_product = $product->getName();
            $code = $product->getSku();

            $price = $product->getPrice();
            $special_price = false;
            if (empty($product->getSpecialPrice())) {
                $price = Mage::helper('core')->currency($product->getFinalPrice(), false, false);

            } else {
                $price = Mage::helper('core')->currency($product->getFinalPrice(), false, false);
                $special_price = Mage::helper('core')->currency($product->getSpecialPrice(), false, false);
            }

            $query = "SELECT * FROM core_url_rewrite WHERE product_id='" . $product_id . "' AND category_id IS NULL";
            $results = $readConnection->fetchAll($query);
            $urllink = $product->getProductUrl();
            foreach ($results as $res) {
                $urllink = 'https://ukrsnab.com.ua/' . $res['request_path'];
            }

            $description = $product->getDescription();
            $paramCurrency = 'UAH';
            $name_product = htmlentities($name_product);


            $yml .= "<name>" . $name_product . "</name>\n";
            $special_date = strtotime($product->getSpecialToDate());
            $date = time();
            if ($special_price && ($special_date >= $date)) {
                $yml .= "<price_old>" . $price . "</price_old>\n";
                $yml .= "<price>" . $special_price . "</price>\n";
            } else
                $yml .= "<price>" . $price . "</price>\n";

            $yml .= "<description><![CDATA[" . $description . "]]></description>\n";
            $yml .= "<url>" . $urllink . "</url>\n";
            $yml .= "<currencyId>" . $paramCurrency . "</currencyId>\n";

            $cats = $product->getCategoryIds();


            $_level = 1;
            $cat_finish = false;
            foreach ($cats as $c) {
                $cat_model = Mage::getModel('catalog/category')->load($c);
                $cat = $cat_model->getData();
                if ($cat['level'] > $_level) {
                    $_level = $cat['level'];
                    $cat_finish = $cat_model;
                }

            }
            if ($cat_finish) {
                $yml .= "<categoryId>" . $cat_finish->getId() . "</categoryId>\n";
            }


            if ($available == 'true')
                $yml .= "<stock_quantity>100</stock_quantity>\n";
            else
                $yml .= "<stock_quantity>0</stock_quantity>\n";


            //$product_modal=Mage::getModel('catalog/product')->load($product->getId());

            $product->load('media_gallery');
            //if ($product_modal) {

            if (count($product->getMediaGalleryImages()) > 0) {

                //  $yml .= "<product_images>\n";
                foreach ($product->getMediaGalleryImages() as $_image) {

                    $file_img = $_image->getFile();//$_image->getData('file');
                    $img = $siteUrl . 'media/catalog/product';
                    $img .= $file_img;


                    $_img = Mage::helper('catalog/image')->init($product, 'thumbnail', $_image->getFile());//$file_img);


                    $mv_w_big = 600;

                    $_img_big = (string)Mage::helper('catalog/image')->init($product, 'image', $file_img)->resize($mv_w_big, 500);//, $mv_h_big);

                    $yml .= "<picture>" . $img . "</picture>\n";
                    //echo  $_img."\r\n".$_img_big;

                    //die();
                }
                // $yml .= "</product_images>\n";
            }

            $vendorname = false;
            if ($product->getProizvoditel() !== false) {
                $vendorname = $product->getAttributeText('proizvoditel');// $product->getProizvoditel();
            }

            if ($vendorname)
                $yml .= "<vendor>" . $vendorname . "</vendor>\n";

            $entityTypeId = Mage::getResourceModel('catalog/product')->getTypeId();
            $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')
                ->setEntityTypeFilter($entityTypeId);

            $attributes = $product->getAttributes();
            $attributeSetId = $product->getAttributeSetId();


            $skus[] = $product->getSku();

            foreach ($attributes as $attribute) {


                $data_attr = $attribute->getData();

                $attr_group = $data_attr['attribute_set_info'];
                $attribute_group_id = $attribute->getAttributeGroupId();
                foreach ($attr_group as $att) {
                    $attribute_group_id = $att['group_id'];
                }

                if ($attribute_group_id != '') {
                    $_query = "SELECT * FROM eav_attribute_group WHERE attribute_group_name='Особые характеристики'
AND attribute_group_id='" . $attribute_group_id . "' LIMIT 1";
                    //echo $_query . "\r\n\r\n";
                    //die();
                    $results_attr_gr = $readConnection->fetchAll($_query);
                    if (count($results_attr_gr) > 0) {
                        /*echo  $product->getName() . ' ' . $product->getId().'<pre>$results_attr_gr '.$attribute->getAttributeSetId().' '.$attributeSetId;
                        print_r($results_attr_gr);
                        echo '</pre>';*/
                        //if ($attribute->getAttributeSetId() == $attributeSetId) {
                        $attributeLabel = $attribute->getFrontendLabel();
                        $value = $attribute->getFrontend()->getValue($product);
                        // echo $product->getName() . ' ' . $product->getId().' '.$attributeLabel.' value='.$value."\r\n";
                        //echo '$attributeLabel'.$attributeLabel.' '.$value."\r\n";
                        if (($attributeLabel != '') && ($value != 'No') && ($value != '')) {//($attributeLabel!='Гарантия')&&
                            //echo $attributeLabel . '-' . $value;
                            //echo "<br />";//' . $label . '-
                            $yml .= "<param name=\"$attributeLabel\">" . $value . "</param>\n";
                            //die();
                        }
                        //}
                    }
                }
            }


            $yml .= "<param name=\"Доставка/Оплата\">Доставка: Самовывоз из магазина, Самовывоз из Новой Почты 2-5 дней, Самовывоз из отделений Интайм 3- 6 дней, Курьер по вашему адресу г.Киев 1-2 дня. Оплата: Предоплата, Наличными, Visa/MasterCard, Безналичным с НДС</param>\n";


            $yml .= "		</offer>\n";
            //}
        }

        $yml .= "		</offers>\n";
        $yml .= "	</shop>\n";
        $yml .= "</yml_catalog>\n";
        $file = 'feed_products.xml';
        $handle = fopen('/var/www/ukrsnab.com.ua/htdocs/' . $file, "w+");

        fwrite($handle, header('Content-Type: text/xml; charset=utf8'));
        fwrite($handle, $yml);
        fclose($handle);

        return $this;
    }
}

Class SimpleXMLElementExtended extends SimpleXMLElement
{

    /**
     * Adds a child with $value inside CDATA
     * @param unknown $name
     * @param unknown $value
     */
    public function addChildWithCDATA($name, $value = NULL)
    {
        $new_child = $this->addChild($name);

        if ($new_child !== NULL) {
            $node = dom_import_simplexml($new_child);
            $no = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($value));
        }

        return $new_child;
    }
}
