<?php
class Webfresh_Ukrsnab_Model_Spinnerdb extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        $this->_init('ukrsnab/spinnerdb');
    }

    public function loadByCategoryId($categoryId)
    {
        $page = $this->getCollection()
            ->addFilterByCategoryId($categoryId)
            ->getFirstItem();
        return $page;
    }

    public function loadByUrl($url)
    {
        $page = $this->getCollection()
            ->addFilterByUrl($url)
            ->getFirstItem();

        return $page;
    }

}