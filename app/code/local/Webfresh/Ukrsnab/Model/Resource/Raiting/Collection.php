<?php

class Webfresh_Ukrsnab_Model_Resource_Raiting_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract //Mage_Core_Model_Resource_Db_Collection_Abstract
{

    /**
     * Constructor method
     */
    protected function _construct()
    {
        //parent::_construct();
        $this->_init('ukrsnab/raiting');
    }

    public function addFilterByUrl($value)
    {
        $this->getSelect()->where('main_table.url = ?', $value);

        return $this;
    }
}