<?php
/* @var $this Mage_Eav_Model_Entity_Setup */

// Add an extra column to the catalog_eav_attribute-table:



$installer = new Mage_Eav_Model_Entity_Setup('core_setup');//$this;//new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();



$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'promo_feed', array(
    'group'         => 'General Information',
    'input'         => 'select',
    'type'          => 'int',
    'source_model'=>'eav/entity_attribute_source_boolean',
    'label'         => 'Выводить в промua',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));


$attributeSets = Mage::getResourceModel('eav/entity_attribute_set_collection')
    ->setEntityTypeFilter('4'); // Catalog Product Entity Type ID

$yourAttributeCode='include_feed_promo';
foreach ($attributeSets as $attributeSet) {
    $installer->addAttributeToSet(
        Mage_Catalog_Model_Product::ENTITY,   // Entity type
        $attributeSet->getAttributeSetName(), // Attribute set name
        'General',                            // Attribute set group name
        $yourAttributeCode,
        100                                   // Position on the attribute set group
    );
}



$installer->endSetup();