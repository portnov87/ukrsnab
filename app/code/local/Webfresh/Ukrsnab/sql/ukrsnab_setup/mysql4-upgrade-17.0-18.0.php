<?php
/* @var $this Mage_Eav_Model_Entity_Setup */

// Add an extra column to the catalog_eav_attribute-table:



$installer = new Mage_Eav_Model_Entity_Setup('core_setup');//$this;//new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();



$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'show_products_parentcat', array(
    'group'         => 'General Information',
    'input'         => 'select',
    'type'          => 'int',
    'label'         => 'Выводить товары в родительской категории',
    'backend'       => '',
    'source_model'  => 'eav/entity_attribute_source_boolean',
    'visible'       => true,
    'required'      => false,
    'is_user_defined'=>1,
    'default_value'=>1,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));



$installer->endSetup();