<?php
/* @var $this Mage_Eav_Model_Entity_Setup */

// Add an extra column to the catalog_eav_attribute-table:
/*$this->getConnection()->addColumn(
    $this->getTable('catalog/eav_attribute'),
    'tooltip',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'comment'   => 'Tooltip'
    )
);
*/
/*$installer->startSetup();
$installer->getConnection()->addColumn(
    $this->getTable('catalog/eav_attribute'),
    'show_link',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => true,
        'comment'   => 'Show link'
    )
);


$installer->endSetup();
die();*/


$installer = $this;//new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'show_home', array(
    'group'         => 'General Information',
    'input'         => 'select',
    'type'          => 'int',
    'label'         => 'Выводить на главной',
    'backend'       => '',
    'source_model'=>'eav/entity_attribute_source_boolean',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'show_subcategory_home', array(
    'group'         => 'General Information',
    'input'         => 'select',
    'type'          => 'int',
    'source_model'=>'eav/entity_attribute_source_boolean',
    'label'         => 'Выводить на главной (подкатегория)',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'option'     => [
        'values' => [
            0 => 'Да',
            1 => 'Нет',

        ]
    ],
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));
$installer->endSetup();