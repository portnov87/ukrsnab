<?php
/* @var $this Mage_Eav_Model_Entity_Setup */

// Add an extra column to the catalog_eav_attribute-table:
/*$this->getConnection()->addColumn(
    $this->getTable('catalog/eav_attribute'),
    'tooltip',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'comment'   => 'Tooltip'
    )
);
*/
/*$installer->startSetup();
$installer->getConnection()->addColumn(
    $this->getTable('catalog/eav_attribute'),
    'show_link',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => true,
        'comment'   => 'Show link'
    )
);


$installer->endSetup();
die();*/

//
//$installer = $this;
//$installer->startSetup();
//echo $this->getTable('ukrsnab/raiting');
//die();
/*
$installer->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('ukrsnab/raiting')} (
    `entity_id`  int(11) NOT NULL auto_increment,
    `url`        varchar(255) NOT NULL,
    `raiting`    decimal(10,2) NOT NULL,
    `allraiting`      int(11) NOT NULL,
    `votes`      int(11) NOT NULL,        
    PRIMARY KEY  (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();
*/


$installer = new Mage_Eav_Model_Entity_Setup('core_setup');//$this;//new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_title_1', array(
    'group'         => 'FAQ',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'FAQ Заголовок 1',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_content_1', array(
    'group'         => 'FAQ',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'FAQ Текст 1',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_title_2', array(
    'group'         => 'FAQ',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'FAQ Заголовок 2',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_content_2', array(
    'group'         => 'FAQ',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'FAQ Текст 2',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));


$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_title_3', array(
    'group'         => 'FAQ',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'FAQ Заголовок 3',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_content_3', array(
    'group'         => 'FAQ',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'FAQ Текст 3',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));


$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_title_4', array(
    'group'         => 'FAQ',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'FAQ Заголовок 4',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_content_4', array(
    'group'         => 'FAQ',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'FAQ Текст 4',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));


$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_title_5', array(
    'group'         => 'FAQ',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'FAQ Заголовок 5',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_content_5', array(
    'group'         => 'FAQ',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'FAQ Текст 5',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$installer->endSetup();