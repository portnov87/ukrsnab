<?php
/* @var $this Mage_Eav_Model_Entity_Setup */

// Add an extra column to the catalog_eav_attribute-table:



$installer = new Mage_Eav_Model_Entity_Setup('core_setup');//$this;//new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_h2', array(
    'group'         => 'FAQ',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'FAQ h2',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));
$installer->endSetup();