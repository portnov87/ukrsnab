<?php
/* @var $this Mage_Eav_Model_Entity_Setup */

// Add an extra column to the catalog_eav_attribute-table:



$installer = new Mage_Eav_Model_Entity_Setup('core_setup');//$this;//new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();


$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_title_6', array(
    'group'         => 'FAQ',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'FAQ Заголовок 6',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_content_6', array(
    'group'         => 'FAQ',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'FAQ Текст 6',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_title_7', array(
    'group'         => 'FAQ',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'FAQ Заголовок 7',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_content_7', array(
    'group'         => 'FAQ',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'FAQ Текст 6',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));


$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_title_8', array(
    'group'         => 'FAQ',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'FAQ Заголовок 8',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_content_8', array(
    'group'         => 'FAQ',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'FAQ Текст 8',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));


$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_title_9', array(
    'group'         => 'FAQ',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'FAQ Заголовок 9',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_content_9', array(
    'group'         => 'FAQ',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'FAQ Текст 9',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));


$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_title_10', array(
    'group'         => 'FAQ',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'FAQ Заголовок 10',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'faq_content_10', array(
    'group'         => 'FAQ',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'FAQ Текст 10',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$installer->endSetup();