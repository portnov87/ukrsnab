<?php
/* @var $this Mage_Eav_Model_Entity_Setup */

// Add an extra column to the catalog_eav_attribute-table:



$installer = $this;
$installer->startSetup();


$installer->run("
CREATE TABLE IF NOT EXISTS `webfresh_ukrsnab_spinner` (
    `entity_id`  int(11) NOT NULL auto_increment,
    `url`        varchar(255) NOT NULL,    
    `category_id`      int(11) NOT NULL,
    `text`      TEXT NOT NULL,        
    PRIMARY KEY  (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();