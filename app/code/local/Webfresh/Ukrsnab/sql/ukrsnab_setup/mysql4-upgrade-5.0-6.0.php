<?php
/* @var $this Mage_Eav_Model_Entity_Setup */

// Add an extra column to the catalog_eav_attribute-table:
/*$this->getConnection()->addColumn(
    $this->getTable('catalog/eav_attribute'),
    'tooltip',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'comment'   => 'Tooltip'
    )
);
*/
/*$installer->startSetup();
$installer->getConnection()->addColumn(
    $this->getTable('catalog/eav_attribute'),
    'show_link',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => true,
        'comment'   => 'Show link'
    )
);


$installer->endSetup();
die();*/


$installer = $this;
$installer->startSetup();
//echo $this->getTable('ukrsnab/raiting');
//die();

$installer->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('ukrsnab/raiting')} (
    `entity_id`  int(11) NOT NULL auto_increment,
    `url`        varchar(255) NOT NULL,
    `raiting`    decimal(10,2) NOT NULL,
    `allraiting`      int(11) NOT NULL,
    `votes`      int(11) NOT NULL,        
    PRIMARY KEY  (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
//`created_at` datetime NOT NULL default '0000-00-00 00:00:00',
$installer->endSetup();

//
//$installer = $this;//new Mage_Eav_Model_Entity_Setup('core_setup');
//$installer->startSetup();
//
//$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'raiting', array(
//    'group'         => 'General Information',
//    'input'         => 'text',
//    'type'          => 'varchar',
//    'label'         => 'Рейтинг категории',
//    'backend'       => '',
//    'visible'       => true,
//    'required'      => false,
//    'visible_on_front' => true,
//    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
//));
//
//$installer->endSetup();