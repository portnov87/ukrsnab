<?php
class Webfresh_Ukrsnab_Helper_Data extends Mage_Core_Helper_Data
{


    public function getSkinUrl()
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN) . 'frontend/ukrsnab/default/';
    }

    public function getWorkinghours()
    {

        $working_hours=Mage::getStoreConfig('setting_ukrsnab/setting_ukrsnab_common/working_hours');
        return $working_hours;
    }

    public function formatStreet($addressStreet)
    {
        $street = '';

        if ($addressStreet)
        {
            $street = 'ул. ' . array_shift($addressStreet);
            if ($addressStreet)
            {
                $street .= ', д. ' . array_shift($addressStreet);
                if ($addressStreet)
                {
                    $street .= ', кв. ' . array_shift($addressStreet);
                }
            }
        }

        return $street;
    }
    public function getSocials()
    {
        $socials=Mage::getStoreConfig('setting_ukrsnab/setting_ukrsnab_socials');
        $return =array();
        foreach ($socials as $key =>$value)
        {
            if ($value!='') $return[$key]=$value;

        }
        /*echo '<pre>$socials';
        print_r($socials);
        echo '</pre>';*/
        return $return;
        //$textleshenie = Mage::getStoreConfig('setting_paniapteka/setting_other/textleshenie', '');

        //echo $textleshenie;
    }


    public function getChildrenCategories($parent_id=false)
    {
        $children=[];
        if (!$parent_id)
        {
            if ($category=Mage::registry('current_category')) {
                $parent_id =$category->getId();
            }
        }
        if ($parent_id) {
            //$root=Mage::getModel()->load($parent_id);

            //$menu = Mage::getModel('catalog/category')->load($parent_id);//->getCollection();



            $children=Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToSelect(['name','entity_id','svg_icon'])
                ->addAttributeToFilter('parent_id', $parent_id);


            //$children=$menu->getChildrenCategories();//getCategories($parent);
        }
        return $children;


    }
    public function getFeatures($_product=false, $category=false)
    {
        $category=false;
        $array_result=[];

        if ($category)
        {

            $tops = $category->getFeatureAttributes();

            $array_tops = explode("\n", $tops);

            foreach ($array_tops as $t)
            {
                $array_result[trim($t)]=trim($t);
            }
        }
        elseif ($_product){
            $categories=$_product->getCategoryIds();

            foreach ($categories as $category_id)
            {
                $category=Mage::getModel('catalog/category')->load($category_id);
                if ($category) {
                    $tops = $category->getFeatureAttributes();
                    $array_tops = explode("\n", $tops);

                    foreach ($array_tops as $t)
                    {
                        $array_result[trim($t)]=trim($t);
                    }
                }
            }

        }
        elseif ($_category=Mage::registry('current_category'))
        {
                $tops = $_category->getFeatureAttributes();
                $array_tops = explode("\n", $tops);

                foreach ($array_tops as $t)
                {
                    $array_result[trim($t)]=trim($t);
                }

        }
        return $array_result;

    }



    public function isMobile()
    {
        $mobile_detect = new Mobile_Detect();
        return $mobile_detect->isMobile();
    }

    public function isTablet()
    {
        $mobile_detect = new Mobile_Detect();

        return $mobile_detect->isTablet();

    }
}