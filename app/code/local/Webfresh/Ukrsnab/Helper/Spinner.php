<?php

class Webfresh_Ukrsnab_Helper_Spinner extends Mage_Core_Helper_Data
{


    public static function spin(string $text): string
    {
        if (!preg_match('/\{(.*)\}/si', $text)) {
            return self::removeExtraSpaces($text);
        } else {
            preg_match_all('/\{([^{}]*)\}/si', $text, $matches);
            $occur = count($matches[1]);
            for ($i = 0; $i < $occur; ++$i) {
                $words = explode('|', $matches[1][$i]);
                shuffle($words);
                $text = self::replaceOnce($matches[0][$i], $words[0], $text);
            }

            return self::spin($text);
        }
    }

    protected static function replaceOnce(string $search, string $replace, string $subject): string
    {
        $firstChar = strpos($subject, $search);
        if (false !== $firstChar) {
            $beforeStr = substr($subject, 0, $firstChar);
            $afterStr = substr($subject, $firstChar + strlen($search));

            return $beforeStr . $replace . $afterStr;
        } else {
            return $subject;
        }
    }

    protected static function removeExtraSpaces(string $text): string
    {
        return preg_replace('/\s+/', ' ', $text);
    }

    public function generate($categoryId)
    {
        $_helper = Mage::helper('catalog/output');//$this->helper('catalog/output');

        $category = Mage::getModel('catalog/category')->load($categoryId);
        $Category_name = $category->getName();
//        echo $category->getId().' '.$Category_name;
//        die();

        $urlCategory = $category->getUrl();


        $visibility = array(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH, Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH);

        $categoriesChildren = $category->getChildrenCategories();
//        echo '<pre>';
//        print_r($categoriesChildren);
//        echo '</pre>';
//        die();
        $categories = $category->getAllChildren(true);

//        $collection = Mage::getResourceModel('catalog/product_collection');
//        $collection->addCategoriesFilter($categories);
//
//        echo count($collection);
//        die();

        $products = Mage::getResourceModel('catalog/product_collection');
        $allCatIds = explode(",", $category->getAllChildren());
//        echo '<pre>';
//        print_r($allCatIds);
//        echo '</pre>';
        $products
            ->getSelect()
            ->group('entity_id');
        $products
            ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id=entity_id', null, 'left')
            ->addAttributeToFilter('category_id', array('in' => $allCatIds));

        //echo $collection->count()."<br/><br/>";


//        $productCollection = Mage::getResourceModel('catalog/product_collection');
//
//        $productCollection->addCountToCategories($categoriesChildren);


        //$products = Mage::getModel('catalog/category')->load($categoryId)->getProductCollection()->setPages();
//        $products = Mage::getSingleton('catalog/category')->load($categoryId)
//            ->getProductCollection()
//            ->addAttributeToSelect('*');

//        echo count($products)."<br/>";
//        echo $products->getSelect()."<br/>";
//        echo $Category_name.' '.$category->getId()."<br/>";
       // die();


        $manufacturers = [];

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $attributeModel = Mage::getModel('eav/entity_attribute');
        $id_attribute = (int)$attributeModel->loadByCode('catalog_product', 'proizvoditel')->getId();
        $col = 0;
        $listProducts = [];
        $listProductsName = [];
        $filters = [];


        $tops = $category->getFeatureAttributes();
        $array_tops = explode("\n", $tops);

        foreach ($array_tops as $t) {
            $features[trim($t)] = trim($t);
        }
        $category_url=$urlCategory;
        $category_url=str_replace('index.php/','',$category_url);
        foreach ($products as $_product) {

            //echo $_product->getEntityId()."\r\n\r\n";
            $productObject = Mage::getModel('catalog/product')->load($_product->getEntityId());
            if ($productObject->getStatus()==1) {
                $attributes = $productObject->getAttributes();

                foreach ($attributes as $attribute) {
                    $value_id = false;

                    if (in_array(trim($attribute->getAttributeCode()), $features)) {


                        $value = $attribute->getFrontend()->getValue($productObject);

                        if (!$productObject->hasData($attribute->getAttributeCode())) {
                            //$value = Mage::helper('catalog')->__('N/A');
                        } elseif ((string)$value == '') {
                            $value = Mage::helper('catalog')->__('No');
                        } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                            $value = Mage::app()->getStore()->convertPrice($value, true);
                        }

                        if (is_string($value) && strlen($value) && ($value != 'Нет')) {


                            $attrUrlKeyModel = Mage::getResourceModel('catalin_seo/attribute_urlkey');

                            $attr_val = $attrUrlKeyModel->getUrlValue($attribute->getId(), $value_id);//$option['value']);
                            $url_key = $attrUrlKeyModel->getUrlKey($attribute->getAttributeCode());//, $value_id);//$option['value']);

                            $filter_url = $category_url . '/filter/' . $url_key . '/' . $attr_val . '/';

                            if (!isset($filters[$attribute->getStoreLabel()]))
                                $filters[$attribute->getStoreLabel()] = [];

                            if (!in_array($value, $filters[$attribute->getStoreLabel()]))
                                $filters[$attribute->getStoreLabel()][] = $value;

                        }
                    }
                }


                $col++;

                if ($col < 8) {
                    $listProducts[$col] = $productObject;
                    if ($col < 4)
                        $listProductsName[] = $productObject->getName();
                }
                $proizvoditel_id = $productObject->getProizvoditel();

                $id_option = $proizvoditel_id;

                $select = $readConnection->select()
                    ->from('eav_attribute_option_value')
                    ->join(
                        array('eav_attribute_option' => 'eav_attribute_option'),
                        'eav_attribute_option.option_id = eav_attribute_option_value.option_id',
                        array('eav_attribute_option.*')
                    )
                    ->join(
                        array('catalin_seo_attribute' => 'catalin_seo_attribute_url_key'),
                        'eav_attribute_option.option_id = catalin_seo_attribute.option_id',// and eav_attribute_option.attribute_id=:attribute_id',
                        array('catalin_seo_attribute.url_key', 'catalin_seo_attribute.url_value')
                    )
                    ->where('eav_attribute_option.attribute_id=:attribute_id AND eav_attribute_option_value.option_id=:option_id')
                    ->group('eav_attribute_option_value.option_id');

                $bind = array(
                    'attribute_id' => $id_attribute,
                    'option_id' => $id_option
                );

                $results = $readConnection->fetchAll($select, $bind);
                $urlBrand = '';
                $results_array = [];

                foreach ($results as $res) {
                    $urlBrand = $category_url . '/filter/' . $res['url_key'] . '/' . $res['url_value'];// . '/';

                    break;
                }


                if ($urlBrand == '') {
                    $urlBrand = $category_url . '?proizvoditel=' . $proizvoditel_id;
                }


                $manufacturers[$urlBrand] = $productObject->getAttributeText('proizvoditel');
            }
        }

//        echo '<pre>$manufacturers'.count($products);
//        print_r($manufacturers);
//        echo '</pre>';
//        die();
        $Category_product_count = count($products);

        $highpriceProduct = Mage::getModel('catalog/category')->load($categoryId)->getProductCollection()
            ->addAttributeToSelect('final_price')
            ->addAttributeToFilter('visibility', $visibility)
            ->setOrder('price', 'DESC')->getFirstItem();
//        echo $highpriceProduct->getFinalPrice();
//        die();
        $minpriceProduct = Mage::getModel('catalog/category')->load($categoryId)->getProductCollection()
            ->addAttributeToSelect('final_price')
            ->addAttributeToFilter('visibility', $visibility)
            ->setOrder('price', 'ASC')->getFirstItem();

        $storeId = 2;
        $currencyrate = Mage::app()->getStore($storeId)->getCurrentCurrencyRate();

        $Category_lowest_price = round($currencyrate * $minpriceProduct->getFinalPrice(), 2) . ' грн';
        $Category_highest_price = round($currencyrate * $highpriceProduct->getFinalPrice(), 2) . ' грн';


//echo $Category_lowest_price.' '.$Category_highest_price;
//die();


//        $products = Mage::getResourceModel('reports/product_collection')
//            ->addAttributeToSelect('price')->addCategoryFilter($category)
//            ->setOrder('price', 'ASC')->setPage(1, 1);
//        $highprice=999;


        //$Category_product_count

        $listBrands = [];
        $Category_brand = [];

        $template = '<h2>{Захотели купить|Решили купить|Хотите купить|Желаете приобрести|Выбираете|Вам необходимы} ' . mb_strtolower($Category_name) . '
 {по лучшей цене|за оптовую цену|выгодно|по самой низкой цене|недорого|по оптовой цене|за лучшую цену} 
 {в Киеве|в Украине|от официального импортера в Киеве|от официального поставщика в Киеве|от официального импортера в Украине|от официального поставщика в Украине}
  {|с гарантией|с гарантией и сервисным обслуживанием|с доставкой в течение одного дня|с самым большим сроком гранатийного обслуживания}?</h2>
<p>' . $Category_name . ' в {интернет-магазине|магазине|интернет-гипермаркете|онлайн-гипермаркете} {Укрснаб|УКРСНАБ|"УКРСНАБ"|"УкрСнаб"|УкрСнаб|"Укрснаб"} 
{представлены|презентованы|продаются} {в самом широком ассортименте|в большом перечне|в большом каталоге} 
{как для профессиональных, так и для личных нужд|для профессиональных и индивидуальных потребностей|для профессионального и личного использования|,подходящие для личного и профессионального использования|,оптимальные для любителей или профессионалов|,подходящие для промышленного и домашнего использования}. 
{Вы можете выбрать|Вы сможете подобрать|Вы сможете купить} ' . mb_strtolower($Category_name) . ' из ' . $Category_product_count . ' {единиц|вариантов|видов} 
{представленных|в наличии|доступных} {на нашем сайте|в нашем магазине|в интернет-магазине|в магазине|в интернет-гипермаркете|в онлайн-гипермаркете}. 
' . $Category_name . ' {можно купить|доступны|доступны к покупке|возможно преобрести} по 
{низкой цене|низким ценам|оптовым ценам|оптовой цене|розничной цене|цене в розницу|выгодной цене} 
от ' . $Category_lowest_price . ' в {Киеве|Киеве и области|Киеве и Киевской области|Киеве и Украине|Киеве и доставкой по Украине|Киеве, области и доставкой по всей Украине}. </p>';
        if (count($manufacturers) > 0) {
            $template .= '
<p>{На нашем сайте|В нашем магазине|В интернет-магазине|В магазине|В интернет-гипермаркете|В онлайн-гипермаркете} {Укрснаб|УКРСНАБ|"УКРСНАБ"|"УкрСнаб"|УкрСнаб|"Укрснаб"} 
{доступны|доступны к покупке|в налиции|можно купить|возможно купить|Вы можете выбрать|доступны к выбору|официально реализовуются|официально продаются} ' . mb_strtolower($Category_name) . '
 {следующих производителей|таких производителей|производителей}:</p>
<h2>' . $Category_name . ' - доступные {бренды|производители}</h2>
<ul>';

            foreach ($manufacturers as $_brandUrl => $_brandName):
                if ($_brandName!='') {
                    $template .= '<li><a href="' . $_brandUrl . '">' . $_brandName . '</a></li>';

                    $listBrands[] = $_brandName;
                }
            endforeach;
            $template .= '            
</ul>';
        }
        $template .= ' 
<p>Все {популярные|востребованные|покупаемые} в {Киеве|Киеве и области|Киеве и Киевской области|Киеве и Украине} 
' . $Category_name . ' {имеют отличия|отличаются|разнятся|делятся} по {следующим характеристикам|характеристикам|видам}:</p>';
        foreach ($filters as $name => $_filter):
            $template .= '
<h3 class="label_attribute">' . $name . '</h3>
<ul>';
            foreach ($_filter as $f):
                $template .= '<li>' . $f . '</li>';
            endforeach;
            $template .= '</ul>
';
        endforeach;
        $template .= ' 
<p>{На нашем сайте|В нашем магазине|В интернет-магазине|В магазине|В интернет-гипермаркете|В онлайн-гипермаркете} 
{Укрснаб|УКРСНАБ|"УКРСНАБ"|"УкрСнаб"|УкрСнаб|"Укрснаб"} Вы {можете ознакомиться|ознакомитесь} 
с {подробными характеристиками|характеристиками|описанием|детальным описанием} 
{товаров|продукции|выбранных товаров|выбранной продукции}, {а также|и} {найдете|посмотрите} 
{фото|фотографии|изображения} {товаров|продукции} в {отличном качестве|высоком качестве|высоком разрешении}.
 На {страницах|карточках} {товаров|продукции} {можно прочитать|Вы найдете|доступны|доступны к прочтению|размещаются} 
 отзывы {покупателей|клиентов|наших покупателей|наших клиентов} {на что нужно обратить внимание|на что стоит 
 обратить внимание|что стоит учесть} {выбирая|покупая} ' . mb_strtolower($Category_name) . ' в {Киеве|Киеве и области|Киеве и Киевской области|Киеве и Украине|Украине} 
 {как для профессиональных, так и для личных нужд|для профессиональных и индивидуальных потребностей|для профессионального и личного использования}.</p>
<h2>Цены на ' . mb_strtolower($Category_name) . ' в {Киеве|Киеве и области|Киеве и Киевской области|Киеве и Украине|Украине}</h2>

';

        $template .= ' <table class="listproducts_spinner_category">';
        $template .= '
<tr class="headspinner_category">
<td>Товар</td>
<td>Цена</td>
</tr>
';
        $index = 0;
        foreach ($listProducts as $_product):
            $index++;
            $template .= ' <tr> 
	<td>' . $_product->getName() . '</td>
	<td>' .round($currencyrate * $_product->getFinalPrice(), 2) . ' грн</td>
</tr>';
        endforeach;
        $template .= '

	</table>
<div itemscope="" itemtype="https://schema.org/FAQPage" style="margin-top:0px;padding-top:0px;" class="wrapper wrapper-page--info category-description">
<h2>{Воспросы|Часто задаваемые вопросы|Что спрашивают|Что больше всего интересует} про ' . mb_strtolower($Category_name) . ' ?</h2>
<div class="faq-block__item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
<h3 class="faq-block__question" itemprop="name">{По какой стоимости|По какой минимальной стоимости|За какую цену|По какой цене} 
{можно купить|можно приобрести|доступны|продаются} ' . mb_strtolower($Category_name) . '?<i class="icon icon-chevron-down"></i></h3>
<div itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
<div class="faq-block__answer" itemprop="text">
' . $Category_name . ' {можно купить|можно приобрести|доступны|продаются} в {Киеве|Киеве и области|Киеве и Киевской области|Киеве и Украине|Украине} 
{по цене|в цене|по ценам} от ' . $Category_lowest_price . ' до ' . $Category_highest_price . '.
</div>
</div>
</div>
<div class="faq-block__item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
<h3 class="faq-block__question" itemprop="name">{Как быстро Вы доставите|Как скоро можно получить|Быстро ли покупать|Когда Вы сможете доставить} ' . mb_strtolower($Category_name) . '
?<i class="icon icon-chevron-down"></i></h3>
<div itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
<div class="faq-block__answer" itemprop="text">
{Мы отправим|Мы передадим в службу доставки|Курьерская служба начнет обрабатывать заказ на|Вы можете забрать} ' . mb_strtolower($Category_name) . '
 {в день заказа|сразу после заказа|сразу после оплаты|в день оплаты|в день покупки} в {интернет-магазине|магазине|интернет-гипермаркете|онлайн-гипермаркете} {Укрснаб|УКРСНАБ|"УКРСНАБ"|"УкрСнаб"|УкрСнаб|"Укрснаб"}.
</div>
</div>
</div>
<div class="faq-block__item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
<h3 class="faq-block__question" itemprop="name">' . $Category_name . ' каких брендов {можно купить|представлены|можно приобрести|доступны|продаются} 
в Вашем {интернет-магазине|магазине|интернет-гипермаркете|онлайн-гипермаркете}? <i class="icon icon-chevron-down"></i></h3>
<div itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
<div class="faq-block__answer" itemprop="text">
' . implode(', ', $listBrands) . '
</div>
</div>
</div>
<div class="faq-block__item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
<h3 class="faq-block__question" itemprop="name">Какие ' . mb_strtolower($Category_name) . ' {чаще всего покупают|самые востребованные|наиболее универсальные}? <i class="icon icon-chevron-down"></i></h3>
<div itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
<div class="faq-block__answer" itemprop="text">
' . implode(', ', $listProductsName) . '
</div>
</div>
</div>
</div>


';
        $text = $this->spin($template);

        return $text;
    }
}
