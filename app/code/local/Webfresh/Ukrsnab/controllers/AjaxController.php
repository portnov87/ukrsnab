<?php

class Webfresh_Ukrsnab_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function productscategoryAction()
    {
        $category_id= $this->getRequest()->getParam("category_id", false);

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
            "content" => $this->_getContentproducts($category_id),
        )));

    }
    public function _getContentproducts($category_id)
    {
        //$this->loadLayout(array('default', 'ukrsnab_ajax_productscategory'));
        $layout=Mage::app()->getLayout();
        $products = $layout->createBlock('ukrsnab/homecategories_product','home_category_product',['template'=>'ukrsnab/home_catalog_products.phtml']);
        if ($products)
        {
            $products->setData('category_id', $category_id);
            return $products->toHtml();
        }
        return false;
    }




    public function voteratingAction()
    {
        $value= $this->getRequest()->getParam("value", false);
        $url= $this->getRequest()->getParam("url", false);
        if ($url&&$value)
        {
//            $currentUrl = Mage::helper('core/url')->getCurrentUrl();
//            $url = Mage::getSingleton('core/url')->parseUrl($currentUrl);
//            $path = $url->getPath();

            $raiting=Mage::getModel('ukrsnab/raiting')
                ->loadByUrl($url);


            if ($raiting->getId())
            {

                $allraiting=$raiting->getAllraiting();
                $allraiting=$allraiting+$value;
                $votes=$raiting->getVotes() + 1;
                $raitingValue = round($allraiting / $votes,2);
//                $raiting=$raiting->getRaiting()+1;
//                $raiting=Mage::getModel('ukrsnab/raiting');
//                $raiting->setUrl($path);
                $raiting->setAllraiting($allraiting);

                $raiting->setRaiting($raitingValue);
                $raiting->setVotes($votes);

                $raiting->save();
            }else
            {
                $raitingValue=$value;
                $votes=1;
                $allraiting=$raitingValue / $votes;
                $raiting=Mage::getModel('ukrsnab/raiting');
                $raiting->setUrl($url);
                $raiting->setAllraiting($allraiting);
                $raiting->setRaiting($raitingValue);
                $raiting->setVotes($votes);
                $raiting->save();
            }

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
                "raiting" => $raitingValue,
                'votes'=>$votes
            )));
        }



    }

}