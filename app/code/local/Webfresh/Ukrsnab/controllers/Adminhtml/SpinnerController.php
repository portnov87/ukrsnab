<?php
class Webfresh_Ukrsnab_Adminhtml_SpinnerController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('spinner');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('Spinner text Manager'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('ukrsnab/adminhtml_spinner'));
        $this->renderLayout();
    }

    public function generateallAction()
    {
        try {
            $categories=Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToSelect('*')//['name','entity_id','svg_icon'])//name,url, id')
                ->addAttributeToFilter('parent_id', ['gt'=>1])
                ->addAttributeToFilter('is_active', 1);
            $catcount=0;
            foreach ($categories as $cat) {
                if ($cat->getDescription()=='') {

                    $category_id=$cat->getId();
                    $spin=Mage::getModel('ukrsnab/spinnerdb')->loadByCategoryId($category_id);
                    $text = Mage::helper('ukrsnab/spinner')->generate($category_id);
                    if (empty($spin->getId())) {
                        $urCategory = $cat->getUrl();
                        $urCategory=str_replace('index.php/','',$urCategory);
                        $spinnerDb = Mage::getModel('ukrsnab/spinnerdb');
                        $spinnerDb->setData(
                            [
                                'url' => $urCategory,
                                'text' => $text,
                                'category_id' => $category_id
                            ]
                        );
                        //echo '$urCategory='.$urCategory.' '.$category_id;

                        $spinnerDb->save();
                        //die();

                    }else{
                        //$spinnerDb = Mage::getModel('ukrsnab/spinnerdb')->load($spin->getId());
                       // echo $category_id.' '.$spin->getId();
                        //$spinnerDb->setText($text);
                        //$spinnerDb->save();

                        /*Data(
                            ['text'=>$text]
                        );//Text($text);//Data(['text'=>$text]);
                        $spin->save();
                        die();
                        */
                        //echo $text.' '.$spinnerDb->getId();
                    }
                    $catcount++;
                }
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Spinner text was successfully generate for '.$catcount.' categories'));
            $this->_redirect('*/*/');
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/index');
    }
    public function generateAction()
    {
        try {
            //$category_id=$this->getRequest()->getParam('id');
            $id=$this->getRequest()->getParam('id');
            $spin=Mage::getModel('ukrsnab/spinnerdb')->load($id);
            if ($spin) {
                $category_id=$spin->getData('category_id');
                $text = Mage::helper('ukrsnab/spinner')->generate($category_id);

                //$spin = Mage::getModel('ukrsnab/spinnerdb')->loadByCategoryId($category_id);

                $category = Mage::getModel('catalog/category')->load($category_id);
                $urCategory = $category->getUrl();


//                if (empty($spin->getId())) {
//                    $spinnerDb = Mage::getModel('ukrsnab/spinnerdb');
//                    $spinnerDb->setData(
//                        [
//                            'url' => $urCategory,
//                            'text' => $text,
//                            'category_id' => $category_id
//                        ]
//                    );
//
//                    $spinnerDb->save();
//
//                } else {
                    //$spinnerDb = Mage::getModel('ukrsnab/spinnerdb')->load($spin->getId());
                $spin->setText($text);//Data(['text'=>$text]);
                $spin->save();
//                    echo $text.' '.$spin->getId();
//                    die();
//                    $spinnerDb->setData(['text' => $text]);
//                    $spinnerDb->save();
               // }
            }

            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Spinner text was successfully generate'));
            $this->_redirect('*/*/');
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        $this->_redirect('*/*/index');
    }




    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('ukrsnab/spinnerdb');

                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Spinner text was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/index', ['id' => $this->getRequest()->getParam('id')]);
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $IDList = $this->getRequest()->getParam('ids');
        if (!is_array($IDList)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select rewrite(s)'));
        } else {
            try {
                foreach ($IDList as $itemId) {
                    $_model = Mage::getModel('ukrsnab/spinnerdb')
                        ->setIsMassDelete(true)->load($itemId);
                    $_model->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($IDList)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

}
