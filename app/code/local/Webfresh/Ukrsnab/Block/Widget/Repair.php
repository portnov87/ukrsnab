<?php

class Webfresh_Ukrsnab_Block_Widget_Repair
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{

    /* protected $_image='';
     protected $product_id='';*/
    public function _prepareLayout()
    {

        //return 'eeeee;';
        $this->setTemplate('ukrsnab/repair/list.phtml');

        return parent::_prepareLayout();
    }


    public function getCategory()
    {
        if ($category=Mage::registry('current_category')) return $category;
        return false;
    }

    public function getList()
    {
        $products=[];
        if ($category=Mage::registry('current_category')) {
            $products=Mage::registry('current_category')->getProductCollection()->addAttributeToSelect('*');
        }

        return $products;

    }
    protected function _toHtml()
    {
        //$this->_image = $this->getImage();

        return parent::_toHtml();
    }
}


