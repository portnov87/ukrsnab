<?php

class Webfresh_Ukrsnab_Block_Widget_Contacts
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{

    /* protected $_image='';
     protected $product_id='';*/
    public function _prepareLayout()
    {

        //return 'eeeee;';
        $this->setTemplate('ukrsnab/contacts.phtml');

        return parent::_prepareLayout();
    }

    public function getAllPhones()
    {

        $phones=[];

        $phone_arenda=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/phone_arenda');
        $phone_remont=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/phone_remont');
        $phone_prodasha_lulek=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/phone_prodasha_lulek');
        $phone_prodasha=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/phone_prodasha');
        $phone_buhgalteria=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/phone_buhgalteria');


        $viber_remont=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/viber_remont');
        $telegram_remont=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/telegram_remont');

        $viber_arenda=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/viber_arenda');
        $telegram_arenda=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/telegram_arenda');

        $viber_prodasha_lulek=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/viber_prodasha_lulek');
        $telegram_prodasha_lulek=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/telegram_prodasha_lulek');

        $viber_prodasha=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/viber_phone_prodasha');
        $telegram_prodasha=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/telegram_phone_prodasha');

        $viber_buhgalteria=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/viber_buhgalteria');
        $telegram_buhgalteria=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/telegram_buhgalteria');



        $remont_ringostat=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/remont_ringostat');
        $arenda_ringostat=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/arenda_ringostat');
        $prodasha_lulek_ringostat=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/prodasha_lulek_ringostat');
        $prodasha_ringostat=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/prodasha_ringostat');
        $buhgalteria_ringostat=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/buhgalteria_ringostat');


        $time_arenda=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/working_hours_arenda');
        $time_remont=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/working_hours_remont');
        $time_prodasha_lulek=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/working_hours_prodasha_lulek');
        $time_prodasha=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/working_hours_prodasha');
        $time_buhgalteria=Mage::getStoreConfig('phones_ukrsnab/setting_ukrsnab_phones/working_hours_buhgalteria');



        if ($phone_arenda!='')
        {
            $time_arenda=nl2br($time_arenda);
            $phones[]=[
                'number'=>$phone_arenda,
                'name'=>'Сервис проката',
                'time'=>$time_arenda,
                'ringostat'=>$arenda_ringostat,
                'viber'=>$viber_arenda,
                'telegram'=>$telegram_arenda

            ];
        }
        /* if ($phone_remont!='')
         {
             $phones[]=[
                 'number'=>$phone_remont,
                 'name'=>'Сервис ремонта',
                 'time'=>$time_remont,
             ];
         }*/

         if ($phone_prodasha_lulek!='')
         {
             $time_prodasha_lulek=nl2br($time_prodasha_lulek);
             $phones[]=[
                 'number'=>$phone_prodasha_lulek,
                 'name'=>'Сервис продажа люлек',
                 'time'=>$time_prodasha_lulek,
                 'ringostat'=>$prodasha_lulek_ringostat,
                 'viber'=>$viber_prodasha_lulek,
                 'telegram'=>$telegram_prodasha_lulek
             ];
         }

          if ($phone_prodasha!='')
          {
              $time_prodasha=nl2br($time_prodasha);
              $phones[]=[
                  'number'=>$phone_prodasha,
                  'name'=>'Отдел продаж',
                  'time'=>$time_prodasha,
                  'ringostat'=>$prodasha_ringostat,
                  'viber'=>$viber_prodasha,
                  'telegram'=>$telegram_prodasha
              ];
          }

          if ($phone_buhgalteria!='')
          {
              $time_buhgalteria=nl2br($time_buhgalteria);
              $phones[]=[
                  'number'=>$phone_buhgalteria,
                  'name'=>'Бухгалтерия',
                  'time'=>$time_buhgalteria,
                  'ringostat'=>$buhgalteria_ringostat,
                  'viber'=>$viber_buhgalteria,
                  'telegram'=>$telegram_buhgalteria
              ];
          }

          return $phones;

    }

    protected function _toHtml()
    {
        return parent::_toHtml();
    }
}


