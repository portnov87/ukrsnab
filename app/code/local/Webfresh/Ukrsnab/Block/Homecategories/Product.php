<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 04.11.2019
 * Time: 01:43
 */
class Webfresh_Ukrsnab_Block_Homecategories_Product extends Mage_Catalog_Block_Product_Abstract //Mage_Core_Block_Template
{


    public function getProductsByCategory()
    {

        $category_id=$this->getCategoryId();
        if ($category_id) {
            $category = Mage::getModel('catalog/category')->load($category_id);
            if ($category) {
                $products = Mage::getResourceModel('catalog/product_collection')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->addCategoryFilter($category)
                    ->addAttributeToFilter('show_home', 1)
                    ->addAttributeToSort('name', 'desc')
                    ->addAttributeToSelect('*');
                $pageSize = 4;
                $offset = 0;
                $products->setPageSize($pageSize)->setCurPage($offset);
                return $products;
           }
        }
        return [];
    }

}