<?php

class Webfresh_Ukrsnab_Block_Categorylist extends Mage_Core_Block_Template
{
    public function getCatalog()
    {

        $parent     = Mage::app()->getStore()->getRootCategoryId();
        $menu=Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect(['name','entity_id','svg_icon'])//name,url, id')
            ->addAttributeToFilter('show_topmenu', 1);
            //->addAttributeToFilter('level', 1);
        //load($parent);//->getCollection();

        //$children=$menu->getChildrenCategories();//getCategories($parent);


        return $menu;//$this->_menu;

    }




    public function _prepareLayout()
    {

        $this->setTemplate('ukrsnab/category_list.phtml');//webfresh/feedback/recept.phtml');

        return parent::_prepareLayout();
    }


    protected function _toHtml()
    {
        //   $this->_image = $this->getImage();
        return parent::_toHtml();
    }

}
