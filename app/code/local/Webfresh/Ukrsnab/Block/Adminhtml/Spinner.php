<?php

class Webfresh_Ukrsnab_Block_Adminhtml_Spinner extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_spinner';
        $this->_blockGroup = 'ukrsnab';
        $this->_headerText = Mage::helper('ukrsnab')->__('spinner Manager');
        parent::__construct();

        //$this->setTemplate('seo/rewrite.phtml');
    }

    protected function _prepareLayout()
    {

        $this->_addButton('generate_all_button', array(
            'label'   => Mage::helper('catalog')->__('Generate all categories'),
            'onclick' => "setLocation('{$this->getUrl('*/*/generateall')}')",
            'class'   => 'add'
        ));
        $this->_removeButton('add');
//        $this->setChild('generate_all_button',
//            $this->getLayout()->createBlock('adminhtml/widget_button')
//                ->setData(array(
//                'label'     => Mage::helper('ukrsnab')->__('Generate all categories'),
//                'onclick'   => "setLocation('" . $this->getUrl('*/*/generateall') . "')",
//                'class'     => 'add111'
//            ))
//        );

//        echo 'ddddd';
//        die();
        $this->setChild('grid', $this->getLayout()->createBlock('ukrsnab/adminhtml_spinner_grid', 'spinner.grid'));
        return parent::_prepareLayout();
    }

//
//    public function getAddNewButtonHtml()
//    {
//        return $this->getChildHtml('add_new_button');
//    }
//
//    public function getGridHtml()
//    {
//        return $this->getChildHtml('grid');
//    }
//
//    public function getStoreSwitcherHtml()
//    {
//        return $this->getChildHtml('store_switcher');
//    }
}