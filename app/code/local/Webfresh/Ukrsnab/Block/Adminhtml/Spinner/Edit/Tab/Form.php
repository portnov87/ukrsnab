<?php
class Webfresh_Ukrsnab_Block_Adminhtml_Spinner_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('spinner_data');
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('spinner_form', array('legend'=>Mage::helper('seo')->__('General Information')));

//        $fieldset->addField('url', 'text', array(
//            'label'     => Mage::helper('seo')->__('Pattern of Url or Action name'),
//            'class'     => 'required-entry',
//            'required'  => true,
//            'name'      => 'url',
//            'value'     => $model->getUrl(),
//            'note'=>
//                    'Can be a full action name or a request path. Wildcard allowed.
//                    Examples:<br>
//                    /customer/account/login/</br>
//                    /customer/account/*<br>
//                    customer_account_*<br>
//                    *?mode=list'
//        ));


        
        $fieldset->addField('category_id', 'text', array(
            'label'     => Mage::helper('ukrsnab')->__('category_id'),
            'name'      => 'category_id',
            'value'     => $model->getCategoryId(),
        ));
        
        

        $fieldset->addField('text', 'textarea', array(
            'label'     => Mage::helper('ukrsnab')->__('text Description'),
            'name'      => 'text',
            'value'     => $model->getText()
        ));

//        $fieldset->addField('meta_title', 'text', array(
//            'label'     => Mage::helper('seo')->__('Meta Title'),
//            'name'      => 'meta_title',
//            'value'     => $model->getMetaTitle()
//        ));

        /*if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('stores', 'multiselect', array(
                'label'     => Mage::helper('seo')->__('Visible In'),
                'required'  => true,
                'name'      => 'stores[]',
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
                'value'     => $model->getStoreId()
            ));
        }
        else {
            $fieldset->addField('stores', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
        }*/

//        echo '<pre>';
//        print_r($model->getData());
//        echo '</pre>';
//die();
//        $fieldset = $form->addFieldset('rewrite_faq_form', array('legend'=>Mage::helper('seo')->__('FAQ')));
//
//        $fieldset->addField('faq_h2', 'text', array(
//            'label'     => Mage::helper('seo')->__('Title H2'),
//            //'class'     => 'required-entry',
//            'required'  => false,
//            'name'      => 'faq_h2',
//            'value'     => $model->getFaqH2()
//        ));
//
//        $fieldset->addField('faq_title_1', 'text', array(
//            'label'     => Mage::helper('seo')->__('Title_1'),
//            //'class'     => 'required-entry',
//            'required'  => false,
//            'name'      => 'faq_title_1',
//            'value'     => $model->getData('faq_title_1')
//        ));
//
//        $fieldset->addField('faq_content_1', 'textarea', array(
//            'label'     => Mage::helper('seo')->__('Сontent_1'),
//            'name'      => 'faq_content_1',
//            'value'     =>  $model->getData('faq_content_1')
//        ));
//
//
//        $fieldset->addField('faq_title_2', 'text', array(
//            'label'     => Mage::helper('seo')->__('Title 2'),
//            'required'  => false,
//            'name'      => 'faq_title_2',
//            'value'     => $model->getData('faq_title_2'),//$model->getFaqTitle2()
//        ));
//        $fieldset->addField('faq_content_2', 'textarea', array(
//            'label'     => Mage::helper('seo')->__('Сontent 2'),
//            'name'      => 'faq_content_2',
//            'value'     => $model->getData('faq_content_2'),//$model->getFaqContent2()
//        ));
//
//
//        $fieldset->addField('faq_title_3', 'text', array(
//            'label'     => Mage::helper('seo')->__('Title 3'),
//            'required'  => false,
//            'name'      => 'faq_title_3',
//            'value'     => $model->getData('faq_title_3'),//$model->getFaqTitle3()
//        ));
//        $fieldset->addField('faq_content_3', 'textarea', array(
//            'label'     => Mage::helper('seo')->__('Сontent 3'),
//            'name'      => 'faq_content_3',
//            'value'     => $model->getData('faq_content_3'),//$model->getFaqContent3()
//        ));
//
//
//        $fieldset->addField('faq_title_4', 'text', array(
//            'label'     => Mage::helper('seo')->__('Title 4'),
//            'required'  => false,
//            'name'      => 'faq_title_4',
//            'value'     => $model->getData('faq_title_4'),//$model->getFaqTitle4()
//        ));
//        $fieldset->addField('faq_content_4', 'textarea', array(
//            'label'     => Mage::helper('seo')->__('Content 4'),
//            'name'      => 'faq_content_4',
//            'value'     => $model->getData('faq_content_4'),//$model->getFaqContent4()
//        ));
//
//
//        $fieldset->addField('faq_title_5', 'text', array(
//            'label'     => Mage::helper('seo')->__('Title 5'),
//            'required'  => false,
//            'name'      => 'faq_title_5',
//            'value'     => $model->getData('faq_title_5'),//$model->getFaqTitle5()
//        ));
//        $fieldset->addField('faq_content_5', 'textarea', array(
//            'label'     => Mage::helper('seo')->__('Сontent 5'),
//            'name'      => 'faq_content_5',
//            'value'     => $model->getData('faq_content_5'),//$model->getFaqContent5()
//        ));




        return parent::_prepareForm();
    }
}
