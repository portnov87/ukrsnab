<?php
class Webfresh_Ukrsnab_Block_Adminhtml_Spinner_Grid extends Mage_Adminhtml_Block_Widget_Grid {
    public function __construct() {
        parent::__construct();
        $this->setId('spinnerGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _getStore() {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('ukrsnab/spinnerdb')->getCollection();
//        $store = $this->_getStore();
//        if ($store->getId()) {
//            $collection->addStoreFilter($store);
//        }

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('entity_id', array(
                'header'    => Mage::helper('seo')->__('ID'),
                'align'     =>'right',
                'width'     => '50px',
                'index'     => 'entity_id',
        ));

        $this->addColumn('url', array(
                'header'    => Mage::helper('ukrsnab')->__('Url'),
                'align'     =>'left',
                'index'     => 'url',
        ));
          $this->addColumn('category_id', array(
                'header'    => Mage::helper('ukrsnab')->__('category_id'),
                'align'     =>'left',
                'index'     => 'category_id',
        ));

        $this->addColumn('action',
                array(
                'header'    =>  Mage::helper('seo')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                        array(
                                'caption'   => Mage::helper('ukrsnab')->__('Generate'),
                                'url'       => array('base'=> '*/*/generate'),
                                'field'     => 'id'
                        ),
                        array(
                                'caption'   => Mage::helper('ukrsnab')->__('Delete'),
                                'url'       => array('base'=> '*/*/delete'),
                                'field'     => 'id'
                        )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('meta');

        $this->getMassactionBlock()->addItem('delete', array(
                'label'    => Mage::helper('seo')->__('Delete'),
                'url'      => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('seo')->__('Are you sure?')
        ));


        return $this;
    }

//    public function getRowUrl($row) {
//        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
//    }

}