<?php


class Webfresh_Seo_Block_Adminhtml_Meta_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('spinner_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('seo')->__('Spinner Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('general_section', array(
            'label'     => Mage::helper('seo')->__('General Information'),
            'title'     => Mage::helper('seo')->__('General Information'),
            'content'   => $this->getLayout()->createBlock('ukrsnab/adminhtml_spinner_edit_tab_form')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}