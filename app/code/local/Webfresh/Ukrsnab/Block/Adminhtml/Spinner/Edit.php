<?php

class Webfresh_Ukrsnab_Block_Adminhtml_Spinner_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId   = 'rewrite_id';
        $this->_blockGroup = 'seo';
        $this->_controller = 'adminhtml_meta';

        $this->_updateButton('save', 'label', Mage::helper('seo')->__('Save meta'));
        $this->_updateButton('delete', 'label', Mage::helper('seo')->__('Delete meta'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    public function getHeaderText()
    {
        if (Mage::registry('meta_data')->getId() > 0) {
            return Mage::helper('seo')->__("Edit meta '%s'", $this->htmlEscape(Mage::registry('meta_data')->getUrl()));
        } else {
            return Mage::helper('seo')->__("Add meta");
        }
    }
}