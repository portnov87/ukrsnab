<?php

class Webfresh_Ukrsnab_Block_Topmenu extends Mage_Core_Block_Template
{
    /**
     * Top menu data tree
     *
     * @var Varien_Data_Tree_Node
     */
    protected $_menu;
    protected $_imgSize = 100;

    public $array_dop_filters=[
        'category_node_41'=>[],
        'category_node_42'=>[],
    ];

    /**
     * Init top menu tree structure
     */
    public function _construct()
    {

        $this->_menu = new Varien_Data_Tree_Node(array(), 'root', new Varien_Data_Tree());
    }

    /**
     * Get top menu html
     *
     * @param string $outermostClass
     * @param string $childrenWrapClass
     * @return string
     */
    public function getHtml($outermostClass = '', $childrenWrapClass = '')
    {


        Mage::dispatchEvent('page_block_html_topmenu_gethtml_before', array('menu' => $this->_menu, 'block' => $this));
        $this->_menu->setOutermostClass($outermostClass);
        $this->_menu->setChildrenWrapClass($childrenWrapClass);

        $html = $this->_getHtml($this->_menu, $childrenWrapClass);

        Mage::dispatchEvent('page_block_html_topmenu_gethtml_after', array(
            'menu' => $this->_menu,
            'html' => $html
        ));

        return $html;
    }


    protected function getProductsMenu($category_id=false)
    {
        //show_in_menu
       // $products=Mage::getModel('catalog/product')->getCollection()
        $limit=1;
        if ($category_id) {

            //$chil
            $category=Mage::getModel('catalog/category')->load($category_id);
            $catfilters=[];
            if ($category)
            {
                $catfilters=explode(',',$category->getChildren());

            }

            //$catfilters = array(10, 20, 25); //Obviously your category ids will be different
            $conditions = array();
            $categories_parent=Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToFilter('entity_id', array('in'=>$catfilters));

            foreach ($categories_parent as $c_p)
            {

                if ($c_p->hasChildren()) {
                    //$catfilters = explode(',', $c_p->getChildren());
                    $catfilters_childs = explode(',', $c_p->getChildren());
                    $catfilters=array_merge($catfilters,$catfilters_childs);
                }
            }


            /*echo '<pre>$c_p';
            print_r($catfilters);
            echo '</pre>';*/
            //$catfilters$children = $menuTree->getChildren();
            $layout=Mage::app()->getLayout();

            $catfilters[]=$category_id;
            $product_labels = $layout->createBlock('labels/catalog_product','product_labels',['template'=>'databunch/labels/catalog/product.phtml']);

            $product_html=$layout->createBlock(
                'ukrsnab/topmenuproducts',
                'ukrsnab.topmenuproducts',
                array(
                    'template' => 'ukrsnab/product_menu.phtml',
                    'categories_id'=>$catfilters
                )
            )->setChild('product_labels', $product_labels)->toHtml();


            //->toHtml();

           /* foreach ($collection as $product)
            {
                echo '<pre>';
                print_r($product->getData());
                echo '</pre>';
                die();
            }*/
            return $product_html;//$collection;
        }
        return [];


    }
    protected function getPromoBox($item)
    {
        $html = '';

        $promo_image = $item->getPromoImage();

        $promo_product = $item->getProductId();
        $html = '<div class="m-m-dc-right">';
        if ($promo_image != '') {

            $promo_image_img=$promo_image;
            $promo_image = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/category/' . $promo_image;//$item->getPromoImage();
            $promo_image_dir = Mage::getBaseDir('media')  . DS . "catalog" . DS . "category" . DS . $promo_image_img;//$item->getPromoImage();
            //echo $promo_image_dir.' <br/>';
            if (file_exists($promo_image_dir)) {
                $imageResizedPromo = Mage::getBaseDir('media') . DS . "catalog" . DS . "category" . DS . "resized" . DS . "promo" . DS . $promo_image_img;
                $imageResizedPromo_dir =Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . "catalog" . DS . "category" . DS . "resized" . DS . "promo" . DS . $promo_image_img;

                if (!file_exists($imageResizedPromo)) {
                    $imageObj = new Varien_Image($promo_image_dir);
                    $imageObj->constrainOnly(TRUE);
                    $imageObj->keepAspectRatio(TRUE);
                    $imageObj->keepFrame(FALSE);
                    $imageObj->resize(330, 330);
                    $imageObj->save($imageResizedPromo);
                }

                $html .= '
                                                <div class="m-m-dc-r-image">
                                                        <img style="width:330px;height:370px;"  src="' . $imageResizedPromo_dir . '" alt="Акция">
                                                </div>';
            }

        }
        if ($promo_product != '') {

            $product = Mage::getModel('catalog/product')->load($promo_product);
            if ($product) {

                $url_product = '/' . $product->getUrlKey() . '.html';
                $html .= '
						
						<div class="m-m-dc-r-text-wrap">
							<div class="m-m-dc-r-name"><a href="' . $url_product . '">' . $product->getName() . '</a></div>';
                if ($product->getPrimaryAction() != '') $html .= '<div class="m-m-dc-r-descr">' . $product->getPrimaryAction() . '</div>';

                $html .= '<div class="m-m-dc-r-price">' . Mage::helper('core')->currency($product->getPrice()) . '</div>
							<div class="m-m-dc-r-link">
								<a href="' . $url_product . '">Подробнее</a>
							</div>
						</div>';
            }


        }


        $html .= '</div>';

        return $html;
    }

    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Varien_Data_Tree_Node $menuTree
     * @param string $childrenWrapClass
     * @return string
     */
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';
        $i = 0;

        $additioncategories=[
            24,
            31,
            28,
            18,
            14,
            21
        ];
        foreach ($additioncategories as $catId) {
            $category = Mage::getModel('catalog/category')->load($catId);
            if ($category) {

                if ($category->getIsActive()) {
                    $svg = $category->getSvgIcon();
                    if ($svg!='') {

                        $html .= '<div class="catalog-menu--category"><div class="category-icon">' . $svg . '</div>';

                        $html .= '<a href="' . $category->getUrl() . '" class="catalog-menu--category_name">' . $this->escapeHtml($category->getName()) . '</a>';

                        $html .= '<img class="category-arrow" src="' . Mage::helper('ukrsnab/data')->getSkinUrl() . 'img/arrow.svg" alt="category-img" />';
                        if ($category->hasChildren()) {
                            $html .= '<div class="catalog-menu--category_subcategories" >';
                            $html .= '<div class="box" style="display:block">';
                            $html .= "\r\n";


                            foreach ($category->getCategories($category->getId()) as $catChild) {
//                                echo  '<pre>catid';
//                                print_r($catChild->getId());
//                                echo '</pre>';

                                $_catChild=Mage::getModel('catalog/category')->load($catChild->getId());
                                $html .= '<div class="subcategory" style="height:auto!important;     width: 100%;clear: both;">
                                <div class="subcategory-category" style="height:auto!important">';
//                                $i++;
//                                $html .= "\r\n";


                                $html .= '<a href="' . $_catChild->getUrl() . '" class="title">' . $this->escapeHtml($catChild->getName()) . '</a>';

                                $html .= '</div>';
                                $html .= '</div>';

                            }


//                            $html .= 'test';//$this->_getHtmlLevel2($child, $childrenWrapClass);
//                            $html .= "\r\n";
                            $html .= '</div>';

                            $html_product=$this->getProductsMenu($catId);
                            $html .=$html_product;

                            $html .= '</div>';
                        }
                        $html .= '</div>';
                    }
                }
            }
        }
        foreach ($children as $child) {
            $i++;

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }
            //if ($childLevel==1)
            $html .= "\r\n";

            $_id=explode('category-node-',$child->getId());
            /*echo '<pre>ffff' . $_id[1];
            print_R($_id);
            echo '</pre>';
            die();*/
            if (isset($_id[1])) {

                $category_level1 = Mage::getModel('catalog/category')->load($_id[1]);
                /*echo '<pre>' . $child->getId();
                print_R($category_level1->getData());
                echo '</pre>';
                die();*/
                $svg = $category_level1->getSvgIcon();
            }else $svg='';

            $html .= '<div class="catalog-menu--category"><div class="category-icon">'.$svg.'</div>';


            //$html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
            $html .= '<a href="' . $child->getUrl() . '" class="catalog-menu--category_name">'
                . $this->escapeHtml($child->getName()) . '</a>';

            $html .= '<img class="category-arrow" src="'.Mage::helper('ukrsnab/data')->getSkinUrl().'img/arrow.svg" alt="category-img" />';


            //promobox

            $box = '';//$this->getPromoBox($child);
            //' . $outermostClassCode . '

            if ($child->hasChildren()) {
                //if (!empty($childrenWrapClass)) {
                //    $html .= '<div class="' . $childrenWrapClass . '">';
                //}
                //<div class="catalog-menu--category_subcategories">
        //<div class="box">
                $html .= '<div class="catalog-menu--category_subcategories">';
                $html .= '<div class="box">';
                $html .= "\r\n";


                $html .= $this->_getHtmlLevel2($child, $childrenWrapClass);
                $html .= "\r\n";
                $html .= '</div>';


                //products
                //$html .= '<div class="catalog-menu--category_thumbnails">';
                $category_node_string=$child->getId();
                $category_node=explode('category-node-',$category_node_string);

                if (isset($category_node[1])) {
                    $category_id=$category_node[1];


                    $html_product=$this->getProductsMenu($category_id);
                    $html .=$html_product;

                }
                //$html .= '</div>';
                $html .= '</div>';
                $html .= $box;


            }
            $html .= '</div>';
            $html .= "\r\n";
            $counter++;
        }

        return $html;
    }


    protected function _getHtmlLevel2(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';
        $array_category_five=['category-node-4','category-node-8'];
//echo 'id parent'.$menuTree->getId();

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 0;//1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';
        $i = 0;
        /*<div class="subcategory">
                    <div class="subcategory-category">
                        <a href="#" class="title">Техника для бетона</a>
                        <a href="#" class="subcategory-category--page">Отбойные молотки</a>
                        <a href="#" class="subcategory-category--page">Бункеры для бетона</a>
                        <a href="#" class="subcategory-category--page">Бетоннонасосные станции</a>
                        <a href="#" class="subcategory-category--page">Штукатурные станции</a>
                    </div>
                </div>*/
        $style_box='';
        if (in_array($menuTree->getId(),$array_category_five)) $style_box='width:100%';
        $html .= '<div class="box-chunk" style="'.$style_box.'">';
        $category_for_rightcol = [];
        foreach ($children as $child) {

            $html .= '<div class="subcategory">
                    <div class="subcategory-category">';
            $i++;
            $html .= "\r\n";



            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);
            if (!$child->hasChildren()) {
                $category_for_rightcol[] = $child;
                //continue;
            }

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }
            /*<div class="subcategory">
                    <div class="subcategory-category">
                        <a href="#" class="title">Техника для бетона</a>
                        <a href="#" class="subcategory-category--page">Отбойные молотки</a>
                        <a href="#" class="subcategory-category--page">Бункеры для бетона</a>
                        <a href="#" class="subcategory-category--page">Бетоннонасосные станции</a>
                        <a href="#" class="subcategory-category--page">Штукатурные станции</a>
                    </div>
                </div>*/
            $html .= '<a href="' . $child->getUrl() . '" class="title">'.$this->escapeHtml($child->getName()).'</a>';
            //$html .= '<div class="m-m-dc-wrap">';

            /*$html .= '<div class="m-m-dc-title"><a href="' . $child->getUrl() . '" ' . $outermostClassCode . '>'
                . $this->escapeHtml($child->getName()) . '</a></div>';
*/
            if ($child->hasChildren()) {
                if (!empty($childrenWrapClass)) {
                    //  $html .= '<div class="' . $childrenWrapClass . '">';
                }

                $html .= $this->_getHtmlLevel3($child, $childrenWrapClass);
                $html .= "\r\n";

                if (!empty($childrenWrapClass)) {
                    //  $html .= '</div>';
                }
            }else{
                //$html .= '<a href="' . $child->getUrl() . '" class="subcategory-category--page">'.$this->escapeHtml($child->getName()).'</a>';
            }
            $html .= '</div>';
            $html .= '</div>';

            $html .= "\r\n";

            $counter++;

            //if ($child->getId()=='category-node-')
            if (in_array($menuTree->getId(),$array_category_five))//=='category-node-4')||($menuTree->getId()=='category-node-8'))
            {
                if ($counter % 6 == 0) $html .= '</div><div class="box-chunk">';
            }else {
                if ($counter % 5 == 0) $html .= '</div><div class="box-chunk">';
            }
        }
        //$html .= '</div>';
        $html .= '</div>';
        //$html .= '</div>';
        //$html .= '</div>';

//        $html .= '</div>';

        /*
        $html .= '<div class="m-m-dc-right-col">';
        if (count($category_for_rightcol) > 0) {
            $html .= '<div class="links-wrap">';

            foreach ($category_for_rightcol as $_item) {

                $name_cat=$this->escapeHtml($_item->getName());
                $new=$_item->getData('label_new');
                if ($new){
                    $name_cat.='<span  style="color:red;font-size:10px;margin-left:10px;font-weight:700;text-transform:uppercase;" class="new_label_category">new</span>';
                }

                $acrion=$_item->getData('label_action');
                if ($acrion){
                    $name_cat.='<span  style="color:green;font-size:10px;margin-left:10px;font-weight:700;text-transform:uppercase;" class="action_label_category">акция</span>';
                }

                $html .= '<a href = "' . $_item->getUrl() . '" >'.$name_cat . '</a >';




            }

            $html .= '</div>';

        }

              if ($menuTree->getCategoryUrl()=='medicaments') {
                  $html .= '<div class="main-link">
                                      <a href="/medicines/atc/index">Товары по АТС классификации</a>
                                  </div>';
              }
*/


        return $html;
    }

    protected function _getHtmlLevel3(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }


            $html .= '<a href="' . $child->getUrl() . '" class="subcategory-category--page">'.$this->escapeHtml($child->getName()).'</a>';
            /*$html .= '<div class="m-m-dc-link"><a href="' . $child->getUrl() . '" ' . $outermostClassCode . '>'
                . $this->escapeHtml($child->getName()) . '</a></div>';*/
            $html .= "\r\n";
            $counter++;
        }

        return $html;
    }


    /**
     * Generates string with all attributes that should be present in menu item element
     *
     * @param Varien_Data_Tree_Node $item
     * @return string
     */
    protected function _getRenderedMenuItemAttributes(Varien_Data_Tree_Node $item)
    {
        $html = '';
        $attributes = $this->_getMenuItemAttributes($item);

        foreach ($attributes as $attributeName => $attributeValue) {
            $html .= ' ' . $attributeName . '="' . str_replace('"', '\"', $attributeValue) . '"';
        }

        return $html;
    }

    /**
     * Returns array of menu item's attributes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemAttributes(Varien_Data_Tree_Node $item)
    {
        $menuItemClasses = $this->_getMenuItemClasses($item);
        $attributes = array(
            'class' => implode(' ', $menuItemClasses)
        );

        return $attributes;
    }

    /**
     * Returns array of menu item's classes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemClasses(Varien_Data_Tree_Node $item)
    {
        $classes = array();


        if ($item->getLevel() == 0) $classes[] = 'm-m-dropdown';


        return $classes;
    }

}
