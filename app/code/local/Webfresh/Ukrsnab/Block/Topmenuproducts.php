<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 04.09.2018
 * Time: 11:11
 */

class Webfresh_Ukrsnab_Block_Topmenuproducts extends Mage_Catalog_Block_Product_Abstract
//extends Mage_Core_Block_Template
{
    public $limit=2;


    public function getProducts()
    {
        $limit=$this->limit;
        $data=$this->getData();
        $categories_id=$data['categories_id'];

        $conditions=[];

        foreach ($categories_id as $categoryId) {
            if (is_numeric($categoryId)) {
                $conditions[] = "{{table}}.category_id = $categoryId";
            }
        }
        if (count($conditions)>0) {

            $collection = Mage::getResourceModel('catalog/product_collection')
                ->setStoreId(Mage::app()->getStore()->getId())
                // ->addCategoryFilter($category)
                ->addUrlRewrite()
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('*')
                ->addFinalPrice()
                ->addAttributeToFilter('show_in_menu', 1)//featured_product
                ->addAttributeToFilter('type_id', 'simple')
                ->setPageSize($limit)
                ->setCurPage(1);


           /* $collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect(['id', 'image', 'name'])
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', 4)
                ->addAttributeToFilter('show_in_menu', 1)
                ->setPageSize($limit)
                ->setCurPage(1)
                ->addStoreFilter();*/


            $collection->distinct(true)
                ->joinField('category_id', 'catalog/category_product', null, 'product_id = entity_id', implode(" OR ", $conditions), 'inner');

            return $collection;
        }

        return false;


    }

}