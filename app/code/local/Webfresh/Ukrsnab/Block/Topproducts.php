<?php

class Webfresh_Ukrsnab_Block_Topproducts extends Mage_Catalog_Block_Product_Abstract//extends Mage_Core_Block_Template
{

    public $limit=5;

    public function getCategory()
    {
        if ($category=Mage::registry('current_category')) {

            return $category;
        }
        return false;

    }

    public function getProducts()
    {
        $limit=$this->limit;

        $category=$this->getCategory();
        if ($category)
            $category_id=$category->getId();
        else return [];


        $collection = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->addCategoryFilter($category)
            ->addUrlRewrite()
            ->addAttributeToSelect('*')
            ->addFinalPrice()
            ->addAttributeToFilter('show_pin_products', 1)
            ->addAttributeToFilter('type_id', 'simple')
            ->setPageSize($limit)
            ->setCurPage(1);
        return $collection;



        /*if (count($conditions)>0) {



            $collection->distinct(true)
                ->joinField('category_id', 'catalog/category_product', null, 'product_id = entity_id', implode(" OR ", $conditions), 'inner');


            $collection = Mage::getResourceModel('catalog/product_collection')
                ->setStoreId(Mage::app()->getStore()->getId())
                // ->addCategoryFilter($category)
                ->addUrlRewrite()
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('*')
                ->addFinalPrice()
                ->addAttributeToFilter('show_in_menu', 1)//featured_product
                ->addAttributeToFilter('type_id', 'simple')
                ->setPageSize($limit)
                ->setCurPage(1);



            $collection->distinct(true)
                ->joinField('category_id', 'catalog/category_product', null, 'product_id = entity_id', implode(" OR ", $conditions), 'inner');

            return $collection;
        }*/

        return false;


    }

}
