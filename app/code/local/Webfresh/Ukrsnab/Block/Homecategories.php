<?php

class Webfresh_Ukrsnab_Block_Homecategories extends Mage_Catalog_Block_Product_Abstract //Mage_Core_Block_Template
{

    public function getCatalog()
    {

        $parent     = Mage::app()->getStore()->getRootCategoryId();


        $childrens=Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToFilter('show_home', 1)
            ->addAttributeToFilter('level', ['IN',[2,3]])
            //->addAttributeToFilter('parent_id', ['NOT IN',[$parent]])
            ->addAttributeToSelect(['name','url'])
        ->setOrder('position','ASC');
            //->addAttributeToFilter('parent_id', $parent);



       // $menu=Mage::getModel('catalog/category')->load($parent);//->getCollection();

        //$children=$menu->getChildrenCategories();//getCategories($parent);



        //%сфеупщ

        return $childrens;//$this->_menu;

    }


    public function getChildrens($parent_id)
    {
       // $childrens=[];

        $childrens=Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToFilter('show_subcategory_home', 1)
            ->addAttributeToSelect(['name','url'])
            ->addAttributeToSort('position', 'asc')
            ->addAttributeToFilter('parent_id', $parent_id);

        return $childrens;
    }


    public function getProductsByCategory($category_id)
    {
        $category = Mage::getModel('catalog/category')->load($category_id);
        if ($category) {
            $products = Mage::getResourceModel('catalog/product_collection')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->addCategoryFilter($category)
                ->addAttributeToFilter('show_home', 1)
                ->addAttributeToSort('name', 'desc')
                ->addAttributeToSelect('*');
            $pageSize = 4;
            $offset = 0;
            $products->setPageSize($pageSize)->setCurPage($offset);
            return $products;
        }
        return [];
    }
    public function _prepareLayout()
    {

        $this->setTemplate('ukrsnab/home_catalog.phtml');//webfresh/feedback/recept.phtml');

        return parent::_prepareLayout();
    }


    protected function _toHtml()
    {
        //   $this->_image = $this->getImage();
        return parent::_toHtml();
    }

}
