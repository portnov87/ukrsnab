<?php

class Webfresh_Ukrsnab_Block_Topmenucategory extends Mage_Catalog_Block_Navigation //Mage_Core_Block_Template
{
    /**
     * Top menu data tree
     *
     * @var Varien_Data_Tree_Node
     */
    protected $_menu;
    protected $_imgSize = 100;

    /**
     * Init top menu tree structure
     */
    public function _construct()
    {


        $this->_menu = new Varien_Data_Tree_Node(array(), 'root', new Varien_Data_Tree());

    }

    /**
     * Get top menu html
     *
     * @param string $outermostClass
     * @param string $childrenWrapClass
     * @return string
     */
    public function getHtml($outermostClass = '', $childrenWrapClass = '')
    {


        Mage::dispatchEvent('page_block_html_categorymenu_gethtml_before', array('menu' => $this->_menu, 'block' => $this));
        $this->_menu->setOutermostClass($outermostClass);
        $this->_menu->setChildrenWrapClass($childrenWrapClass);
        return $this->_menu;

    }


    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Varien_Data_Tree_Node $menuTree
     * @param string $childrenWrapClass
     * @return string
     */
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';
        $i = 0;
        foreach ($children as $child) {
            $i++;

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }
            //if ($childLevel==1)
            $html .= "\r\n";
            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
            $html .= '<a href="' . $child->getUrl() . '" class="m-m-corner1">'
                . $this->escapeHtml($child->getName()) . '</a>';


            //promobox

            $box = $this->getPromoBox($child);
            //' . $outermostClassCode . '

            if ($child->hasChildren()) {
                if (!empty($childrenWrapClass)) {
                    $html .= '<div class="' . $childrenWrapClass . '">';
                }
                $html .= '<div class="m-m-dc-left">';
                $html .= "\r\n";


                $html .= $this->_getHtmlLevel2($child, $childrenWrapClass);
                $html .= "\r\n";
                $html .= '</div>';
                $html .= $box;

                if (!empty($childrenWrapClass)) {
                    $html .= '</div>';
                }
            }
            $html .= '</li>';
            $html .= "\r\n";
            $counter++;
        }

        return $html;
    }


    protected function _getHtmlLevel2(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';


        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 0;//1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';
        $i = 0;
        //if  ($menuTree->getIncludeInMenu()==1){
        $html .= '<div class="m-m-dc-left-col">';
        $category_for_rightcol = [];
        foreach ($children as $child) {
            $i++;
            $html .= "\r\n";


            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);
            if (!$child->hasChildren()) {
                $category_for_rightcol[] = $child;
                continue;
            }

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }

            $html .= '<div class="m-m-dc-wrap">';

            $html .= '<div class="m-m-dc-title"><a href="' . $child->getUrl() . '" ' . $outermostClassCode . '>'
                . $this->escapeHtml($child->getName()) . '</a></div>';

            if ($child->hasChildren()) {
                if (!empty($childrenWrapClass)) {
                    //  $html .= '<div class="' . $childrenWrapClass . '">';
                }

                $html .= $this->_getHtmlLevel3($child, $childrenWrapClass);
                $html .= "\r\n";

                if (!empty($childrenWrapClass)) {
                    //  $html .= '</div>';
                }
            }
            $html .= '</div>';

            $html .= "\r\n";

            $counter++;
            if ($counter % 3 == 0) $html .= '</div><div class="m-m-dc-left-col">';
        }

        $html .= '</div>';

        $html .= '<div class="m-m-dc-right-col">';
        if (count($category_for_rightcol) > 0) {
            $html .= '<div class="links-wrap">';

            foreach ($category_for_rightcol as $_item) {
                $html .= '<a href = "' . $_item->getUrl() . '" >' . $this->escapeHtml($_item->getName()) . '</a >';

            }

            $html .= '</div>';

        }



        $html .= '</div>';

        return $html;
    }

    protected function _getHtmlLevel3(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }

            $html .= '<div class="m-m-dc-link"><a href="' . $child->getUrl() . '" ' . $outermostClassCode . '>'
                . $this->escapeHtml($child->getName()) . '</a></div>';
            $html .= "\r\n";
            $counter++;
        }

        return $html;
    }


    /**
     * Generates string with all attributes that should be present in menu item element
     *
     * @param Varien_Data_Tree_Node $item
     * @return string
     */
    protected function _getRenderedMenuItemAttributes(Varien_Data_Tree_Node $item)
    {
        $html = '';
        $attributes = $this->_getMenuItemAttributes($item);

        foreach ($attributes as $attributeName => $attributeValue) {
            $html .= ' ' . $attributeName . '="' . str_replace('"', '\"', $attributeValue) . '"';
        }

        return $html;
    }

    /**
     * Returns array of menu item's attributes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemAttributes(Varien_Data_Tree_Node $item)
    {
        $menuItemClasses = $this->_getMenuItemClasses($item);
        $attributes = array(
            'class' => implode(' ', $menuItemClasses)
        );

        return $attributes;
    }

    /**
     * Returns array of menu item's classes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemClasses(Varien_Data_Tree_Node $item)
    {
        $classes = array();


        if ($item->getLevel() == 0) $classes[] = 'm-m-dropdown';


        return $classes;
    }

}
