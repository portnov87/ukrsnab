<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 04.09.2018
 * Time: 11:11
 */

class Webfresh_Ukrsnab_Block_Homesales extends Mage_Catalog_Block_Product_Abstract
//extends Mage_Core_Block_Template
{
    public $limit=20;
    public function getBestsellerProducts()
    {
        $products = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId(Mage::app()->getStore()->getId())
            // ->addCategoryFilter($category)
            ->addUrlRewrite()
            ->addAttributeToSelect('small_image')
            ->addAttributeToSelect('*')
            ->addFinalPrice()
            ->addAttributeToFilter('special_offer', 1)//featured_product
            ->addAttributeToFilter('type_id', 'simple')
            ->setPageSize($this->limit)
            ->setCurPage(1);
        return $products;

    }
}