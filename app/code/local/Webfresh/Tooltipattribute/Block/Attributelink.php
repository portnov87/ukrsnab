<?php

class Webfresh_Tooltipattribute_Block_Attributelink extends Mage_Core_Block_Template
{

    protected $_product = null;

    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = Mage::registry('product');
        }
        return $this->_product;
    }

    public function getUrl()
    {
        $product=$this->getProduct();
        $categories=$product->getCategoryIds();
        $category_url=false;
        /*echo '<pre>$categories';
        print_r($categories);
        echo '</pre>';*/
        if (isset($categories[count($categories)-1])) {
            $category_id = $categories[count($categories) - 1];
            $category=Mage::getModel('catalog/category')->load($category_id);
            if ($category)
            {
                $category_url=$category->getUrl();
            }
        }
       // echo 'url'.$category_url;
        /*echo '<pre>$url';
        print_r($product->getData());
        echo '</pre>';*/
        return $category_url;

    }


    public function getAdditionalData()
    {
        $product = $this->getProduct();

        $features = Mage::helper('ukrsnab/data')->getFeatures($product);//$prod);



        $data_product=$product->getData();
        $attributes = $product->getAttributes();
        $category_url=$this->getUrl();

        foreach ($attributes as $attribute) {
            $value_id=false;

            if (in_array(trim($attribute->getAttributeCode()),$features)) {
                //echo $attribute->getAttributeCode().'<br/>';
                if (isset($data_product[$attribute->getAttributeCode()]))
                    $value_id = $data_product[$attribute->getAttributeCode()];


                //if ($attribute->getShowLink()) {
                //} && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
                $value = $attribute->getFrontend()->getValue($product);

                if (!$product->hasData($attribute->getAttributeCode())) {
                    //$value = Mage::helper('catalog')->__('N/A');
                } elseif ((string)$value == '') {
                    $value = Mage::helper('catalog')->__('No');
                } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                    $value = Mage::app()->getStore()->convertPrice($value, true);
                }

                if (is_string($value) && strlen($value)&&($value!='Нет')) {


                    if ($attribute->getShowLink()) {
                        $attrUrlKeyModel = Mage::getResourceModel('catalin_seo/attribute_urlkey');

                        //if ($attrUrlKeyModel)
                        $attr_val = $attrUrlKeyModel->getUrlValue($attribute->getId(), $value_id);//$option['value']);
                        $url_key = $attrUrlKeyModel->getUrlKey($attribute->getAttributeCode());//, $value_id);//$option['value']);

                        $filter_url = $category_url . '/filter/' . $url_key . '/' . $attr_val . '/';
                        $data[$attribute->getAttributeCode()] = array(
                            'label' => $attribute->getStoreLabel(),
                            'value' => $value,
                            'ulr_key' => $attr_val,
                            'url' => $filter_url,
                            'tooltip' => $attribute->getTooltip(),
                            'code' => $attribute->getAttributeCode()
                        );
                    }
                    else{

                        $data[$attribute->getAttributeCode()] = array(
                            'label' => $attribute->getStoreLabel(),
                            'value' => $value,
                            'ulr_key' => '',
                            'url' => '',
                            'tooltip' => $attribute->getTooltip(),
                            'code' => $attribute->getAttributeCode()
                        );
                    }
                }
                //}
            }
        }
        return $data;

        /*


        foreach ($features as $feature) {
            if ($feature != '') {
                $feature=trim($feature);
                //$attr=$_product->getAttribute($feature);
                //if ($attr){
                $value=$product_data[trim($feature)];
                //$_product->getData($feature);
                $value_1 = $_helper->productAttribute($prod, $value, $feature);//
                $_attr_value = $_product->getAttributeText($feature);
                if ($_attr_value=='')
                    $_attr_value=$value_1;
                //echo $product_data[$feature]. ' '.$feature.' $value_1 = '.$value.' '.$value_1." =---- " .$_attr_value."\r\n\r\n";

                $attributeLabel = Mage::getModel('eav/entity_attribute')
                    ->loadByCode($entityType, $feature);
                if ($attributeLabel) {
                    $_attr_label = $attributeLabel->getFrontendLabel();

                    if ($_attr_value != '') {


                        $values_feature[$_attr_label]=[$_attr_value;
                        //$value= $prod->getAttributeText($feature);


                        $class = 'characteristic';
                        ?>

                        <?php
                    }
                }
                //  }
            }

        }

        return $values_feature;*/
    }




    /**
     * $excludeAttr is optional array of attribute codes to
     * exclude them from additional data array
     *
     * @param array $excludeAttr
     * @return array
     */
    /*
    public function getAdditionalData(array $excludeAttr = array())
    {
        $data = array();
        $product = $this->getProduct();
        $data_product=$product->getData();
        $attributes = $product->getAttributes();
        $category_url=$this->getUrl();
        foreach ($attributes as $attribute) {
            $value_id=false;
            if (isset($data_product[$attribute->getAttributeCode()]))
                $value_id=$data_product[$attribute->getAttributeCode()];


            if ($attribute->getShowLink())
            {
                //} && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
                $value = $attribute->getFrontend()->getValue($product);

                if (!$product->hasData($attribute->getAttributeCode())) {
                    //$value = Mage::helper('catalog')->__('N/A');
                } elseif ((string)$value == '') {
                    $value = Mage::helper('catalog')->__('No');
                } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                    $value = Mage::app()->getStore()->convertPrice($value, true);
                }

                if (is_string($value) && strlen($value)) {


                    $attrUrlKeyModel = Mage::getResourceModel('catalin_seo/attribute_urlkey');

                    $attr_val=$attrUrlKeyModel->getUrlValue($attribute->getId(), $value_id);//$option['value']);
                    $url_key=$attrUrlKeyModel->getUrlKey($attribute->getAttributeCode());//, $value_id);//$option['value']);

                    $filter_url=$category_url.'/filter/'.$url_key.'/'.$attr_val.'/';
                    $data[$attribute->getAttributeCode()] = array(
                        'label' => $attribute->getStoreLabel(),
                        'value' => $value,
                        'ulr_key'=>$attr_val,
                        'url'=>$filter_url,
                        'code'  => $attribute->getAttributeCode()
                    );
                }
            }
        }
        return $data;
    }*/



}
