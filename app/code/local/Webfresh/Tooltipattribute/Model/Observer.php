<?php
class Webfresh_Tooltipattribute_Model_Observer
{
    /**
     * Hook that allows us to edit the form that is used to create and/or edit attributes.
     * @param Varien_Event_Observer $observer
     */
    public function addFieldToAttributeEditForm($observer)
    {

        // Add an extra field to the base fieldset:
        $fieldset = $observer->getForm()->getElement('base_fieldset');
        $fieldset->addField('tooltip', 'textarea', array(
            'name' => 'tooltip',
            'label' => Mage::helper('core')->__('Tooltip'),
            'title' => Mage::helper('core')->__('Tooltip')
        ));

        $fieldset->addField('show_open_default', 'select', array(
            'name' => 'show_open_default',
            'label' => Mage::helper('core')->__('show_open_default'),
            'title' => Mage::helper('core')->__('show_open_default'),
            'values'=>[
                0=>'Открытый фильтр',
                1=>'Закрыт по умолчанию'
            ]
        ));

        $fieldset->addField('show_link', 'select', array(
            'name' => 'show_link',
            'label' => Mage::helper('core')->__('show link'),
            'title' => Mage::helper('core')->__('show link'),
            'values'=>[
                0=>'Не выводить ссылкой',
                1=>'Выводить ссылкой'
            ]
        ));
        $fieldset->addField('show_link_all', 'select', array(
            'name' => 'show_link_all',
            'label' => Mage::helper('core')->__('Выводить показать все'),
            'title' => Mage::helper('core')->__('Выводить показать все'),
            'values'=>[
                0=>'Не выводить показать все',
                1=>'Выводить показать все'
            ]
        ));
    }
}