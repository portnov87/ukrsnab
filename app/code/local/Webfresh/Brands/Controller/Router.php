<?php
/**
 * @category    Fishpig
 * @package    Fishpig_AttributeSplash
 * @license      http://fishpig.co.uk/license.txt
 * @author       Ben Tideswell <ben@fishpig.co.uk>
 */

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);

class Webfresh_Brands_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    /**
     * Cache for request object
     *
     * @var Zend_Controller_Request_Http
     */
    protected $_request = null;

    /**
     * Initialize Controller Router
     *
     * @param Varien_Event_Observer $observer
     */
    public function initControllerRouters(Varien_Event_Observer $observer)
    {

        $requestUri = Mage::app()->getRequest()->getPathInfo();
        //$requestUri=Mage::app()->getRequest();
        //echo 'tetet';
        if (strpos($requestUri, Webfresh_Brands_Helper_Data::URL_KEY) === false)
            return false;

        if (strpos($requestUri, 'page') !== false)
            return false;


        if ($requestUri == '/brands') {

            /*list($module, $url_brand) = explode('/', $requestUri);


            echo '<pre>' . $requestUri;
            print_r($module);
            echo '</pre>';
            echo '<pre>';
            print_r($url_brand);
            echo '</pre>';
            die();
            if ($url_brand) {


            } else {*/

            // echo 'rererer';
            //die();
            $observer = Mage::getSingleton('Webfresh_Brands_Model_Observer');
            $observer->setBrands(true);
            //$this->getRequest()

            Mage::app()->getRequest()
                ->setModuleName('brands')
                ->setControllerName('index')
                ->setActionName('view');
            //->setParam('category_id', $_category->getId());
            return true;
            //}
        }


        //echo 'tests';die();
        $observer->getEvent()->getFront()->addRouter('brands', $this);
    }

    /**
     * Get the request object
     *
     * @return Zend_Controller_Request_Http
     */
    public function getRequest()
    {
        return $this->_request;
    }


    /**
     * Validate and Match Cms Page and modify request
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request)
    {

        $this->_request = $request;


        if (($requestUri = $this->_preparePathInfo($request->getPathInfo())) === false) {
            return false;
        }


        if ($this->_match($requestUri) !== false) {
            $request->setAlias(
                Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
                $requestUri// . Mage::getStoreConfig('attributeSplash/seo/url_suffix')
            );


            $helper_catalin_seo = Mage::helper('catalin_seo');
            $identifier = ltrim($request->getPathInfo(), '/');


            $urlSplit = explode($helper_catalin_seo->getRoutingSuffix(), $identifier, 2);
            // Parse url params
            $params = explode('/', trim($urlSplit[1], '/'));
            $layerParams = array();
            $total = count($params);
            for ($i = 0; $i < $total - 1; $i++) {
                if (isset($params[$i + 1])) {
                    $layerParams[$params[$i]] = urldecode($params[$i + 1]);
                    ++$i;
                }
            }
            $layerParams += $request->getPost();
            // Add params to request
            $request->setParams($layerParams);
            if ($page) {
                $request->setParam($pageVarName, $page);
            }
            // Save params in registry - used later to generate links
            /*echo '<pre>$layerParams';
            print_r($layerParams);
            echo '</pre>';
            die();
            */Mage::register('layer_params', $layerParams);


            return true;
        }

        return false;
    }

    /**
     * Match the request against all enabled splash routes
     *
     * @return bool
     */
    protected function _match(&$requestUri)
    {
        $isDoubleBarrel = strpos($requestUri, '/') !== false;


        /*echo ITDelight_Brands_Helper_Data::URL_KEY;*/
        /*echo '<pre>';
        print_r($requestUri);
                echo '</pre>';die();*/
        if (strpos($requestUri, Webfresh_Brands_Helper_Data::URL_KEY) === false)
            return false;

        if (strpos($requestUri, 'page') !== false)
            return false;
        //   echo 'test';die();
        //if ($isDoubleBarrel) {


        list($module, $url_brand) = explode('/', $requestUri);


        if ($url_brand) {

            return $this->_loadBrand($url_brand);
        } else {


            $observer = Mage::getSingleton('Webfresh_Brands_Model_Observer');
            $observer->setBrands(true);
            $this->getRequest()
                ->setModuleName('brands')
                ->setControllerName('index')
                ->setActionName('view');
            //->setParam('category_id', $_category->getId());
            return true;
        }
        /*
        if (!$url_category) return false;
        else {


            $observer = Mage::getSingleton('ITDelight_NewProducts_Model_Observer');
            $categories_novinki = $observer->getNovinkiChilds();


        }*/


        //return $this->_loadCategory($url_category);

        // }

        /*if ($this->_loadCategory($requestUri)) {
            return true;
        }
*/
        return false;
    }

    /**
     * Prepare the path info variable
     *
     * @param string $pathInfo
     * @return false|string
     */
    protected function _preparePathInfo($pathInfo)
    {
        $requestUri = trim($pathInfo, '/');

        return $requestUri;
    }


    /**
     * Load city by city_code
     *
     * @param string $pageUrlKey
     * @param string $groupUrlKey = null
     * @return bool
     */
    protected function _loadBrand($url_key)
    {
        $url_key = urldecode($url_key);



        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $attributeModel = Mage::getModel('eav/entity_attribute');
        $id_attribute = (int)$attributeModel->loadByCode('catalog_product', 'proizvoditel')->getId();

        $select = $readConnection->select()
            ->from('eav_attribute_option_value')
            ->join(
                array('eav_attribute_option' => 'eav_attribute_option'),
                'eav_attribute_option.option_id = eav_attribute_option_value.option_id',
                array('eav_attribute_option.*')
            )
            ->join(
                array('catalin_seo_attribute' => 'catalin_seo_attribute_url_key'),
                'eav_attribute_option.option_id = catalin_seo_attribute.option_id and catalin_seo_attribute.url_value=:url_value',// and eav_attribute_option.attribute_id=:attribute_id',
                array('catalin_seo_attribute.url_key', 'catalin_seo_attribute.url_value')
            )
            ->join(
                array('brands_info' => 'brands_info'),
                'eav_attribute_option.option_id = brands_info.option_id'// and eav_attribute_option.attribute_id=:attribute_id',
                //array('brands_info.logo')
            )
            ->where('eav_attribute_option.attribute_id=:attribute_id')
            ->group('eav_attribute_option_value.option_id');

        $bind = array(
            'attribute_id' => $id_attribute,
            'url_value' => $url_key
        );

        $results = $readConnection->fetchRow($select, $bind);
        if ($results) {


            if (count($results) > 0) {
                $this->getRequest()
                    ->setModuleName('brands')
                    ->setControllerName('index')
                    ->setActionName('detail')
                    //->setParam('url_brand', $results['url_key'])
                    ->setParam('url_brand', $results['url_value'])
                    ->setParam('option_id', $results['option_id']);

                if (!Mage::registry('current_brand')) {
                    Mage::register('current_brand', $results);
                }

                $observer = Mage::getSingleton('Webfresh_Brands_Model_Observer');
                $observer->setBrands(true);


                return true;
            }

        }

        return false;
    }

}
