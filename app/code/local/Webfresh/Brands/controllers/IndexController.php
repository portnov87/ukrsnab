<?php
/**
 * @category    Fishpig
 * @package     Fishpig_AttributeSplash
 * @license     http://fishpig.co.uk/license.txt
 * @author      Ben Tideswell <help@fishpig.co.uk>
 */

class Webfresh_Brands_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Display the splash page
     *
     * @return void
     */
    public function viewAction()
    {
        //echo 'test';die();
        $this->loadLayout();
        //echo  'd';die();

        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbsBlock->addCrumb('home', array(
                'label' => Mage::helper('catalog')->__('Home'),
                'title' => Mage::helper('catalog')->__('Go to Home Page'),
                'link' => Mage::getBaseUrl()
            ));
            $breadcrumbsBlock->addCrumb('brands', array(
                'label' =>'Бренды',//Mage::helper('catalog')->__(''),
                'title' => 'Бренды',//Mage::helper('catalog')->__('Go to Home Page'),
                //'link' => '/b/'//Mage::getBaseUrl()
            ));

            if ($headBlock = $this->getLayout()->getBlock('head')) {

                $title = 'Бренды';
                $description = 'Бренд';
                $headBlock->setTitle($title);
                $headBlock->setDescription($description);

            }

        }


        $this->renderLayout();

    }

    /**
     * Display the splash page
     *
     * @return void
     */
    public function detailAction()
    {

        $this->loadLayout();



        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbsBlock->addCrumb('home', array(
                'label' => Mage::helper('catalog')->__('Home'),
                'title' => Mage::helper('catalog')->__('Go to Home Page'),
                'link' => Mage::getBaseUrl()
            ));
            $breadcrumbsBlock->addCrumb('brands', array(
                'label' =>'Бренды',//Mage::helper('catalog')->__(''),
                'title' => 'Бренды',//Mage::helper('catalog')->__('Go to Home Page'),
                'link' => '/'.Webfresh_Brands_Helper_Data::URL_KEY.'/'
            ));

             if ($current_brand = Mage::registry('current_brand')) {

                 $breadcrumbsBlock->addCrumb($current_brand['url_key'], array(
                     'label' =>$current_brand['value'],
                     'title' => $current_brand['value'],
                 ));


                 if ($headBlock = $this->getLayout()->getBlock('head')) {

                     $title = $current_brand['value'];//.' купить оптом : выгодная цена, доставка по Украине! Optomarket';
                     $description = $title;//'&#129034; Заказать обувь и аксессуары '.$current_brand['value'].' оптом в интернет-магазине 👞Optomarket 👞 Недорого&#129034;Высокое качество&#129034;Бесплатная доставка&#129034;Описание товара и отзывы&#129034; Звоните ☎ +38 068 458 88 90';


                     $title=$current_brand['meta_title'];
                     if ($title=='')
                         $title=$current_brand['name'];


                     $description=$current_brand['meta_description'];
                     if ($description=='')
                         $description=$current_brand['name'];


                     $headBlock->setTitle($title);
                     $headBlock->setDescription($description);

                 }


             }
        }

        // return json formatted response for ajax
        if ($this->getRequest()->isAjax()) { //print_r($this->getRequest()->isAjax()); die();
            $listing         = $this->getLayout()->getBlock('product_list')->toHtml();
            $layer           = $this->getLayout()->getBlock('catalog.leftnav')->toHtml();
            $head            = $this->getLayout()->getBlock('head')->toHtml();
            $metaTitle       = $this->getLayout()->getBlock('head')->getTitle();
            $metaDescription = $this->getLayout()->getBlock('head')->getDescription();
            $metaRobots      = $this->getLayout()->getBlock('head')->getRobots();

            // Fix urls that contain '___SID=U'
            $urlModel        = Mage::getSingleton('core/url');
            $listing         = $urlModel->sessionUrlVar($listing);
            $layer           = $urlModel->sessionUrlVar($layer);
            $metaTitle       = $urlModel->sessionUrlVar($metaTitle);
            $metaDescription = $urlModel->sessionUrlVar($metaDescription);
            $metaRobots      = $urlModel->sessionUrlVar($metaRobots);

            $response = array(
                'listing'         => $listing,
                'layer'           => $layer,
                'metaTitle'       => $metaTitle,
                'metaDescription' => $metaDescription,
                'metaRobots'      => $metaRobots
            );

            $this->getResponse()->setHeader('Content-Type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($response));
        } else {
            $this->renderLayout();
        }


    }



}
