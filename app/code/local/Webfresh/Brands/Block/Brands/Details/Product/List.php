<?php
/**
 * @category    Fishpig
 * @package     Fishpig_AttributeSplash
 * @license     http://fishpig.co.uk/license.txt
 * @author      Ben Tideswell <help@fishpig.co.uk>
 */

class Webfresh_Brands_Block_Brands_Details_Product_List extends Mage_Catalog_Block_Product_List
{

    /**
     * Get catalog layer model
     *
     * @return Mage_Catalog_Model_Layer
     */
    public function getLayer()
    {
        //return
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('Webfresh_Brands_Model_Catalog_Layer');//Mage::getSingleton('catalog/layer');
    }


	/**
	 * Retrieves the current layer product collection
	 *
	 * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 */

	protected function _getProductCollection()
	{


        if (is_null($this->_productCollection)) {

            //$this->_productCollection = Mage::getSingleton('Webfresh_Brands_Model_Catalog_Layer');
            $layer = $this->getLayer();//Mage::getSingleton('Webfresh_Brands_Model_Catalog_Layer');//Mage::getSingleton('Catalin_SEO_Model_Catalog_Layer');//
            //Mage::getModel('Webfresh_brands/catalog_layer');

            $params=Mage::app()->getRequest()->getParams();
            /*echo '<pre>';
            print_R(Mage::app()->getRequest()->getParams());
            echo '</pre>';die();
            */
            if (isset($params['cat']))
            {
                $this->_productCollection = $layer
                    //->setCurrentCategory($params['cat'])
                    ->getProductCollection();

                //$this->setCategoryId($params['cat']);

            }else{

                $this->_productCollection = $layer
                    //->setCurrentCategory(2)
                    ->getProductCollection();
                //$this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
                //->getSize();

            }

            //$this->prepareSortableFieldsByCategory($layer->getCurrentCategory());



            if ($orders = Mage::getSingleton('catalog/config')->getAttributeUsedForSortByArray()) {
                if (isset($orders['position'])) {
                    unset($orders['position']);
                }

                $this->setAvailableOrders($orders);

                if (!$this->getSortBy()) {
                    $category = Mage::getModel('catalog/category')->setStoreId(
                        Mage::app()->getStore()->getId()
                    );

                    $this->setSortBy($category->getDefaultSortBy());
                }
            }
        }
        return $this->_productCollection;

	}
}
