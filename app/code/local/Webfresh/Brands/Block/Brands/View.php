<?php
/**
 * @category    Fishpig
 * @package     Fishpig_AttributeSplash
 * @license     http://fishpig.co.uk/license.txt
 * @author      Ben Tideswell <help@fishpig.co.uk>
 */

class Webfresh_Brands_Block_Brands_View extends Mage_Core_Block_Template //Mage_Catalog_Block_Category_View //
{
    /**
     * Adds the META information to the resulting page
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        return $this;
    }




    public function getBrands()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $attributeModel = Mage::getModel('eav/entity_attribute');
        $id_attribute=(int)$attributeModel->loadByCode('catalog_product', 'manufacturer')->getId();


        $select = $readConnection->select()
            ->from('eav_attribute_option_value')
            ->join(
                array('eav_attribute_option' => 'eav_attribute_option'),
                'eav_attribute_option.option_id = eav_attribute_option_value.option_id',
                array('eav_attribute_option.*')
            )
            ->join(
                array('catalin_seo_attribute' => 'catalin_seo_attribute_url_key'),
                'eav_attribute_option.option_id = catalin_seo_attribute.option_id',// and eav_attribute_option.attribute_id=:attribute_id',
                array('catalin_seo_attribute.url_key', 'catalin_seo_attribute.url_value')
            )
            ->where('eav_attribute_option.attribute_id=:attribute_id')
        ->group('eav_attribute_option_value.option_id');

        $bind = array(
            'attribute_id' => $id_attribute
        );

        $results = $readConnection->fetchAll($select, $bind);

        $results_array=[];
        foreach ($results as $res){
            $first_letter=mb_substr ( $res['value'] , 0, 1);
            $results_array[$first_letter][]=$res;
        }

        ksort($results_array);

        $result_ar=[];
        foreach ($results_array as $key=> $res){
            /*usort($res, function($a,$b){
                return ($a['value']-$b['value']);
            });
*/
            $result_ar[$key]=$this->customMultiSort($res,'value');
        }


        return $result_ar;//$results_array;
    }

    /**
     * Сортируем многомерный массив по значению вложенного массива
     * @param $array array многомерный массив который сортируем
     * @param $field string название поля вложенного массива по которому необходимо отсортировать
     * @return array отсортированный многомерный массив
     */
    protected function customMultiSort($array,$field) {
        $sortArr = array();
        foreach($array as $key=>$val){
            $sortArr[$key] = $val[$field];
        }

        array_multisort($sortArr,$array);

        return $array;
    }
}
