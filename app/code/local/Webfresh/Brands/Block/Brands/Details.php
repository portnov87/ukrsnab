<?php
/**
 * @category    Fishpig
 * @package     Fishpig_AttributeSplash
 * @license     http://fishpig.co.uk/license.txt
 * @author      Ben Tideswell <help@fishpig.co.uk>
 */

class Webfresh_Brands_Block_Brands_Details extends Mage_Core_Block_Template //Mage_Catalog_Block_Category_View //
{
    /**
     * Adds the META information to the resulting page
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        return $this;
    }


    public function blockNoProducts()
    {
       return '';
    }
    public function is_count_products()
    {
        $product_layer = Mage::getSingleton('Webfresh_Brands_Model_Catalog_Layer')->getProductCollection();
        return count($product_layer);
    }


    public function getBrand()
    {
        if ($current_brand=Mage::registry('current_brand')) {
            //Mage::register('current_brand', $results);
            return $current_brand;
        }
        return false;
    }
    /**
     * Retrieves and renders the product list block
     *
     * @return string
     */
    public function getProductListHtml()
    {
        return $this->getProductListBlock()->toHtml();
    }

    /**
     * Retrieve the product list block
     *
     * @return Mage_Catalog_Block_Product_List
     */
    public function getProductListBlock()
    {
        if ($block = $this->getChild('product_list')) {
            if (!$block->hasColumnCount()) {
                $block->setColumnCount(2);
            }

            return $block;
        }

        return false;
    }

    /**
     * Retrieve the number of products per row
     *
     * @return int
     */
   /* public function getSplashPageProductsPerRow()
    {
        return Mage::getStoreConfig('attributeSplash/page/column_count');
    }*/

    /**
     * Retrieves the HTML for the CMS block
     *
     * @return string
     */
    /* public function getCmsBlockHtml()
    {
        if (!$this->getData('cms_block_html')) {
            $html = $this->getLayout()->createBlock('cms/block')
                ->setBlockId($this->getSplashPage()->getCmsBlock())->toHtml();

            $this->setData('cms_block_html', $html);
        }

        return $this->getData('cms_block_html');
    }*/
}
