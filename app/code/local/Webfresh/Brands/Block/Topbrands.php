<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 13.11.2017
 * Time: 18:04
 */


class Webfresh_Brands_Block_Topbrands extends Mage_Core_Block_Template
{
    protected $_data_attribute=false;
    public function _prepareLayout()
    {

        // $this->setTemplate('webfresh/finer/home/advantages.phtml');//webfresh/feedback/recept.phtml');

        return parent::_prepareLayout();
    }



    public function getBrands()
    {
        $data=$this->getDataAttribute();
        if (isset($data['brands'])){
            return $data['brands'];
        }
        return [];
    }
    public function getAlphabet(){

        $data=$this->getDataAttribute();
        if (isset($data['alphabet'])){
            return $data['alphabet'];
        }
        return [];
    }
    public function getDataAttribute()
    {
        /*echo '<pre>$_data';
        print_r($this->_data_attribute);
        echo '</pre>';
        die();*/
        if ($this->_data_attribute)
            return $this->_data_attribute;

        $_data=Mage::helper('brands')->getBrands();


        $this->_data_attribute=$_data;
        return $this->_data_attribute;

    }
    protected function _toHtml()
    {

        return parent::_toHtml();
    }
}
