<?php

/* * ****************************************************
 * Package   : Brand
 * Author    : HIEPNH
 * Copyright : (c) 2015
 * ***************************************************** */
?>
<?php

class Webfresh_Brands_Block_Adminhtml_Brand extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_brand';
        $this->_blockGroup = 'brands';
        $this->_headerText = Mage::helper('brands')->__('Manage Brands');
        $this->_addButtonLabel = Mage::helper('brands')->__('Add New Brand');
        $this->_addButton('import', array(
            'label' => 'Synhronyze',
            'onclick' => 'setLocation(\'' . $this->getSynchronizationUrl() . '\')',
            'class' => 'scalable',
        ));
        /*$this->_addButton('refresh', array(
            'label' => 'Refresh Url Rewrite',
            'onclick' => 'setLocation(\'' . $this->getRefreshUrl() . '\')',
            'class' => 'scalable',
        ));*/
        parent::__construct();
    }

    public function getSynchronizationUrl() {
        return $this->getUrl('*/*/synchronization');
    }


}
