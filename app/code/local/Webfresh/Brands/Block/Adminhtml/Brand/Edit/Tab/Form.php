<?php

/* * ****************************************************
 * Package   : Brand
 * Author    : HIEPNH
 * Copyright : (c) 2015
 * ***************************************************** */
?>
<?php

class Webfresh_Brands_Block_Adminhtml_Brand_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('brand_form', array('legend' => Mage::helper('brands')->__('Item information')));

        $fieldset->addField('name', 'text', array(
            'label' => Mage::helper('brands')->__('name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'name',
        ));


        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'proizvoditel');
        $options=$attribute->getSource()->getAllOptions(false);

        $options_all=[];
        $options_all[0]=['value'=>0,'label'=>'Выберите произовдителя'];//'Выберите произовдителя';

        foreach  ($options as $key=>$options){
            $options_all[]=$options;
        }
        /*echo  '<pre>';
        print_R($options_all);
        echo  '</pre>';
*/
        $fieldset->addField('option_id', 'select', array(
            'name' => 'option_id',
            'label' => 'Производитель',
            'title' => 'Производитель',
            'values' => $options_all,
            'required' => false,
        ));


        /*$fieldset->addField('url_key', 'text', array(
            'label' => Mage::helper('brands')->__('URL Key'),
            'required' => false,
            'name' => 'url_key',
        ));*/

        $fieldset->addField('logo', 'image', array(
            'label' => Mage::helper('brands')->__('Logo'),
            'required' => false,
            'name' => 'logo',
        ));

        /*$fieldset->addField('image', 'image', array(
            'label' => Mage::helper('brands')->__('Image'),
            'required' => false,
            'name' => 'image',
        ));*/

        $fieldset->addField('description', 'editor', array(
            'name' => 'description',
            'label' => Mage::helper('brands')->__('Description'),
            'title' => Mage::helper('brands')->__('Description'),
            'style' => 'width:700px; height:250px;',
            'wysiwyg' => true,
            'required' => false,
        ));
        $fieldset->addField('meta_h1', 'text', array(
            'label' => Mage::helper('brands')->__('meta_h1'),
            'required' => false,
            'name' => 'meta_h1',
        ));

        $fieldset->addField('meta_title', 'text', array(
            'label' => Mage::helper('brands')->__('meta title'),
            'required' => false,
            'name' => 'meta_title',
        ));

        $fieldset->addField('meta_description', 'textarea', array(
            'name' => 'meta_description',
            'label' => Mage::helper('brands')->__('Meta Description'),
            'title' => Mage::helper('brands')->__('Meta Description'),
            'required' => false,
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('stores', 'multiselect', array(
                'label' => Mage::helper('brands')->__('Store View'),
                'title' => Mage::helper('brands')->__('Store View'),
                'required' => true,
                'name' => 'stores[]',
                'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
                'value' => 'stores'
            ));
        } else {
            $fieldset->addField('stores', 'hidden', array(
                'name' => 'stores[]',
                'value' => Mage::app()->getStore(true)->getId()
            ));
        }

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('brands')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('brands')->__('Enabled'),
                ),
                array(
                    'value' => 2,
                    'label' => Mage::helper('brands')->__('Disabled'),
                ),
            ),
        ));

        $fieldset->addField('show_main', 'select', array(
            'label' => Mage::helper('brands')->__('show_main'),
            'name' => 'show_main',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('brands')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('brands')->__('No'),
                ),
            ),
        ));



        if (Mage::getSingleton('adminhtml/session')->getBrandData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getBrandData());
            Mage::getSingleton('adminhtml/session')->setBrandData(null);
        } elseif (Mage::registry('brand_data')) {
            $form->setValues(Mage::registry('brand_data')->getData());
        }
        return parent::_prepareForm();
    }

}
