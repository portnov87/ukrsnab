<?php
/**
 * Created by PhpStorm.
 * User: �����
 * Date: 17.03.2015
 * Time: 14:51
 */

class Webfresh_Brands_Model_Observer
{
    /**
     * @var bool
     */
    protected $_brands = false;

    /**
     * @var int Previous Category ID
     */
    protected $_previousCategory;

    /**
     * @return int
     */
    public function getPreviousCategory()
    {
        return $this->_previousCategory;
    }

    /**
     * @param int $previousCategory
     */
    public function setPreviousCategory($previousCategory)
    {
        $this->_previousCategory = $previousCategory;
    }

    /**
     * @return boolean
     */
    public function isBrandsPage()
    {
        return $this->_brands;
    }

    /**
     * @param boolean $newProducts
     */
    public function setBrands($brands)
    {
        $this->_brands = $brands;
    }

    /**
     * @param $observer Varien_Event_Observer
     */
    public function setHeaderData(Varien_Event_Observer $observer)
    {
        return;

    }


    /**
     * @param $event Varien_Event_Observer
     */
    public function resetCategory($event)
    {
        /** @var $currentCategory Mage_Catalog_Model_Category */

        $currentCategory = $event->getCategory();

        if ($currentCategory->getUrlKey() != Webfresh_Brands_Helper_Data::CATEGORY_URL_KEY) {
            return;
        }

        $this->setBrands(true);

        $this->setPreviousCategory($currentCategory->getId());

        $category = Mage::getModel('catalog/category')->load(
            Mage::getModel('core/store')->load(Mage_Core_Model_App::DISTRO_STORE_ID)
                ->getRootCategoryId()
        );

        Mage::unregister('current_category');
        Mage::unregister('current_entity_key');

        Mage::register('current_category', $category);
        Mage::register('current_entity_key', $category->getPath());
    }



	/*
    public function catalogProductCollectionLoadBefore($observer){

        if ($current_brand=Mage::registry('current_brand')) {


            $adapter = Mage::getSingleton('core/resource')
                ->getConnection('core_read');
            $store_id = 1;//$current_brand['store_id'];


            $collection = $observer->getCollection();
            $attribute_id =$current_brand['attribute_id'];
            $option_id =$current_brand['option_id'];
            // echo $option_id;die();



            $alias = 'brand_index';
            $table=Mage::getSingleton('core/resource')
                ->getTableName('catalog/product_index_eav');

            $collection->addAttributeToSelect('*');
            $collection->getSelect()
                //->select('*')
                ->join(
                    array($alias => $table),
                    "`{$alias}`.`entity_id` = `e`.`entity_id`"
                    . $adapter->quoteInto(" AND `{$alias}`.`attribute_id` = ? ", $attribute_id)
                    . $adapter->quoteInto(" AND `{$alias}`.`store_id` = ? ", $store_id)
                    . $adapter->quoteInto(" AND `{$alias}`.`value` = ?",$option_id),
                    ''
                );
        }
    }*/
}