<?php
/**
 * Created by PhpStorm.
 * User: �����
 * Date: 20.03.2015
 * Time: 12:53
 */

class Webfresh_Brands_Model_Catalog_Layer extends Catalin_SEO_Model_Catalog_Layer
{



    public function prepareProductCollection($collection)
    {
        //echo 'test';
        //die();
        $adapter = Mage::getSingleton('core/resource')
            ->getConnection('core_read');
        $store_id=1;
        if ($current_brand=Mage::registry('current_brand')) {

            $attribute_id =$current_brand['attribute_id'];
            $option_id =$current_brand['option_id'];
            // echo $option_id;die();

            $alias = 'manufacturer_index';
            $table=Mage::getSingleton('core/resource')
                ->getTableName('catalog/product_index_eav');

            $collection->addAttributeToSelect('*');
            $collection->getSelect()
                //->select('*')
                ->join(
                    array($alias => $table),
                    "`{$alias}`.`entity_id` = `e`.`entity_id`"
                    . $adapter->quoteInto(" AND `{$alias}`.`attribute_id` = ? ", $attribute_id)
                    . $adapter->quoteInto(" AND `{$alias}`.`store_id` = ? ", $store_id)
                    . $adapter->quoteInto(" AND `{$alias}`.`value` = ?",$option_id),
                    ''
                );

            //die();
        }


        return $this;
    }


    public function getProductCollection()
    {


        $adapter = Mage::getSingleton('core/resource')
            ->getConnection('core_read');
        $store_id = 1;//$current_brand['store_id'];
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId($store_id);


        if ($current_brand=Mage::registry('current_brand')) {
            $option_id =$current_brand['option_id'];
            if (isset($this->_productCollections[$this->getCurrentCategory()->getId().'_'.$option_id])) {
                $collection = $this->_productCollections[$this->getCurrentCategory()->getId().'_'.$option_id];
            } else {
                $collection = $this->getCurrentCategory()->getProductCollection();
                $this->prepareProductCollection($collection);
                $this->_productCollections[$this->getCurrentCategory()->getId().'_'.$option_id] = $collection;
            }

        }else{
            if (isset($this->_productCollections[$this->getCurrentCategory()->getId()])) {
                $collection = $this->_productCollections[$this->getCurrentCategory()->getId()];
            } else {
                $collection = $this->getCurrentCategory()->getProductCollection();
                $this->prepareProductCollection($collection);
                $this->_productCollections[$this->getCurrentCategory()->getId()] = $collection;
            }
        }

        $collection->addFinalPrice();


        return $collection;

    }
/*
    protected function prepareCollection($collection)
    {
        $request = Mage::app()->getRequest();//->getParam

        $brand = $request->getParam('url_brand');

        if ($brand) {



            $attributeModel = Mage::getModel('eav/entity_attribute');
            $id_attribute = (int)$attributeModel->loadByCode('catalog_product', 'brand')->getId();

            $collection->getSelect()
                ->join(
                    array($alias => $this->getTable('catalog/product_index_eav')),
                    "`{$alias}`.`entity_id` = `e`.`entity_id`"
                    . $this->_getReadAdapter()->quoteInto(" AND `{$alias}`.`attribute_id` = ? ", $page->getAttributeId())
                    . $this->_getReadAdapter()->quoteInto(" AND `{$alias}`.`store_id` = ? ", $page->getStoreId())
                    . $this->_getReadAdapter()->quoteInto(" AND `{$alias}`.`value` = ?", $page->getOptionId()),
                    ''
                );


            $collection
                ->getSelect()
                ->joinInner(
                    array('product_entity_int' => 'catalog_product_entity_int'),
                    "`product_entity_int`.`entity_id` = `e`.`entity_id`"
                    . " and `product_entity_int`.`store_id`=0 and `product_entity_int`.`attribute_id` = " . $id_attribute
                    . " and `product_entity_int`.`value` is not null"
                //. " and `product_entity_int`.`value` = `option`.`option_id`"
                )

                ->joinInner(
                    array('option_value' => 'eav_attribute_option_value'),
                    "`option_value`.`option_id` = `product_entity_int`.`value`"
                    . " and `option_value`.`store_id` = 0"
                    . " and `option_value`.`value` = '{$brand}'"
                )
            ;
        }


        return $collection;
    }*/
}