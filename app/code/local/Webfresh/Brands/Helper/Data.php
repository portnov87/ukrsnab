<?php

class Webfresh_Brands_Helper_Data extends Mage_Core_Helper_Abstract
{
    const URL = 'brands';
    const CATEGORY_URL_KEY = 'brands';
    const URL_KEY = 'brands';
    const NAME = 'Brands';
    const TEMPLATE = 'two_columns_left';

    public function getArrayBrands($limit=false)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $attributeModel = Mage::getModel('eav/entity_attribute');
        $id_attribute = (int)$attributeModel->loadByCode('catalog_product', 'proizvoditel')->getId();

        $select = $readConnection->select()
            ->from('eav_attribute_option_value')
            ->join(
                array('eav_attribute_option' => 'eav_attribute_option'),
                'eav_attribute_option.option_id = eav_attribute_option_value.option_id',
                array('eav_attribute_option.*')
            )
            ->join(
                array('catalin_seo_attribute' => 'catalin_seo_attribute_url_key'),
                'eav_attribute_option.option_id = catalin_seo_attribute.option_id',// and eav_attribute_option.attribute_id=:attribute_id',
                array('catalin_seo_attribute.url_value')
            )
            ->join(
                array('brands_info' => 'brands_info'),
                'eav_attribute_option.option_id = brands_info.option_id',// and eav_attribute_option.attribute_id=:attribute_id',
                array('brands_info.logo')
            )
            ->where('eav_attribute_option.attribute_id=:attribute_id   AND brands_info.status=1  AND  brands_info.show_main=1')
            ->group('eav_attribute_option_value.option_id');


        if ($limit) {
            $select->limit($limit);
            //echo '$limit.'.$limit;
            // $select->offset(0);
        }
        $bind = array(
            'attribute_id' => $id_attribute
        );

        $results = $readConnection->fetchAll($select, $bind);
        return  $results;
    }

    public function getArrayBrandsTop($limit=false)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $attributeModel = Mage::getModel('eav/entity_attribute');
        $id_attribute = (int)$attributeModel->loadByCode('catalog_product', 'manufacturer')->getId();

        $select = $readConnection->select()
            ->from('eav_attribute_option_value')
            ->join(
                array('eav_attribute_option' => 'eav_attribute_option'),
                'eav_attribute_option.option_id = eav_attribute_option_value.option_id',
                array('eav_attribute_option.*')
            )
            ->join(
                array('catalin_seo_attribute' => 'catalin_seo_attribute_url_key'),
                'eav_attribute_option.option_id = catalin_seo_attribute.option_id',// and eav_attribute_option.attribute_id=:attribute_id',
                array('catalin_seo_attribute.url_value')
            )
            /*->join(
                array('brands_info' => 'brands_info'),
                'eav_attribute_option.option_id = brands_info.option_id',// and eav_attribute_option.attribute_id=:attribute_id',
                array('brands_info.logo')
            )*/
            ->where('eav_attribute_option.attribute_id=:attribute_id')
            ->group('eav_attribute_option_value.option_id');


        if ($limit) {
            $select->limit($limit);
            //echo '$limit.'.$limit;
            // $select->offset(0);
        }
        $bind = array(
            'attribute_id' => $id_attribute
        );

        $results = $readConnection->fetchAll($select, $bind);
        return  $results;
    }

        /**
     * @return array
     */
    public function getBrands($limit=false)
    {
        /*$resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $attributeModel = Mage::getModel('eav/entity_attribute');
        $id_attribute=(int)$attributeModel->loadByCode('catalog_product', 'manufacturer')->getId();

        $select = $readConnection->select()
            ->from('eav_attribute_option_value')
            ->join(
                array('eav_attribute_option' => 'eav_attribute_option'),
                'eav_attribute_option.option_id = eav_attribute_option_value.option_id',
                array('eav_attribute_option.*')
            )
            ->join(
                array('catalin_seo_attribute' => 'catalin_seo_attribute_url_key'),
                'eav_attribute_option.option_id = catalin_seo_attribute.option_id',// and eav_attribute_option.attribute_id=:attribute_id',
                array('catalin_seo_attribute.url_value')
                //array('catalin_seo_attribute.value')
            )
            ->where('eav_attribute_option.attribute_id=:attribute_id')
            ->group('eav_attribute_option_value.option_id');


        if ($limit){
            $select->limit(0,$limit);
           // $select->offset(0);
        }
        $bind = array(
            'attribute_id' => $id_attribute
        );

        $results = $readConnection->fetchAll($select, $bind);

*/
        $results=$this->getArrayBrandsTop($limit);
        $results_array=[];
        $alphabet=[];
        foreach ($results as $res){
            $first_letter=mb_substr ( $res['value'] , 0, 1);
            $first_letter=mb_strtoupper($first_letter);
            if(preg_match('@[A-Z]@u',$first_letter)) {
                //continue;
                //}//echo 'Английские буквы есть';
                $alphabet[$first_letter] = $first_letter;
                $results_array[$first_letter][] = $res;
            }
            else{
                if(preg_match('[0-9]',$_letter)) {
                    $first_letter='0-9';
                }else{
                    $first_letter='А-Я';
                }

                $alphabet[$first_letter] = $first_letter;
                $results_array[$first_letter][] = $res;
            }
        }

            ksort($alphabet);
        ksort($results_array);

        $result_ar=[];
        foreach ($results_array as $key=> $res){
            /*usort($res, function($a,$b){
                return ($a['value']-$b['value']);
            });
*/
            $result_ar[$key]=$this->customMultiSort($res,'value');
        }


        return ['brands'=>$result_ar,'alphabet'=>$alphabet];//$results_array;
    }



    /**
     * Сортируем многомерный массив по значению вложенного массива
     * @param $array array многомерный массив который сортируем
     * @param $field string название поля вложенного массива по которому необходимо отсортировать
     * @return array отсортированный многомерный массив
     */
    protected function customMultiSort($array,$field) {
        $sortArr = array();
        foreach($array as $key=>$val){
            $sortArr[$key] = $val[$field];
        }

        array_multisort($sortArr,$array);

        return $array;
    }


    public function getAttributeOptionValue($argAttribute, $argValue) {
        $attributeModel = Mage::getModel('eav/entity_attribute');
        $attributeOptionsModel = Mage::getModel('eav/entity_attribute_source_table');
        $attributeCode = $attributeModel->getIdByCode('catalog_product', $argAttribute);
        $attribute = $attributeModel->load($attributeCode);
        $attributeOptionsModel->setAttribute($attribute);
        $options = $attributeOptionsModel->getAllOptions(false);
        foreach ($options as $option) {
            if ($option['label'] == $argValue) {
                return $option['value'];
            }
        }
        return false;
    }

    public function addAttributeOption($argAttribute, $argValue) {
        $attributeModel = Mage::getModel('eav/entity_attribute');
        $attributeOptionsModel = Mage::getModel('eav/entity_attribute_source_table');
        $attributeCode = $attributeModel->getIdByCode('catalog_product', $argAttribute);
        $attribute = $attributeModel->load($attributeCode);
        $attributeOptionsModel->setAttribute($attribute)->getAllOptions(false);
        $value['option'] = array($argValue, $argValue);
        $result = array('value' => $value);
        $attribute->setData('option', $result);
        $attribute->save();
    }

    public function editAttributeOption($argAttribute, $model){
        $attributeModel = Mage::getModel('eav/entity_attribute');
        $attributeOptionsModel = Mage::getModel('eav/entity_attribute_source_table');
        $attributeCode = $attributeModel->getIdByCode('catalog_product', $argAttribute);
        $attribute = $attributeModel->load($attributeCode);
        $attributeOptionsModel->setAttribute($attribute);
        $options = $attributeOptionsModel->getAllOptions(false);
        $values = array();
        $data = array();
        foreach ($options as $option) {
            $value = $option['value'];
            $label = $option['label'];
            if($option['value'] == $model->getOptionId()){
                $label = $model->getTitle();
            }
            $values[$value] = array($label);
        }
        $data['option']['value'] = $values;

        $attribute->addData($data);
        $attribute->save();
    }

    public function deleteAttributeOptionValue($argAttribute, $model){
        $attributeModel = Mage::getModel('eav/entity_attribute');
        $attributeOptionsModel = Mage::getModel('eav/entity_attribute_source_table');
        $attributeCode = $attributeModel->getIdByCode('catalog_product', $argAttribute);
        $attribute = $attributeModel->load($attributeCode);
        $attributeOptionsModel->setAttribute($attribute);
        $options = $attributeOptionsModel->getAllOptions(false);
        $values = array();
        $valuesDelete = array();
        $data = array();
        foreach ($options as $option) {
            $value = $option['value'];
            $label = $option['label'];
            $values[$value] = array($label);
        }

        foreach ($options as $option) {
            $value = $option['value'];
            if($value == $model->getOptionId()){
                $valuesDelete[$value] = 1;
            }else{
                $valuesDelete[$value] = '';
            }
        }
        $data['option']['value'] = $values;
        $data['option']['delete'] = $valuesDelete;

        $attribute->addData($data);
        $attribute->save();
    }
}