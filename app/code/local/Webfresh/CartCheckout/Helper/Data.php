<?php


class Webfresh_CartCheckout_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function plural_form($n, $forms)
    {
        return $n % 10 == 1 && $n % 100 != 11 ? $forms[0] : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $forms[1] : $forms[2]);
    }


    public function getDefaultCountry()
    {
        $ret = Mage::getStoreConfig('general/country/default');
        return $ret;
    }

    public function getDefaultCity()
    {
        $ret = '-';
        return $ret;
    }

    public function getDefaultPostcode()
    {
        $ret = '-';
        return $ret;
    }

    public function getDefaultShippingMethod($quote)
    {
        $ret = NULL;

        $first = NULL;
        $address = $quote->getShippingAddress();
        $address->collectShippingRates()->save();

        $_shippingRateGroups = $address->getGroupedAllShippingRates();

        foreach ($_shippingRateGroups as $code => $_rates){
            foreach ($_rates as $_rate){


                if ($first === NULL)
                    $first = $_rate->getCode();
            }
        }

        if (!$ret){
            $ret = $first;
        }

        return $ret;
    }

    protected function _canUseMethod($quote, $method)
    {
        if (!$method->canUseForCountry($quote->getBillingAddress()->getCountry())) {
            return false;
        }

        if (!$method->canUseForCurrency($quote->getStore()->getBaseCurrencyCode())) {
            return false;
        }

        /**
         * Checking for min/max order total for assigned payment method
         */
        $total = $quote->getBaseGrandTotal();
        $minTotal = $method->getConfigData('min_order_total');
        $maxTotal = $method->getConfigData('max_order_total');

        if((!empty($minTotal) && ($total < $minTotal)) || (!empty($maxTotal) && ($total > $maxTotal))) {
            return false;
        }
        return true;
    }
    protected function _getPaymentMethods($quote)
    {
        $store = $quote ? $quote->getStoreId() : null;
        $methods = Mage::helper('payment')->getStoreMethods($store, $quote);
        $total = $quote->getBaseSubtotal() + $quote->getShippingAddress()->getBaseShippingAmount();
        foreach ($methods as $key => $method) {
            if ($this->_canUseMethod($quote, $method)
                && ($total != 0
                    || $method->getCode() == 'free'
                    || ($quote->hasRecurringItems() && $method->canManageRecurringProfiles()))) {
            } else {
                unset($methods[$key]);
            }
        }

        return $methods;
    }
    public function getDefaultPeymentMethod($quote)
    {
        $ret = NULL;


        $paymentMethods = $this->_getPaymentMethods($quote);

        if ($ret === NULL && isset($paymentMethods[0]))
            $ret = $paymentMethods[0]->getCode();


        return $ret;

    }

    public function getNameShippingMethod($method, $child = '')
    {
        $name = '';
        switch ($method) {
            case 'webfresh_courierdelivery_standard':
                $name = $this->__('Courier');
                break;
            case 'webfresh_pickupdelivery_standard':
            case 'webfresh_pickupdelivery_fast_shipping':
                $name = Mage::helper('pickupdelivery')->getNamePickupmethod($method);
                break;

            case 'sy_novaposhta_type_WarehouseWarehouse':
                $name = $this->__('Novaposhta WarehouseWarehouse');
                //$name=Mage::helper('pickupdelivery')->getNamePickupmethod($method);
                break;
            case 'sy_novaposhta_type_WarehouseDoors':
                $name = $this->__('Novaposhta WarehouseDoors');
                //$name=Mage::helper('pickupdelivery')->getNamePickupmethod($method);
                break;
        }
        return $name;

    }

    public function getCurrentCityModel()
    {
        if ($city_model = Mage::registry('current_city_model')) {
            return $city_model;
        }

        $city = false;
        $current_city = Mage::helper('storelocator/data')->getCurrentCity();
        if (isset($current_city['id'])) {
            // search into base
            $city = Mage::getModel('storelocator/city')->load($current_city['id']);
        }
        return $city;
    }
}
