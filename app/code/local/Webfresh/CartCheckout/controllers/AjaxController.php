<?php

class Webfresh_CartCheckout_AjaxController extends Mage_Core_Controller_Front_Action
{
    /**
     * @param $product_id
     */
    public function setProduct($product_id)
    {
        if ($product = Mage::registry('current_product')) {
            return;// $product;
        }

        $product = Mage::getModel('catalog/product')->load($product_id);
        Mage::register('current_product', $product);
    }

    /**
     * @param $store_id
     */
    public function setStoreId($store_id)
    {
        if ($store = Mage::registry('current_store_id')) {
            return;
        }


        Mage::register('current_store_id', $store_id);
    }

    /**
     * @return bool|mixed
     */
    public function getProduct()
    {
        if ($product = Mage::registry('current_product')) {
            return $product;
        }
        return false;
    }

    /**
     *
     */
    public function getstorelocatorAction()
    {
        $city_id = $this->getRequest()->getPost("city_id", false);
        $product_id = $this->getRequest()->getPost("product_id", false);

        //$district_id = $this->getRequest()->getPost("district", false);
        $this->setProduct($product_id);
        /*$districts = Mage::helper('storelocator')->getDistrictsByCity($city_id);
        $html_districts = '<select name="district_storelocator"  id="district_storelocator">
                <option>Все</option>';

        foreach ($districts as $district):
            $html_districts .= '<option value="'.$district['district'].'">'.$district['district'].'</option>';
        endforeach;
        $html_districts .= '</select>';
        //getDistrictsByCity($city_id)
*/


        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
            "storelocators" => $this->_getStorelocatorsHtml($city_id, $district_id),
            //'districts' => $html_districts,
        )));
    }

    public function getstoresAction()
    {
        $city_id = $this->getRequest()->getPost("city_id", false);
        $product_id = $this->getRequest()->getPost("product_id", false);

        //$district_id = $this->getRequest()->getPost("district", false);
        $this->setProduct($product_id);
        /*$districts = Mage::helper('storelocator')->getDistrictsByCity($city_id);
        $html_districts = '<select name="district_storelocator"  id="district_storelocator">
                <option>Все</option>';

        foreach ($districts as $district):
            $html_districts .= '<option value="'.$district['district'].'">'.$district['district'].'</option>';
        endforeach;
        $html_districts .= '</select>';
        //getDistrictsByCity($city_id)
*/


        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
            "storelocators" => $this->_getStoresHtml($city_id),
            //'districts' => $html_districts,
        )));
    }


    /**
     *
     */
    public function pickupproductAction()
    {
        $product_id = $this->getRequest()->getPost("product_id", "");
        $store_id = $this->getRequest()->getPost("store_id", "");
        $with_map = $this->getRequest()->getPost("with_map", false);
        $this->setProduct($product_id);
        $this->setStoreId($store_id);

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
            "productpickup" => $this->_getStorelocatorsProductHtml($product_id, $store_id,$with_map),
        )));
    }

    /**
     * @param $city_id
     * @param $productsSku
     * @return mixed
     */
    public function _setProductsInStore($city_id,$productsSku)
    {
        if ($stores_with_product = Mage::registry('store_with_product'))
            return $stores_with_product;

        $stores_with_product=Mage::getModel('storelocator/products')
            ->productsInStore($city_id,$productsSku);
        Mage::register('store_with_product',$stores_with_product);
        return $stores_with_product;
    }


    /**
     *
     */
    public function pickupAction()
    {
        $product_id = $this->getRequest()->getPost("product_id", "");
        $city_id = $this->getRequest()->getPost("city_id", "");
        $this->setProduct($product_id);
        $product=$this->getProduct();
        $this->_setProductsInStore($city_id,[$product->getSku()]);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
            "pickup" => $this->_getPickupHtml($product_id, $city_id),
        )));
    }

    /**
     *
     */
    public function productshippingAction()
    {
        $product_id = $this->getRequest()->getPost("product_id", "");
        $city_id = $this->getRequest()->getPost("city_id", "");

        $this->setProduct($product_id);
        $product = $this->getProduct();

        $current_url= $this->getRequest()->getPost("current_url", "/");
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
            "shipping" => $this->_getShippingHtml($product_id, $city_id,$current_url),
        )));
    }

    /**
     * @param $product_id
     * @param $city_id
     * @param string $current_url
     * @return mixed
     */
    public function _getShippingHtml($product_id,$city_id,$current_url='/')
    {
        $this->loadLayout(array('default', 'cartcheckout_cart_ajax_shipping'));
        return $this->getLayout()
            ->getBlock('product.shipping')
            ->setData('product_id', $product_id)
            ->setData('current_url',$current_url)
            ->setData('city_id', $city_id)
        ->toHtml();
    }

    /**
     * @param $product_id
     * @param $store_id
     * @return mixed
     */
    public function _getStorelocatorsProductHtml($product_id,$store_id,$with_map=false)
    {

        $this->loadLayout(array('default', 'cartcheckout_cart_ajax_pickupproduct'));
        return $this->getLayout()
            ->getBlock('storelocator.pickupproduct')
            ->setData('product_id', $product_id)
            ->setData('store_id', $store_id)
            ->setData('with_map', $with_map)
            ->toHtml();
    }

    /**
     * @param $city_id
     * @param $district
     * @return mixed
     */
    public function _getStorelocatorsHtml($city_id, $district)
    {

        $this->loadLayout(array('default', 'cartcheckout_cart_ajax_getstorelocator'));
        return $this->getLayout()
            ->getBlock('storelocator.pickup')
            //->setData('district', $district)
            ->setData('city_id', $city_id)
            ->toHtml();
    }

    /**
     * @param $city_id
     * @param $district
     * @return mixed
     * Блок Наличие в аптеках
     */
    public function _getStoresHtml($city_id)
    {

        $this->loadLayout(array('default', 'cartcheckout_cart_ajax_stores'));
        return $this->getLayout()
            ->getBlock('shipping.pickup')
            //->setData('district', $district)
            ->setData('city_id', $city_id)
            ->toHtml();
    }

    /**
     * @param $product_id
     * @param $city_id
     * @return mixed
     */
    public function _getPickupHtml($product_id, $city_id)
    {

        $this->loadLayout(array('default', 'cartcheckout_cart_ajax_pickup'));
        return $this->getLayout()
            ->getBlock('shipping.pickup')
            ->setData('product_id', $product_id)
            ->setData('city_id', $city_id)
            ->toHtml();
    }




}