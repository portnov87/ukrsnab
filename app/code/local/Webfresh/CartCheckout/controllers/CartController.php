<?php
//require_once(Mage::getModuleDir('controllers','Mage_Checkout').DS.'CartController.php');extends Mage_Checkout_CartController//
require_once Mage::getBaseDir('code') . DS . 'core/Mage/Checkout/controllers/CartController.php';

class Webfresh_CartCheckout_CartController extends Mage_Checkout_CartController
//class Webfresh_CartCheckout_CartController extends Mage_Core_Controller_Front_Action
{

    public function getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    private function _setShippingMethod($method = false)
    {
        $quote = $this->getOnepage()->getQuote();
        $shipping = $quote->getShippingAddress();
        $shipping->setShippingMethod($method);
        if (!$method) {
            $shipping->removeAllShippingRates();
            $shipping->setShippingAmount(0)
                ->setBaseShippingAmount(0)
                ->setShippingMethod('')
                ->setShippingDescription('');

            Mage::getSingleton('checkout/session')->setSelectedShipping(false);
        } else {
            $this->_getSession()->setSelectedShipping($method);
        }
        $shipping->setCollectShippingRates(true)->save();
    }

    private function _getReviewHtml()
    {
        $this->loadLayout(array('default', 'oneclickcartcheckout_all'));
        return ($this->getLayout()->getBlock('review.info')->toHtml());
    }

    private function _getPaymentMethodsHtml()
    {
        $this->loadLayout(array('default', 'oneclickcartcheckout_all'));
        return ($this->getLayout()->getBlock('checkout.onepage.payment')->toHtml());
    }

    private function _getAgreementHtml()
    {
        $this->loadLayout(array('default', 'oneclickcartcheckout_all'));
        return ($this->getLayout()->getBlock('checkout.onepage.agreements')->toHtml());
    }

    public function saveShippingMethodAction()
    {
        if ($this->getRequest()->isPost()) {
            $shipping_method = $this->getRequest()->getPost('shipping_method', "");//'pickupshipping_pickupshipping';//
            $this->_setShippingMethod($shipping_method);
            $quote = $this->getOnepage()->getQuote();
            if ($shipping_method == 'pickupshipping_pickupshipping') {
                $shipping = $quote->getShippingAddress();
                $shipping->setData("country_id", "UA");

                $data = array('method' => 'pickuppayment');
                $payment = $this->getOnepage()->getQuote()->getPayment();
                //  $payment->importData($data);
                $quote->getShippingAddress()->setPaymentMethod('pickuppayment');//$payment->getMethod());
                //$quote->getPayment()->importData($data);
            } elseif (preg_match("/couriershipping_couriershipping/i", $shipping_method)) {
                $data = array('method' => 'cashondelivery');
                $payment = $this->getOnepage()->getQuote()->getPayment();
                $payment->importData($data);
                $quote->getShippingAddress()->setPaymentMethod($payment->getMethod());
                $quote->getPayment()->importData($data);

            } else {
            }
            $this->_getSession()->setSelectedShipping($shipping_method);
            $quote->getShippingAddress()->setCollectShippingRates(true)->save();
            die(json_encode(array(
                "shipping_method" => $this->_getShippingMethodsHtml(),
                "review" => $this->_getReviewHtml(),
                "payment" => $this->_getPaymentMethodsHtml(),
                "agreement" => $this->_getAgreementHtml()
            )));
        }
    }

    private function _getMiniCartHtml()
    {
        $this->loadLayout(array('default', 'cartcheckout_cart_updatecart'));
        return $this->getLayout()->getBlock('minicart_head')->toHtml();
    }

    private function _getCartCheckoutHtml()
    {
        $this->loadLayout(array('default', 'checkout_cart_index'));
        return $this->getLayout()->getBlock('checkout.cart')->toHtml();
    }

    private function _getCartMobileHtml()
    {
        $this->loadLayout(array('default', 'cartcheckout_cart_updatecart'));
        return $this->getLayout()->getBlock('checkout.cart.onepage.mobile')->toHtml();
    }

    private function _getCartModalHtml()
    {
        $this->loadLayout(array('default', 'cartcheckout_cart_updatecart'));
        return $this->getLayout()->getBlock('modal.sidebar')->toHtml();
    }

    private function _getCartHtml()
    {
        $this->loadLayout(array('default', 'cartcheckout_cart_updatecart'));
        return $this->getLayout()->getBlock('checkout.cart')->toHtml();
    }


    private function _getTopCartBar()
    {
        $this->loadLayout(array('default', 'oneclickcartcheckout_all'));
        $toplinks = $this->getLayout()->getBlock('cart_sidebar')->toHtml();
        return $toplinks;
    }

    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    protected function _getShippingMethodsHtml()
    {
        $this->loadLayout(array('default', 'oneclickcartcheckout_all'));
        return $this->getLayout()->getBlock('oneclickcartcheckout.shipping_methods')->toHtml();
    }


    public function updateCartAction()
    {


        $block= $this->getRequest()->getPost("block", "");
        $item_id = $this->getRequest()->getPost("item_id", "");
        $type = $this->getRequest()->getPost("type", "");

        if (!$type) {
            $this->_redirect('checkout/cart/');

        } else {



            try {

                $cart = $this->_getCart();
                if (!$cart->getCustomerSession()->getCustomer()->getId() && $cart->getQuote()->getCustomerId()) {
                    $cart->getQuote()->setCustomerId(null);
                }

                foreach ($cart->getItems() as $item) {

                    if ($item_id == $item->getProductId()) {
                        if ($type == "decrease") {
                            $item->setQty($item->getQty() - 1);
                        } else {
                            $item->setQty($item->getQty() + 1);
                        }
                        $item->save();
                        break;
                    }
                }
                $cart->save();
                $this->_getSession()->setCartWasUpdated(true);

                //$this->getOnepage()->initCheckout();
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addException($e, $this->__('Cannot update shopping cart.'));
            }

            $method = $this->getOnepage()->getQuote()->getShippingAddress()->getShippingMethod();
            if (!$method) {
                $this->_setShippingMethod(false);
            } else {
                $this->_setShippingMethod($method);
            }



            $forms=[Mage::helper('ukrsnab')->__('title_pro1'),Mage::helper('ukrsnab')->__('title_pro2'),Mage::helper('ukrsnab')->__('title_pro3')];
            $q=Mage::helper('checkout/cart')->getCart()->getItemsCount();
            $quantity=$q.' '.Mage::helper('cartcheckout')->plural_form($q, $forms);
            $summa=Mage::helper('checkout')->formatPrice(Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal());

            if ($block=='modal')
            {
                $cart = $this->_getCartModalHtml();
            }else {
                $cart = $this->_getCartHtml();
            }

            $cart_checkout=$this->_getCartCheckoutHtml();
            $mobile_cart = $this->_getCartMobileHtml();
            $minicart_head=$this->_getMiniCartHtml();
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
                "cart" => $cart,
                "mobile_cart"=>$mobile_cart,
                'quantity'=>$quantity,
                'cart_checkout'=>$cart_checkout,
                'summa'=>$summa,
                'minicart_head'=>$minicart_head

            )));
       }

    }


    public function updatePostAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {

            $this->loadLayout();
            $response = array();

            if (!$this->_validateFormKey()) {
                $this->_getSession()->addError('Неверный ключ формы');
                $response['global_message'] = $this->getLayout()->getBlock('custom_global_messages')->toHtml();
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
                return;
            }

            try {
                $cart = $this->_getCart();
                $cart->getQuote()->setTotalsCollectedFlag(false);
                $this->_getCart()->getResetSummaryQty();
                $updateAction = (string)$this->getRequest()->getParam('update_cart_action');

                switch ($updateAction) {
                    case 'empty_cart':
                        $this->_emptyShoppingCart();
                        break;
                    case 'update_qty':
                        $this->_updateShoppingCart();
                        break;
                    default:
                        $this->_updateShoppingCart();
                }

                $cart->getQuote()->setTotalsCollectedFlag(false);
                $this->_getCart()->getResetSummaryQty();


                $forms=[Mage::helper('ukrsnab')->__('title_pro1'),Mage::helper('ukrsnab')->__('title_pro2'),Mage::helper('ukrsnab')->__('title_pro3')];
                $q=Mage::helper('checkout/cart')->getCart()->getItemsCount();
                $quantity=$q.' '.Mage::helper('cartcheckout')->plural_form($q, $forms);
                $summa=Mage::helper('checkout')->formatPrice(Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal());

                if ($block=='modal')
                {
                    $cart = $this->_getCartModalHtml();
                }else {
                    $cart = $this->_getCartHtml();
                }

                $cart_checkout=$this->_getCartCheckoutHtml();
                $mobile_cart = '';//$this->_getCartMobileHtml();
                $minicart_head='';//$this->_getMiniCartHtml();

                $response=array(
                    "cart" => $cart,
                    "mobile_cart"=>$mobile_cart,
                    'quantity'=>$quantity,
                    'cart_checkout'=>$cart_checkout,
                    'summa'=>$summa,
                    'minicart_head'=>$minicart_head

                );
                /*$this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
                    "cart" => $cart,
                    "mobile_cart"=>$mobile_cart,
                    'quantity'=>$quantity,
                    'cart_checkout'=>$cart_checkout,
                    'summa'=>$summa,
                    'minicart_head'=>$minicart_head

                )));
                */


                /*
                $response['minicart'] = $this->getLayout()->getBlock('minicart_head')->toHtml();
                $response['cart_popup'] = $this->getLayout()->getBlock('cart_popup')->toHtml();
                $cartBlock = $this->getLayout()->createBlock('checkout/cart')->setTemplate('checkout/cart.phtml');
                $response['updated_cart'] = $cartBlock->toHtml();
                $cartTotalsBlock = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml');
                $block_bonus_banner=$this->getLayout()->createBlock('core/template')->setTemplate('onestepcheckout/onepage/bonus_banner.phtml')->toHtml();

                $response['updated_cart_totals'] =$cartTotalsBlock->toHtml();
                $response['bonus_banner'] =$block_bonus_banner;


                $cartCouponBlock = $this->getLayout()->createBlock('checkout/cart_coupon')->setTemplate('checkout/cart/coupon.phtml');
                $response['updated_cart_coupon'] = $cartCouponBlock->toHtml();
                $productsCount = 0;
                if($this->_getCart()->getSummaryQty()){
                    foreach(Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems() as $item){
                        $productsCount += $item->getQty();
                    }
                }
                $response['updated_cart_text'] = $productsCount.' товаров на сумму '.Mage::helper('core')->currency(Mage::helper('checkout/cart')->getQuote()->getSubtotal(), true, false);
*/
            } catch (Mage_Core_Exception $e) {
                if ($this->_getSession()->getUseNotice(true)) {
                    $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                    }
                }

                $url = $this->_getSession()->getRedirectUrl(true);
                if ($url) {
                    $this->getResponse()->setRedirect($url);
                } else {
                    $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
                }
                $response['global_message'] = $this->getLayout()->getBlock('custom_global_messages')->toHtml();
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
                return;
            } catch (Exception $e) {
                $this->_getSession()->addException($e, $this->__('Cannot update shopping cart.'));
                Mage::logException($e);
                $response['global_message'] = $this->getLayout()->getBlock('custom_global_messages')->toHtml();
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
                return;
            }

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            return;

        }else{
            parent::updatePostAction();
        }
    }


    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    public function saveOrderAction()
    {
        if (Mage::helper("checkout/cart")->getItemsCount() != 0):
            $result = $this->getOnepage()->savePayment(
                $this->getRequest()->getPost('payment', array())
            );

            $billing_data = $this->getRequest()->getPost("billing", array());
            $shipping_data = $this->getRequest()->getPost("shipping", array());
            $pickupStatus = $this->getRequest()->getPost("customer_pickup");
            //echo $pickupStatus
            $quote = $this->getOnepage()->getQuote();
            $shipping = $quote->getShippingAddress();
            $billing = $quote->getBillingAddress();
            $ship_address = $shipping;
            if ($this->getRequest()->getPost("register") == "on") {
                $this->getOnepage()->saveCheckoutMethod("register");
            }

            $customerBillingAddressId = $this->getRequest()->getPost("billing_address_id", false);
            $customerShippingAddressId = $this->getRequest()->getPost("shipping_address_id", false);

            $user = Mage::getSingleton('customer/session')->getCustomer();
            $email = $user->getEmail();
            $address = $shipping;
            if (!$email) $email = trim($billing_data["email"]);

            if ($customerBillingAddressId) {

                $address = $shipping;//$this->getOnepage()->getQuote()->getShippingAddress();//Mage::getModel('customer/address')->load($customerBillingAddressId);
                $billing_data["firstname"] = $address->getFirstname();
                $billing_data["lastname"] = $address->getLastname();
                $billing_data["email"] = $email;
                $billing_data["company"] = $address->getCompany();
                $billing_data["city"] = $address->getCity();
                $billing_data["street"] = array($address->getStreet());
                $billing_data["region"] = '';//$address->getRegion();
                $billing_data["region_id"] = '';//$address->getRegionId();
                $billing_data["country_id"] = $address->getCountryId();
                $billing_data["postcode"] = $address->getPostcode();
                $billing_data["telephone"] = $address->getTelephone();
            }
            $ship_address = $shipping;
            if ($customerShippingAddressId && !$pickupStatus) {

                //$this->getOnepage()->getQuote()->getShippingAddress();//Mage::getModel('customer/address')->load($customerShippingAddressId);
                //$shipping = $this->getOnepage()->getQuote()->getShippingAddress();
                $shipping_data["firstname"] = $ship_address->getFirstname();
                $shipping_data["lastname"] = $ship_address->getLastname();
                $shipping_data["company"] = $ship_address->getCompany();
                $shipping_data["city"] = $ship_address->getCity();
                $shipping_data["street"] = array($ship_address->getStreet());
                $shipping_data["region"] = '';//$ship_address->getRegion();
                $shipping_data["region_id"] = '';//$ship_address->getRegionId();
                $shipping_data["country_id"] = $ship_address->getCountryId();
                $shipping_data["postcode"] = $ship_address->getPostcode();
                $shipping_data["telephone"] = $ship_address->getTelephone();
            }

            if ($pickupStatus) {
                $billing_data["email"] = $email;
                $billing_data["country_id"] = "UA";
                $billing_data["region"] = '';//"-";
                $billing_data["region_id"] = '';//"-";
                $billing_data["city"] = $ship_address->getCity();//"-";
                $billing_data["postcode"] = "-";
                $billing_data["street"] = array("-");
                $billing_data["use_for_shipping"] = "false";
                $this->getOnepage()->saveBilling($billing_data, $customerBillingAddressId);

                $shipping_data["firstname"] = $billing_data["firstname"];
                $shipping_data["lastname"] = $billing_data["lastname"];
                $shipping_data["telephone"] = $billing_data["telephone"];
                $shipping_data["city"] = $ship_address->getCity();
                $shipping_data["street"] = array('-');
                $shipping_data["region"] = '';//'-';
                $shipping_data["region_id"] = '';//'-';
                $shipping_data["country_id"] = 'UA';
                $shipping_data["postcode"] = '-';
                $this->getOnepage()->saveShipping($shipping_data, $customerShippingAddressId);
            } else {
                $billing_data["email"] = $email;
                if (@!$billing_data["country_id"]) {
                    $billing_data["country_id"] = "UA";
                }
                if (@!$billing_data["region"]) {
                    $billing_data["region"] = "";
                }
                if (@!$billing_data["region_id"]) {
                    $billing_data["region_id"] = "";
                }
                if (@!$billing_data["city"]) {
                    $billing_data["city"] = $ship_address->getCity();
                }
                if (@!$billing_data["postcode"]) {
                    $billing_data["postcode"] = "-";
                }
                if (@!$billing_data["telephone"]) {
                    $billing_data["telephone"] = "-";
                }
                if (@!$billing_data["street"] || empty($billing_data["street"][0])) {
                    $billing_data["street"] = array("-", "-");
                }

                if (@!$shipping_data["country_id"]) {
                    $shipping_data["country_id"] = "UA";
                }
                if (@!$shipping_data["region"]) {
                    $shipping_data["region"] = "";
                }
                if (@!$shipping_data["region_id"]) {
                    $shipping_data["region_id"] = "";
                }
                if (@!$shipping_data["city"]) {
                    $shipping_data["city"] = $ship_address->getCity();
                }
                if (@!$shipping_data["postcode"]) {
                    $shipping_data["postcode"] = "-";
                }
                if (@!$shipping_data["telephone"]) {
                    $shipping_data["telephone"] = "-";
                }
                if (@!$shipping_data["street"] || empty($shipping_data["street"][0])) {
                    $shipping_data["street"] = array("-", "-");
                }

                $this->getOnepage()->saveBilling($billing_data, $customerBillingAddressId);

                if ($billing_data["use_for_shipping"] == "true") {
                    $this->getOnepage()->saveShipping($billing_data, $customerShippingAddressId);
                } else {
                    $this->getOnepage()->saveShipping($shipping_data, $customerShippingAddressId);
                }
            }


            $result = array();
            try {
                if ($requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds()
                    && preg_match("/couriershipping_couriershipping/i", $this->getRequest()->getPost('shipping_method', ''))
                ) {
                    if (!empty($requiredAgreements) && !is_array($requiredAgreements)) $requiredAgreements = array($requiredAgreements);
                    $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
                    if ($diff = array_diff($requiredAgreements, $postedAgreements)) {
                        $result['success'] = false;
                        $result['error'] = true;
                        $result['error_messages'] = $this->__('Please agree to all the terms and conditions before placing the order.');
                        $this->_getSession()->addError($this->__('Please agree to all the terms and conditions before placing the order.'));
                    }
                }


                if ($data = $this->getRequest()->getPost('payment', false)) {
                    $payment = $quote->getPayment();
                    $payment->importData($data);
                    $shipping->setPaymentMethod($payment->getMethod());
                    $quote->getPayment()->importData($data);

                }

                $redirectUrl = $quote->getPayment()->getCheckoutRedirectUrl();
                if ($redirectUrl) {
                    header("Location: " . $redirectUrl);
                    die($redirectUrl);
                }

                $data = $this->getRequest()->getPost('shipping_method', '');

                $result = $this->getOnepage()->saveShippingMethod($data);

                $note = htmlspecialchars($this->getRequest()->getPost('customer_comment', ""));
                if ($this->getRequest()->getPost('pdd', null)) {
                    $note .= "<br/>" . Mage::helper('oneclickcartcheckout')->__('Preferred Delivery Date') . ": <b>" . $this->getRequest()->getPost('pdd', null) . "</b>";
                }
                $pickup = $this->getRequest()->getPost('pickup', array());
                if ($pickup['storelocation']) {
                    $store = Mage::getModel('storelocator/storelocator')->load($pickup['storelocation']);

                }

                $quote->setCustomerNoteNotify(1)->setCustomerNote($note);
                $or = $this->getOnepage()->saveOrder();
                //$quote->save();
                $order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastOrderId());
                $order->setCustomerNoteNotify(1)->setCustomerNote($note);
                //if (!empty($pickup) && isset($pickup['storelocation']) && !empty($pickup['storelocation_value']))
                if (!empty($pickup)) {
                    //$shipping_description = htmlspecialchars('Самовывоз. <br/>'.$store->title.'<br/>'.$store->description);
                    $shipping_description = Mage::helper('oneclickcartcheckout')->__('Pickup in the store (%s) -', $store->title . '<br/>' . $store->description);//$pickup['storelocation_value']);
                    $order->setShippingDescription($shipping_description);
                }
                $order->save();

                $redirectUrl = $this->getOnepage()->getCheckout()->getRedirectUrl();
                $result['success'] = true;
                $result['error'] = false;
            } catch (Mage_Core_Exception $e) {

                Mage::helper('checkout')->sendPaymentFailedEmail($quote, $e->getMessage());
                $result['success'] = false;
                $result['error'] = true;
                $result['error_messages'] = $e->getMessage() . "";
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::helper('checkout')->sendPaymentFailedEmail($quote, $e->getMessage());
                $result['success'] = false;
                $result['error'] = true;
                $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
                $this->_getSession()->addError($this->__('There was an error processing your order. Please contact us or try again later.'));
            }

            if ($result['error'] == true) {
                $this->_getSession()->setCustomerData($this->getRequest()->getPost(null, array()));
                $this->getResponse()->setRedirect(Mage::getBaseUrl() . "checkout/onepage");
            } else {
                $this->_getSession()->setCustomerData(null);
                $quote->save();
                $this->getResponse()->setRedirect(Mage::getBaseUrl() . "checkout/onepage/success");
            }
        else:
            $this->_getSession()->addSuccess($this->__('You havent any items in your cart'));
            $this->getResponse()->setRedirect("/");
        endif;
    }

    protected function _getQuote()
    {
        return $this->_getCart()->getQuote();
    }


    public function useCouponAction()
    {
        if (!$this->_getCart()->getQuote()->getItemsCount()) {
            return;
        }

        $couponCode = (string)$this->getRequest()->getParam('coupon');
        if ($this->getRequest()->getParam('remove_coupon') == 1) {
            $couponCode = '';
        }
        $oldCouponCode = $this->_getQuote()->getCouponCode();

        if (!strlen($couponCode) && !strlen($oldCouponCode)) {

            die(json_encode(array(
                "review" => $this->_getReviewHtml(),
                "cart" => $this->_getCartHtml(),
                "coupon_html" => $this->_getCouponHtml()
            )));
        }

        try {
            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->_getQuote()->setCouponCode(strlen($couponCode) ? $couponCode : '')
                ->collectTotals()
                ->save();
            $result = '';
            if ($couponCode) {
                if ($couponCode == $this->_getQuote()->getCouponCode()) {
                    $result['success'] = $this->__("Coupon code %s was applied.", Mage::helper('core')->htmlEscape($couponCode));
                } else {
                    $result['error'] = $this->__("Coupon code %s is not valid.", Mage::helper('core')->htmlEscape($couponCode));
                }
            } else {
                $result['success'] = $this->__('Coupon code was canceled.');
            }

        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            $result['error'] = $e->getMessage();
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Cannot apply the coupon code.'));
            $result['error'] = $this->__('Cannot apply the coupon code.');
            Mage::logException($e);
        }

        die(json_encode(array(
            "review" => $this->_getReviewHtml(),
            "cart" => $this->_getCartHtml(),
            "coupon_html" => $this->_getCouponHtml(),
            "result" => $result
        )));

    }


    function _getCouponHtml()
    {
        $this->loadLayout(array('default', 'oneclickcartcheckout_all'));
        return $this->getLayout()->getBlock('oneclickcartcheckout.coupon')->toHtml();
    }



    public function ChangePickupAction()
    {
        $pickupStatus = $this->getRequest()->getPost("customer_pickup");
        $shipping_data = $this->getRequest()->getPost('shipping', array());
        $billing_data = $this->getRequest()->getPost('billing', array());
        $quote = $this->getOnepage()->getQuote();
        $shipping = $quote->getShippingAddress();
        $billing = $quote->getBillingAddress();
        $customerAddressId = $this->getRequest()->getPost("billing_address_id");
        if ($pickupStatus) {
            if ($customerAddressId) {
                $address = Mage::getModel('customer/address')->load($customerAddressId);
                $shipping->setCity($address->getCity());
                $shipping->setRegion($address->getRegion());
                $shipping->setRegionId($address->getRegionId());
                $shipping->setCountryId($address->getCountryId());
                $shipping->setPostCode($address->getPostCode());
            } else {
                $shipping->setCountryId("UA");
                $shipping->setRegion("");
                $shipping->setRegionId("");
                $shipping->setCity("-");
                $shipping->setPostCode("-");
            }

            $this->_setShippingMethod('pickupshipping_pickupshipping');
            $data = array('method' => 'pickuppayment');
            $payment = $this->getOnepage()->getQuote()->getPayment();
            $payment->importData($data);
            $shipping->setPaymentMethod('pickuppayment');//$payment->getMethod());
            $quote->getPayment()->importData($data);
        } else {
            if ($customerAddressId) {
                $address = Mage::getModel('customer/address')->load($customerAddressId);
                $shipping->setCity($address->getCity());
                $shipping->setRegion('');//$address->getRegion());
                $shipping->setRegionId($address->getRegionId());
                $shipping->setCountryId($address->getCountryId());
                $shipping->setPostCode($address->getPostCode());
            } else {
                $shipping->setCountryId($billing_data["country_id"]);
                $shipping->setRegionId('');//$billing_data["region_id"]);
                $billing->setCountryId($billing_data["country_id"]);
                $billing->setCity($billing_data["city"]);
                $billing->setRegion('');//$billing_data["region"]);
                $billing->setRegionId('');//$billing_data["region_id"]);
                $billing->setPostCode($billing_data["postcode"]);
                $billing->setData("use_for_shipping", 1);
            }

            $this->_setShippingMethod(false);

        }
        die(json_encode(array(
            "review" => $this->_getReviewHtml(),
            "shipping_method" => $this->_getShippingMethodsHtml(),
            "payment" => $this->_getPaymentMethodsHtml(),
            "agreement" => $this->_getAgreementHtml()
        )));
    }

    public function ChangeAddressAction()
    {
        $AddressId = $this->getRequest()->getPost("address_id");
        if ($AddressId) {
            $address = Mage::getModel('customer/address')->load($AddressId);
            $quote = $this->getOnepage()->getQuote();
            $shipping = $quote->getShippingAddress();
            $shipping->setCountryId($address->getCountryId());
            $shipping->setRegion($address->getRegion());
            $shipping->setRegionId($address->getRegionId());
            $shipping->setCity($address->getCity());
            $shipping->setPostCode($address->getPostCode());
            $shipping->setCollectShippingRates(true)->save();
            $this->_setShippingMethod(false);
        }

        die(json_encode(array(
            "review" => $this->_getReviewHtml(),
            "shipping_method" => $this->_getShippingMethodsHtml(),
            "payment" => $this->_getPaymentMethodsHtml(),
            "agreement" => $this->_getAgreementHtml()
        )));

    }


    /**
     * Add product to shopping cart action
     */
    public function addAction()
    {
        $tmp_product='';

        $cart = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    $message = $this->__('%s was successfully added to your shopping cart.', $product->getName());
                    $img = "<img src='" . Mage::helper('catalog/image')->init($product, 'image')->resize(60, null) . "' />";
                    $tmp_product = '<div id="message_ajax"><div class="ajaxcart_image">' . $img . '</div><div class="ajaxcart_message">' . $message . '</div></div>';
                    $check = 'success';
                }
            }
        } catch (Mage_Core_Exception $e) {

            if ($this->_getSession()->getUseNotice(true)) {
                //$tmp_product	=	'<div id="message_ajax">'.Mage::helper('core')->escapeHtml($e->getMessage()).'</div>';
                Mage::helper('ajaxcart')->getOptionsHtml($this, $product->getId());
            } else {
                $tmp_product = '<div id="message_ajax">' . Mage::helper('core')->escapeHtml($e->getMessage()) . '</div>';
            }
        } catch (Exception $e) {


            $check = 'failed';
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
        }

        $url = $this->getRequest()->getParam('return_url');
        if ($url) {
            header("Location:" . $url);
            exit;
        }
        $is_ajax = $this->getRequest()->getParam('no_ajax',false);
        if ($is_ajax){
            $url_cart='/checkout/cart';
            header("Location:" . $url_cart);
            exit;
        }



        $arr['options'] = 2;
        $arr['check'] = $check;
        $arr['msg'] = $tmp_product;
        if ($check == 'success') {
            $this->loadLayout();
            $layout = $this->getLayout();
            $block_sidebar = $layout->getBlock('minicart_head');


            $cartBlockLink = $layout->createBlock('checkout/links');
            $topLinkBlock = $layout->createBlock('page/template_links')->setChild('link.cart', $cartBlockLink);
            $cartBlockLink->addCartLink();
            $links = $topLinkBlock->getLinks();
            $link = array_shift($links);

            $arr['total'] = strip_tags(Mage::helper('checkout')->formatPrice($cart->getQuote()->getSubtotal()));
            $arr['qty'] = $cart->getItemsQty();
            $arr['toplink'] = $link->getLabel();
            $arr['sidebar'] = $block_sidebar->toHtml();


            $path_category = Mage::helper('seo')->getPathCategories($product);


            $block_ajax = $layout->getBlock('cartmodal')->setProduct($product);
            ////->createBlock('cartmodal');//cartcheckout/checkout_cart_modal');

            if ($block_ajax) $modal = $block_ajax->toHtml();
            else $modal ='';


            $minicart_head=$this->_getMiniCartHtml();
            $arr['minicart_head']=$minicart_head;


            //echo '<pre>';
            $gtm_items='';

            $items_for_gtm=[];
            $products='';
            $products_ar=[];
            foreach ($cart->getItems() as $item) {

                //$gtm_items
                $product_id=$item->getProductId();

                $_productItem=$item->getProduct();
                /*$items_for_gtm[]=[
                    'id'=>$product_id,
                    'google_business_vertical'=>'retail'
                ];*/
                $items_for_gtm[]="{'id': ".$product_id.",'google_business_vertical': 'retail'}";
                $qty=$item->getQty();
                $category='';
                $brand='';

                $products_ar[]="{'name':'".$_productItem->getName()."','id': '".$_productItem->getId()."','price': '".$_productItem->getFinalPrice()."','brand': '".$brand."','category': '".$category."','quantity': ".$qty."}";
            }


            if (count($products_ar)>0)
                $products='['.implode(',',$products_ar).']';

            //json_encode($products_ar);//'['.implode(',',$products_ar).']';

            $gtm_items='';
            if  (count($items_for_gtm)>0)
                $gtm_items=json_encode($items_for_gtm);


            //'['.implode(',',$items_for_gtm).']';

            $value=$cart->getQuote()->getSubtotal();


           /* [{
                'name': 'Product 1',
                                'id': 'ID1',
                                'price': '23.5',
                                'brand': 'Brand 1',
                                'category': 'Category',
                                'quantity': 3   - количество товаров
                            }]
*/
            $arr['value']=$value;
            $arr['products']=$products;

            $arr['facebook']=[
                "id"=> $product->getId(),
                "price"=> $product->getFinalPrice(),
                "content_name"=> $product->getName(),
                "currency"=>"UAH",
                "content_category"=>$path_category
            ];


            $arr['gtm_items']=$gtm_items;
            $arr['modal'] = $modal;//$block_ajax_sidebar->toHtml();
        }
        if ($this->getRequest()->isAjax()) {


            if (preg_match('/MSIE/i', $_SERVER['HTTP_USER_AGENT'])) {    // if IE
                echo json_encode($arr, JSON_HEX_TAG);
                exit;
            } else {    // other browser
                echo json_encode($arr);
                exit;
            }
        }else{
            $url_cart='/checkout/cart';
            header("Location:" . $url_cart);
            exit;
        }
    }



    public function getcartAction()
    {
        $this->loadLayout();
        $layout = $this->getLayout();
        //$block_ajax = $layout->createBlock('cartcheckout/checkout_cart_modal');
        $block_ajax = $layout->getBlock('cartmodal');
        $modal = $block_ajax->toHtml();
        echo json_encode(['modal'=> $modal]);
        exit;
    }
    public function removeAction()
    {
        $id=$this->getRequest()->getParam('item_id');
        $this->_getCart()->removeItem($id)->save();
        $this->_updateShoppingCart();

        $forms=[Mage::helper('ukrsnab')->__('title_pro1'),Mage::helper('ukrsnab')->__('title_pro2'),Mage::helper('ukrsnab')->__('title_pro3')];
        $q=Mage::helper('checkout/cart')->getCart()->getItemsCount();
        $quantity=$q.' '.Mage::helper('cartcheckout')->plural_form($q, $forms);
        $summa=Mage::helper('checkout')->formatPrice(Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal());
        $minicart_head=$this->_getMiniCartHtml();
        echo json_encode(array(
            'quantity'=>$quantity,
            'summa'=>$summa,//Mage::helper('checkout')->formatPrice(Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal()),
            'item_id'=>$id,
            'minicart_head'=>$minicart_head
        ));exit;



    }


    private function _getTotalHtml(){
        //$this->loadLayout();
        $this->loadLayout(array('default', 'onestepcheckout_onepage_index'));
        return ($this->getLayout()->getBlock('onestepcheckout.checkout.cart.totals')->toHtml());
    }

}