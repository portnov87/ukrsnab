<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 22.11.2017
 * Time: 13:16
 */
/* @var $installer Mage_Customer_Model_Entity_Setup */
/*$installer = $this;
$installer->startSetup();

$addressHelper = Mage::helper('customer/address');
//$store         = Mage::app()->getStore(Mage_Core_Model_App::ADMIN_STORE_ID);


$eavConfig = Mage::getSingleton('eav/config');

// update customer address user defined attributes data
$attributes = array(
    'timeshipping'           => array(
        'label'    => 'Время доставки',
        'backend_type'     => 'varchar',
        'frontend_input'    => 'text',
        'is_user_defined'   => 1,
        'is_system'         => 0,
        'is_visible'        => 1,
        'sort_order'        => 140,
        'is_required'       => 0,
        'multiline_count'   => 0,
        'validate_rules'    => array(
            'max_text_length'   => 255,
            'min_text_length'   => 1
        ),
    ),
    'pharma'           => array(
        'label'    => 'Аптека',
        'backend_type'     => 'varchar',
        'frontend_input'    => 'text',
        'is_user_defined'   => 1,
        'is_system'         => 0,
        'is_visible'        => 1,
        'sort_order'        => 140,
        'is_required'       => 0,
        'multiline_count'   => 0,
        'validate_rules'    => array(
            'max_text_length'   => 255,
            'min_text_length'   => 1
        ),
    ),
);

foreach ($attributes as $attributeCode => $data) {
    $attribute = $eavConfig->getAttribute('customer_address', $attributeCode);
    //$attribute->setWebsite($store->getWebsite());
    $attribute->addData($data);
    $usedInForms = array(
        'adminhtml_customer_address',
        'customer_address_edit',
        'customer_register_address'
    );
    $attribute->setData('used_in_forms', $usedInForms);
    $attribute->save();
}

$installer->run("
        ALTER TABLE {$this->getTable('sales_flat_quote_address')} ADD COLUMN `pharma` VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL AFTER `fax`;
         ALTER TABLE {$this->getTable('sales_flat_order_address')} ADD COLUMN `pharma` VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL AFTER `fax`;
         ALTER TABLE {$this->getTable('sales_flat_quote_address')} ADD COLUMN `timeshipping` VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL AFTER `fax`;
         ALTER TABLE {$this->getTable('sales_flat_order_address')} ADD COLUMN `timeshipping` VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL AFTER `fax`;
        ");
$installer->endSetup();
*/
//echo 'test';die();