<?php

class Webfresh_CartCheckout_Block_Catalog_Product_Bookitform extends Mage_Core_Block_Template
{
    public function getProduct()
    {
        $product = Mage::registry('current_product');
        return $product;
    }

    public function getStoreId()
    {
        if ($store = Mage::registry('current_store_id')) {
            return $store;
        }

        return false;
    }

    protected function _toHtml()
    {

        $this->setTemplate('webfresh/cartcheckout/form_bookit.phtml');
        return parent::_toHtml();
    }

}