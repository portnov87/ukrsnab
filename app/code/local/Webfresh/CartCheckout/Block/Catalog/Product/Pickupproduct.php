<?php

class Webfresh_CartCheckout_Block_Catalog_Product_Pickupproduct extends Mage_Core_Block_Template
{
    /**
     * @return mixed
     */
    public function getProduct()
    {
        $product = Mage::registry('current_product');
        return $product;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        if ($this->getData('city_id'))
        {
            return $this->getData('city_id');
        }
            $current_city = Mage::helper('storelocator/data')->getCurrentCity();
            return $current_city['id'];

    }

    /**
     * @return mixed
     */
    protected function getStorelocatorId()
    {
        return $this->getData('store_id');
    }

    /**
     * @return bool|Mage_Core_Model_Abstract
     */
    public function getStorelocator()
    {
        if ($storelocator_id=$this->getStorelocatorId())
        {
            $storelocator = Mage::getModel('storelocator/storelocator')->load($storelocator_id);

            return $storelocator;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getQtyProductInStorelocator()
    {
        $storelocator=$this->getStorelocatorId();
        $product=$this->getProduct();
        $sku=$product->getSku();
        $qtyInStore=Mage::getModel('storelocator/products')->getQtyProductInStorelocator($storelocator,$sku);
        return $qtyInStore;

    }


    /**
     * @return string
     */
    protected function _toHtml()
    {

        $this->setTemplate('webfresh/cartcheckout/pickupproduct.phtml');
        return parent::_toHtml();
    }

}