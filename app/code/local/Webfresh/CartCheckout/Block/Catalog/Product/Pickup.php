<?php
class Webfresh_CartCheckout_Block_Catalog_Product_Pickup extends Mage_Core_Block_Template
{
    public function getProduct()
    {
        $product=Mage::registry('current_product');
        return $product;
    }
    public function getCity()
    {
        if ($this->getData('city_id')){

            $city_model = Mage::getModel('storelocator/city')->load($this->getData('city_id'));

            if ($city_model) {
                $city=['id' => $city_model->getCityId(), 'free_shipping' => $city_model->getCourierFree(), 'code' => $city_model->getCityCode(), 'name' => $city_model->getCityName()];

            }
            //$city
        }else {
            $city = Mage::helper('storelocator/data')->getCurrentCity();
        }
        return $city;

    }



    public function _getProductsInStore($city_id,$productsSku)
    {
        if (Mage::registry('store_with_product')) {
            return Mage::registry('store_with_product');
        }
        $stores_with_product=Mage::getModel('storelocator/products')
            ->productsInStore($city_id,$productsSku);
        Mage::unregister('store_with_product');
        Mage::register('store_with_product',$stores_with_product);

        return $stores_with_product;

    }


    public function getAttributesProduct()
    {

        $product_id=$this->getProduct()->getid();
        $product=Mage::getModel('catalog/product')->load($product_id);
        $data=[];
        $packaging=$product->getPackaging();//getAttributeText( 'packaging');
        $dosage=$product->getAttributeText( 'dosage');
        $volume=$product->getAttributeText( 'volume');

        if ($packaging!='')$data[]=$packaging;
        if (($packaging!=$dosage)&&($dosage!=''))$data[]=$dosage;
        if ($volume!='')$data[]=$volume;
        return $data;


    }
    //district



    protected function _toHtml()
    {

        $this->setTemplate('webfresh/cartcheckout/pickup.phtml');
        return parent::_toHtml();
    }
}