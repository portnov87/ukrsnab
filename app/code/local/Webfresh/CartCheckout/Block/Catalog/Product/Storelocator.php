<?php

class Webfresh_CartCheckout_Block_Catalog_Product_Storelocator extends Mage_Core_Block_Template
{
    public function getProduct()
    {
        if ($this->getData('product')) {
            return $this->getData('product');
        }
        $product = Mage::registry('current_product');
        return $product;
    }

    public function getTypeblock()
    {
        if ($this->getData('typeblock')) {
            return $this->getData('typeblock');
        }
        return 'list';
    }

    public function getCity()
    {
        if ($this->getData('city_id')) {
            return $this->getData('city_id');
        }
        $current_city = Mage::helper('storelocator/data')->getCurrentCity();
        return $current_city['id'];

    }

    public function _getProductsInStore($city_id, $productsSku)
    {
        if (Mage::registry('store_with_product')) {
            return Mage::registry('store_with_product');
        }
        $stores_with_product = Mage::getModel('storelocator/products')
            ->productsInStore($city_id, $productsSku, 'all');
        Mage::unregister('store_with_product');
        Mage::register('store_with_product', $stores_with_product);

        /*echo "<pre>store";
        print_r($stores_with_product);
        echo "</pre>";*/
        return $stores_with_product;

    }


    public function getDistrict()
    {
        if ($this->getData('district')) {
            return $this->getData('district');
        }
        return false;
    }

    protected function _toHtml()
    {


        if ($this->getTypeblock() == 'map') {
            //echo $this->getTypeblock();

            $this->setTemplate('webfresh/cartcheckout/storelocator_map.phtml');
        } else {
            $this->setTemplate('webfresh/cartcheckout/storelocator.phtml');
        }

        return parent::_toHtml();
    }

}