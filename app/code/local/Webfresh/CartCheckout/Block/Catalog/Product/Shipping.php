<?php

class Webfresh_CartCheckout_Block_Catalog_Product_Shipping
    extends Mage_Core_Block_Template
{
    public $_product = false;
    public $_data_shipping = false;

    public function getCity()
    {
        $current_city=Mage::helper('storelocator')->getCurrentCity();
        return $current_city;
    }
    /**
     * @return bool|Mage_Core_Model_Abstract
     */
    public function getProduct()
    {
        if (!$this->_product) {

                /*echo '<pre>getdata111'.$this->getData('product_id');
                print_r(Mage::registry('current_product'));
                echo '</pre>';*/
            $this->_product =Mage::registry('current_product');//!==
            if (!Mage::registry('current_product')) {
                $product = Mage::getModel('catalog/product')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->load($this->getData('product_id'));
                $this->_product = $product;
                //Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId())->load($this->getData('product_id'));
                //->addAttributeToSelect('*')

            }
        }

        return $this->_product;
    }

    /**
     * @return bool
     * return [
     * 'order_status',
     * 'kyrier',
     * 'np',
     * 'pickup',
     * 'courier_amount_cart',
     * 'courier_price',
     * 'courier_free',
     * 'for_order'
     * ]
     *
     */
    public function getDataShipping()
    {
        //echo 'id'.$this->_product->getId();
        if (!$this->_product)
            $this->getProduct();

        if (!$this->_data_shipping)
            $this->_data_shipping = $this->_product->getDataShipping();

        return $this->_data_shipping;
        //$result = $product->getDataShipping();
    }

    /**
     *
     */
    public function getInfoCourierDelivery()
    {
        if (!$this->_product) $product = $this->getProduct();
        else  $product = $this->_product;

        $data_shipping = $this->getDataShipping();

        $price = $product->getPrice();
        $courier_free = $data_shipping['courier_free'];
        if ($price >= $courier_free)
            return Mage::helper('cartcheckout')->__("delivery free").'*';
        $infodeliverycourier='';
        $cost_delivery = $this->getCostDelivery();
        //if ($cost_delivery > 0)
            //$infodeliverycourier[] = $cost_delivery;
        $infodeliverycourier[] = $this->getCostFreeDelivery();
        return Mage::helper('cartcheckout')->__("delivery free").'*';
        return implode(', ', $infodeliverycourier);
    }

    /**
     * @return mixed
     */
    public function getCostDelivery()
    {
        $data_shipping = $this->getDataShipping();
        $courier_price = $data_shipping['courier_price'];
        return Mage::helper('core')->currency($courier_price, true, false);
    }

    /**
     * @return string
     */
    public function getCostFreeDelivery()
    {
        $data_shipping = $this->getDataShipping();
        $courier_free = Mage::helper('core')->currency($data_shipping['courier_free'], true, false);
        return Mage::helper('cartcheckout')->__("from %s free", $courier_free);
    }

    /**
     * @return mixed
     */
    public function getCurierDeliveryDay()
    {
        $data_shipping = $this->getDataShipping();
        $for_order = $data_shipping['for_order'];
        return Mage::helper('courierdelivery')->HtmlCurierDeliveryDay($this->_product, $for_order);//getCurierDeliveryDay();
    }


    /**
     * @return string
     */
    protected function _toHtml()
    {
        $this->getProduct();
        $this->setTemplate('webfresh/cartcheckout/shipping_product.phtml');
        return parent::_toHtml();
    }
}