<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 28.07.2017
 * Time: 15:10
 */
class Webfresh_CartCheckout_Block_Checkout_Cart_Item_Renderer extends Mage_Checkout_Block_Cart_Item_Renderer
{

    public function getAttributesProduct()
    {
        $product_id=$this->getProduct()->getid();
        $product=Mage::getModel('catalog/product')->load($product_id);
        $data=[];
        $packaging=$product->getAttributeText( 'packaging');
        $dosage=$product->getAttributeText( 'dosage');
        $volume=$product->getAttributeText( 'volume');
       // if ($packaging!='')$data[]=$packaging;
        if ($dosage!='')$data[]=$dosage;
        if ($volume!='')$data[]=$volume;
        return $data;


    }
}