<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 28.07.2017
 * Time: 15:10
 */
class Webfresh_CartCheckout_Block_Checkout_Cart_Modal extends Mage_Checkout_Block_Cart_Sidebar//Mage_Core_Block_Template
{


    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    protected function _toHtml()
    {
        $this->setTemplate('webfresh/cartcheckout/modal-cart.phtml');
        return parent::_toHtml();
    }
}