<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 28.07.2017
 * Time: 15:10
 */
class Webfresh_CartCheckout_Block_Checkout_Cart_Sidebar extends Mage_Checkout_Block_Cart_Sidebar
{

    public function __construct()
    {
        parent::__construct();
        $this->addItemRender('default', 'cartcheckout/checkout_cart_item_renderer', 'webfresh/cartcheckout/cart/item/default.phtml');
    }


    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    protected function _toHtml()
    {

        $this->setTemplate('webfresh/cartcheckout/cart/cart-sidebar.phtml');
        return parent::_toHtml();
    }
}