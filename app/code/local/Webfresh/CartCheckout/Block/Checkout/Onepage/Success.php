<?php

/**
 * Created by PhpStorm.
 * User: portn
 * Date: 21.11.2017
 * Time: 11:24
 */
class Webfresh_CartCheckout_Block_Checkout_Onepage_Success extends Mage_Checkout_Block_Onepage_Success
{


    /**
     * Get last order ID from session, fetch it and check whether it can be viewed, printed etc
     */
    protected function _prepareLastOrder()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
        if ($orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            if ($order->getId()) {
                $isVisible = !in_array($order->getState(),
                    Mage::getSingleton('sales/order_config')->getInvisibleOnFrontStates());


                $shipping = $order->getShippingAddress()->getData();
                $billing = $order->getBillingAddress()->getData();
                $lastname = $billing['firstname'];
                $phone = $billing['telephone'];
                $email = $billing['email'];
                if ($shipping['city'] != '')
                    $city = $shipping['city'];
                else {

                    $city = '';
                }


                /*
                echo "<pre>";
                print_r($shipping);
                echo "</pre>";die();*/
                //if (isset($shipping['shipping_method_child'])) $shipping_method_child=$shipping['shipping_method_child'];
                //else $shipping_method_child='';

                $shipping_method = $order->getShippingMethod();

                $shipping_method_name = Mage::helper('cartcheckout')->getNameShippingMethod($shipping_method);
                if (isset($shipping['street'])) $street = $shipping['street'];
                else $street = '';

                $address = $street;
                $comment = $order->getCustomerNote();



                if (strpos($shipping_method, 'sy_novaposhta') !== false) {


                    if (strpos($shipping_method, 'WarehouseDoor') !== false) {

                        if (isset($shipping['novaposhta_address'])):
                            if ($shipping['novaposhta_address'] != ''):

                                $address = $shipping['novaposhta_address'];

                            endif;
                        endif;
                    } elseif (strpos($shipping_method, 'WarehouseWarehouse') !== false) {

                        if (isset($shipping['novaposhta_warehouses'])):
                            if ($shipping['novaposhta_warehouses'] != ''):

                                $address = $shipping['novaposhta_warehouses'];
                                $address=iconv('windows-1251', 'utf-8', trim($address));

                            endif;
                        endif;

                    }

                    if (isset($shipping['novaposhta_note'])):
                        if ($shipping['novaposhta_note'] != ''):
                            $comment = $shipping['novaposhta_note'];
                        endif;
                    endif;
                }


            if ($shipping_method=='webfresh_courierdelivery_standard') {
                if ($street != ''){
                    $address = $street;

                }

             }
            elseif(($shipping_method=='webfresh_pickupdelivery_standard')||($shipping_method=='webfresh_pickupdelivery_fast_shipping')){
                //$storelocator_id=$shipping['pickup_storelocator'];
                //$storelocator=Mage::helper('storelocator')->getStorelocatorById($storelocator_id);
                if (isset($shipping['pharma'])) {
                    $storelocator = Mage::helper('storelocator')->getStorelocatorById($shipping['pharma']);

                    if ($storelocator){
                        $address=$storelocator->getTitle();

                    }
                }
            }




                $when = '';
                $when = $shipping['timeshipping'];
                //if (isset($shipping['day_for_delivery']))$when=$shipping['day_for_delivery'];
                //if (isset($shipping['time_delivery']))$when.=': '.$shipping['time_delivery'];
                //if (isset($shipping['customer_note']))
                //shipping['customer_note'];


                //if (isset($shipping['shipping_description'])) $comment=$shipping['shipping_description'];
                //if (isset($shipping['timeshipping']))$when=$shipping['timeshipping'];




                $this->addData(array(
                    'is_order_visible' => $isVisible,
                    'view_order_id' => $this->getUrl('sales/order/view/', array('order_id' => $orderId)),
                    'print_url' => $this->getUrl('sales/order/print', array('order_id' => $orderId)),
                    'can_print_order' => $isVisible,
                    'can_view_order' => Mage::getSingleton('customer/session')->isLoggedIn() && $isVisible,
                    'order_id' => $order->getIncrementId(),
                    'phone' => $phone,
                    'lastname' => $lastname,
                    'address'=>$address,
                    'email' => $email,
                    'city' => $city,
                    'shipping_method' => $shipping_method_name,
                    'street' => $street,
                    'when' => $when,
                    'comment' => $comment,
                    'order_model' => $order,
                    'cart_items' => $order->getAllItems()
                ));
            }
        }
    }

    public function getOrder()
    {
        return $this->_getData('order_model');
    }

    public function getItems()
    {
        return $this->_getData('cart_items');
    }

    public function getAddress()
    {
        return $this->_getData('address');
    }
    public function getPhone()
    {
        return $this->_getData('phone');
    }

    public function getLastname()
    {
        return $this->_getData('lastname');
    }

    public function getEmail()
    {
        return $this->_getData('email');
    }

    public function getCity()
    {
        return $this->_getData('city');
    }

    public function getShippingMethod()
    {
        return $this->_getData('shipping_method');
    }

    public function getStreet()
    {
        return $this->_getData('street');
    }

    public function getWhen()
    {
        return $this->_getData('when');
    }

    public function getComment()
    {
        return $this->_getData('comment');
    }
}