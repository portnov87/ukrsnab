<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 28.07.2017
 * Time: 15:10
 */
class Webfresh_CartCheckout_Block_Checkout_Onepage_Shipping_Method_Available extends Mage_Checkout_Block_Onepage_Shipping_Method_Available
{

    /*public function getShippingMethodFormHtml($rateCode)
    {
        return $this->getChildHtml('shipping.method.' . $rateCode);
    }
*/

    //getAddressShippingMethod()

    public function getAddressShippingMethod()
    {
        $method=$this->getAddress()->getShippingMethod();
        if ($method!='') {
            if (strpos('webfresh_courierdelivery', $method) !== false)
                return 'webfresh_courierdelivery_stantard';
            elseif ($method == 'webfresh_pickupdelivery')
                return 'webfresh_pickupdelivery';
        }


        return $method;
    }

    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    public function getProductsSku()
    {
        $cart = $this->_getCart()->getQuote();
        $result=[];

        foreach ($cart->getAllItems() as $item) {
            $product=$item->getProduct();
            //$result[]='"'.$product->getSku().'"';
            $result[]=$product->getSku();
        }

        return $result;
    }

    public function canUse($rateCode,$carrier,$city)
    {
        switch($rateCode)
        {
            case 'webfresh_pickupdelivery':
            case 'webfresh_courierdelivery':
                return $carrier->allowMethod($city);
            break;
            //case 'webfresh_courierdelivery':
              //  break;
        }
        return true;

    }




/*
    public function getGroupedAllShippingRates()
    {
        //return false;
        //dd($this->getShippingRatesCollection()->toArray());
        $rates = array();
        $result = Mage::getModel('shipping/shipping')->collectRates($request)->getResult();


        if ($result) {
            $shippingRates = $result->getAllRates();
            echo '<pre>shippingRates';
            print_r($shippingRates);
            echo '</pre>';
            die();
        }



        $carriers = Mage::getSingleton('shipping/config')->getAllCarriers();
        foreach ($carriers as $carrierCode=>$rate) {
            if (!$rate->isActive()) {
                continue;
            }
           // echo 'car'.$rate->getCarrier().'<br/>';
            if (!$rate->isDeleted()){// && $rate->getCarrierInstance()) {
                if (!isset($rates[$rate->getId()])) {
                    $rates[$rate->getId()] = array();
                }

                $r=Mage::getModel('shipping/config')->getCarrierInstance($rate->getId());
                $rates[$rate->getId()] = $r;//$rate->getCarrierInstance();
               // echo $rate->getId();
                //die();
                //$rates[$rate->getId()][0]->carrier_sort_order = $rate->->getSortOrder();
            }
            //echo 'code '.$carrierCode;

        }

       // uasort($rates, array($this, '_sortRates'));
        return $rates;
    }
*/

    public function getShippingRates()
    {
        //return false;
        //if (empty($this->_rates)) {
        $groups = $this->getAddress()->getGroupedAllShippingRates();
       /* echo '<pre>groups';
        print_r($groups);
        echo '</pre>';*/
       /* $groups = $this->getGroupedAllShippingRates();
        return $this->_rates = array();//$groups;
        //  }
        return $this->_rates;
*/
        if (empty($this->_rates)) {
            $this->getAddress()->collectShippingRates()->save();

           // $groups = $this->getGroupedAllShippingRates();//
            //$groups = $this-> getAddress()->getGroupedAllShippingRates();

            $new_group=[];
            $city=Mage::helper('cartcheckout')->getCurrentCityModel();

           // echo ';group;';
            foreach ($groups as $key=>$_rate)
            {
                //echo 'key'.$key.'<br/>';
                $carrier = Mage::getModel('shipping/config')->getCarrierInstance($key);
                if ($this->canUse($key,$carrier,$city))
                {
                  /* echo '<pre>car';
                    print_r($_rate);
                    echo '</pre>';die();*/

                    $new_group[$key]=$_rate;
                }

            }
            $this->_rates = $new_group;//$groups;

            return $this->_rates;
        }

        return $this->_rates;
    }

    /*
    public function getShippingRates()
    {

        if (empty($this->_rates)) {
            $this->getAddress()->collectShippingRates()->save();

            $groups = $this->getAddress()->getGroupedAllShippingRates();

            $new_group=[];
            $city=Mage::helper('cartcheckout')->getCurrentCityModel();

            foreach ($groups as $key=>$_rate)
            {
                $carrier = Mage::getModel('shipping/config')->getCarrierInstance($key);
                if ($this->canUse($key,$carrier,$city))
                {
                    $new_group[$key]=$_rate;
                }

            }


            return $this->_rates = $new_group;//$groups;
        }

        return $this->_rates;
    }
    */


    public function getCurrentcity()
    {

        if (!$this->hasData('current_city')) {
            $this->setData('current_city', Mage::helper('storelocator/data')->getCurrentCity());
        }
        return $this->getData('current_city');

    }
    public function getLocatorsByCurrentcity(){
        $city=$this->getCurrentcity();
        $city_id=$city['id'];
        $collection=Mage::helper('storelocator')->getPharmas($city_id,'array');


            //Mage::getModel('storelocator/storelocator')->getStorelocators($city_id);
        return $collection;
    }

}