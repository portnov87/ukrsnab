<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 16.10.2017
 * Time: 10:53
 */
class Webfresh_CartCheckout_Block_Checkout_Onepage_Billing extends Mage_Checkout_Block_Onepage_Billing
{
    /**
     * Return Sales Quote Address model
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getAddress()
    {
        if (is_null($this->_address)) {
            if ($this->isCustomerLoggedIn()) {
                $this->_address = $this->getQuote()->getBillingAddress();
                if (!$this->_address->getFirstname()) {
                    $this->_address->setFirstname($this->getQuote()->getCustomer()->getFirstname());
                }
                /*if (!$this->_address->getMiddlename()) {
                    $this->_address->setMiddlename($this->getQuote()->getCustomer()->getMiddlename());
                }
                if (!$this->_address->getLastname()) {
                    $this->_address->setLastname($this->getQuote()->getCustomer()->getLastname());
                }*/
            } else {
                //$this->_address = Mage::getModel('sales/quote_address');
                $temp_data = Mage::getModel('sales/quote_address');
                $customer_data = Mage::getSingleton('checkout/session')->getCustomerData();
               /* echo "<pre>custmer data";
                print_r($customer_data);
                echo "</pre>";die();*/
                if ($customer_data) {
                    if (isset($customer_data['billing'])) {
                        foreach ($customer_data['billing'] as $key => $data) {
                            $temp_data->setData($key, $data);
                        }
                    }
                }
                $this->_address = $temp_data;
            }
        }

        return $this->_address;
    }
}