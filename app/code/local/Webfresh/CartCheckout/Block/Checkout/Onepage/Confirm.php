<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 21.11.2017
 * Time: 11:30
 */

class Webfresh_CartCheckout_Block_Checkout_Onepage_Confirm extends Mage_Checkout_Block_Onepage_Abstract//extends Mage_Core_Block_Template
{
    public function getBilling()
    {
        //return $this->getQuote()->getBillingAddress();
        if (is_null($this->_address)) {
           /* if ($this->isCustomerLoggedIn()) {
                $this->_address = $this->getQuote()->getBillingAddress();
                if (!$this->_address->getFirstname()) {
                    $this->_address->setFirstname($this->getQuote()->getCustomer()->getFirstname());
                }
                if (!$this->_address->getMiddlename()) {
                    $this->_address->setMiddlename($this->getQuote()->getCustomer()->getMiddlename());
                }
                if (!$this->_address->getLastname()) {
                    $this->_address->setLastname($this->getQuote()->getCustomer()->getLastname());
                }
            } else {*/
                //$this->_address = Mage::getModel('sales/quote_address');
                $temp_data = Mage::getModel('sales/quote_address');
                $customer_data = Mage::getSingleton('checkout/session')->getCustomerData();
                /* echo "<pre>custmer data";
                 print_r($customer_data);
                 echo "</pre>";die();*/
                if ($customer_data) {
                    if (isset($customer_data['billing'])) {
                        foreach ($customer_data['billing'] as $key => $data) {
                            $temp_data->setData($key, $data);
                        }
                    }
                }
                $this->_address = $temp_data;
            //}
        }

        return $this->_address;
    }

    public function getShipping()
    {
        //return $this->getQuote()->getShippingAddress();
        if (is_null($this->_address)) {
            /*if ($this->isCustomerLoggedIn()) {
                $this->_address = $this->getQuote()->getBillingAddress();
                if (!$this->_address->getFirstname()) {
                    $this->_address->setFirstname($this->getQuote()->getCustomer()->getFirstname());
                }
                if (!$this->_address->getMiddlename()) {
                    $this->_address->setMiddlename($this->getQuote()->getCustomer()->getMiddlename());
                }
                if (!$this->_address->getLastname()) {
                    $this->_address->setLastname($this->getQuote()->getCustomer()->getLastname());
                }
            } else {*/
                //$this->_address = Mage::getModel('sales/quote_address');
                $temp_data = Mage::getModel('sales/quote_address');
                $customer_data = Mage::getSingleton('checkout/session')->getCustomerData();
                /*echo "<pre>custmer data";
                print_r($_SESSION);
                echo "</pre>";die();*/
                if ($customer_data) {
                    if (isset($customer_data['shipping'])) {
                        foreach ($customer_data['shipping'] as $key => $data) {
                            $temp_data->setData($key, $data);
                        }
                    }
                }
                $this->_address = $temp_data;
           // }
        }

        return $this->_address;
    }

    public function getShippingMethod()
    {
        return $this->getQuote()->getShippingAddress()->getShippingMethod();
    }

    public function getShippingDescription()
    {
        return $this->getQuote()->getShippingAddress()->getShippingDescription();
    }

    public function getShippingAmount()
    {
        /*$amount = $this->getQuote()->getShippingAddress()->getShippingAmount();
        $filter = Mage::app()->getStore()->getPriceFilter();
        return $filter->filter($amount);*/
        //return $this->helper('checkout')->formatPrice(
        //    $this->getQuote()->getShippingAddress()->getShippingAmount()
        //);
        return $this->getQuote()->getShippingAddress()->getShippingAmount();
    }

}
