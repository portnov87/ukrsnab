<?php
$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
try {


    $installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('seo/rewrite')};
CREATE TABLE IF NOT EXISTS {$this->getTable('seo/rewrite')} (
  `rewrite_id` int(10) unsigned NOT NULL auto_increment,
  `attribute_code` varchar(30) NOT NULL default '',
  `option_id` int(10) unsigned NOT NULL,
  `rewrite` varchar(40) NOT NULL default '',
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY  (`rewrite_id`),
  CONSTRAINT `UNQ_FILTERURLS_REWRITE_ATTRIBUTECODE_OPTIONID_STOREID` UNIQUE (`attribute_code`, `option_id`, `store_id`),
  CONSTRAINT `UNQ_FILTERURLS_REWRITE_REWRITE_STOREID` UNIQUE (`rewrite`, `store_id`),
  CONSTRAINT `FK_FILTERURLS_REWRITE_STORE` FOREIGN KEY (`store_id`) REFERENCES `{$this->getTable('core/store')}` (`store_id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rewrite values for attribute options' AUTO_INCREMENT=1;
");

    $installer->run("
CREATE TABLE `{$this->getTable('seo/meta')}` (
    `rewrite_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Rewrite Id',
    `url` varchar(255) NOT NULL COMMENT 'Name',
    `title` text COMMENT 'title',
    `description` text COMMENT 'description',
    `meta_title` text COMMENT 'meta_title',
    `meta_keywords` text COMMENT 'meta_keywords',
    `meta_description` text COMMENT 'meta_description',
    `robots` varchar(255) NOT NULL COMMENT 'Robots',
    `is_active` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Active',
    PRIMARY KEY (`rewrite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rewrite';

CREATE TABLE `{$this->getTable('seo/rewrite_store')}` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
    `rewrite_id` int(11) NOT NULL COMMENT 'Rewrite Id',
    `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
    PRIMARY KEY (`id`),
    KEY `FK_SEOREWRITE_STORE_ID` (`store_id`),
    KEY `FK_SEOREWRITE_STORE_REWRITE_ID` (`rewrite_id`),
    CONSTRAINT `fk_seorewrite_store_id` FOREIGN KEY (`store_id`) REFERENCES `{$this->getTable('core/store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_seorewrite_store_rewrite_id` FOREIGN KEY (`rewrite_id`) REFERENCES `{$this->getTable('seo/rewrite')}` (`rewrite_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rewrite 2 Stores';");


    /*



-- DROP TABLE IF EXISTS `webfresh_seo_rewrite`;
CREATE TABLE `webfresh_seo_rewrite` (
  `rewrite_id` int(10) unsigned NOT NULL auto_increment,
  `attribute_code` varchar(30) NOT NULL default '',
  `option_id` int(10) unsigned NOT NULL,
  `rewrite` varchar(40) NOT NULL default '',
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY  (`rewrite_id`),
  CONSTRAINT `UNQ_FILTERURLS_REWRITE_ATTRIBUTECODE_OPTIONID_STOREID` UNIQUE (`attribute_code`, `option_id`, `store_id`),
  CONSTRAINT `UNQ_FILTERURLS_REWRITE_REWRITE_STOREID` UNIQUE (`rewrite`, `store_id`),
  CONSTRAINT `FK_FILTERURLS_REWRITE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rewrite values for attribute options' AUTO_INCREMENT=1;



    CREATE TABLE `webfresh_seo_meta` (
    `rewrite_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Rewrite Id',
    `url` varchar(255) NOT NULL COMMENT 'Name',
    `title` text COMMENT 'title',
    `description` text COMMENT 'description',
    `meta_title` text COMMENT 'meta_title',
    `meta_keywords` text COMMENT 'meta_keywords',
    `meta_description` text COMMENT 'meta_description',
    `robots` varchar(255) NOT NULL COMMENT 'Robots',
    `is_active` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Active',
    PRIMARY KEY (`rewrite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rewrite';

CREATE TABLE `webfresh_seo_rewrite_st` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
    `rewrite_id` int(11) NOT NULL COMMENT 'Rewrite Id',
    `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
    PRIMARY KEY (`id`),
    KEY `FK_SEOREWRITE_STORE_ID` (`store_id`),
    KEY `FK_SEOREWRITE_STORE_REWRITE_ID` (`rewrite_id`),
    CONSTRAINT `fk_seorewrite_store_id` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_seorewrite_store_rewrite_id` FOREIGN KEY (`rewrite_id`) REFERENCES `webfresh_seo_rewrite` (`rewrite_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rewrite 2 Stores';
    */

    //$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
//PRODUCT
    $setup->addAttribute('catalog_category', 'product_meta_title_tpl', array(
        'group'         => 'SEO',
        'input'         => 'text',
        'type'          => 'text',
        'label'         => 'Шаблон Meta Title продуктов',///Child Products Meta Title',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined' => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

    $setup->addAttribute('catalog_category', 'product_meta_description_tpl', array(
        'group'         => 'SEO',
        'input'         => 'textarea',
        'type'          => 'text',
        'label'         => 'Шаблон Meta Description продуктов',//Child Products Meta Description',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined' => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

    $setup->addAttribute('catalog_category', 'product_title_tpl', array(
        'group'         => 'SEO',
        'input'         => 'text',
        'type'          => 'text',
        'label'         => 'Шаблон H1 продуктов',//Child Products H1',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined' => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

    $setup->addAttribute('catalog_category', 'product_description_tpl', array(
        'group'         => 'SEO',
        'input'         => 'textarea',
        'type'          => 'text',
        'label'         => 'Шаблон SEO description продуктов',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined'  => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

//CATEGORY
    $setup->addAttribute('catalog_category', 'category_meta_title_tpl', array(
        'group'         => 'SEO',
        'input'         => 'text',
        'type'          => 'text',
        'label'         => 'Шаблон подкатегорий Meta title',//Child Categories Meta Title',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined' => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

    $setup->addAttribute('catalog_category', 'category_meta_description_tpl', array(
        'group'         => 'SEO',
        'input'         => 'textarea',
        'type'          => 'text',
        'label'         => 'Шаблон подкатегорий Meta Description',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined' => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));


    $setup->addAttribute('catalog_category', 'category_title_tpl', array(
        'group'         => 'SEO',
        'input'         => 'text',
        'type'          => 'text',
        'label'         => 'Шаблон подкатегорий H1',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined' => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

    $setup->addAttribute('catalog_category', 'category_description_tpl', array(
        'group'         => 'SEO',
        'input'         => 'textarea',
        'type'          => 'text',
        'label'         => 'Шаблон seo писания подкатегорий',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined'  => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

// FILTER
    $setup->addAttribute('catalog_category', 'filter_meta_title_tpl', array(
        'group'         => 'SEO',
        'input'         => 'text',
        'type'          => 'text',
        'label'         => 'Шаблон Meta Title для фильтров',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined' => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

    $setup->addAttribute('catalog_category', 'filter_meta_description_tpl', array(
        'group'         => 'SEO',
        'input'         => 'textarea',
        'type'          => 'text',
        'label'         => 'Шаблон Meta Description для фильтров',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined' => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));


    $setup->addAttribute('catalog_category', 'filter_title_tpl', array(
        'group'         => 'SEO',
        'input'         => 'text',
        'type'          => 'text',
        'label'         => 'Шаблон H1 для фильтров',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined' => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

    $setup->addAttribute('catalog_category', 'filter_description_tpl', array(
        'group'         => 'SEO',
        'input'         => 'textarea',
        'type'          => 'text',
        'label'         => 'Шаблон SEO description для фильтров',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined'  => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));


// FILTER
    $setup->addAttribute('catalog_category', 'manufacturer_meta_title_tpl', array(
        'group'         => 'SEO',
        'input'         => 'text',
        'type'          => 'text',
        'label'         => 'Шаблон Meta Title для страницы фильтра бренд',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined' => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

    $setup->addAttribute('catalog_category', 'manufacturer_meta_description_tpl', array(
        'group'         => 'SEO',
        'input'         => 'textarea',
        'type'          => 'text',
        'label'         => 'Шаблон Meta Description для страницы фильтра Бренд',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined' => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));


    $setup->addAttribute('catalog_category', 'manufacturer_title_tpl', array(
        'group'         => 'SEO',
        'input'         => 'text',
        'type'          => 'text',
        'label'         => 'Шаблон H1 для страницы фильтра бренд',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined' => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));

    $setup->addAttribute('catalog_category', 'manufacturer_description_tpl', array(
        'group'         => 'SEO',
        'input'         => 'textarea',
        'type'          => 'text',
        'label'         => 'Шаблон SEO description для страницы фильтра бренд',
        'backend'       => '',
        'visible'       => 1,
        'required'      => 0,
        'user_defined'  => 1,
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));
echo 'test';die();

} catch (Exception $e) {
Mage::logException($e);
}
$installer->endSetup();