<?php
$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
try {


    //$installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_h2 VARCHAR(255) AFTER robots;");

    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_title_6 VARCHAR(255) AFTER faq_title_5;");
    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_content_6 TEXT AFTER faq_title_5;");

    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_title_7 VARCHAR(255) AFTER faq_title_5;");
    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_content_7 TEXT AFTER faq_title_5;");

    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_title_8 VARCHAR(255) AFTER faq_title_5;");
    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_content_8 TEXT AFTER faq_title_5;");

    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_title_9 VARCHAR(255) AFTER faq_title_5;");
    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_content_9 TEXT AFTER faq_title_5;");

    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_title_10 VARCHAR(255) AFTER faq_title_5;");
    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_content_10 TEXT AFTER faq_title_5;");


    $installer->endSetup();

} catch (Exception $e) {
Mage::logException($e);
}
$installer->endSetup();