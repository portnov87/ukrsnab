<?php
$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
try {


    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_title_1 VARCHAR(255) AFTER robots;");
    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_content_1 TEXT AFTER faq_title_1;");

    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_title_2 VARCHAR(255) AFTER faq_content_1;");
    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_content_2 TEXT AFTER faq_title_2;");

    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_title_3 VARCHAR(255) AFTER faq_title_2;");
    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_content_3 TEXT AFTER faq_title_3;");

    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_title_4 VARCHAR(255) AFTER faq_content_3;");
    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_content_4 TEXT AFTER faq_title_4;");

    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_title_5 VARCHAR(255) AFTER faq_title_4;");
    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_content_5 TEXT AFTER faq_title_5;");

    $installer->endSetup();

} catch (Exception $e) {
Mage::logException($e);
}
$installer->endSetup();