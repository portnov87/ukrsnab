<?php
$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
try {


    $installer->run("ALTER TABLE `{$installer->getTable('seo/meta')}` ADD COLUMN faq_h2 VARCHAR(255) AFTER robots;");

    $installer->endSetup();

} catch (Exception $e) {
Mage::logException($e);
}
$installer->endSetup();