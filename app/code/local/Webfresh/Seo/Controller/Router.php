<?php
/**
 * @category    Fishpig
 * @package    Fishpig_AttributeSplash
 * @license      http://fishpig.co.uk/license.txt
 * @author       Ben Tideswell <ben@fishpig.co.uk>
 */

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);

class Webfresh_Seo_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    /**
     * Cache for request object
     *
     * @var Zend_Controller_Request_Http
     */
    protected $_request = null;

    /**
     * Initialize Controller Router
     *
     * @param Varien_Event_Observer $observer
     */
    public function initControllerRouters(Varien_Event_Observer $observer)
    {
        /*
if ($_SERVER['REMOTE_ADDR']=='188.130.177.18') {
     $installer = new Mage_Eav_Model_Entity_Setup('core_setup');
     $installer->startSetup();


     $installer->addAttribute('catalog_category', 'manufacturer_meta_description_tpl', array(
         'group'         => 'SEO',
         'input'         => 'textarea',
         'type'          => 'text',
         'label'         => 'Шаблон Meta Description для страницы фильтра Бренд',
         'backend'       => '',
         'visible'       => 1,
         'required'      => 0,
         'user_defined' => 1,
         'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
     ));


     $installer->addAttribute('catalog_category', 'manufacturer_title_tpl', array(
         'group'         => 'SEO',
         'input'         => 'text',
         'type'          => 'text',
         'label'         => 'Шаблон H1 для страницы фильтра бренд',
         'backend'       => '',
         'visible'       => 1,
         'required'      => 0,
         'user_defined' => 1,
         'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
     ));

     $installer->addAttribute('catalog_category', 'manufacturer_description_tpl', array(
         'group'         => 'SEO',
         'input'         => 'textarea',
         'type'          => 'text',
         'label'         => 'Шаблон SEO description для страницы фильтра бренд',
         'backend'       => '',
         'visible'       => 1,
         'required'      => 0,
         'user_defined'  => 1,
         'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
     ));

}*/

        $observer->getEvent()->getFront()->addRouter('seo', $this);
        $current_url = Mage::app()->getRequest()->getPathInfo();
        $current_url = $_SERVER['REQUEST_URI'];
        //cho $current_url;die();
        $origin_url = Mage::app()->getRequest()->getOriginalPathInfo();

        if ((strpos($current_url, 'snabadmin') === false) && (strpos($current_url, 'ajax') === false) && (strpos($current_url, 'checkout') === false) && (strpos($current_url, '.html') === false)) {


            //https://ukrsnab.com.ua//storoitelnoe-oborudovanie/vibroplity/
            //https://ukrsnab.com.ua//stroitelnoe-oborudovanie/vibroplity/
            //if ((substr($current_url, -1) !== '/') && (strpos($current_url, '?') === false)) {
            switch ($current_url) {
                case '/customer/account/create':
//                case '/remont-i-obsluzhivaniye-glubinnykh-vibratorov':
//                case '/remont-i-obsluzhivaniye-vibroplit':
//                case '/remont-i-obsluzhivaniye-vibrokatkov':
//                case '/remont-i-obsluzhivaniye-frezerovalnykh-mashin':
//                case '/remont-i-obsluzhivaniye-generatorov':
//                case '/remont-i-obsluzhivaniye-elektrostantsiy':
//                case '/remont-i-obsluzhivaniye-parketoshlifovalnykh-i-tsiklevochnykh-mashin':
//                case '/remont-i-obsluzhivaniye-mozaichno-shlifovalnykh-mashin':
                    if (substr($origin_url, -1) !== '/') {
                        $new_url = $origin_url . '/';//substr($url, -1);
                        //if ($_SERVER['REMOTE_ADDR']=='78.46.62.217') {

                        $new_url = str_replace('//', '/', $new_url);


                        if ($new_url != $current_url) {

                            if (substr($origin_url, 1) !== '/') {
                                // $new_url='/'.$new_url;
                            }
                            if (isset($_SERVER['QUERY_STRING'])) {
                                if ($_SERVER['QUERY_STRING'] != '') {
                                    $new_url .= '?' . $_SERVER['QUERY_STRING'];
                                }
                            }
                            if ($_SERVER['REMOTE_ADDR'] == '188.130.177.18') {
                                // echo substr($origin_url, -1);//mb_substr($origin_url, 1). '$new_url.'.$new_url;die();
                            }

                            header('Location: ' . $new_url, true, 301);
                            exit();
                        }
                    }

                    break;

            }


        }

        if ($_SERVER['REMOTE_ADDR'] == '188.130.177.18') {
            //  echo $new_url.' '.$current_url;die();
        }
        $redirect_from = [
            '/otoplenie/gazovye-pushki',
            '/otoplenie/dizelnye-pushki',
            '/otoplenie/electricheskie-teplovie-nogi',
            '/otoplenie/infrakrasnye-obogrevateli',

            '/uborochnaja-tehnika',
            '/uborochnaja-tehnika/snegouborshchiki',
            '/uborochnaja-tehnika/pylesosy',
            '/uborochnaja-tehnika/podmetalnye-mashiny',
            '/uborochnaja-tehnika/minimojka',
            '/uborochnaja-tehnika/gladilnye-sistemy',
            '//motopompy-do-1000l-min.html',
            '//motopompy-do-1600l-min.html',
            '//motopompy-bolee-1600l-min.html',
            '//motopompy-forte.html',
            '//motopompy-kipor.html',
            '//motopompy-honda.html',
            '/sad-ogorod/selkhoztekhnika/senokosilka',
            '/sad-ogorod/selkhoztekhnika/traktory',
            '/sad-ogorod/selkhoztekhnika/minitraktora',
            '/sad-ogorod/selkhoztekhnika/motobloki',
            '/mototehnika/navesnoe-oborudovanie',
            '//generatory-kentavr.html',
            '//generatory-kipor.html',
            '//generatory-honda.html',
            '//generatory-hyundai.html',
            '//generatory-weekender.html',
            '//generatory-matari.html',
            '//betonomeshalki-kentavr.html',
            '//betonomeshalki-rostekh.html',
            '//betonomeshalki-forte.html',
            '//betonomeshalki-altard.html',
            '//betonomeshalki-limeks.html',
            '//betonomeshalki-vitals.html',
            '//betonomeshalki-som.html',
            '//betonomeshalki-agrimotor.html',
            '//betonomeshalki-werk.html',
            '//betonomeshalki-ot-70-l-do-150-l.html',
            '//betonomeshalki-ot-150-l-do-220-l.html',
            '//betonomeshalki-ot-220-l-do-500-l.html',
            '/generatory-1-kvt-2-kvt.html',
            '/generatory-2-kvt-3-kvt.html',
            '/generatory-3-kvt-5-kvt.html',
            '/generatory-5-kvt-10-kvt.html',
            '/generatory-10-kvt-15-kvt.html',
            '/generatory-15-kvt-50-kvt.html',
            '/storoitelnoe-oborudovanie/'
        ];
        $redirect_to = [
            '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki',
            '/teplovoe-oborudovanie/teplovye-pushki/dizelnye-pushki',
            '/teplovoe-oborudovanie/teplovye-pushki/elektricheskiye-teplovyye-pushki',
            '/teplovoe-oborudovanie/infrakrasnye-obogrevateli',
            '/uborochnaya-tekhnika',

            '/uborochnaya-tekhnika/snegouborshchiki',
            '/uborochnaya-tekhnika/pylesosy',
            '/uborochnaya-tekhnika/podmetalnye-mashiny',
            '/uborochnaya-tekhnika/minimojka',
            '/uborochnaya-tekhnika/gladilnye-sistemy',

            '/vodosnab/motopompy/?filtr_kpd_motopompy=417',
            '/vodosnab/motopompy/?filtr_kpd_motopompy=419',
            '/vodosnab',
            '/vodosnab/motopompy/?proizvoditel=200',
            '/vodosnab/motopompy/?proizvoditel=185',
            '/vodosnab/motopompy/?proizvoditel=193',
            '/selkhoztekhnika/senokosilka',
            '/selkhoztekhnika/traktory',
            '/selkhoztekhnika/minitraktora',
            '/mototehnika/motobloki-i-motokultivatory',
            '/selkhoztekhnika/navesnoe-oborudovanie',
            '/energosnab/generatory/?proizvoditel=155',
            '/energosnab/generatory/?proizvoditel=185',
            '/energosnab/generatory/?proizvoditel=193',
            '/energosnab/generatory/generatory-hyundai',
            '/energosnab/generatory/generatory-wekender',
            '/energosnab/generatory/generatory-matari',
            '/stroitelnoe-oborudovanie/betonomeshalki/?proizvoditel=155',
            '/stroitelnoe-oborudovanie/betonomeshalki/?proizvoditel=172',
            '/stroitelnoe-oborudovanie/betonomeshalki/?proizvoditel=200',
            '/stroitelnoe-oborudovanie/betonomeshalki/?proizvoditel=224',
            '/stroitelnoe-oborudovanie/betonomeshalki/?proizvoditel=182',
            '/stroitelnoe-oborudovanie/betonomeshalki/?proizvoditel=163',
            '/stroitelnoe-oborudovanie/betonomeshalki/?proizvoditel=151',
            '/stroitelnoe-oborudovanie/betonomeshalki/?proizvoditel=227',
            '/stroitelnoe-oborudovanie/betonomeshalki/?proizvoditel=161',
            '/stroitelnoe-oborudovanie/betonomeshalki/?filtr_obem_betonomesh=401',
            '/stroitelnoe-oborudovanie/betonomeshalki/?filtr_obem_betonomesh=400',
            '/stroitelnoe-oborudovanie/betonomeshalki/?filtr_obem_betonomesh=399',
            '/energosnab/generatory/generatory-1-kvt-2-kvt',
            '/energosnab/generatory/generatory-2-kvt-2-9-kvt',
            '/energosnab/generatory/generatory-3-kvt-4-9-kvt',
            '/energosnab/generatory/generatory-5-kvt-9-9-kvt',
            '/energosnab/generatory/generatory-10-kvt-14-9-kvt',
            '/energosnab/generatory/generatory-15-kvt-49-9-kvt',
            '/stroitelnoe-oborudovanie/'
        ];

        foreach ($redirect_from as $key => $value) {
            if ($current_url == $value) {
                if (isset($redirect_to[$key])) {
                    if ($value != $redirect_to[$key]) {
                        //  if ($_SERVER['REMOTE_ADDR']=='78.46.62.217') {
                        //    echo  $redirect_to[$key].' '.$value;die();
                        header('Location: ' . $redirect_to[$key], true, 301);
                        exit();
                        //}
                    }
                }
            }
        }


        $brands_from = [
            '/stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/glubinnye-vibratory-avant' => '/stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/filter/proizvoditiel/avant',
            '/stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/glubinnye-vibratory-ntp' => '/stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/filter/proizvoditiel/ntp-1',
            '/stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/glubinnye-vibratory-iv' => '/stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/filter/proizvoditiel/krasnyi-maiak',
            '/stroitelnoe-oborudovanie/vibratory/vibratory-krasnyj-majak' => '/stroitelnoe-oborudovanie/vibratory/filter/proizvoditiel/krasnyi-maiak',
            '/stroitelnoe-oborudovanie/vibratory/vibratory-enar' => '/stroitelnoe-oborudovanie/vibratory/filter/proizvoditiel/enar',
            '/stroitelnoe-oborudovanie/vibratory/vibratory-avant' => '/stroitelnoe-oborudovanie/vibratory/filter/proizvoditiel/avant',

            '/otbojnye-molotki/otbojnye-molotki-makita' => '/otbojnye-molotki/filter/proizvoditiel/makita',
            '/otbojnye-molotki/otbojnye-molotki-titan' => '/otbojnye-molotki/filter/proizvoditiel/titan',
            '/otbojnye-molotki/otbojnye-molotki-baumaster' => '/otbojnye-molotki/filter/proizvoditiel/baumaster',
            '/otbojnye-molotki/otbojnye-molotki-jenergomash' => '/otbojnye-molotki/filter/proizvoditiel/enierghomash',
            '/otbojnye-molotki/otbojnye-molotki-krasnyj-majak' => '/otbojnye-molotki/filter/proizvoditiel/krasnyi-maiak',
            '/otbojnye-molotki/otbojnye-molotki-sturm' => '/otbojnye-molotki/filter/proizvoditiel/sturm',


            '/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-stal' => '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/stal',
            '/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-rosteh' => '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/rostiekh',
            '/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-kentavr' => '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/kientavr',
            '/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-forte' => '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/forte',
            '/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-altrad' => '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/altrad-liv',
            '/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-limex' => '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/limex',
            '/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-vitals' => '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/vitals',
            '/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-agrimotor' => '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/agrimotor',
            '/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-werk' => '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/werk',
            '/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-sturm' => '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/sturm',
            '/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalki-com-ht' => '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/som',
            '/stroitelnoe-oborudovanie/betonomeshalki/betonomeshalky-orange' => '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/orange',


            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-avant' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/avant',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-masalta' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/masalta',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-ntc' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/ntc',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-enar' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/enar',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-honker' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/honker',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-swepac' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/swepac',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-biedronka' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/biedronka',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-agt' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/agt',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-krasnuy-mayak' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/krasnyi-maiak',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-kentavr' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/kientavr',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-ntp' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/ntp-1',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplity-werk' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/werk',
            '/stroitelnoe-oborudovanie/vibroplity/vybroplyty-costa' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/costa',
            '/stroitelnoe-oborudovanie/vibroplity/vybroplyty-mikasa' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/mikasa',
            '/stroitelnoe-oborudovanie/vibroplity/vibroplyty-scheppach' => '/stroitelnoe-oborudovanie/vibroplity/filter/proizvoditiel/scheppach',

            '/stroitelnoe-oborudovanie/vibrorejki/vibrorejki-enar' => '/stroitelnoe-oborudovanie/vibrorejki/filter/proizvoditiel/enar',
            '/stroitelnoe-oborudovanie/vibrorejki/vibroreyki-spektrum' => '/stroitelnoe-oborudovanie/vibrorejki/filter/proizvoditiel/spm',
            '/stroitelnoe-oborudovanie/vibrorejki/vibroreyki-technoflex' => '/stroitelnoe-oborudovanie/vibrorejki/filter/proizvoditiel/technoflex',


            '/stroitelnoe-oborudovanie/vibrotrambovki/vibrotrambovki-agt' => '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel/agt',
            '/stroitelnoe-oborudovanie/vibrotrambovki/vibrotrambovki-nts' => '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel/ntc',
            '/stroitelnoe-oborudovanie/vibrotrambovki/vibrotrambovki-enar' => '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel/enar',
            '/stroitelnoe-oborudovanie/vibrotrambovki/vibrotrambovki-biedronka' => '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel/biedronka',
            '/stroitelnoe-oborudovanie/vibrotrambovki/vibrotrambovki-honker' => '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel/honker',
            '/stroitelnoe-oborudovanie/vibrotrambovki/vibrotrambovki-masalta' => '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel/masalta',
            '/stroitelnoe-oborudovanie/vibrotrambovki/vibrotrambovkvi-fastverdini' => '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel/fastverdini',
            '/stroitelnoe-oborudovanie/vibrotrambovki/vibrotrambovkvi-kentavr' => '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel/kientavr',
            '/stroitelnoe-oborudovanie/vibrotrambovki/vibrotrambovki-avant' => '/stroitelnoe-oborudovanie/vibrotrambovki/filter/proizvoditiel/avant',

            '/stroitelnoe-oborudovanie/zatirochnye-mashiny/zatirochnye-mashiny-avant' => '/stroitelnoe-oborudovanie/zatirochnye-mashiny/filter/proizvoditiel/avant',

            '/stroitelnoe-oborudovanie/shvonarezchiki/shvonarezchiki-avant' => '/stroitelnoe-oborudovanie/shvonarezchiki/filter/proizvoditiel/avant',
            '/stroitelnoe-oborudovanie/shvonarezchiki/shvonarezchiki-masalta' => '/stroitelnoe-oborudovanie/shvonarezchiki/filter/proizvoditiel/masalta',
            '/stroitelnoe-oborudovanie/shvonarezchiki/shvonarezchiki-enar' => '/stroitelnoe-oborudovanie/shvonarezchiki/filter/proizvoditiel/enar',
            '/stroitelnoe-oborudovanie/shvonarezchiki/shvonarezchiki-agt' => '/stroitelnoe-oborudovanie/shvonarezchiki/filter/proizvoditiel/agt',
            '/stroitelnoe-oborudovanie/shvonarezchiki/shvonarezchiki-ntc' => '/stroitelnoe-oborudovanie/shvonarezchiki/filter/proizvoditiel/ntc',
            '/stroitelnoe-oborudovanie/shvonarezchiki/shvonarezchiki-werk' => '/stroitelnoe-oborudovanie/shvonarezchiki/filter/proizvoditiel/werk',


            '/mototehnika/motobloki-i-kul-tivatory-kentavr' => '/mototehnika/motobloki-i-motokultivatory/filter/proizvoditiel/kientavr',
            '/mototehnika/motobloki-i-kul-tivatory-forte' => '/mototehnika/motobloki-i-motokultivatory/filter/proizvoditiel/forte',
            '/mototehnika/motobloki-i-motokul-tivatory-zirka' => '/mototehnika/motobloki-i-motokultivatory/filter/proizvoditiel/zirka',


            '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-equation' => '/teplovoe-oborudovanie/teplovye-pushki/filter/proizvoditiel/equation',
            '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-sakuma' => '/teplovoe-oborudovanie/teplovye-pushki/filter/proizvoditiel/sakuma',
            '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-grunhelm' => '/teplovoe-oborudovanie/teplovye-pushki/filter/proizvoditiel/grunhelm',
            '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-master' => '/teplovoe-oborudovanie/teplovye-pushki/filter/proizvoditiel/master',
            '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-forte' => '/teplovoe-oborudovanie/teplovye-pushki/filter/proizvoditiel/forte',

            '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-grunfeld' => '/teplovoe-oborudovanie/teplovye-pushki/filter/proizvoditiel/grunfeld',
            '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-vitals' => '/teplovoe-oborudovanie/teplovye-pushki/filter/proizvoditiel/vitals',
            '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-biemmedue' => '/teplovoe-oborudovanie/teplovye-pushki/filter/proizvoditiel/biemmedue',
            '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-ballu' => '/teplovoe-oborudovanie/teplovye-pushki/filter/proizvoditiel/ballu',
            '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-avant' => '/teplovoe-oborudovanie/teplovye-pushki/filter/proizvoditiel/avant',
            '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-termija' => '/teplovoe-oborudovanie/teplovye-pushki/filter/proizvoditiel/tiermiia',


            '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/gazovye-teplovye-pushki-grunhelm' => '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/filter/proizvoditiel/grunhelm',
            '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/gazovye-teplovye-pushki-master' => '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/filter/proizvoditiel/master',
            '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/gazovye-teplovye-pushki-vitals' => '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/filter/proizvoditiel/vitals',
            '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/gazovye-teplovye-pushki-sakuma' => '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/filter/proizvoditiel/sakuma',
            '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/gazovyk-teplovye-pushki-grunfeld' => '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/filter/proizvoditiel/grunfeld',


            '/teplovoe-oborudovanie/teplovye-pushki/dizelnye-pushki/dizel-nye-pushki-sakuma' => '/teplovoe-oborudovanie/teplovye-pushki/dizelnye-pushki/filter/proizvoditiel/sakuma',
            '/teplovoe-oborudovanie/teplovye-pushki/dizelnye-pushki/dizel-nyja-teplovyk-pushki-grunhelm' => '/teplovoe-oborudovanie/teplovye-pushki/dizelnye-pushki/filter/proizvoditiel/grunfeld',
            '/teplovoe-oborudovanie/teplovye-pushki/dizelnye-pushki/dizel-nye-teplovye-pushki-vitals' => '/teplovoe-oborudovanie/teplovye-pushki/dizelnye-pushki/filter/proizvoditiel/vitals',
            '/teplovoe-oborudovanie/teplovye-pushki/dizelnye-pushki/dizel-nye-teplovye-pushki-master' => '/teplovoe-oborudovanie/teplovye-pushki/dizelnye-pushki/filter/proizvoditiel/master',


            '/teplovoe-oborudovanie/teplovye-pushki/elektricheskiye-teplovyye-pushki/teplovye-pushki-termija' => '/teplovoe-oborudovanie/teplovye-pushki/elektricheskiye-teplovyye-pushki/filter/proizvoditiel/tiermiia',
            '/teplovoe-oborudovanie/teplovye-pushki/elektricheskiye-teplovyye-pushki/jelektricheskie-teplovye-pushki-sakuma' => '/teplovoe-oborudovanie/teplovye-pushki/elektricheskiye-teplovyye-pushki/filter/proizvoditiel/sakuma',
            '/teplovoe-oborudovanie/teplovye-pushki/elektricheskiye-teplovyye-pushki/jelektricheskie-teplovye-pushki-vitals' => '/teplovoe-oborudovanie/teplovye-pushki/elektricheskiye-teplovyye-pushki/filter/proizvoditiel/vitals',
            '/teplovoe-oborudovanie/teplovye-pushki/elektricheskiye-teplovyye-pushki/jelektricheskie-teplovye-pushki-grunfeld' => '/teplovoe-oborudovanie/teplovye-pushki/elektricheskiye-teplovyye-pushki/filter/proizvoditiel/grunfeld',
            '/teplovoe-oborudovanie/teplovye-pushki/elektricheskiye-teplovyye-pushki/jelektricheskie-teplovye-pushki-avant' => '/teplovoe-oborudovanie/teplovye-pushki/elektricheskiye-teplovyye-pushki/filter/proizvoditiel/avant',


            '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-na-otrabotannom-masle/teplovye-pushki-na-otrabotannom-masle-master' => '/teplovoe-oborudovanie/teplovye-pushki/teplovye-pushki-na-otrabotannom-masle/filter/proizvoditiel/master',

            '/energosnab/generatory/generatory-kama' => '/energosnab/generatory/filter/proizvoditiel/kama',
            '/energosnab/generatory/generatory-gude' => '/energosnab/generatory/filter/proizvoditiel/gude',
            '/energosnab/generatory/generatory-makita' => '/energosnab/generatory/filter/proizvoditiel/makita',
            '/energosnab/generatory/generatory-green-power' => '/energosnab/generatory/filter/proizvoditiel/greenpower',
            '/energosnab/generatory/generatory-sturm' => '/energosnab/generatory/filter/proizvoditiel/sturm',
            '/energosnab/generatory/generatory-firman' => '/energosnab/generatory/filter/proizvoditiel/firman',
            '/energosnab/generatory/generatory-enmax' => '/energosnab/generatory/filter/proizvoditiel/enmax',
            '/energosnab/generatory/generatory-gerrard' => '/energosnab/generatory/filter/proizvoditiel/gerrard',
            '/energosnab/generatory/generatory-daewoo' => '/energosnab/generatory/filter/proizvoditiel/daewoo',
            '/energosnab/generatory/generatory-fogo' => '/energosnab/generatory/filter/proizvoditiel/fogo',
            '/energosnab/generatory/generatory-eisemann' => '/energosnab/generatory/filter/proizvoditiel/eisemann',
            '/energosnab/generatory/generatory-dedra' => '/energosnab/generatory/filter/proizvoditiel/dedra',
            '/energosnab/generatory/generatory-honda' => '/energosnab/generatory/filter/proizvoditiel/honda',
            '/energosnab/generatory/generatory-kipor' => '/energosnab/generatory/filter/proizvoditiel/kipor',

            '/energosnab/generatory/generatory-kentavr' => '/energosnab/generatory/filter/proizvoditiel/kentavr',
            '/energosnab/generatory/generatory-hyundai' => '/energosnab/generatory/filter/proizvoditiel/hyundai',
            '/energosnab/generatory/generatory-wekender' => '/energosnab/generatory/filter/proizvoditiel/wekender',
            '/energosnab/generatory/generatory-iron-angel' => '/energosnab/generatory/filter/proizvoditiel/iron-angel',
            '/energosnab/generatory/generatory-weima' => '/energosnab/generatory/filter/proizvoditiel/weima',


            '/energosnab/generatory/generatory-matari' => '/energosnab/generatory/filter/proizvoditiel/matari',
            '/energosnab/generatory/generatory-endress' => '/energosnab/generatory/filter/proizvoditiel/endress',
            '/energosnab/generatory/generatory-dalgakiran' => '/energosnab/generatory/filter/proizvoditiel/dalgakiran',

            '/energosnab/generatory/generatory-hitachi' => '/energosnab/generatory/filter/proizvoditiel/hitachi',
            '/energosnab/generatory/generatory-generac' => '/energosnab/generatory/filter/proizvoditiel/generac',
            '/energosnab/generatory/generatory-titan' => '/energosnab/generatory/filter/proizvoditiel/titan',
            '/energosnab/generatory/generatory-vitals' => '/energosnab/generatory/filter/proizvoditiel/vitals',
            '/energosnab/generatory/generatory-nik' => '/energosnab/generatory/filter/proizvoditiel/nik',
            '/energosnab/generatory/generatory-sunshow' => '/energosnab/generatory/filter/proizvoditiel/sunshow',
            '/energosnab/generatory/generatory-biedronka' => '/energosnab/generatory/filter/proizvoditiel/biedronka',
            '/energosnab/generatory/generatory-odwerk' => '/energosnab/generatory/filter/proizvoditiel/odwerk',
            '/energosnab/generatory/generatory-elemax' => '/energosnab/generatory/filter/proizvoditiel/elemax',
            '/energosnab/generatory/generatory-cummins' => '/energosnab/generatory/filter/proizvoditiel/cummins',
            '/energosnab/generatory/generatory-energy-power' => '/energosnab/generatory/filter/proizvoditiel/energy-power',
            '/energosnab/generatory/generatory-forte' => '/energosnab/generatory/filter/proizvoditiel/forte',
            '/energosnab/generatory/generatory-werk' => '/energosnab/generatory/filter/proizvoditiel/werk',

            '/energosnab/generatory/generatory-black-decker' => '/energosnab/generatory/filter/proizvoditiel/black-decker',
            '/energosnab/generatory/generatory-scheppach' => '/energosnab/generatory/filter/proizvoditiel/scheppach',
            '/energosnab/generatory/generatory-stark' => '/energosnab/generatory/filter/proizvoditiel/stark',
            '/energosnab/generatory/generatory-vulkan' => '/energosnab/generatory/filter/proizvoditiel/vulkan',
            '/energosnab/generatory/generatory-rato' => '/energosnab/generatory/filter/proizvoditiel/rato',


            '/sad-ogorod/sadovaya-tekhnika/motobloki' => '/mototehnika/motobloki-i-motokultivatory',

            '/sad-ogorod/sadovaya-tekhnika/senokosilka' => '/selkhoztekhnika/senokosilka',
            '/sad-ogorod/sadovaya-tekhnika/traktory' => '/selkhoztekhnika/traktory',
            '/sad-ogorod/sadovaya-tekhnika/minitraktora' => '/selkhoztekhnika/minitraktora',
            '/stroitelnoe-oborudovanie/vibratory/ploshchadochnye-vibratory_secure=x1' => '/stroitelnoe-oborudovanie/vibratory/ploshchadochnye-vibratory',
            '/sad-ogorod/sadovaya-tekhnika/motobloki' => '/mototehnika/motobloki-i-motokultivatory',
            /*'/energosnab/generatory/generatory-bolee-50-kvt/' => '/energosnab/generatory/filter/moshchnost-ghienieratora/50-kvt-100-kvt/',*/
            '/energosnab/generatory/generatory-15-kvt-49-9-kvt' => '/energosnab/generatory/filter/moshchnost-ghienieratora/15-kvt-50-kvt',
            '/energosnab/generatory/generatory-10-kvt-14-9-kvt' => '/energosnab/generatory/filter/moshchnost-ghienieratora/10-kvt-15-kvt',
            '/energosnab/generatory/generatory-5-kvt-9-9-kvt' => '/energosnab/generatory/filter/moshchnost-ghienieratora/5-kvt-10-kvt',
            '/energosnab/generatory/generatory-3-kvt-4-9-kvt' => '/energosnab/generatory/filter/moshchnost-ghienieratora/3-kvt-5-kvt',
            '/energosnab/generatory/generatory-2-kvt-2-9-kvt' => '/energosnab/generatory/filter/moshchnost-ghienieratora/2-kvt-3-kvt',
            '/energosnab/generatory/generatory-1-kvt-2-kvt' => '/energosnab/generatory/filter/moshchnost-ghienieratora/1-kvt-2-kvt',

            '/energosnab/generatory/generatory-tagred' => '/energosnab/generatory/filter/proizvoditiel/tagred',




            '/vibroplita-bert-c90.html' => '/vibroplita-avant-ap-95.html'
            //''=>'',

            //'generatory-2-kvt-2-9-kvt'
        ];


        $origin_url_for_redirect = $origin_url;
        if ($_SERVER['REMOTE_ADDR'] == '188.130.177.18') {
//echo '$origin_url'.$origin_url;die();
            //if (isset($_GET[]))
        }

        /* if ($_SERVER['REMOTE_ADDR'] == '78.46.62.217') {
            echo $origin_url.' '.$brands_from[$origin_url];
        }*/
        if (isset($brands_from[$origin_url])) {
            if ($_SERVER['REMOTE_ADDR'] == '188.130.177.18') {
//echo '$origin_url'.$origin_url. ' '.$brands_from[$origin_url];die();
                //if (isset($_GET[]))
            }
            $origin_url_for_redirect = $brands_from[$origin_url];
            header('Location: ' . $brands_from[$origin_url], true, 301);
            exit();
        }


        if (strpos($current_url, '?') !== false) {


            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $attributeModel = Mage::getModel('eav/entity_attribute');
            foreach ($_GET as $key => $value) {
                $id_attribute = (int)$attributeModel->loadByCode('catalog_product', $key)->getId();
                $id_option = $value;

                $select = $readConnection->select()
                    ->from('eav_attribute_option_value')
                    ->join(
                        array('eav_attribute_option' => 'eav_attribute_option'),
                        'eav_attribute_option.option_id = eav_attribute_option_value.option_id',
                        array('eav_attribute_option.*')
                    )
                    ->join(
                        array('catalin_seo_attribute' => 'catalin_seo_attribute_url_key'),
                        'eav_attribute_option.option_id = catalin_seo_attribute.option_id',// and eav_attribute_option.attribute_id=:attribute_id',
                        array('catalin_seo_attribute.url_key', 'catalin_seo_attribute.url_value')
                    )
                    ->where('eav_attribute_option.attribute_id=:attribute_id AND eav_attribute_option_value.option_id=:option_id')
                    ->group('eav_attribute_option_value.option_id');

                $bind = array(
                    'attribute_id' => $id_attribute,
                    'option_id' => $id_option
                );

                $results = $readConnection->fetchAll($select, $bind);
                /*echo '<pre>$results';
                print_r($results);
                echo  '</pre>';*/
                $url_redirect = false;
                if (count($results) > 0) {
                    foreach ($results as $res) {
                        if (substr($origin_url_for_redirect, -1) !== '/') $origin_url_for_redirect .= '/';
                        if (strpos($origin_url_for_redirect, 'filter') === false) {
                            $origin_url_for_redirect .= 'filter/';
                        }
                        $url_redirect = $origin_url_for_redirect . $res['url_key'] . '/' . $res['url_value'];// . '/';
                        break;
                    }

                    if (($url_redirect) && ($url_redirect != $current_url)) {
                        header('Location: ' . $url_redirect, true, 301);
                        exit();
                    }
                }

                //break;


            }
        }

        if (strpos($current_url, '/filter/') !== false) {
            $origin_url_for_redirect=$current_url;
            if (substr($origin_url_for_redirect, -1) === '/') {
                $origin_url_for_redirect = substr($origin_url_for_redirect, 0, -1);
                if ($origin_url_for_redirect!=$current_url){
                    header('Location: ' . $origin_url_for_redirect, true, 301);
                    exit();
                }

            }
        }

        // redirect brands
        /*
        /stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/glubinnye-vibratory-avant/  /stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/filter/proizvoditiel/avant
    /stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/glubinnye-vibratory-ntp/  /stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/filter/proizvoditiel/ntp-1
    /stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/glubinnye-vibratory-iv/   /stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/filter/proizvoditiel/krasnyi-maiak


    /stroitelnoe-oborudovanie/vibratory/vibratory-krasnyj-majak/  /stroitelnoe-oborudovanie/vibratory/filter/proizvoditiel/krasnyi-maiak
    /stroitelnoe-oborudovanie/vibratory/vibratory-enar/   /stroitelnoe-oborudovanie/vibratory/filter/proizvoditiel/enar
    /stroitelnoe-oborudovanie/vibratory/vibratory-avant/  /stroitelnoe-oborudovanie/vibratory/filter/proizvoditiel/avant



        */


        //if ($_SERVER['REMOTE_ADDR']=='78.46.62.217') {


        $request = Mage::app()->getRequest();
        //$this->_request = $request;
        $current_url = Mage::app()->getRequest()->getPathInfo();
        //echo  $current_url;
        //die();
        if (strpos($current_url, 'currency/switch/') !== false) {

            Mage::app()->getResponse()->setHeader('HTTP/1.1', '404 Not Found');
            Mage::app()->getResponse()->setHeader('Status', '404 File not found');
            $pageId = Mage::getStoreConfig('web/default/cms_no_route');
            //  if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
            //$observer->getEvent()->getFront()->_forward('defaultNoRoute');
            $request->setControllerName('indexController')
                ->setModuleName('Mage_Cms')
                ->setActionName('defaultNoRoute')
                ->setDispatched(false);
            /* ->setModuleName('cms')
                 ->setControllerName($controllerName)
                 ->setActionName($actionName)
                 ->setParam('id', $product->getId())
                 ->setParam('category_id', $id);*/
            //}


            //echo 'test';die();
            return false;
        }


        if (
            (strpos($current_url, '-v-dnepre-dnepropetrovske') !== false)
            || (strpos($current_url, '-v-uzhgorode') !== false)
            || (strpos($current_url, '-v-vinnice') !== false)
            || (strpos($current_url, '-v-chernigove') !== false)
            || (strpos($current_url, '-v-zhitomire') !== false)
            || (strpos($current_url, '-v-sumah') !== false)
            || (strpos($current_url, '-v-poltave') !== false)
            || (strpos($current_url, '-v-har-kove') !== false)
            || (strpos($current_url, '-v-zaporozh-e') !== false)
            || (strpos($current_url, '-v-hersone') !== false)
            || (strpos($current_url, '-v-nikolaeve') !== false)
            || (strpos($current_url, '-v-kirovograde') !== false)
            || (strpos($current_url, '-v-cherkassah') !== false)
            || (strpos($current_url, '-v-hmel-nickom') !== false)
            || (strpos($current_url, '-v-ternopole') !== false)
            || (strpos($current_url, '-v-rovno') !== false)
            || (strpos($current_url, '-v-lucke') !== false)
            || (strpos($current_url, '-vo-l-vove') !== false)
            || (strpos($current_url, '-v-chernovcah') !== false)
            || (strpos($current_url, '-v-ivano-frankovske') !== false)
            || (strpos($current_url, '-v-odesse') !== false)
        ) {
            $_explode_current_url = explode('/', $current_url);
            $url_category = $_explode_current_url[count($_explode_current_url) - 1];
            // echo '$url_category'.$url_category.'<br/>';
            $category = Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToFilter('url_key', $url_category)
                ->getFirstItem();
            /*echo '<pre>$category';
            print_r($category);//Mage::app()->getRequest());
            echo '</pre>';
            die();*/
            if ($category) {
                $parent_category = $category->getParentCategory();

                if ($parent_category) {
                    $url_category = $parent_category->getUrlPath();
                    //echo '$url_category'.$url_category;die();
                    if ($url_category) {
                        header("Location: /" . $url_category, TRUE, 301);
                        exit();
                    }
                }
            }
            //echo $parent_category->getId().' '.
            // echo $category->getName() . ' ' . $category->getUrlPath();


            //die();
        }

    }

    /**
     * Get the request object
     *
     * @return Zend_Controller_Request_Http
     */
    public function getRequest()
    {
        return $this->_request;
    }

    /**
     * Validate and Match Cms Page and modify request
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request)
    {


        // }
        // $requestUri = $this->_preparePathInfo($request->getPathInfo());


        /*if (($requestUri = $this->_preparePathInfo($request->getPathInfo())) === false) {
            return false;
        }*/
        /*
                if ($this->_match($requestUri) !== false) {
                    $request->setAlias(
                        Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
                        $requestUri// . Mage::getStoreConfig('attributeSplash/seo/url_suffix')
                    );

                    Mage::helper('alphabet')->clearLayerRewrites();

                    return true;
                }
        */


        return false;
    }


    /**
     * Prepare the path info variable
     *
     * @param string $pathInfo
     * @return false|string
     */
    protected function _preparePathInfo($pathInfo)
    {
        $requestUri = trim($pathInfo, '/');

        return $requestUri;
    }

}
