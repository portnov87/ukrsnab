<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Advanced SEO Suite
 * @version   1.0.3
 * @revision  239
 * @copyright Copyright (C) 2013 Mirasvit (http://mirasvit.com/)
 */


class Webfresh_Seo_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $config;

    public function __construct()
    {
        $this->_config = Mage::getModel('seo/config');
    }

    public function getBaseUri()
    {
        $baseStoreUri = parse_url(Mage::getUrl(), PHP_URL_PATH);
        if ($baseStoreUri == '/') {
            return $_SERVER['REQUEST_URI'];
        } else {
            return DS . str_replace($baseStoreUri, '', $_SERVER['REQUEST_URI']);;
        }
    }

    protected function checkRewrite()
    {
        $uri = $this->getBaseUri();
        $uri=str_replace('?isLayerAjax=1','',$uri);

        $collection = Mage::getModel('seo/meta')->getCollection()
            ->addStoreFilter(Mage::app()->getStore())
            ->addEnableFilter();
        //echo '<div class="checkRewrite" style="disaply:none;">';
        foreach ($collection as $rewrite) {
            //echo $uri.' '.$rewrite->getUrl()."<br/><br/>\r\n\r\n";
            if ($this->checkPattern($uri, $rewrite->getUrl())) {
                return $rewrite;
            }elseif($uri==$rewrite->getUrl()){
                return $rewrite;
            }
        }
        return false;
        //echo '</div>';
    }

    /**
     * Возвращает сео-данные для текущей страницы
     *
     * Возвращает объект с методами:
     * getTitle() - заголовок H1
     * getDescription() - SEO текст
     * getMetaTitle()
     * getMetaKeyword()
     * getMetaDescription()
     *
     * Если для данной страницы нет СЕО, то возвращает пустой Varien_Object
     *
     * @return Varien_Object $result
     */
    public function getCurrentSeo()
    {
        if (Mage::app()->getStore()->getCode() == 'admin') {
            return new Varien_Object();
        }

        $isCategory = Mage::registry('current_category') || Mage::registry('category');


        if ($isCategory) {
            $filters = Mage::getSingleton('catalog/layer')->getState()->getFilters();
            $isFilter = count($filters) > 0;
           // echo '$filters'.count($filters);
        }


        if (Mage::registry('current_product') || Mage::registry('product')) {
            $seo = Mage::getSingleton('seo/object_product');
        } elseif ($isCategory && $isFilter) {
            $seo = Mage::getSingleton('seo/object_filter');
        } elseif ($isCategory) {
            $seo = Mage::getSingleton('seo/object_category');
        } else {
            $seo = new Varien_Object();
        }

        if ($seoRewrite = $this->checkRewrite()) {

            foreach ($seoRewrite->getData() as $k => $v) {
                if ($v) {
                    $seo->setData($k, $v);
                }
            }
        }
        if (Mage::registry('current_category')) {
            $page = Mage::app()->getFrontController()->getRequest()->getParam('p');
            if ($page > 1) {
                $seo->setMetaTitle(Mage::helper('seo')->__("Страница %s | %s", $page, $seo->getMetaTitle()));
                $seo->setDescription('');
            }
        }

        return $seo;
    }

    public function checkPattern($string, $pattern, $caseSensative = false)
    {
        if (!$caseSensative) {
            $string = strtolower($string);


            $pattern = strtolower($pattern);
        }

        $parts = explode('*', $pattern);
        $index = 0;
        $shouldBeFirst = true;
        $shouldBeLast = true;

        foreach ($parts as $part) {
            if ($part == '') {
                $shouldBeFirst = false;
                continue;
            }
            $index = strpos($string, $part, $index);
            if ($index === false) {
                return false;
            }
            if ($shouldBeFirst && $index > 0) {
                return false;
            }
            $shouldBeFirst = false;
            $index += strlen($part);
        }

        if (count($parts) == 1) {
            return $string == $pattern;
        }
        $last = end($parts);
        if ($last == '') {
            return true;
        }
        if (strrpos($string, $last) === false) {
            return false;
        }
        return true;
    }

    public function cleanMetaTag($tag)
    {
        $tag = strip_tags($tag);
        //~ $tag = html_entity_decode($tag);//for case we have tags like &nbsp; added by some extensions //in some hosting adds unrecognized symbols
//        $tag = preg_replace('/[^a-zA-Z0-9_ \-()\/%-&]/s', '', $tag);
        $tag = preg_replace('/\s{2,}/', ' ', $tag); //remove unnecessary spaces
        $tag = preg_replace('/\"/', ' ', $tag); //remove " because it destroys html
        $tag = trim($tag);
        return $tag;
    }


    public function normalize($string)
    {
        $table = array(
            'Š' => 'S', 'š' => 's', 'Đ' => 'Dj', 'đ' => 'dj', 'Ž' => 'Z', 'ž' => 'z', 'Č' => 'C', 'č' => 'c', 'Ć' => 'C', 'ć' => 'c',
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
            'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
            'Õ' => 'O', 'Ö' => 'Oe', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'ae', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
            'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
            'ô' => 'o', 'õ' => 'o', 'ö' => 'oe', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b',
            'ÿ' => 'y', 'Ŕ' => 'R', 'ŕ' => 'r', 'ü' => 'ue', '/' => '', '-' => '', '&' => '', ' ' => '', '(' => '', ')' => ''
        );

        $string = strtr($string, $table);
        $string = Mage::getSingleton('catalog/product_url')->formatUrlKey($string);
        $string = str_replace(array('-'), '', $string); //убираем системные разделители частей урла
        return $string;
    }


    /**
     * get path for Ecomerc
     */
    public function getPathCategories($_product)
    {

        $path_cat_name = [];
        $categories = $_product->getCategoryIds();
        if (isset($categories[count($categories) - 1])) {
            $id_parent_cat = $categories[count($categories) - 1];
          //  echo '$id_parent_cat'.$id_parent_cat.'<br/>';
            $category = Mage::getModel('catalog/category')->load($id_parent_cat);
            if ($category) {
                $path = $category->getPath();
                $path_explode = explode('/', $path);

               if (count($path_explode) == 4) {
// надо взять раздел на один выше
                   if (isset($categories[count($categories) - 2])) {
                       $id_parent_cat = $categories[count($categories) - 2];
                       $category = Mage::getModel('catalog/category')->load($id_parent_cat);
                       if ($category) {
                           $path = $category->getPath();
                           $path_explode = explode('/', $path);
                       }
                   }
                }



                foreach ($path_explode as $cat) {
                    if (($cat != 1) && ($cat != 3)){
                        $cat_model = Mage::getModel('catalog/category')->load($cat);

                        if ($cat_model) {
                            $path_cat_name[] = trim($cat_model->getName());
                        }
                    }
                }


                return implode('/',$path_cat_name);
            }
            /**/
        }
        return $path_cat_name;
    }


    public function getFaq($data)
    {
        $faqArray=[];
        if (isset($data['faq_title_1'])&&isset($data['faq_content_1']))
        {
            if (($data['faq_title_1']!='')&&($data['faq_content_1']!=''))
            {
                $faqArray[$data['faq_title_1']]=$data['faq_content_1'];
            }

        }

        if (isset($data['faq_title_2'])&&isset($data['faq_content_2']))
        {
            if (($data['faq_title_2']!='')&&($data['faq_content_2']!=''))
            {
                $faqArray[$data['faq_title_2']]=$data['faq_content_2'];
            }

        }

        if (isset($data['faq_title_3'])&&isset($data['faq_content_3']))
        {
            if (($data['faq_title_3']!='')&&($data['faq_content_3']!=''))
            {
                $faqArray[$data['faq_title_3']]=$data['faq_content_3'];
            }

        }

        if (isset($data['faq_title_4'])&&isset($data['faq_content_4']))
        {
            if (($data['faq_title_4']!='')&&($data['faq_content_4']!=''))
            {
                $faqArray[$data['faq_title_4']]=$data['faq_content_4'];
            }

        }

        if (isset($data['faq_title_5'])&&isset($data['faq_content_5']))
        {
            if (($data['faq_title_5']!='')&&($data['faq_content_5']!=''))
            {
                $faqArray[$data['faq_title_5']]=$data['faq_content_5'];
            }

        }
        return $faqArray;

    }

    public function getFaqH2($data)
    {

        $h2='';
        if (isset($data['faq_h2']))
        {
            if ($data['faq_h2']!='')
                $h2 = $data['faq_h2'];

        }
        return $h2;
    }
}
