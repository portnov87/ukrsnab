<?php

class Webfresh_Seo_Helper_Parse extends Mage_Core_Helper_Abstract
{
  	//e.g. of str
    //[product_name][, model: {product_model}!] [product_nonexists]  [buy it {product_nonexists} !]
	public function parse($str, $objects, $additional = array(), $storeId = false)
    {
		if (trim($str) == '') {
			return null;
		}

        $b1Open = '[ZZZZZ';
        $b1Close = 'ZZZZZ]';
        $b2Open = '{WWWWW';
        $b2Close = 'WWWWW}';

        $str = str_replace('[', $b1Open, $str);
        $str = str_replace(']', $b1Close, $str);
        $str = str_replace('{', $b2Open, $str);
        $str = str_replace('}', $b2Close, $str);
        $pattern = '/\[ZZZZZ[^ZZZZZ\]]*ZZZZZ\]/';
        // echo $pattern."\n";
        // echo $str."\n";
        preg_match_all($pattern, $str, $matches, PREG_SET_ORDER);
        $vars = array();
        foreach ($matches as $matche) {
            $vars[$matche[0]] = $matche[0];
        }
        //print_R($vars);die;
        //echo count($objects);
        /* echo '<pre>$vars';
                                    print_r($vars);
                                    echo '</pre>';*/
   
		foreach ($objects as $key => $object) {
                    
                    /**/
                    /*if ($key!='category')
                    {
                             echo '<pre>ff'.$key;
                    print_r($object);
                    echo '</pre>';
                    die();
                    }*/
                    $data = $object->getData();
                    
                   
                    if (isset($additional[$key])) {
                        $data = array_merge($data, $additional[$key]);
                    }
 
                                   
			foreach ($data as $dataKey => $value) {
				if (is_array($value) || is_object($value)) {
					continue;
				}

                                    $value = $this->checkForConvert($object, $key, $dataKey, $value, $storeId);
                                    //echo $key.' '.$value.' <br/>';
                                   
                                    foreach ($vars as $k =>$v) {
                                        $k1 = $b2Open.$key.'_'.$dataKey.$b2Close;
                                        $k2 = $b1Open.$key.'_'.$dataKey.$b1Close;
                                       /* if ($value == '') {
                                            if (stripos($v, $k1) !== false || stripos($v, $k2) !== false) {
                                                $vars[$k] = '';
                                                continue;
                                            }
                                        }*/

                                                    $v = str_replace($k1, $value, $v);
                                                    $v = str_replace($k2, $value, $v);
                                                    $vars[$k] = $v;
                                    }
			}
        } 
        // print_R($str);
        // echo "\n";

		foreach ($vars as $k =>$v) {
			//if no attibute like [product_nonexists]
			if ($v == $k) {
				$v = '';
            }
            //remove start and end symbols from the string (trim)
            if (substr($v, 0, strlen($b1Open)) == $b1Open) {
                    $v = substr($v, strlen($b1Open), strlen($v));
            }
            if (strpos($v, $b1Close) === strlen($v)-strlen($b1Close)) {
                $v = substr($v, 0, strlen($v)-strlen($b1Close));
            }

		    //if no attibute like [buy it {product_nonexists} !]
		    if (stripos($v, $b2Open) !== false || stripos($v, $b1Open) !== false) {
                $v = '';
		    }
            $str = str_replace($k, $v, $str);
		}
		return $str;
	}

	protected function checkForConvert($object, $key, $dataKey, $value, $storeId)
    {
        if ($key == 'product' || $key == 'category') {
            if ($key == 'product') {
                $attribute = Mage::getSingleton('catalog/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, $dataKey);
            } else {
                //if ($dataKey=='price_min') return $value;
               // else 
                    $attribute = Mage::getSingleton('catalog/config')->getAttribute(Mage_Catalog_Model_Category::ENTITY, $dataKey);
            }
            if ($storeId) {
                $attribute->setStoreId($storeId);
            }
            if ($attribute->getId() > 0) {
                try {
                    $valueId = $object->getDataUsingMethod($dataKey);
                    $value = $attribute->getFrontend()->getValue($object);
                } catch(Exception $e) {//possible that some extension is removed, but we have it attribute with source in database
                    $value = '';
                }
                if ($value == 'No' && $valueId == '') {
                    $value = '';
                }
                switch ($dataKey) {
                    case 'price_min':case 'price':
                        $value = Mage::helper('core')->currency($value, true, false);
                        break;
                    case 'special_price':
                        $value = Mage::helper('core')->currency($value, true, false);
                        break;                    
                }
	        } else {
                switch ($dataKey) {
                    case 'final_price':
                        $value = Mage::helper('core')->currency($value, true, false);
                        break;    
                }
            }
        }
        if (is_array($value)) {
           if (isset($value['label'])) {
               $value = $value['label'];
           } else {
               $value = '';
           }
        }
	    return $value;
	}

        
         public function parseFilterInformationFromRequest($requestString, $storeId) {
        // case 1: there is a speaking url for current request path -> not our business
        $rewrite = Mage::getResourceModel('catalog/url')->getRewriteByRequestPath($requestString, $storeId);
        if ($rewrite && $rewrite->getUrlRewriteId()) {
            return false;
        }

        $configUrlSuffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
        $shortRequestString = substr($requestString, 0, strrpos($requestString, '/')) . $configUrlSuffix;
        $rewrite = Mage::getResourceModel('catalog/url')->getRewriteByRequestPath($shortRequestString, $storeId);

        // case 2: the shortened request path cannot be found as rewrite -> no category -> not our business
        if (!$rewrite || !$rewrite->getUrlRewriteId() || !$rewrite->getCategoryId()) {
            return false;
        }

        // case 3: we have a category. May be our business.
        $categoryId = $rewrite->getCategoryId();
        $category = Mage::getModel('catalog/category')->load($categoryId);
        if (!$category->getId()) {
            return false;
        }

        // get last part of the URL - if we have filter base urls the filter options are lowercased and concetenated by
        // dashes. The standard file extension of catalog pages may have to be removed first.
        $filterString = substr($requestString, strrpos($requestString, '/') + 1);
        if (substr($filterString, -strlen($configUrlSuffix)) == $configUrlSuffix) {
            $filterString = substr($filterString, 0, -strlen($configUrlSuffix));
        }

        // get different filter option values and active filterable attributes
        // if one of them is empty, this is not our business
        $filterInfos = explode('-', $filterString);

        // try to translate filter option values to request parameters using the rewrite models
        $params = array();
        $rewriteCollection = Mage::getModel('seo/rewrite')
            ->getCollection()
            ->addFieldToFilter('rewrite', array('in' => $filterInfos))
            ->addFieldToFilter('store_id', $storeId);

        // Ugly workaround. If rewrite doesn't exist in the current store view,
        // search for the rewrite in other store views and take the first.
        // @todo generate non existing rewrites on every filterurl request
        if(count($rewriteCollection) == 0)
        {
            $rewriteCollection = Mage::getModel('seo/rewrite')
                ->getCollection()
                ->addFieldToFilter('rewrite', array('in' => $filterInfos));

            $rewriteCollection->getSelect()->group('rewrite');
        }

        if (count($rewriteCollection) == count($filterInfos)) {
            foreach ($rewriteCollection as $rewrite) {
                //@dva fix
                if (!isset($params[$rewrite->getAttributeCode()])) {
                    $params[$rewrite->getAttributeCode()] = array();
                }
                $params[$rewrite->getAttributeCode()][] = $rewrite->getOptionId();
            }
        }
        else {
            return false;
        }

        // return structured result
        return array(
            'categoryId' => $categoryId,
            'additionalParams' => $params
        );
    }
        
}
