<?php

/**
 * Created by PhpStorm.
 * User: portn
 * Date: 18.01.2018
 * Time: 19:29
 */
class Webfresh_Seo_Helper_Catalog extends Mage_Catalog_Helper_Data
{
    /**
     * Return current category path or get it from current category
     * and creating array of categories|product paths for breadcrumbs
     *
     * @return string
     */
    /*  public function getBreadcrumbPath()
      {
          if (!$this->_categoryPath) {

              $path = array();
              if ($category = $this->getCategory()) {
                  $pathInStore = $category->getPathInStore();
                  $pathIds = array_reverse(explode(',', $pathInStore));

                  $categories = $category->getParentCategories();

                  // add category path breadcrumb
                  foreach ($pathIds as $categoryId) {
                      if (isset($categories[$categoryId]) && $categories[$categoryId]->getName()) {
                          $path['category'.$categoryId] = array(
                              'label' => $categories[$categoryId]->getName(),
                              'link' => $this->_isCategoryLink($categoryId) ? $categories[$categoryId]->getUrl() : ''
                          );
                      }
                  }
              }

              if ($this->getProduct()) {
                  $path['product'] = array('label'=>$this->getProduct()->getName());
              }

              $this->_categoryPath = $path;
          }
          return $this->_categoryPath;
      }*/

    public function getBreadcrumbPath()
    {
        $catp = array();
        if (!$this->_categoryPath) {


            $path = array();


            if ($this->getProduct()) {
                //$categoriesid = $this->getProduct()->getCategoryIds();
                $categoriesid = [];
                //$level=
                $_pathIds=[];
                foreach ($this->getProduct()->getCategoryCollection()->addAttributeToSelect('path')->addAttributeToSelect('is_active') as $cat) {
                    //echo $cat->getName().' '.$cat->getId().'<br/>';
                    /* echo '<pre>$categories';
                     print_r($cat->getData());
                     echo '</pre>';
                     $level=*/
                    $p=explode('/', $cat->getPath());
                    $_pathIds[count($p)] = $p;
                    $categoriesid[] = $cat->getId();

                }
               // unset($_pathIds[0]);
                //unset($_pathIds[1]);

                $category = false;
                    //if ($_SERVER['REMOTE_ADDR'] == '188.130.177.18') {
                    $true_path=max($_pathIds);
                        //unset($true_path[0]);
                        //unset($true_path[1]);
                if (isset($true_path[count($true_path)-1])) {
                    $category_id = $true_path[count($true_path) - 1];

                    $category = Mage::getModel('catalog/category')->load($category_id);
                }
                        // echo ;
                    /*echo '<pre>$pathIds';
                    print_r(max($_pathIds));
                    echo '</pre>';
                        echo '<pre>$pathIds';
                        print_r($_pathIds);
                        echo '</pre>';
*/
                //}
                // $cats=Mage::getModel('catalog/category')->getCollection()
                //->addFieldToFilter('robots', array('eq' => 2))
                //   ->addFieldToFilter('is_active', 1);


                /*foreach ($categoriesid as $cat) {
                    $_category = Mage::getModel('catalog/category')->load($cat);
                    if (!$_category->hasChildren()) {
                        $category = $_category;
                        break;
                    }
                }*/

                if (!$category) {
                    if (count($categoriesid) > 0) {
                        $category_id = $categoriesid[count($categoriesid) - 1];
                        $category = Mage::getModel('catalog/category')->load($category_id);
                    }
                }

                /*if ($_SERVER['REMOTE_ADDR'] == '188.130.177.18') {
                    //Mage::registry('current_category');//, $category);
                    //$categories = $category->getParentCategories();
                    echo '<pre>$categories';
                    print_r(Mage::registry('current_category'));
                    echo '</pre>';
                }*/


            } else {
                $category = $this->getCategory();
            }


            if ($category) {
                $pathInStore = $category->getPathInStore();
                $pathIds = array_reverse(explode(',', $pathInStore));

                $categories = $category->getParentCategories();


                if ($_SERVER['REMOTE_ADDR'] == '188.130.177.18') {
                    /*echo '<pre>$pathIds';
                    print_r($pathIds);
                    echo '</pre>';
                    echo '<pre>';
                    print_r($categories);
                    echo '</pre>';
                    die();*/
                }
                // add category path breadcrumb
                $c = count($pathIds) - 1;
                $i = 1;
                if ($this->getProduct()) {
                    $current_product = $this->getProduct()->getId();
                } else $current_product = false;

                foreach ($pathIds as $categoryId) {
                    if (isset($categories[$categoryId])) {


                        $path['category' . $categoryId] = array(
                            'label' => $categories[$categoryId]->getName(),
                            'category_id' => $categoryId,
                            'type' => 'category',
                            //'lastcategory' => $lastcategory,
                            //'show_other_category' => $show_other_category,
                            'current_product_id' => $current_product,
                            'link' => $this->_isCategoryLink($categoryId) ? $categories[$categoryId]->getUrl() : ''
                        );
                        $i++;

                        // }
                    }
                }

                $path['category' . $category->getId()] = array(
                    'label' => $category->getName(),
                    'category_id' => $category->getId(),
                    'type' => 'category',
                    'current_product_id' => $current_product,
                    'link' => $this->_isCategoryLink($category->getId()) ? $category->getUrl() : ''
                );
            }
           /* if ($_SERVER['REMOTE_ADDR'] == '188.130.177.18') {
                echo '<pre>$path' . $category->getId() . ' ' . $category->getName();
                print_R($path);
                echo '</pre>';
            }*/

            /*if ($_SERVER['REMOTE_ADDR']=='78.46.62.217') {

                echo '<pre>$path';
                print_r($path);
                echo '</pre>';
                die();
            }*/


            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $attributeModel = Mage::getModel('eav/entity_attribute');

            if ($this->getProduct()) {
                $prod = $this->getProduct();
                $proizvoditel_id = $prod->getData('proizvoditel');
                $proizvoditel_name = $prod->getAttributeText('proizvoditel');
                if ($proizvoditel_id && ($proizvoditel_name != '')) {


                    $id_attribute = (int)$attributeModel->loadByCode('catalog_product', 'proizvoditel')->getId();
                    $id_option = $proizvoditel_id;

                    $select = $readConnection->select()
                        ->from('eav_attribute_option_value')
                        ->join(
                            array('eav_attribute_option' => 'eav_attribute_option'),
                            'eav_attribute_option.option_id = eav_attribute_option_value.option_id',
                            array('eav_attribute_option.*')
                        )
                        ->join(
                            array('catalin_seo_attribute' => 'catalin_seo_attribute_url_key'),
                            'eav_attribute_option.option_id = catalin_seo_attribute.option_id',// and eav_attribute_option.attribute_id=:attribute_id',
                            array('catalin_seo_attribute.url_key', 'catalin_seo_attribute.url_value')
                        )
                        ->where('eav_attribute_option.attribute_id=:attribute_id AND eav_attribute_option_value.option_id=:option_id')
                        ->group('eav_attribute_option_value.option_id');

                    $bind = array(
                        'attribute_id' => $id_attribute,
                        'option_id' => $id_option
                    );

                    $results = $readConnection->fetchAll($select, $bind);
                    $url = '';
                    $results_array = [];
                    if ($category) {
                        foreach ($results as $res) {
                            $url = ($this->_isCategoryLink($category->getId()) ? $category->getUrl() : '') . '/filter/' . $res['url_key'] . '/' . $res['url_value'];// . '/';

                            //$first_letter=mb_substr ( $res['value'] , 0, 1);
                            //$results_array[$first_letter][]=$res;
                            break;
                        }


                        if ($url == '') {
                            $url = ($this->_isCategoryLink($category->getId()) ? $category->getUrl() : '') . '?proizvoditel=' . $proizvoditel_id;
                        }
                    }

                    $path['proizvoditel'] = array(
                        'label' => $proizvoditel_name,
                        //'category_id' => $category->getId(),
                        'type' => 'brand',
                        'current_product_id' => $current_product,
                        'link' => $url
                    );
                }
                $path['product'] = array('label' => $this->getProduct()->getName());
            }

            $this->_categoryPath = $path;
        }
        return $this->_categoryPath;
    }

}