<?php
class Webfresh_Seo_Block_Adminhtml_Meta_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('meta_data');
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('rewrite_form', array('legend'=>Mage::helper('seo')->__('General Information')));
        $fieldset->addField('url', 'text', array(
            'label'     => Mage::helper('seo')->__('Pattern of Url or Action name'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'url',
            'value'     => $model->getUrl(),
            'note'=>
                    'Can be a full action name or a request path. Wildcard allowed.
                    Examples:<br>
                    /customer/account/login/</br>
                    /customer/account/*<br>
                    customer_account_*<br>
                    *?mode=list'
        ));

        $fieldset->addField('title', 'text', array(
            'label'     => Mage::helper('seo')->__('Title'),
            'name'      => 'title',
            'value'     => $model->getTitle()
        ));
        
        $fieldset->addField('alt_name', 'text', array(
            'label'     => Mage::helper('seo')->__('alt name'),
            'name'      => 'alt_name',
            'value'     => $model->getAltName(),
            'note'=>'Альтернативное название - для брендов'
        ));
        
        

        $fieldset->addField('description', 'textarea', array(
            'label'     => Mage::helper('seo')->__('Seo Description'),
            'name'      => 'description',
            'value'     => $model->getDescription()
        ));

        $fieldset->addField('meta_title', 'text', array(
            'label'     => Mage::helper('seo')->__('Meta Title'),
            'name'      => 'meta_title',
            'value'     => $model->getMetaTitle()
        ));
/*
         $fieldset->addField('robots', 'select', array(
            'label'     => Mage::helper('seo')->__('Robots'),
            'name'      => 'description',
            'value'     => $model->getDescription()
        ));
  */      
        $fieldset->addField('robots', 'select', array(
          'label'     =>Mage::helper('seo')->__('Robots'),       
          'required'  => false,
          'name'      => 'robots',
          //'value'  => '1',
          'value'     => $model->getRobots(),
          'values' => Mage::getModel('seo/meta')->getListRobots(),
          'disabled' => false,
          'readonly' => false,
          'tabindex' => 1
        ));

        $fieldset->addField('meta_description', 'textarea', array(
            'label'     => Mage::helper('seo')->__('Meta Description'),
            'name'      => 'meta_description',
            'value'     => $model->getMetaDescription()
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('seo')->__('Is Active'),
            'name'      => 'is_active',
            'values'    => Mage::getSingleton('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value'     => $model->getIsActive()
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('stores', 'multiselect', array(
                'label'     => Mage::helper('seo')->__('Visible In'),
                'required'  => true,
                'name'      => 'stores[]',
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
                'value'     => $model->getStoreId()
            ));
        }
        else {
            $fieldset->addField('stores', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
        }

//        echo '<pre>';
//        print_r($model->getData());
//        echo '</pre>';
//die();
        $fieldset = $form->addFieldset('rewrite_faq_form', array('legend'=>Mage::helper('seo')->__('FAQ')));

        $fieldset->addField('faq_h2', 'text', array(
            'label'     => Mage::helper('seo')->__('Title H2'),
            //'class'     => 'required-entry',
            'required'  => false,
            'name'      => 'faq_h2',
            'value'     => $model->getFaqH2()
        ));

        $fieldset->addField('faq_title_1', 'text', array(
            'label'     => Mage::helper('seo')->__('Title_1'),
            //'class'     => 'required-entry',
            'required'  => false,
            'name'      => 'faq_title_1',
            'value'     => $model->getData('faq_title_1')
        ));

        $fieldset->addField('faq_content_1', 'textarea', array(
            'label'     => Mage::helper('seo')->__('Сontent_1'),
            'name'      => 'faq_content_1',
            'value'     =>  $model->getData('faq_content_1')
        ));


        $fieldset->addField('faq_title_2', 'text', array(
            'label'     => Mage::helper('seo')->__('Title 2'),
            'required'  => false,
            'name'      => 'faq_title_2',
            'value'     => $model->getData('faq_title_2'),//$model->getFaqTitle2()
        ));
        $fieldset->addField('faq_content_2', 'textarea', array(
            'label'     => Mage::helper('seo')->__('Сontent 2'),
            'name'      => 'faq_content_2',
            'value'     => $model->getData('faq_content_2'),//$model->getFaqContent2()
        ));


        $fieldset->addField('faq_title_3', 'text', array(
            'label'     => Mage::helper('seo')->__('Title 3'),
            'required'  => false,
            'name'      => 'faq_title_3',
            'value'     => $model->getData('faq_title_3'),//$model->getFaqTitle3()
        ));
        $fieldset->addField('faq_content_3', 'textarea', array(
            'label'     => Mage::helper('seo')->__('Сontent 3'),
            'name'      => 'faq_content_3',
            'value'     => $model->getData('faq_content_3'),//$model->getFaqContent3()
        ));


        $fieldset->addField('faq_title_4', 'text', array(
            'label'     => Mage::helper('seo')->__('Title 4'),
            'required'  => false,
            'name'      => 'faq_title_4',
            'value'     => $model->getData('faq_title_4'),//$model->getFaqTitle4()
        ));
        $fieldset->addField('faq_content_4', 'textarea', array(
            'label'     => Mage::helper('seo')->__('Content 4'),
            'name'      => 'faq_content_4',
            'value'     => $model->getData('faq_content_4'),//$model->getFaqContent4()
        ));


        $fieldset->addField('faq_title_5', 'text', array(
            'label'     => Mage::helper('seo')->__('Title 5'),
            'required'  => false,
            'name'      => 'faq_title_5',
            'value'     => $model->getData('faq_title_5'),//$model->getFaqTitle5()
        ));
        $fieldset->addField('faq_content_5', 'textarea', array(
            'label'     => Mage::helper('seo')->__('Сontent 5'),
            'name'      => 'faq_content_5',
            'value'     => $model->getData('faq_content_5'),//$model->getFaqContent5()
        ));










        //туче

        $fieldset->addField('faq_title_6', 'text', array(
            'label'     => Mage::helper('seo')->__('Title_6'),
            //'class'     => 'required-entry',
            'required'  => false,
            'name'      => 'faq_title_6',
            'value'     => $model->getData('faq_title_6')
        ));

        $fieldset->addField('faq_content_6', 'textarea', array(
            'label'     => Mage::helper('seo')->__('Сontent_6'),
            'name'      => 'faq_content_6',
            'value'     =>  $model->getData('faq_content_6')
        ));


        $fieldset->addField('faq_title_7', 'text', array(
            'label'     => Mage::helper('seo')->__('Title 7'),
            'required'  => false,
            'name'      => 'faq_title_7',
            'value'     => $model->getData('faq_title_7'),//$model->getFaqTitle2()
        ));
        $fieldset->addField('faq_content_7', 'textarea', array(
            'label'     => Mage::helper('seo')->__('Сontent 7'),
            'name'      => 'faq_content_7',
            'value'     => $model->getData('faq_content_7'),//$model->getFaqContent2()
        ));


        $fieldset->addField('faq_title_8', 'text', array(
            'label'     => Mage::helper('seo')->__('Title 8'),
            'required'  => false,
            'name'      => 'faq_title_8',
            'value'     => $model->getData('faq_title_88'),//$model->getFaqTitle3()
        ));
        $fieldset->addField('faq_content_8', 'textarea', array(
            'label'     => Mage::helper('seo')->__('Сontent 8'),
            'name'      => 'faq_content_8',
            'value'     => $model->getData('faq_content_8'),//$model->getFaqContent3()
        ));


        $fieldset->addField('faq_title_9', 'text', array(
            'label'     => Mage::helper('seo')->__('Title 9'),
            'required'  => false,
            'name'      => 'faq_title_9',
            'value'     => $model->getData('faq_title_9'),//$model->getFaqTitle4()
        ));
        $fieldset->addField('faq_content_9', 'textarea', array(
            'label'     => Mage::helper('seo')->__('Content 9'),
            'name'      => 'faq_content_9',
            'value'     => $model->getData('faq_content_9'),//$model->getFaqContent4()
        ));


        $fieldset->addField('faq_title_10', 'text', array(
            'label'     => Mage::helper('seo')->__('Title 10'),
            'required'  => false,
            'name'      => 'faq_title_10',
            'value'     => $model->getData('faq_title_10'),//$model->getFaqTitle5()
        ));
        $fieldset->addField('faq_content_10', 'textarea', array(
            'label'     => Mage::helper('seo')->__('Сontent 10'),
            'name'      => 'faq_content_10',
            'value'     => $model->getData('faq_content_10'),//$model->getFaqContent5()
        ));




        return parent::_prepareForm();
    }
}
