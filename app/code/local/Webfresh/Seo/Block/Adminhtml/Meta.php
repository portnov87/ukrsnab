<?php

class Webfresh_Seo_Block_Adminhtml_Meta extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_meta';
        $this->_blockGroup = 'seo';
        $this->_headerText = Mage::helper('seo')->__('meta Manager');
        parent::__construct();

        //$this->setTemplate('seo/rewrite.phtml');
    }

    protected function _prepareLayout()
    {
        $this->setChild('add_new_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                'label'     => Mage::helper('seo')->__('Add Meta'),
                'onclick'   => "setLocation('" . $this->getUrl('*/*/add') . "')",
                'class'     => 'add'
            ))
        );
        $this->setChild('grid', $this->getLayout()->createBlock('seo/adminhtml_meta_grid', 'meta.grid'));
        return parent::_prepareLayout();
    }

    public function getAddNewButtonHtml()
    {
        return $this->getChildHtml('add_new_button');
    }

    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }

    public function getStoreSwitcherHtml()
    {
        return $this->getChildHtml('store_switcher');
    }
}