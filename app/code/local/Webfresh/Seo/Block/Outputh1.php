<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Catalog product related items block
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Webfresh_Seo_Block_Outputh1 extends Mage_Catalog_Block_Product_Abstract
{


    /**
     * Default MAP renderer type
     *
     * @var string
     */
    public $h1 = '';
    public $sorting = '';

    protected function _prepareData()
    {
        $this->sorting = $this->getShowSorting();


        if ($product = Mage::registry('current_product')){

        }
        elseif ($category = Mage::registry('current_category')) {


            /*$layer = Mage::getModel('catalog/layer');//Singleton('ITDelight_Brands_Model_Catalog_Layer');

            $count_products = $layer
                // ->setCurrentCategory($category->getID())
                ->getProductCollection()
                ->getSize();


            if ($count_products > 0)
                $count_products = '<span class="count_in_h1">' . $count_products . ' ' . Mage::helper('seo')->plural_form((int)$count_products, array('товар', 'товара', 'товаров')) . '</span>';
            else $count_products = '';


            if ($h1 = Mage::registry('h1')) {

                $this->h1 = $h1 . $count_products;
            } else {

                if ($category) {
                    if ($category->getH1() != '')
                        $this->h1 = Mage::helper('catalog/output')->categoryAttribute($category, $category->getH1(), 'h1') . $count_products;
                    else
                        $this->h1 = Mage::helper('catalog/output')->categoryAttribute($category, $category->getName(), 'name') . $count_products;
                } elseif ($headtitle = Mage::helper('catalogsearch')->getEscapedQueryText())
                    $this->h1 = $this->__("Search results for '%s'", $headtitle);
            }*/

            if ($category) {
                if ($category->getH1() != '')
                    $this->h1 = Mage::helper('catalog/output')->categoryAttribute($category, $category->getH1(), 'h1') . $count_products;
                else
                    $this->h1 = Mage::helper('catalog/output')->categoryAttribute($category, $category->getName(), 'name') . $count_products;
            } elseif ($headtitle = Mage::helper('catalogsearch')->getEscapedQueryText())
                $this->h1 = $this->__("Search results for '%s'", $headtitle);


        } /*elseif ($observer->isNewProducts()) {
            $layer = Mage::getSingleton('ITDelight_NewProducts_Model_Catalog_Layer');

            $count_products = $layer
                // ->setCurrentCategory($category->getID())
                ->getProductCollection()
                ->getSize();


            if ($count_products > 0)
                $count_products = '<span class="count_in_h1">' . $count_products . ' ' . Mage::helper('seo')->plural_form((int)$count_products, array('товар', 'товара', 'товаров')) . '</span>';
            else $count_products = '';


            if ($h1 = Mage::registry('h1')) {

                $this->h1 = $h1 . $count_products;
            } else {


                    $this->h1 = 'Новинки ' . $count_products;
                    //$this->__("Search results for '%s'", $headtitle);
            }
        } else {
            $observer_brand = Mage::getSingleton('ITDelight_Brands_Model_Observer');

            if ($brand = Mage::registry('current_brand')) {

                $brand_name = $brand['value'];

                $layer = Mage::getSingleton('ITDelight_Brands_Model_Catalog_Layer');

                $count_products = $layer
                    // ->setCurrentCategory($category->getID())
                    ->getProductCollection()
                    ->getSize();


                $count_products = $count_products . ' ' . Mage::helper('seo')->plural_form((int)$count_products, array('товар', 'товара', 'товаров'));

                $this->h1 = 'Бренд "' . $brand_name . '" <span class="count_in_h1">' . $count_products . '</span>';
            } elseif ($observer_brand->isBrandsPage()) {
                $this->h1 = 'Бренды';
            }

            if ($h1 = Mage::registry('h1')) {

                $this->h1 = $h1;
            }
        }*/
        return $this;
    }

    protected function _beforeToHtml()
    {
        $this->_prepareData();
        return parent::_beforeToHtml();
    }


}

