<?php

/**
 * Created by PhpStorm.
 * User: portn
 * Date: 18.01.2018
 * Time: 18:50
 */

ini_set("display_errors", 1);
error_reporting(E_ALL);
class Webfresh_Seo_Block_Breadcrumbs extends Mage_Catalog_Block_Breadcrumbs
{

    protected function _prepareLayout()
    {
        //echo 'terer';die();
        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbsBlock->addCrumb('home', array(
                'label' => Mage::helper('catalog')->__('Home'),
                'title' => Mage::helper('catalog')->__('Go to Home Page'),
                'link' => Mage::getBaseUrl()
            ));

            $title = array();


            $path = Mage::helper('catalog')->getBreadcrumbPath();
         /*   echo '<pre>$path';
            print_r($path);
            echo '</pre>';
            die();*/

            $i = 1;
            foreach ($path as $name => $breadcrumb) {
                $breadcrumb_new = $breadcrumb;

                $breadcrumbsBlock->addCrumb($name, $breadcrumb_new);
                $title[] = $breadcrumb['label'];
                $i++;
            }


        }
        return parent::_prepareLayout();
    }
}


