<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * One page checkout success page
 *яя
 * @category   Mage
 * @package    Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Webfresh_Seo_Block_Checkout_Onepage_Success extends Mage_Checkout_Block_Onepage_Success
{


    /**
     * Get last order ID from session, fetch it and check whether it can be viewed, printed etc
     */
    protected function _prepareLastOrder()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();

        if ($orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            if ($order->getId()) {
                $total=number_format($order->getGrandTotal(),0, '','');//, 2, '.', '');//Mage::helper('core')->currency($order->getGrandTotal(), false, false);
                    $shipping_amount=Mage::helper('core')->currency($order->getShippingAmount(), false, false);


                $isVisible = !in_array($order->getState(),
                    Mage::getSingleton('sales/order_config')->getInvisibleOnFrontStates());
                $this->addData(array(
                    'is_order_visible' => $isVisible,
                    'view_order_id' => $this->getUrl('sales/order/view/', array('order_id' => $orderId)),
                    'print_url' => $this->getUrl('sales/order/print', array('order_id'=> $orderId)),
                    'can_print_order' => $isVisible,
                    'can_view_order'  => Mage::getSingleton('customer/session')->isLoggedIn() && $isVisible,
                    'order_id'  => $order->getIncrementId(),
                    'products'=>$this->_getProducts($order),
                    'shipping_amount'=>$shipping_amount,
                    'total'=>$total

                ));
            }
        }
    }

    public function getProducts()
    {
        return $this->getData('products');
    }


    protected function _getProducts($order)
    {
//return 'ddsdsdsd';//$products_for_empty;ыувчыффя го421]2432ФЯЙЫЦ23545К4       ]   ]]11Ъ





       /* $cart = Mage::getModel('checkout/cart')->getQuote();
        $products_for_empty = [];
        echo '<pre>';
        print_r($cart->getAllItems());
        echo '</pre>';
        die();getAllItems()*/
        foreach ($order->getAllItems() as $_item) {
            //echo $_item->getName().'<br/><br/>';

            $product = $_item->getProduct();

            $_product = Mage::getModel('catalog/product')->load($product->getId());
            $name = $_product->getName();
            $sku = $_product->getSku();
            $brand = $_product->getAttributeText('proizvoditel');
            $path_category = Mage::helper('seo')->getPathCategories($_product);
            $price = Mage::helper('core')->currency($_product->getPrice(), false, false);

            $products_for_empty[] ="{
'name': '" . $name . "',
'id': '" . $sku . "',
'price': '" . $price . "',
'brand': '" . $brand . "',
'category': '" . $path_category . "',
'quantity': '" . $_item->getQtyOrdered() . "'                              //quantity (целочисленная переменная) — количество товарных единиц, удаленных из корзины.
}";

        }
        return $products_for_empty;



    }

    public function getShippinAmount()
    {
        return $this->getData('shipping_amount');
    }
    public function getAmount(){
        return $this->getData('total');
    }

}
