<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 20.02.2018
 * Time: 16:46
 */
class Webfresh_Seo_Block_Category_View extends Mage_Catalog_Block_Category_View
{
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $this->getLayout()->createBlock('catalog/breadcrumbs');

        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $category = $this->getCurrentCategory();
            //if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
                /*if ($category) {
                    $current_url=$category->getUrlPath();
                    if (
                        (strpos($current_url,'-v-dnepre-dnepropetrovske')!==false)
                        ||(strpos($current_url,'-v-uzhgorode')!==false)
                        ||(strpos($current_url,'-v-vinnice')!==false)
                        ||(strpos($current_url,'-v-chernigove')!==false)
                        ||(strpos($current_url,'-v-zhitomire')!==false)
                        ||(strpos($current_url,'-v-sumah')!==false)
                        ||(strpos($current_url,'-v-poltave')!==false)
                        ||(strpos($current_url,'-v-har-kove')!==false)
                        ||(strpos($current_url,'-v-zaporozh-e')!==false)
                        ||(strpos($current_url,'-v-hersone')!==false)
                        ||(strpos($current_url,'-v-nikolaeve')!==false)
                        ||(strpos($current_url,'-v-kirovograde')!==false)
                        ||(strpos($current_url,'-v-cherkassah')!==false)
                        ||(strpos($current_url,'-v-hmel-nickom')!==false)
                        ||(strpos($current_url,'-v-ternopole')!==false)
                        ||(strpos($current_url,'-v-rovno')!==false)
                        ||(strpos($current_url,'-v-lucke')!==false)
                        ||(strpos($current_url,'-vo-l-vove')!==false)
                        ||(strpos($current_url,'-v-chernovcah')!==false)
                        ||(strpos($current_url,'-v-ivano-frankovske')!==false)
                        ||(strpos($current_url,'-v-odesse')!==false)
                    ){
                        $parent_category=$category->getParentCategory();

                        if ($parent_category){
                            $url_category=$parent_category->getUrlPath();
                            if ($url_category) {
                                header("Location: /".$url_category,TRUE,301);
                                exit();
                            }
                        }
                        //echo $parent_category->getId().' '.
                       // echo $category->getName() . ' ' . $category->getUrlPath();


                        //die();
                    }

              //  }

            }*/


            if ($title = $category->getMetaTitle()) {
                $headBlock->setTitle($title);
            }
            if ($description = $category->getMetaDescription()) {
                $headBlock->setDescription($description);
            }
            if ($keywords = $category->getMetaKeywords()) {
                $headBlock->setKeywords($keywords);
            }
            if ($this->helper('catalog/category')->canUseCanonicalTag()) {

                if (Mage::app()->getRequest()->getParam('p'))

                {
                    $url=$category->getUrl().'?p='.Mage::app()->getRequest()->getParam('p');

                }else{
                    $url=$category->getUrl();
                }
                //$headBlock->addLinkRel('canonical', $url);
            }else{

                if ((Mage::app()->getRequest()->getParam('p') && Mage::app()->getRequest()->getParam('p')!=1) || Mage::app()->getRequest()->getParam('limit') || Mage::app()->getRequest()->getParam('order')|| Mage::app()->getRequest()->getParam('mode')) {
                    $url=$category->getUrl();
                    $headBlock->addLinkRel('canonical', $url);
                }else{
                    $url=$category->getUrl();
                    $layer = Mage::getSingleton('catalog/layer');
                    $state = $layer->getState();

                    if (count($state->getFilters()) > 1) {
                        $headBlock->addLinkRel('canonical', $url);
                    }
                    //$headBlock->removeItem('link_rel', $url);
                    //echo '$canonicalUrl '.$canonicalUrl;
                }


                /*if (Mage::app()->getRequest()->getParam('p') || Mage::app()->getRequest()->getParam('limit') || Mage::app()->getRequest()->getParam('order')|| Mage::app()->getRequest()->getParam('mode')) {

                    $url=$category->getUrl();
                    $headBlock->addLinkRel('canonical', $url);
                }*/
            }


            if ($this->IsRssCatalogEnable() && $this->IsTopCategory()) {
                $title = $this->helper('rss')->__('%s RSS Feed',$this->getCurrentCategory()->getName());
                $headBlock->addItem('rss', $this->getRssLink(), 'title="'.$title.'"');
            }
        }

        return $this;

    }
}