<?php

class Webfresh_Seo_Block_Cms_Page extends Mage_Cms_Block_Page
{
    /**
     * Prepare global layout
     *
     * @return Mage_Cms_Block_Page
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $page = $this->getPage();


        // show breadcrumbs
        if (Mage::getStoreConfig('web/default/show_cms_breadcrumbs')
            && ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs'))
            && ($page->getIdentifier()!==Mage::getStoreConfig('web/default/cms_home_page'))
            && ($page->getIdentifier()!==Mage::getStoreConfig('web/default/cms_no_route'))) {

            $page_title=$page->getContentHeading();
            if ($page_title==''){
                $page_title=$page->getTitle();
            }
            //echo $page_title;die();
            $breadcrumbs->addCrumb('home', array('label'=>Mage::helper('cms')->__('Home'), 'title'=>Mage::helper('cms')->__('Go to Home Page'), 'link'=>Mage::getBaseUrl()));
            $breadcrumbs->addCrumb('cms_page', array('label'=>$page_title, 'title'=>$page_title));
        }

        $root = $this->getLayout()->getBlock('root');
        if ($root) {
            $root->addBodyClass('cms-'.$page->getIdentifier());
        }



        $head = $this->getLayout()->getBlock('head');
        if ($head) {
            $head->setTitle($page->getTitle());
            $head->setKeywords($page->getMetaKeywords());
            $head->setDescription($page->getMetaDescription());
        }

        if (strpos('',$page->getIdentifier('remont'))!==false){

            $
            $head->setDescription($page->getMetaDescription());
        }

        //return parent::_prepareLayout();
    }
}
