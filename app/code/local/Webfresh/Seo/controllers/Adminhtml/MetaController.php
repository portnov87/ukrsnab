<?php
class Webfresh_Seo_Adminhtml_MetaController extends Mage_Adminhtml_Controller_Action
{/*
    public function preDispatch()
    {
        parent::preDispatch();
        

        return $this;
    }*/

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('cms');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('Meta Manager'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('seo/adminhtml_meta'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_title($this->__('New Meta'));

        $_model = Mage::getModel('seo/meta');
        Mage::register('meta_data', $_model);
        Mage::register('current_meta', $_model);

        $this->_initAction();
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Seo Meta Manager'), Mage::helper('adminhtml')->__('Meta Manager'), $this->getUrl('*/*/'));
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Add Meta'), Mage::helper('adminhtml')->__('Add Meta'));

        $this->_addContent($this->getLayout()->createBlock('seo/adminhtml_meta_edit'))
            ->_addLeft($this->getLayout()->createBlock('seo/adminhtml_meta_edit_tabs'));

        $this->renderLayout();
    }

    public function editAction()
    {
        $rewriteId = $this->getRequest()->getParam('id');
        $_model = Mage::getModel('seo/meta')->load($rewriteId);

        if ($_model->getId()) {
            $this->_title($_model->getId() ? $_model->getName() : $this->__('New Meta'));

            Mage::register('meta_data', $_model);
            Mage::register('current_meta', $_model);

            $this->_initAction();
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Seo Meta Manager'), Mage::helper('adminhtml')->__('Meta Manager'), $this->getUrl('*/*/'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Edit Meta'), Mage::helper('adminhtml')->__('Edit Meta'));

            $this->_addContent($this->getLayout()->createBlock('seo/adminhtml_meta_edit'))
                ->_addLeft($this->getLayout()->createBlock('seo/adminhtml_meta_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('seo')->__('The meta does not exist.'));
            $this->_redirect('*/*/');
        }
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $_model = Mage::getModel('seo/meta');

            $_model->setData($data)
                ->setId($this->getRequest()->getParam('id'));

            try {
                $_model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('seo')->__('meta was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['id' => $_model->getId()]);

                    return;
                }
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);

                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('seo')->__('Unable to find meta to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('seo/meta');

                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Rewrite was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $IDList = $this->getRequest()->getParam('meta');
        if (!is_array($IDList)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select rewrite(s)'));
        } else {
            try {
                foreach ($IDList as $itemId) {
                    $_model = Mage::getModel('seo/rewrite')
                        ->setIsMassDelete(true)->load($itemId);
                    $_model->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($IDList)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $IDList = $this->getRequest()->getParam('meta');
        if (!is_array($IDList)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select rewrite(s)'));
        } else {
            try {
                foreach ($IDList as $itemId) {
                    $_model = Mage::getSingleton('seo/rewrite')
                        ->setIsMassStatus(true)
                        ->load($itemId)
                        ->setIsActive($this->getRequest()->getParam('status'))
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($IDList))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

}
