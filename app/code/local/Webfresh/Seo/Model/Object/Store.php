<?php

class Webfresh_Seo_Model_Object_Store extends Varien_Object
{
    public function _construct() {
        $this->setData(Mage::app()->getStore()->getData());
        if (Mage::getStoreConfig('general/store_information/name')) {
            $this->setName(Mage::getStoreConfig('general/store_information/name'));
        }
        $this->setPhone(Mage::getStoreConfig('general/store_information/phone'));
        $this->setAddress(Mage::getStoreConfig('general/store_information/address'));
        $this->setEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
        $this->setUrl(Mage::getBaseUrl());
    }

}