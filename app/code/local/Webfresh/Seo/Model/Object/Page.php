<?php

class Webfresh_Seo_Model_Object_Page extends Mirasvit_Seo_Model_Object_Abstract
{
	protected $_page;
	protected $_parseObjects = array();

    public function _construct()
    {
        parent::_construct();
		$this->_page = Mage::getSingleton('cms/page');

		if ($this->_page) {
			$this->_parseObjects['page'] = $this->_page;
		}
		$this->_parseObjects['store'] = Mage::getModel('seo/object_store');
		$this->init();
	}

	protected function init()
    {
		if ($this->_page->getMetaTitle()) {
			$this->setMetaTitle($this->parse($this->_page->getMetaTitle()));
		}
		if ($this->_page->getMetaKeywords()) {
			$this->setMetaKeywords($this->parse($this->_page->getMetaKeywords()));
		}
		if ($this->_page->getMetaDescription()) {
			$this->setMetaDescription($this->parse($this->_page->getMetaDescription()));
		}
        if ($this->_page->getTitle()) {
			$this->setTitle($this->parse($this->_page->getTitle()));
		}
		if ($this->_page->getDescription()) {
			$this->setDescription($this->parse($this->_page->getDescription()));
		}
	}
}


