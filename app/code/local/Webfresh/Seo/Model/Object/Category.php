<?php

class Webfresh_Seo_Model_Object_Category extends Webfresh_Seo_Model_Object_Abstract
{
    protected $_product;
    protected $_category;
    protected $_parseObjects = array();

    public function _construct()
    {
        parent::_construct();
        $this->_category = Mage::registry('current_category');

        if (!$this->_category) {
            $this->_category = Mage::registry('seo_current_category');
        }

        if (!$this->_category) {
            return;
        }

        /*echo '<pre>parent';
        print_r($this->_category->getParentCategory());
        echo '</pre>';*/
        $lowestProductPrice=$count_pr=0;
        if ($this->_category && $parent = $this->_category->getParentCategory()) {
            /*if (Mage::app()->getStore()->getRootCategoryId() != $parent->getId()) {
                $this->setAdditionalVariable('category', 'parent_name', $parent->getName());
                $this->setAdditionalVariable('category', 'parent_url', $parent->getUrl());
            }
            $this->setAdditionalVariable('category', 'url', $this->_category->getUrl());

            $prodCollection = $this->_category->getProductCollection()
                ->addAttributeToFilter('price', array('gt' => 0))
                ->addAttributeToSort('price', 'asc');

            $prodCollection_c = Mage::getResourceModel('catalog/product_collection')
                ->addCategoryFilter($this->_category);
            $prodCollection_c->getSelect()->group('e.entity_id');
            $count_pr = $prodCollection_c->count();

            if ($count_pr > 0)
                $lowestProductPrice = round($prodCollection_c->getFirstItem()->getPrice(), 2);
            else $lowestProductPrice = 0;*/
            $this->setAdditionalVariable('category', 'price_min', $lowestProductPrice);
            $this->setAdditionalVariable('category', 'product_count', $count_pr);
        }
        if ($this->_category) {
            $this->_parseObjects['category'] = $this->_category;
        }

        //мы можем создавать данную модель при расчете сео продукта
        $this->_product = Mage::registry('current_product');
        if ($this->_product) {
            $this->_parseObjects['product'] = $this->_product;
            $this->setAdditionalVariable('product', 'url', $this->_product->getProductUrl());
        }
        $this->_parseObjects['store'] = Mage::getModel('seo/object_store');
        $this->_parseObjects['pager'] = Mage::getModel('seo/object_pager');
        $this->_parseObjects['filter'] = Mage::getModel('seo/object_wrapper_filter');

        $this->init();
    }

    protected function init()
    {
        // вначале устанавливаем из родительских категорий. низший приоритет.
        if (!$this->_category) {
            return;
        }

        $ids = $this->_category->getParentIds();//array($this->_category->getParentCategory()->getId());//
        /*  echo '<pre>$ids';
          print_r($ids);
          echo '</pre>';*/

        /*$collection = Mage::getModel('catalog/category')->getCollection()
                                        ->addAttributeToSelect('*')
                                        ->addIdFilter($ids)
                                        ->addIsActiveFilter()
                                        ;$collection*/
        foreach ($ids as $id_category) {
            $category = Mage::getModel('catalog/category')->load($id_category);
            // echo $category->getId().'ddd<br/>';
            $this->process($category);
        }
        //устанавливаем из текущей категории
        $this->processCurrentCategory($this->_category);

        if (!$this->getTitle()) {
            $this->setTitle($this->_category->getName());
        }

        if (!$this->getMetaTitle()) {
            $this->setMetaTitle($this->_category->getName());
        }


        if (!$this->getMetaDescription()) {
            $this->setMetaDescription(Mage::helper('core/string')->substr($this->_category->getDescription(), 0, 255));
        }
    }

    protected function processCurrentCategory()
    {
        // set for current category. it has high priority.
        if ($this->_category->getMetaTitle()) {
            $this->setMetaTitle($this->parse($this->_category->getMetaTitle()));
        }

        if ($this->_category->getMetaDescription()) {
            $this->setMetaDescription($this->parse($this->_category->getMetaDescription()));
        }
        //not necessary for most of customers
        //~ if ($this->_category->getDescription()) {
        //~ $this->setDescription($this->parse($this->_category->getDescription()));
        //~ }
        if ($this->_category->getProductTitleTpl()) {
            $this->setProductTitle($this->parse($this->_category->getProductTitleTpl()));
        }
        if ($this->_category->getProductDescriptionTpl()) {
            $this->setProductDescription($this->parse($this->_category->getProductDescriptionTpl()));
        }
        if ($this->_category->getProductMetaDescriptionTpl()) {
            $this->setProductMetaDescription($this->parse($this->_category->getProductMetaDescriptionTpl()));
        }
    }

    protected function process($category)
    {
        if ($category->getCategoryMetaTitleTpl()) {
            $this->setMetaTitle($this->parse($category->getCategoryMetaTitleTpl()));
        }
        if ($category->getCategoryMetaDescriptionTpl()) {
            $this->setMetaDescription($this->parse($category->getCategoryMetaDescriptionTpl()));
        }
        if ($category->getCategoryTitleTpl()) {
            $this->setTitle($this->parse($category->getCategoryTitleTpl()));
        }

        if ($category->getCategoryDescriptionTpl()) {
            $this->setDescription($this->parse($category->getCategoryDescriptionTpl()));
        }

        if ($category->getProductTitleTpl()) {
            $this->setProductTitle($this->parse($category->getProductTitleTpl()));
        }
        if ($category->getProductDescriptionTpl()) {
            $this->setProductDescription($this->parse($category->getProductDescriptionTpl()));
        }
        if ($category->getProductMetaTitleTpl()) {
            $this->setProductMetaTitle($this->parse($category->getProductMetaTitleTpl()));
        }
        if ($category->getProductMetaDescriptionTpl()) {
            $this->setProductMetaDescription($this->parse($category->getProductMetaDescriptionTpl()));
        }
    }
}