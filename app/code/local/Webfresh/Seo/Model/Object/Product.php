<?php

class Webfresh_Seo_Model_Object_Product extends Webfresh_Seo_Model_Object_Abstract
{
	protected $_product;
	protected $_category;
	protected $_parseObjects = array();

    public function _construct()
    {
        parent::_construct();
		$this->_product = Mage::registry('current_product');
		if (!$this->_product) {
			$this->_product = Mage::registry('product');
		}
		if (!$this->_product) {
			return;
		}

		$this->_category = Mage::registry('current_category');

		$this->_parseObjects['product'] = $this->_product;

		$this->setAdditionalVariable('product', 'url', $this->_product->getProductUrl());
		$this->setAdditionalVariable('product', 'final_price', $this->_product->getFinalPrice());

		if ($this->_category) {
			$this->_parseObjects['category'] = $this->_category;
		} elseif ($this->_product) {
		    $categoryIds = $this->_product->getCategoryIds();
	
		    if (count($categoryIds) > 0) {
		        $categoryId = max($categoryIds);
		        //we need this for multi websites configuration

		        $categoryRootId = Mage::app()->getStore()->getRootCategoryId();		        
		        $category = Mage::getModel('catalog/category')->getCollection()
		        				->addFieldToFilter('path', array('like' => "%/{$categoryRootId}/%"))
		        				->addFieldToFilter('entity_id', $categoryIds)
		        				->setOrder('level', 'desc')
		        				->setOrder('entity_id', 'desc')
		        				->getFirstItem()
		        			;
		        //load category with flat data attributes
		        $category = Mage::getModel('catalog/category')->load($category->getId());
				$this->_category = $category;
		        $this->_parseObjects['category'] = $category;
		        if (!Mage::registry('seo_current_category')) {// to be sure that register will not be done twice
			        Mage::register('seo_current_category', $category);
			    };
		    }

		}

		$this->_parseObjects['store'] = Mage::getModel('seo/object_store');

		$this->init();

	}

	protected function init()
    {
		if ($this->_category) {
			$categorySeo = Mage::getSingleton('seo/object_category');
			$this->setMetaTitle($categorySeo->getProductMetaTitle());
			$this->setMetaKeywords($categorySeo->getProductMetaKeywords());
			$this->setMetaDescription($categorySeo->getProductMetaDescription());

			$this->setTitle($categorySeo->getProductTitle());
			$this->setDescription($categorySeo->getProductDescription());
			
			$this->addAdditionalVariables($categorySeo->getAdditionalVariables());
		}

		if ($this->_product->getMetaTitle()) {
			$this->setMetaTitle($this->parse($this->_product->getMetaTitle()));
		}
		if ($this->_product->getMetaDescription()) {
			$this->setMetaDescription($this->parse($this->_product->getMetaDescription()));
		}

        if (!$this->getTitle()) {
			$this->setTitle($this->_product->getName());
		}

        if (!$this->getMetaTitle()) {
			$this->setMetaTitle($this->_product->getName());
		}

        if (!$this->getMetaDescription()) {
			$this->setMetaDescription(Mage::helper('core/string')->substr($this->_product->getDescription(), 0, 255));
		}
	}
}