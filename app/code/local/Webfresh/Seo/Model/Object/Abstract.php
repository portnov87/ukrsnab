<?php

class Webfresh_Seo_Model_Object_Abstract extends Varien_Object
{
    protected $config, $_additional, $_store;

    public function _construct()
    {
        parent::_construct();
        $this->_config = Mage::getModel('seo/config');
        $this->_additional = array(
            'category' => array(),
            'product' => array(),
        );
    }

	protected function parse($str)
    {
        $storeId = false;
        if ($this->_store) {
            $storeId = $this->_store->getId();
        }
       /* echo '<pre>';
        print_r($this->_parseObjects);
        echo '</pre>';die();*/
    	return Mage::helper('seo/parse')->parse($str, $this->_parseObjects, $this->_additional, $storeId);
	}

    protected function setAdditionalVariable($objectName, $variableName, $value)
    {
        $this->_additional[$objectName][$variableName] = $value;
    }

    public function addAdditionalVariables($variables)
    {
        $this->_additional = array_merge_recursive($this->_additional, $variables);
    }

    public function getAdditionalVariables() {
        return $this->_additional;
    }
}
