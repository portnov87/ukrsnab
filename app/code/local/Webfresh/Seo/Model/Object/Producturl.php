<?php

class Webfresh_Seo_Model_Object_ProductUrl extends Mirasvit_Seo_Model_Object_Abstract
{
	protected $_product;
	protected $_parseObjects = array();

    public function _construct()
    {
        parent::_construct();
		$this->init();
	}

	public function setProduct($product) {
	    $this->_product = $product;
	    $this->_parseObjects['product'] = $this->_product;
	    return $this;
	}

	public function setStore($store) {
	   $this->_parseObjects['store'] = $store;
       $this->_store = $store;
	   return $this;
	}

	protected function init()
    {

	}

	public function parse($template) {
	    return parent::parse($template);
	}
}

