<?php
class Webfresh_Seo_Model_Config
{
	/*public function isEnabled() {
		return Mage::getStoreConfig('seo/general/layered_navigation_friendly_urls');
	}*/
        
	public function isEnabledFriendlyUrls() {
		return Mage::getStoreConfig('seo/general/layered_navigation_friendly_urls');
	}

        
        const NO_TRAILING_SLASH = 1;
    const TRAILING_SLASH = 2;

    const URL_FORMAT_SHORT = 1;
    const URL_FORMAT_LONG = 2;

	public function isAddCanonicalUrl() {
		return Mage::getStoreConfig('seo/general/is_add_canonical_url');
	}

	public function getCrossDomainStore() {
		return Mage::getStoreConfig('seo/general/crossdomain');
	}

	public function getTrailingSlash() {
        return Mage::getStoreConfig('seo/general/trailing_slash');
	}

	public function getCanonicalUrlIgnorePages() {
	    $pages = Mage::getStoreConfig('seo/general/canonical_url_ignore_pages');
	    $pages = explode("\n", trim($pages));
	    $pages = array_map('trim',$pages);
	    return $pages;
	}

	public function getNoindexPages() {
	    $pages = Mage::getStoreConfig('seo/general/noindex_pages');
	    $pages = explode("\n", trim($pages));
	    $pages = array_map('trim',$pages);
	    return $pages;
	}

	public function isEnabledSeoUrls() {
		return Mage::getStoreConfig('seo/general/layered_navigation_friendly_urls');
	}

	public function getProductUrlFormat() {
       return Mage::getStoreConfig('seo/general/product_url_format');
	}

	public function getProductUrlKey($store) {
       return Mage::getStoreConfig('seo/general/product_url_key', $store);
	}

	public function isPagingPrevNextEnabled() {
		return Mage::getStoreConfig('seo/general/is_paging_prevnext');
	}

	public function isOpenGraphEnabled() {
	    return Mage::getStoreConfig('seo/general/is_rich_snippets');
	}

	public function isRichSnippetsEnabled() {
	    return Mage::getStoreConfig('seo/general/is_opengraph');
	}
}
