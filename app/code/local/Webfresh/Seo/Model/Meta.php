<?php
class Webfresh_Seo_Model_Meta extends Mage_Core_Model_Abstract
{
    const CACHE_LIFETIME = 3600;
    const CACHE_TAG              = 'seo';
    protected $_cacheTag         =  self::CACHE_TAG;
    
    protected function _construct()
    {
        $this->_init('seo/meta');
    }
    
    public function getListRobots()
    {
        return array(1=>'NOINDEX,FOLLOW',2=>'INDEX,FOLLOW',3=>'NOINDEX,NOFOLLOW');
    }
}