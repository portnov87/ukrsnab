<?php

/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 03.09.2018
 * Time: 18:08
 */
class Webfresh_Seo_Model_Resource_Product extends Mage_Catalog_Model_Resource_Product
{

    public function getCategoryCollection($product)
    {
        $collection = Mage::getResourceModel('catalog/category_collection')
            ->joinField('product_id',
                'catalog/category_product',
                'product_id',
                'category_id = entity_id',
                null)
            ->addAttributeToSelect('*')
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('product_id', (int)$product->getId());
        return $collection;
    }
}