<?php

class Webfresh_Seo_Model_Resource_Meta_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    /**
     * Constructor method
     */
    protected function _construct()
    {
        $this->_init('seo/meta');
    }

    /**
     * Add Filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     * @return My_rewrite_Model_Mysql4_News_Collection
     */
    public function addStoreFilter($store)
    {
        if (!Mage::app()->isSingleStoreMode()) {
            if ($store instanceof Mage_Core_Model_Store) {
                $store = array($store->getId());
            }

            /*$this->getSelect()
                ->join(array('store_table' => $this->getTable('seo/rewrite_store')), 'main_table.rewrite_id = store_table.rewrite_id', array())
                ->where('store_table.store_id in (?)', array(0, $store));*/
            return $this;
        }
        return $this;
    }
public function addEnableFilter($status = 1)
    {
        $this->getSelect()->where('main_table.is_active = ?', $status);
        return $this;
    }
}