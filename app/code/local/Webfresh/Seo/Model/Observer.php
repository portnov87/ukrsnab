<?php

class Webfresh_Seo_Model_Observer extends Varien_Object
{
    protected $isProductTitlePrinted = false;

    public function getConfig()
    {
        return Mage::getSingleton('seo/config');
    }


    public function applyMeta($observer)
    {
        //echo 'addr'.$_SERVER['REMOTE_ADDR'];

        $headBlock = Mage::app()->getLayout()->getBlock('head');
        if ($headBlock) {
            $request = Mage::app()->getRequest();
            $module = $request->getControllerModule();
            $module_controller = $request->getControllerName();
            $module_controller_action = $request->getActionName();

            $fullActionName = strtolower($module . "_" . $module_controller . "_" . $module_controller_action);

            // echo $fullActionName;
            // $fullAction = $observer->getEvent()->getControllerAction()->getFullActionName();
            //echo $fullAction;
            // die();
            /* switch ($fullActionName)
             {
                 case 'itdelight_contacts_index_index':
                 case 'contacts_index_index':
                     $title='Контакты | Optomarket';

                     $description='Контакты 👞Optomarket 👞 Недорого&#129034;Высокое качество&#129034;Бесплатная доставка&#129034;Описание товара и отзывы&#129034; Звоните ☎ +38 068 458 88 90';
                 $headBlock->setDescription($description);
                 $headBlock->setTitle($title);
                     break;
                 case 'mage_cms_page_view':
                 case 'cms_page_view':
                     $title=$headBlock->getTitle().' | Optomarket';
                     $description=$headBlock->getTitle().' 👞Optomarket 👞 Недорого&#129034;Высокое качество&#129034;Бесплатная доставка&#129034;Описание товара и отзывы&#129034; Звоните ☎ +38 068 458 88 90';
                 $headBlock->setDescription($description);
                 $headBlock->setTitle($title);
                     break;
             }
 */


            if ($product = Mage::registry('current_product')) {

                /* if ($_SERVER['REMOTE_ADDR'] == '78.46.62.217') {
                     echo '<pre>';
                     print_r($product);
                     echo '</pre>';
                     die();
                 }*/

                $description = $product->getMetaDescription();
                $title = $product->getMetaTitle();
                $product_name = mb_strtolower($product->getName());
                if ($description == '') {

                    $description = 'Заказать ' . $product_name . ' ➤ По лучшей цене ➤ Официальный дилер в Украине... ➤Сегодня_выгодное_предложение... получи СКИДКУ от... +Доставку по Украине... ★ОТЗЫВЫ ★Акции ★СКИДКИ.... ☎️Звони сейчас +38(067)230-60-90';
                    //$product->getName().' - Ukrsnab.com.ua | Первый интернет-магазин строительной техники и профессионального инструмента.';
                    $headBlock->setDescription($description);
                }

                /* if ($_SERVER['REMOTE_ADDR'] == '78.46.62.217') {
                     echo '$description'.$description;die();
                 }*/

                if ($title == '') {
                    $title = 'Купить ' . $product_name . ': продажа по низким ценам ' . $product_name . ' в ';
                    $headBlock->setTitle($title);//Description($description);
                }

            } else {

                if (!$seo = Mage::helper('seo')->getCurrentSeo()) {
                    return;
                }

                if ($seo->getMetaTitle()) {

                    if ($splash_page = Mage::registry('splash_page')) {

                    } else
                        $headBlock->setTitle(Mage::helper('seo')->cleanMetaTag($seo->getMetaTitle()), '1');
                }
                if ($seo->getMetaDescription()) {
                    //Removes HTML tags and unnecessary whitespaces from Description Meta Tag
                    $description = $seo->getMetaDescription();
                    $description = Mage::helper('seo')->cleanMetaTag($description);

                    $headBlock->setDescription($description);
                }
            }
        }
    }


    public function addCustomAttributeOutputHandler(Varien_Event_Observer $observer)
    {

        $outputHelper = $observer->getEvent()->getHelper();
        $outputHelper->addHandler('productAttribute', $this);
        $outputHelper->addHandler('categoryAttribute', $this);
    }

    public function categoryAttribute(Mage_Catalog_Helper_Output $outputHelper, $outputHtml, $params)
    {
        if (!Mage::registry('current_category')) {
            return $outputHtml;
        }
        /*if ($_SERVER['REMOTE_ADDR'] == '78.46.62.217') {
            echo '$outputHtml'.$outputHtml;
        }*/

        $seo = Mage::helper('seo')->getCurrentSeo();

        switch ($params['attribute']) {
            case 'name':
                $outputHtml = $seo->getTitle();
                break;
            case 'description':
                $_outputHtml = '';
                $layer = Mage::getSingleton('catalog/layer');
                $state = $layer->getState();
                if (count($state->getFilters()) > 1) {
                    $_outputHtml = '';
                }
                if ($seo->getDescription() != '') $_outputHtml = $seo->getDescription();


                if ($_outputHtml != '') {
                    return $_outputHtml;
                    // $outputHtml = 'd'.$_outputHtml;
                }
                //echo $outputHtml;die();
                // break;

//return $seo->getDescription();

                $layer = Mage::getSingleton('catalog/layer');
                $state = $layer->getState();
                if (count($state->getFilters()) > 1) {
                    $outputHtml = '';
                }
                if ($seo->getDescription() != '') {
                    $data = $seo->getData();

                    if (count($state->getFilters()) < 2)
                        $outputHtml = $data['description'];
                    return $outputHtml;
                } else {

                    if (count($state->getFilters()) == 0) {
                        if (isset($params['category'])) {
                            $outputHtml = $params['category']->getDescription();
                        }
                    }
                }

                if ($outputHtml==''){
                    $category=Mage::registry('current_category');
                    $spinnerCollection=Mage::getModel('ukrsnab/spinnerdb')->getCollection()->addFilterByCategoryId($category->getId());
                    foreach ($spinnerCollection as $_spinner)
                    {
                        $outputHtml=$_spinner->getData('text');
                        break;
                    }
                }
                // if ($_SERVER['REMOTE_ADDR'] == '78.46.62.217') {


                break;
        }

        return $outputHtml;
    }

    public function productAttribute(Mage_Catalog_Helper_Output $outputHelper, $outputHtml, $params)
    {
        /*if (!Mage::registry('current_product') || $this->isProductTitlePrinted) {
            return $outputHtml;
        }
        $seo = Mage::helper('seo')->getCurrentSeo();
        switch ($params['attribute']) {
            case 'name':
                $this->isProductTitlePrinted = true;
                $outputHtml = $seo->getTitle();
        }*/

        return $outputHtml;
    }

    public function addFieldsToCmsEditForm($e)
    {
        $form = $e->getForm();

        $fieldset = $form->addFieldset('seo_fieldset', ['legend' => Mage::helper('seo')->__('SEO Data'), 'class' => 'fieldset-wide']);

        $fieldset->addField('meta_title', 'text', [
            'name' => 'meta_title',
            'label' => Mage::helper('seo')->__('Meta Title'),
            'title' => Mage::helper('seo')->__('Meta Title'),
        ]);

        $fieldset->addField('description', 'textarea', [
            'name' => 'description',
            'label' => Mage::helper('seo')->__('SEO Description'),
            'title' => Mage::helper('seo')->__('SEO Description'),
        ]);
    }

    /**
     * Check is Request from AJAX
     * Magento 1.4.1.1 does not have this function in core
     *
     * @return boolean
     */
    public function isAjax()
    {
        $request = Mage::app()->getRequest();
        if ($request->isXmlHttpRequest()) {
            return true;
        }
        if ($request->getParam('ajax') || $request->getParam('isAjax')) {
            return true;
        }

        return false;
    }


    public function checkUrl($e)
    {
        $action = $e->getControllerAction();
        $url = $action->getRequest()->getRequestString();
        $fullUrl = $_SERVER['REQUEST_URI'];

        if (Mage::app()->getStore()->isAdmin()) {
            return;
        }

        if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
            return;
        }
        if ($this->isAjax()) {
            return;
        }

        $urlToRedirect = $this->getUrlWithCorrectEndSlash($url);

        if ($url != $urlToRedirect) {
            $this->redirect($urlToRedirect);
        }

        if (substr($url, -1) != '/' && $this->getConfig()->getTrailingSlash() == Webfresh_Seo_Model_Config::TRAILING_SLASH) {
            if (!in_array($extension, ['html', 'htm', 'php', 'xml', 'rss'])) {
                $this->redirect($url . '/?' . $_SERVER['QUERY_STRING']);
            }
        } elseif ($url != '/' && substr($url, -1) == '/' && $this->getConfig()->getTrailingSlash() == Webfresh_Seo_Model_Config::NO_TRAILING_SLASH) {
            $this->redirect(rtrim($url, '/') . '?' . $_SERVER['QUERY_STRING']);
        }

        if (substr($fullUrl, -4, 4) == '?p=1') {
            $this->redirect(substr($fullUrl, 0, -4));
        }

        if (in_array(trim($fullUrl, '/'), ['home', 'index.php', 'index.php/home'])) {
            $this->redirect('/');
        }
    }

    protected function getUrlWithCorrectEndSlash($url)
    {
        $extension = substr(strrchr($url, '.'), 1);

        if (substr($url, -1) != '/' && $this->getConfig()->getTrailingSlash() == Webfresh_Seo_Model_Config::TRAILING_SLASH) {
            if (!in_array($extension, ['html', 'htm', 'php', 'xml', 'rss'])) {
                $url .= '/';
                if ($_SERVER['QUERY_STRING']) {
                    $url .= '?' . $_SERVER['QUERY_STRING'];
                }
            }
        } elseif ($url != '/' && substr($url, -1) == '/' && $this->getConfig()->getTrailingSlash() == Webfresh_Seo_Model_Config::NO_TRAILING_SLASH) {
            $url = rtrim($url, '/');
            if ($_SERVER['QUERY_STRING']) {
                $url .= '?' . $_SERVER['QUERY_STRING'];
            }
        }

        return $url;
    }

    protected function redirect($url)
    {
        Mage::app()->getFrontController()->getResponse()
            ->setRedirect($url, 301)
            ->sendResponse();
        die;
    }

    public function addCategorySeoTab($e)
    {
        $tabs = $e->getTabs();
        $ids = $tabs->getTabsIds();

        $attributeSetId = $tabs->getCategory()->getDefaultAttributeSetId();
        $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
            ->setAttributeSetFilter($attributeSetId)
            ->addFieldToFilter('attribute_group_name', 'SEO')
            ->load();
        $group = $groupCollection->getFirstItem();
        if ($group) {
            $tabs->removeTab('group_' . $group->getAttributeGroupId());
        }
        $tabs->addTab('seo', [
            'label' => Mage::helper('seo')->__('SEO'),
            'content' => Mage::app()->getLayout()->createBlock(
                'seo/adminhtml_catalog_category_tab_seo',
                'category.seo'
            )->toHtml(),
        ]);
    }

    public function checkProductUrlRedirect($e)
    {
        $urlFormat = $this->getConfig()->getProductUrlFormat();
        if ($urlFormat != Webfresh_Seo_Model_Config::URL_FORMAT_SHORT &&
            $urlFormat != Webfresh_Seo_Model_Config::URL_FORMAT_LONG
        ) {
            return;
        }

        if ($this->isAjax()) {
            return;
        }

        $action = $e->getControllerAction();

        if ($action->getRequest()->getModuleName() != 'catalog') { //we redirecto only for catalog
            return;
        }
        if ($action->getRequest()->getControllerName() != 'product') { //we redirecto only for catalog
            return;
        }
        if ($action->getRequest()->getActionName() != 'view') { //we redirecto only from products page. not from images views.
            return;
        }

        $url = ltrim($action->getRequest()->getRequestString(), '/');
        $product = $e->getProduct();
        //we need this because we need to load url rewrites
        //maybe its possible to optimize
        $products = Mage::getModel('catalog/product')->getCollection()
            ->addFieldToFilter('entity_id', $product->getId());


        $product = $products->getFirstItem();
        $productUrl = str_replace(Mage::getBaseUrl(), '', $product->getProductUrl());
        $productUrl = $this->getUrlWithCorrectEndSlash($productUrl);

        if ($productUrl != $url) {
            $url = $product->getProductUrl();
            $url = $this->getUrlWithCorrectEndSlash($url);
            $this->redirect($url);
        }
    }

    public function setupProductUrls($e)
    {
        $collection = $e->getCollection();
        $this->_addUrlRewrite($collection);
    }

    /**
     * Add URL rewrites to collection
     *
     */
    protected function _addUrlRewrite($collection)
    {
        $urlFormat = $this->getConfig()->getProductUrlFormat();
        if ($urlFormat != Webfresh_Seo_Model_Config::URL_FORMAT_SHORT &&
            $urlFormat != Webfresh_Seo_Model_Config::URL_FORMAT_LONG
        ) {
            return;
        }

        $urlRewrites = null;

        if (!$urlRewrites) {
            $productIds = [];
            foreach ($collection->getItems() as $item) {
                $productIds[] = $item->getEntityId();
            }
            if (!count($productIds)) {
                return;
            }
            $storeId = Mage::app()->getStore()->getId();
            if ($collection->getStoreId()) {
                $storeId = $collection->getStoreId();
            }
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $tablePrefix = (string)Mage::getConfig()->getTablePrefix();
            $select = $connection->select()
                ->from($tablePrefix . 'core_url_rewrite', ['product_id', 'request_path'])
                ->where('store_id = ?', $storeId)
                ->where('is_system = ?', 1)
                ->where('product_id IN(?)', $productIds)
                ->order('category_id desc'); // more priority is data with category id
            if ($urlFormat == Webfresh_Seo_Model_Config::URL_FORMAT_SHORT) {
                $select->where('category_id IS NULL');
            }

            $urlRewrites = [];
            foreach ($connection->fetchAll($select) as $row) {
                if (!isset($urlRewrites[$row['product_id']])) {
                    $urlRewrites[$row['product_id']] = $row['request_path'];
                }
            }
        }

        foreach ($collection->getItems() as $item) {
            if (isset($urlRewrites[$item->getEntityId()])) {
                $item->setData('request_path', $urlRewrites[$item->getEntityId()]);
            } else {
                $item->setData('request_path', false);
            }
        }
    }

    public function setupPagingMeta()
    {
        if ($this->getConfig()->isPagingPrevNextEnabled()) {
            Mage::getModel('seo/paging')->createLinks();
        }
    }

    public function saveProductBefore($observer)
    {
        $product = $observer->getProduct();
        if ($product->getStoreId() == 0
            //~ && $product->getOrigData('url_key') != $product->getData('url_key')

        ) {
            Mage::getModel('seo/system_template_worker')->processProduct($product);
        }
    }

    public function httpResponseSendBeforeEvent($e)
    {
        Mage::getSingleton('seo/opengraph')->modifyHtmlResponse($e);
    }
}

