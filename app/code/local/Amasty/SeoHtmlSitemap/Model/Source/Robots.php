<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_SeoHtmlSitemap
 */


class Amasty_SeoHtmlSitemap_Model_Source_Robots
{
    public function toOptionArray()
    {
        $hlp = Mage::helper('amseohtmlsitemap');
        return array(
            array('label' => $hlp->__('INDEX, FOLLOW'), 'value' => 'INDEX, FOLLOW'),
            array('label' => $hlp->__('NOINDEX, FOLLOW'), 'value' => 'NOINDEX, FOLLOW'),
            array('label' => $hlp->__('INDEX, NOFOLLOW'), 'value' => 'INDEX, NOFOLLOW'),
            array('label' => $hlp->__('NOINDEX, NOFOLLOW'), 'value' => 'NOINDEX, NOFOLLOW')
        );
    }
}
