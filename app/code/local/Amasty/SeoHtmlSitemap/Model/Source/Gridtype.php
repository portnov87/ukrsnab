<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_SeoHtmlSitemap
 */


class Amasty_SeoHtmlSitemap_Model_Source_Gridtype
{
    const TYPE_TREE = 1;
    const TYPE_LIST = 2;

    public function toOptionArray()
    {
        $hlp = Mage::helper('amseohtmlsitemap');
        return array(
            array('value' => self::TYPE_TREE, 'label'=> $hlp->__('Tree')),
            array('value' => self::TYPE_LIST, 'label'=> $hlp->__('List'))
        );
    }
}
