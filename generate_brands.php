<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 04.09.2018
 * Time: 11:58
 */


date_default_timezone_set('Europe/Kiev');
require_once('app/Mage.php');
umask(0);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
ini_set('memory_limit', '2048M');

ob_implicit_flush(1);

echo str_pad('', 1024);
@ob_flush();
flush();
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);

function getBrands()
{
    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');
    $attributeModel = Mage::getModel('eav/entity_attribute');
    $id_attribute = (int)$attributeModel->loadByCode('catalog_product', 'proizvoditel')->getId();

    $select = $readConnection->select()
        ->from('eav_attribute_option_value')
        ->join(
            array('eav_attribute_option' => 'eav_attribute_option'),
            'eav_attribute_option.option_id = eav_attribute_option_value.option_id',
            array('eav_attribute_option.*')
        )
        ->join(
            array('catalin_seo_attribute' => 'catalin_seo_attribute_url_key'),
            'eav_attribute_option.option_id = catalin_seo_attribute.option_id',// and eav_attribute_option.attribute_id=:attribute_id',
            array('catalin_seo_attribute.url_value')
        )
        ->where('eav_attribute_option.attribute_id=:attribute_id')
        ->group('eav_attribute_option_value.option_id');

    $bind = array(
        'attribute_id' => $id_attribute
    );

    $results = $readConnection->fetchAll($select, $bind);

    $results_array = [];
    foreach ($results as $res) {
        $first_letter = mb_substr($res['value'], 0, 1);
        $results_array[$first_letter][] = $res;
    }

    ksort($results_array);

    $result_ar = [];
    foreach ($results_array as $key => $res) {

        $result_ar[$key] = customMultiSort($res, 'value');
    }


    return $result_ar;
}


function customMultiSort($array, $field)
{
    $sortArr = array();
    foreach ($array as $key => $val) {
        $sortArr[$key] = $val[$field];
    }

    array_multisort($sortArr, $array);

    return $array;
}

$brands = getBrands();

/**
 * Получение ресурсной модели
 */
$resource = Mage::getSingleton('core/resource');

/**
 * Установка соединения для записи
 */
$writeConnection = $resource->getConnection('core_write');


foreach ($brands as $letter => $values) {
    foreach ($values as $value) {
        $name = $value['value'];
        $url = $value['url_value'];
        $option_id = $value['option_id'];

        $search_brand = Mage::getModel('attributeSplash/page')->getCollection()
            ->addFieldToFilter('url_key', $url);
        if (count($search_brand) == 0) {
            $new_brand = Mage::getModel('attributeSplash/page');




            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $tablePrefix = (string)Mage::getConfig()->getTablePrefix();
            $select = $connection->select()
                ->from($tablePrefix . 'catalog_product_index_eav', ['attribute_id', 'value', 'entity_id'])
                ->joinInner(
                    array('catalog_category_product_index' => "catalog_category_product_index"),
                    'catalog_category_product_index.product_id = catalog_product_index_eav.entity_id',//main_table.id',
                    array('product_id' => 'product_id')
                )
                ->joinInner(
                    array('catalog_category_entity' => "catalog_category_entity"),
                    'catalog_category_product_index.category_id = catalog_category_entity.entity_id',//main_table.id',
                    array('entity_id' => 'entity_id', 'path' => 'path')
                )
                ->where($tablePrefix . 'catalog_product_index_eav.attribute_id = ?', 141)
                ->where($tablePrefix . 'catalog_product_index_eav.value = ?', $option_id)
                ->where($tablePrefix . 'catalog_product_index_eav.store_id = ?', 1);


            $categoriesid = [];

            $category_id_for_save = [];
            foreach ($connection->fetchAll($select) as $cat) {
                $path = $cat['path'];
                $ids = explode('/', $path);
                if (isset($ids[2]) && (!isset($category_id_for_save[$ids[2]]))) {
                    $category_id_for_save[$ids[2]] = $ids[2];
                }

            }


            $urlRewrites = [];


            $table_plash_category = 'attributesplash_page_category';

            $category_for_save=0;
            if (count($category_id_for_save)==1){
                foreach ($category_id_for_save as $category_id) {
                    $category_for_save = $category_id;
                }
            }
            $new_brand->setData([
                    'option_id' => $option_id,
                    'category_id' => $category_for_save,
                    'display_name' => $name,
                    'url_key' => $url,
                    'is_enabled' => 1,
                    'display_mode' => 'PRODUCTS',
                    'other' => serialize([])
                ]
            );
            if ($new_brand->save()) {
                $pageid = $new_brand->getPageId();
                echo '$pageid ' . $pageid.' '.$name . ' ' . $url . "<br/>";

                echo '<pre>$category_id_for_save';
                print_r($category_id_for_save);
                echo '</pre>';

                foreach ($category_id_for_save as $category_id) {
                    $query = "INSERT INTO {$table_plash_category} (page_id, category_id) VALUES('{$pageid}', '{$category_id}')";
                    $writeConnection->query($query);
                }

            }
            //die();
        }

    }
}

/*
echo '<pre>';
print_r($brands);
echo '</pre>';*/