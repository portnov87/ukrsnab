<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

//$_SERVER['HTTPS'] = 'on';
//$_SERVER['HTTP_X_FORWARDED_PROTO']='https';
foreach (array(
             'SERVER_PORT' => 443,
             'HTTP_X_FORWARDED_PROTO' => 'https',
             'HTTP_CF_VISITOR' => '{"scheme":"https"}'
         ) as $key => $value) {
    if (isset($_SERVER[$key]) && $_SERVER[$key] == $value) {
        $_SERVER['HTTPS'] = 'on';
        break;
    }
}
$redirect_brands = [
    'bittner',
    'bionorica',
    'berlin-chemie',
    'bayer',
    'yuriya-farm',
    'borshchagovskiy-hfz',
    'biofarma',
    'boiron',
    'heel',
    'richter',
    'guna',
    'interchem',
    'lauma',
    'kalina',
    'ikarov',
    'biokon',
    'effect',
    'aromatika',
    'laroche',
    'stiefel-lab',
    'styx',
    'himalaya',
    'weleda',
    'lierac',
    'flora-farm',
    'elfa',
    'evalar-cosmetics',
    'bioderma',
    'walmark',
    'solgar-vitamin',
    'ekomed',
    'kvz',
    'healthyway',
    'elit-pharm',
    'evalar-bad',
    'sandoz',
    'sir',
    'schering-plough'
];

$redirect_to = [
    '/generator-benzinovyj-vitals-ers-2-8b.html'=>'/generatory/benzinovye-generatory',
    '/dizel-nyj-generator-kipor-kde16ea3.html'=>'/generatory/dizel-nye-generatory',

    '/generatory/dizelnye-generatory'=>'/generatory/dizel-nye-generatory',


    '/dizel-nyj-generator-endress-ese-80-pw-ms.html'=>'/generatory/dizelnye-generatory',
    '/contacts.html'=>'/contact',
    '/remont.html' => '/remont',
    '/prokat.html' => '/prokat',
    '/dostavka-i-oplata.html' => '/dostavka-i-oplata',
    '/garantiya.html' => '/garantiya',
    '/clients.html' => '/clients',
    '/vakansii.html' => '/vakansii',
    '/proizvoditeli.html' => '/proizvoditeli',
    '/pnevmooborudovanie/kompressory'=>'/kompressory/',
    //'/stroitelnoe-oborudovanie/shlifovalnye-mashiny'=>'/shlifovalnye-mashiny/',
    '/vodosnab/motopompy'=>'/motopompy/',
    '/stroitelnoe-oborudovanie/almaznaja-tehnika'=>'/almaznaja-tehnika/',
    '/adapter-dlja-koronok-husqvarna-1-2-gm-1-1-4m.html'=>'/',
    '/akkumuljatornyj-kul-tivator-grunfeld-t20xd.html'=>'/',
    '/arenda_storitelnoy_lulki.html'=>'/prokat/arenda-stroitel-noj-ljul-ki.html',
    '/arenda-stroitelnykh-lesov'=>'/prokat/arenda-stroitel-nyh-lesov.html',
    '/benzinovyj-generator-matari-matari-s-8990e.html'=>'/generatory/benzinovye-generatory',
    '/benzinovyj-generator-matari-mx10000e-ats.html'=>'/generatory/benzinovye-generatory',
    '/betonomeshalka-agrimotor-v-1510.html'=>'/',
    '/diskovaja-pila-makita-hs7601.html'=>'/',
    '/dizel-nyj-generator-forte-fgd-6500e.html'=>'/generatory/dizelnye-generatory',
    '/dizel-nyj-generator-kipor-kde115ss3.html'=>'/generatory/dizelnye-generatory',
    '/dizel-nyj-generator-kipor-kde13ss.html'=>'/generatory/dizelnye-generatory',
    '/dizel-nyj-generator-kipor-kde16ea3.html'=>'/generatory/dizelnye-generatory',
    '/dizel-nyj-generator-kipor-kde20ssp3.html'=>'/generatory/dizelnye-generatory',
    '/dizel-nyj-generator-kipor-kde40stp3.html'=>'/generatory/dizelnye-generatory',
    '/dizel-nyj-generator-matari-md7000e.html'=>'/generatory/dizelnye-generatory',
    '/drel-shurupovert-makita-6261dwpe.html'=>'/',
    '/dvigatel-benzinovyj-honda-gx160-3817.html'=>'/',
    '/energosnab/generatory/benzinovye-generatory/filter/moshchnost-ghienieratora/2-kvt-3-kvt'=>'/generatory/benzinovye-generatory',
    '/energosnab/generatory/benzinovye-generatory/filter/proizvoditiel/forte'=>'/generatory/benzinovye-generatory',
    '/energosnab/generatory/benzinovye-generatory/filter/proizvoditiel/gude'=>'/generatory/benzinovye-generatory',
    '/energosnab/generatory/benzinovye-generatory/filter/proizvoditiel/kientavr'=>'/generatory/benzinovye-generatory',
    '/energosnab/generatory/benzinovye-generatory/filter/proizvoditiel/tagred'=>'/generatory/benzinovye-generatory',
    '/energosnab/generatory/gazovye-generatory/filter/moshchnost-ghienieratora/50-kvt-100-kvt'=>'/generatory/benzinovye-generatory',
    '/fen-stroitel-nyj-makita-hg651ck-termovozduhoduvka.html'=>'/',
    '/gazonokosilka-gardena-power-max-36e.html'=>'/',
    '/gazovaja-teplovaja-pushka-grunhelm-ggh-15.html'=>'/',
    '/gazovaja-teplovaja-pushka-grunhelm-ggh-50.html'=>'/',
    '/gazovaja-teplovaja-pushka-master-blp-30-m.html'=>'/',
    '/gazovaja-teplovaja-pushka-vitals-gh-151.html'=>'/',
    '/generator-benzinovij-konner-sohnen-ks-10000e-ats.html'=>'/',
    '/generator-dizel-nyj-cummins-c17-d5.html'=>'/',
    '/generator-matari-md100.html'=>'/',
    '/generator-matari-mx14000e.html'=>'/generatory',
    '/gibkij-val-tax-i-taxe-5-0-m-dlja-glubinnogo-vibratora-avmu-ispanija.html'=>'/',
    '/gibkij-val-tax-i-taxe-6-0-m-dlja-glubinnogo-vibratora-avmu-ispanija.html'=>'/',
    '/gibkij-val-tdx-i-tdxe-5-0-m-dlja-glubinnogo-vibratora-dingo-ispanija.html'=>'/',
    '/glubinnyj-vibrator-avant-101-1-1-8-4621.html'=>'/',
    '/hitachi-dh50mb.html'=>'/',
    '/http-dev-ukrsnab-com-ua-vibracionnaja-shlifmashina-makita-bo3710.html'=>'/',
    '/informaciya.html'=>'/',
    '/instrument/elektroinstrument/benzorezy'=>'/',
    '/instrument/elektroinstrument/shurupoverty'=>'/',
    '/jelektricheskaja-teplovaja-pushka-termija-15000-15-0-kvt.html'=>'/',
    '/makita-hw112-mojka-vysokogo-davlenija-mini-mojka.html'=>'/',
    '/makita-js1600-nozhnicy-listovye-dlja-metalla.html'=>'/',
    '/makita-lxt600-nabor-akkumuljatornyh-instrumentov-makita.html'=>'/',
    '/makita-uc3520a-jelektricheskaja-cepnaja-pila.html'=>'/',
    '/makita-ut120-universal-naja-drel-mikser-makita.html'=>'/',
    '/makita-ut2204-stroitel-nyj-mikser-makita.html'=>'/',
    '/mashina-zatirochnaja-masalta-ms-60-n.html'=>'/',
    '/mashina-zatirochnaja-masalta-ms-90-n.html'=>'/',
    '/motopompa-honda-eternus-wt30x.html'=>'/',
    '/mototehnika'=>'/',
    '/mototraktor-forte-101-el-2059.html'=>'/',
    '/otbojnye-molotki/filter/proizvoditiel/makita'=>'/',
    '/otbojnye-molotki/filter/tip-otboinogho-molotka/pnievmatichieskii'=>'/',
    '/perforator-makita-hr2470-makita.html'=>'/',
    '/poluavtomat-invertornyj-dnipro-m-mig-mma-195-2in1-igbt.html'=>'/',
    '/prodasha_storitelnoy_lulki.html'=>'/prodasha-storitelnoy-lulki',
    '/quickshop/index/view/path/benzinovaja-vibroplita-masalta-msr90-2.html'=>'/',
    '/quickshop/index/view/path/vibroplita-avant-ap-65-atribe.html'=>'/',
    '/quickshop/index/view/path/vibroplita-kentavr-vp-60kb-66-kg-kitaj-html.html'=>'/',
    '/remont-i-obsluzhivaniye-vibrotrambovok/'=>'/',
    '/review/product/list/id/2957/category/42/'=>'/',
    '/review/product/list/id/3615/category/147/'=>'/',
    '/review/product/list/id/3928/category/44/'=>'/',
    '/selkhoztekhnika/traktory'=>'/',
    '/shlifmashina-bolgarka-makita-ga5030.html'=>'/',
    '/shurupovert-makita-df011dse-2302.html'=>'/',
    '/snegouborschik-al-ko-snowline-55-e.html'=>'/',
    '/snegouborschik-al-ko-snowline-620-e.html'=>'/',
    '/snegouborschik-husqvarna-5524st.html'=>'/',
    '/snegouborschik-husqvarna-st261e.html'=>'/',
    '/stroitelnoe-oborudovanie/almaznaja-tehnika/ustanovki-dlja-almaznogo-burenija/filter/proizvoditiel/cayken'=>'/',
    '/stroitelnoe-oborudovanie/mozaichnoshlifovalnye-mashiny/filter/oblast-primienieniia/dlia-pola/proizvoditiel/spm'=>'/',
    '/stroitelnoe-oborudovanie/prokat.html'=>'/',
    '/stroitelnoe-oborudovanie/vibratory/ploshchadochnye-vibratory/zapchasti-i-komplektujuschie-k-ploschadochnym-vibratoram/filter/proizvoditiel/krasnyi-maiak'=>'/',
    '/stroitelnoe-oborudovanie/vibroplity/prokat.html'=>'/',
    '/stroitelnoe-oborudovanie/vibroplity/vibroplity-dlja-uplotnenija-peska'=>'/',
    '/stroitelnoe-oborudovanie/vibroplity/vibroplity-dlja-uplotnenija-schebnja'=>'/',
    '/stroitelnoe-oborudovanie/vibroplity/vibroplity-ot-40-do-89-kg/filter/proizvoditiel/avant'=>'/',
    '/stroitelnoe-oborudovanie/vibroplity/vibroplity-ot-40-do-89-kg/filter/proizvoditiel/krasnyi-maiak'=>'/',
    '/stroitelnoe-oborudovanie/vibroplity/vibroplity-s-dvigatelem-honda/filter/proizvoditiel-dvighatielia/honda/tip-dvighatielia/bienzinovyi/vies-vibroplity/40-kgh-89-kgh?dir=desc&order=price'=>'/',
    '/stroitelnoe-oborudovanie/vibroplity/vibroplity-s-dvigatelem-honda/filter/proizvoditiel/honker/proizvoditiel-dvighatielia/honda/tip-dvighatielia/bienzinovyi/vies-vibroplity/40-kgh-89-kgh?dir=desc&order=price'=>'/',
    '/stroitelnoe-oborudovanie/vibroplity/vibroplity-s-dvigatelem-honda/filter/vies-vibroplity/90-kgh-119-kgh'=>'/',
    '/sturm-rh2592r.html'=>'/',
    '/svarochnyj-apparaty-dnipro-m-mini-mma-250-dpfc-disp-pl-pan-kejs-m-f.html'=>'/',
    '/svarochnyj-poluavtomat-schweis-iws-300.html'=>'/',
    '/teplovaja-pushka-na-otrabotannom-masle-master-wa-33b.html'=>'/',
    '/teplovoe-oborudovanie/teplovye-pushki/elektricheskiye-teplovyye-pushki/filter/proizvoditiel/master'=>'/',
    '/teplovoe-oborudovanie/teplovye-pushki/filter/proizvoditiel/ballu'=>'/',
    '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/filter/moshchnost-tieplovoi-pushki/bolieie-10-kvt'=>'/',
    '/teplovoe-oborudovanie/teplovye-pushki/gazovye-pushki/filter/proizvoditiel/master'=>'/',
    '/traktor-partner-p11577rb.html'=>'/',
    '/transformator-krasnyj-majak-tszi-1-6.html'=>'/',
    '/transformator-krasnyj-majak-tszi-2-5.html'=>'/',
    '/transformator-krasnyj-majak-tszi-4-0-uhl.html'=>'/',
    '/uglovaja-shlifmashina-makita-9565cvr.html'=>'/',
    '/ustanovka-almaznogo-burenija-titan-pdakb-260.html'=>'/',
    '/ustanovka-almaznogo-burenija-titan-pdakb2-102-ns100.html'=>'/',
    '/ustanovka-almaznogo-burenija-titan-pdakb2-300.html'=>'/',
    '/ustanovka-almaznogo-burenija-titan-pdakb3-102.html'=>'/',
    '/vibrobulava-ah48-dlja-glubinnogo-vibratora-dingo-i-avmu.html'=>'/',
    '/vibrobulava-m-5-afp-10-m.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibrobulava-m-5-afp-5m.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibrobulava-m-6-afp-5-m.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibrobulava-m-6-afp-7-m.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibrobulava-m-8-af.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibrobulava-mb-32.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibrobulava-mb-62.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibroplita-avant-ap-65.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibroplita-avant-ap-90-4654.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibroplita-avant-aph-3-2958.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibroplita-avant-aph-5-2793.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibroplita-masalta-ms-125-126-kg-koreya-html-4550.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibroplita-ntc-it-1060-vc-65-kg-chekhiya-html.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibroplita-ntc-it-1590-vc-98-kg-chekhiya-html.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibroplita-scheppach-hp-1300s-4822.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibroplita-wiber-ugms-27-4508.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibrotrambovka-ammann-aph-110-95.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibrotrambovka-avant-ar-82e-4617.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibrotrambovki-nts'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vodosnab/tekhnika-dlya-poliva'=>'/stroitelnoe-oborudovanie/vibroplity',

    '/vibrorejka-spm-rv-01d2-5-m-ukraina.html'=>'/stroitelnoe-oborudovanie/vibrorejki',
    '/dvigatel-dizel-nyj-forte-d101e.html'=>'/dvigateli/dizel-nye-dvigateli',
    '/vibrorejka-spm-rv-01d4-2-5-m-ukraina.html'=>'/stroitelnoe-oborudovanie/vibrorejki',
    '/vibrorejka-spm-rv-01-3-5-m-ukraina.html'=>'/stroitelnoe-oborudovanie/vibrorejki',
    '/glubinnyj-vibrator-enar-dingo.html'=>'/stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory',
    '/mashina-zatirochnaja-enar-t-900-e.html'=>'/stroitelnoe-oborudovanie/zatirochnye-mashiny',
    '/stroitelnoe-oborudovanie/betonomeshalki/filter/proizvoditiel/forte'=>'/',
    '/vibroplita-enar-zen-16-dgh-94-kg-ispaniya-html.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/vibrorejka-ntp-vifom-2-m-kitaj.html'=>'/stroitelnoe-oborudovanie/vibrorejki',
    '/parketoshlifoval-naja-ploskoshlifoval-naja-mashina-new-golia-eh-italija.html'=>'/stroitelnoe-oborudovanie/shlifovalnye-mashiny',
    '/kompressor-bmsh-so-243-50-l-ukraina.html'=>'/kompressory',
    '/dvigatel-benzinovyj-loncin-g420fd-1915.html'=>'/dvigateli/benzinovye-dvigateli',
    '/kompressor-bmsh-so-415m-ukraina.html'=>'/kompressory',
    '/mashina-zatirochnaja-enar-t-900-x-2-dvuhrotornaja.html'=>'/stroitelnoe-oborudovanie/zatirochnye-mashiny',
    '/batareja-dlja-xdl-40a.html'=>'/',
    '/parketoshlifoval-naja-ploskoshlifoval-naja-mashina-rt-58-es-italija.html'=>'/stroitelnoe-oborudovanie/shlifovalnye-mashiny',
    '/vibrorejka-technoflex-pgm-1-5-m-ispaniya.html'=>'/stroitelnoe-oborudovanie/vibrorejki',
    '/vibrorejka-spm-rv-01d4-4-m-ukraina.html'=>'/stroitelnoe-oborudovanie/vibrorejki',
    '/vibrorejka-tornado-e-turbo-2000-ispaniya-2-metra.html'=>'/stroitelnoe-oborudovanie/vibrorejki',
    '/mashina-zatirochnaja-enar-t-900-h.html'=>'/stroitelnoe-oborudovanie/zatirochnye-mashiny',
    '/mototehnika/motobloki-i-motokultivatory/filter/proizvoditiel/kientavr'=>'/sadovaja-tehnika/kul-tivatory',
    '/parketoshlifoval-naja-ploskoshlifoval-naja-mashina-rt-58-eh-italija.html'=>'/stroitelnoe-oborudovanie/shlifovalnye-mashiny',
    '/ploskoshlifoval-naja-mashina-rt-17-es.html'=>'/stroitelnoe-oborudovanie/shlifovalnye-mashiny/ploskoshlifovalnye-mashiny',
    '/vibrorejka-tornado-e-4000-ispaniya-4-metra.html'=>'/stroitelnoe-oborudovanie/vibrorejki',
    '/pompa-benzinovaja-konner-sohnen-ks-80tw-3541.html'=>'/motopompy',
    '/pompa-benzinovaja-konner-sohnen-ks-80cw-3542.html'=>'/motopompy',
    '/pompa-benzinovaja-konner-sohnen-ks-80-3540.html'=>'/motopompy',
    '/pompa-benzinovaja-konner-sohnen-ks-50.html'=>'/motopompy',
    '/vibroplita-wacker-neuson-dps-1850h-vario-dizel-nym-dvigatelem-i-s-dvumja-stepenjami-uplotnenija.html'=>'/stroitelnoe-oborudovanie/vibroplity/dizelnye-vibroplity',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir2ps-s-zahvatom-15r-12h-x-55.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/vibrorejka-technoflex-vifom-1-5-m-ispaniya.html'=>'/stroitelnoe-oborudovanie/vibrorejki',
    '/kompressor-bmsh-so-7b-ukraina.html'=>'/kompressory',
    '/vibrorejka-spm-rv-01-5-m-ukraina.html'=>'/stroitelnoe-oborudovanie/vibrorejki',
    '/vibrorejka-qzh-4000-ispaniya-4-metra.html'=>'/stroitelnoe-oborudovanie/vibrorejki',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir9ps-s-zahvatom-25r-x-75.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir9ps-s-zahvatom-23r-x-70.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir15bv-s-zahvatom-25h-x-108.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir30bs-s-zahvatom-28h-x-160.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir40bv-s-zahvatom-32h-x-160.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir40bv-s-zahvatom-28h-x-160.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/vibroplita-wacker-neuson-wpu-1550a-s-chetyrehtaktnym-benzinovym-dvigatelem.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir3ps-s-zahvatom-19h-x-50.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/preseparator-dlja-professional-nogo-stroitel-nogo-pylesosa-sc320-020.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/promyshlennye-pylesosy/preseparatory',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir21bs-s-zahvatom-32h-x-160.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir21bv-s-zahvatom-28h-x-160.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir40bs-s-zahvatom-28h-x-160.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir40bs-s-zahvatom-32h-x-160.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/molotok-otbojnyj-pnevmaticheskij-ingersoll-rand-ir30bv-s-zahvatom-32h-x-160.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/otbojnye-molotki',
    '/preseparator-dlja-professional-nogo-stroitel-nogo-pylesosa-sc320-010.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/promyshlennye-pylesosy/preseparatory',
    '/preseparator-dlja-professional-nogo-stroitel-nogo-pylesosa-sc450-060.html'=>'/stroitelnoe-oborudovanie/betonnye-raboty/promyshlennye-pylesosy/preseparatory',
    '/dizel-nyj-dvigatel-dlja-glubinnogo-vibratora-wacker-neuson-l5000-225.html'=>'/stroitelnoe-oborudovanie/vibratory/glubinnye-vibratory/filter/page3?p=3',
    '/vibroplita-wacker-neuson-wpu-1550aw-s-benzinovym-dvigatelem-2998.html'=>'/vibroplita-wacker-neuson-wpu-1550aw-s-benzinovym-dvigatelem-2998.html',
    '/dps-1850h-basic-vibroplita-prjamogo-hoda-s-dizel-nym-dvigatelem.html'=>'/stroitelnoe-oborudovanie/vibroplity/dizelnye-vibroplity',
    '/vesovaja-trap-telega-vis-serii-vtt015-gruzopod-emnost-ju-3-t.html'=>'/gruzopod-emnoe-oborudovanie/telezhki-transportirovochnye',
    '/vibroplita-wacker-neuson-wpu-1550aw-s-benzinovym-dvigatelem.html'=>'/stroitelnoe-oborudovanie/vibroplity',
    '/zatirochnaja-mashina-wacker-neuson-ct-48-11a.html'=>'/stroitelnoe-oborudovanie/zatirochnye-mashiny',
    '/zatirochnaja-mashina-wacker-neuson-ct-24-4a.html'=>'/stroitelnoe-oborudovanie/zatirochnye-mashiny',
    '/vesy-rokla-vis-2vp4-r-p-s-termoprinterom.html'=>'/gruzopod-emnoe-oborudovanie/rokly',
    '/rotornyj-lazernyj-nivelir-agp-160.html'=>'/jelektroinstrument/izmeritel-nyj-instrument/niveliry',
    '/rotornyj-lazernyj-nivelir-agp-150.html'=>'/jelektroinstrument/izmeritel-nyj-instrument/niveliry',
    '/zatirochnaja-mashina-wacker-neuson-ct-48-8a.html'=>'/stroitelnoe-oborudovanie/zatirochnye-mashiny',
    '/fil-tr-kartridzh-dlja-svc-2-2-220hm.html'=>'/',
    '/fil-tr-kartridzh-nera-dlja-svc-2-2-220.html'=>'/',
    '/fil-tr-kartridzh-dlja-svc-5-5-380al.html'=>'/',
    '/katushka-s-provolokoj-dlja-xdl-40a.html'=>'/',
    '/avtomaticheskij-vjazatel-provoloki-xdl-40a.html'=>'/',
    '/vesy-rokla-vis-2vp4-r-p-s-wifi.html'=>'/gruzopod-emnoe-oborudovanie/rokly',
    '/chehol-derzhatel-dlja-xdl-40a.html'=>'/',
    '/vesy-rokla-vis-2vp4-r-p.html'=>'/gruzopod-emnoe-oborudovanie/rokly',
    '/vesy-rokla-vis-3vp4-r-p.html'=>'/gruzopod-emnoe-oborudovanie/rokly',
    '/vesy-rokla-vis-1vp4-r-p.html'=>'/gruzopod-emnoe-oborudovanie/rokly',
    '/udlinitel-dlja-xdl-40a.html'=>'/'
];
//optimize.sh -i media -o media1
$url_for_red = $_SERVER['REQUEST_URI'];
if (isset($redirect_to[$url_for_red])) {
    header('HTTP/1.1 301 Moved Permanently');//редиректим
    header("Location: " . $redirect_to[$url_for_red]);

    exit;
}


$uploadfile = 'redirects.csv';
if (file_exists($uploadfile)) {
    $delimeter = ',';
    $handle_redirects = fopen($uploadfile, "r");
    $issetproduct = 0;
    $rows = 0;
    $redirects_url=[];
    $url_for_red = $_SERVER['REQUEST_URI'];
    while (($csv = fgetcsv($handle_redirects, 0, $delimeter)) !== false) {

        $rows++;
        if ($rows > 1) {

            $redirect_from = $csv[0];
            $redirect_to = $csv[2];
            $redirects_url[$csv[0]]=$csv[2];
            //if (isset($redirects_url[$url_for_red])) {

            if ($url_for_red==$csv[0]){


                if ($csv[2]!='') {

//                    echo '<pre>';
//                    print_r($csv);
//                    echo '</pre>';
//                    die();
                    header('HTTP/1.1 301 Moved Permanently');//редиректим
                    header("Location: " . $csv[2]);

                    exit;
                }else{
//                    header('HTTP/1.1 301 Moved Permanently');//редиректим
//                    header("Location: https://ukrsnab.com.ua/");
//                    exit;
                }
            }
        }

    }
    /*
    if (count($redirects_url)>0)
    {
       // if (isset($redirects_url[]))

        if (isset($redirects_url[$url_for_red])) {
            header('HTTP/1.1 301 Moved Permanently');//редиректим
            header("Location: " . $redirects_url[$url_for_red]);

            exit;
        }

    }*/


}




                if (version_compare(phpversion(), '5.3.0', '<')===true) {
    echo  '<div style="font:12px/1.35em arial, helvetica, sans-serif;">
<div style="margin:0 0 25px 0; border-bottom:1px solid #ccc;">
<h3 style="margin:0; font-size:1.7em; font-weight:normal; text-transform:none; text-align:left; color:#2f2f2f;">
Whoops, it looks like you have an invalid PHP version.</h3></div><p>Magento supports PHP 5.3.0 or newer.
<a href="http://www.magentocommerce.com/install" target="">Find out</a> how to install</a>
 Magento using PHP-CGI as a work-around.</p></div>';
    exit;
}

/**
 * Compilation includes configuration file
 */
define('MAGENTO_ROOT', getcwd());

$compilerConfig = MAGENTO_ROOT . '/includes/config.php';
if (file_exists($compilerConfig)) {
    include $compilerConfig;
}

$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
$maintenanceFile = 'maintenance.flag';

if (!file_exists($mageFilename)) {
    if (is_dir('downloader')) {
        header("Location: downloader");
    } else {
        echo $mageFilename." was not found";
    }
    exit;
}

if (file_exists($maintenanceFile)) {
    include_once dirname(__FILE__) . '/errors/503.php';
    exit;
}

require MAGENTO_ROOT . '/app/bootstrap.php';
require_once $mageFilename;

#Varien_Profiler::enable();

if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) {
    Mage::setIsDeveloperMode(true);
}

//ini_set('display_errors', 1);

umask(0);

/* Store or website code */
$mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';

/* Run store or run website */
$mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';

Mage::run($mageRunCode, $mageRunType);
