<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 07.07.2018
 * Time: 21:49
 */

Class SimpleXMLElementExtended extends SimpleXMLElement
{
    /**
     * Adds a child with $value inside CDATA
     * @param unknown $name
     * @param unknown $value
     */
    public function addChildWithCDATA($name, $value = NULL)
    {
        $new_child = $this->addChild($name);

        if ($new_child !== NULL) {
            $node = dom_import_simplexml($new_child);
            $no = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($value));
        }

        return $new_child;
    }
}

umask(0);
ini_set("display_errors", 1);
error_reporting(E_ALL);
//libxml_disable_entity_loader(false);
/*ob_implicit_flush(1);

echo str_pad('', 1024);
@ob_flush();
flush();
*/
date_default_timezone_set("Europe/Kiev");
//header('Content-Type: text/xml; charset=utf-8', true); //set document header content type to be XML

$title = 'Ukrsnab - интернет-гипермаркет строительной и промышленной техники';
$link = 'https://ukrsnab.com.ua';

$shippingcountry = 'UA';
$shippingcountrylong = ' UAH';
$shippingservice = 'Standard';
$shippingprice = '5.95' . $shippingcountrylong;








function xsanatise($var)
{
    $var = strip_tags($var);
    $var = htmlspecialchars($var, ENT_XML1, 'UTF-8');
    $var = htmlspecialchars($var, ENT_QUOTES, 'UTF-8');
    $var = preg_replace('/[\x00-\x1f]/', '', $var);
    $var = str_ireplace(array('<', '>', '&', '\'', '"'), array('&lt;', '&gt;', '&amp;', '&apos;', '&quot;'), $var);
    $var = str_replace('&nbsp;', ' ', $var);
    $var = str_replace('&ndash;', ' ', $var);

    return $var;
}

function xsanatisegoogle($var)
{

    $var = htmlspecialchars($var, ENT_XML1, 'UTF-8');
    $var = str_replace('&amp;gt;', '>', $var);
    $var = str_replace('&amp;gt,', '>', $var);
    $var = str_replace('&gt,', '>', $var);
    $var = str_replace('&gt;', '>', $var);

    $var = str_replace('&amp;', '&', $var);


    return $var;
}


function xcheck($var)
{

    $var = trim($var);

    if (strlen($var) >= 1 && preg_match('/[A-Z]+[a-z]+[0-9]+/', $var) !== false) {
        return true;
    } else {
        return false;
    }

}


function xcheckgoogle($var)
{

    $var = trim($var);

    if (strlen($var) >= 1 && preg_match('/^[1-9][0-9]*$/', $var) !== false) {
        return true;
    } else {
        return false;
    }

}


require_once dirname(__FILE__) . '/app/Mage.php';
Mage::app()->getCacheInstance()->flush();

Mage::app('admin')->setUseSessionInUrl(false);
Mage::app('default');
$skus=[];

Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);


$handle_ids = fopen('old_products.csv', "r");
$issetproduct = 0;
$delimeter='~';
$rows = 0;
while (($csv = fgetcsv($handle_ids, 0, $delimeter)) !== false) {
    $_id = $csv[1];
    $sku= $csv[0];
    $_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);

    if ($_product) {
        if ($_product->getId()) {
            $_product->setOldMagentoId($_id);
            //$_product->setData(['old_magento_id'=>$_id]);//OldMagentoId($id);//old_magento_id
            $_product->save();
            /*echo '<pre>getdata'.$_product->getId();
            print_r($_product->getData());
            echo '</pre>';*/
        }
    }

    //echo $_id .' '.$sku."\r\n";

    //die();
}
function getPath($product)
{
    $path = array();


    if ($product) {
        //$categoriesid = $this->getProduct()->getCategoryIds();
        $categoriesid = [];
        //$level=
        foreach ($product->getCategoryCollection() as $cat) {
            $categoriesid[] = $cat->getId();
        }
        if (count($categoriesid) > 0) {
            $category_id = $categoriesid[count($categoriesid) - 1];
            $category = Mage::getModel('catalog/category')->load($category_id);
        }


    }
    /*} else {
        $category = $this->getCategory();
    }*/


    if ($category) {
        $pathInStore = $category->getPathInStore();
        $pathIds = array_reverse(explode(',', $pathInStore));

        $categories = $category->getParentCategories();

        // add category path breadcrumb
        $c = count($pathIds);// - 1;
        $i = 1;
        if ($product) {
            $current_product = $product->getId();
        } else $current_product = false;

        foreach ($pathIds as $categoryId) {
            if (isset($categories[$categoryId])) {


                if ($categories[$categoryId]->getName() != '') {
                    $path['category' . $categoryId] = $categories[$categoryId]->getName();
                }

                $i++;

                // }
            }
        }

        if ($category->getName() != '') {
            $path['category' . $category->getId()] = $category->getName();

        }
    }


    return $path;

}


function getCategories()
{
    $collection = Mage::getModel('catalog/category')
        ->getCollection()
        ->addAttributeToSelect('*');
    //->addUrlRewrite()
    //->addAttributeToFilter('status', array('eq' => 1));

    return $collection;

}

function generate_feed($file)
{
    $skus=[];
    $siteUrl = 'https://ukrsnab.com.ua/';
    $siteName = $companyName = 'УкрСнаб';


    $yml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
    $yml .= "<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n";
    $yml .= "<yml_catalog date=\"" . date('Y-m-d H:i') . "\">\n";
    $yml .= "	<shop>\n";
    $yml .= "		<name>$siteName</name>\n";
    $yml .= "		<company>$companyName</company>\n";
    $yml .= "		<url>$siteUrl</url>\n";


    $yml .= "		<currencies>\n";
    $yml .= "			<currency id=\"UAH\" rate=\"1\"/>\n";
    //$yml .= "			<currency id=\"USD\" rate=\"CBRF\"/>\n";
    //$yml .= "			<currency id=\"EUR\" rate=\"CBRF\"/>\n";
    $yml .= "		</currencies>\n";

    /* $yml .= "		<categories>\n";

     $categories = getCategories();
     foreach ($categories as $category) {
         $parent = $category->getParentId();
         $idcategory = $category->getId();
         $name = $category->getName();
         if ($parent == 3) {
             $yml .= '<category id="' . $idcategory . '">' . $name . '</category>' . "\n";
         } else {
             $yml .= '<category id="' . $idcategory . '" parentId="' . $parent . '">' . $name . '</category>' . "\n";
         }
     }
     $yml .= "		</categories>\n";*/


    $collection_product = Mage::getModel('catalog/product')
        ->getCollection()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('include_in_feed_rozetka', array('eq' => 1));
    //->addUrlRewrite()
    //->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)); //STATUS_DISABLED
    $resource = Mage::getSingleton('core/resource');

    /**
     * Установка соединения для чтения
     */
    $readConnection = $resource->getConnection('core_read');


    if (count($collection_product)) {


        $cats_new = [];
        foreach ($collection_product as $pr) {
            //if ($pr->getData('include_in_feed_rozetka') == '1') {
            $cats = $pr->getCategoryIds();

            /*$collection_cats = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect('*');*/
            //$cats
            foreach ($cats as $c) {
                $cat_model = Mage::getModel('catalog/category')->load($c);
                if ($cat_model) {
                    if ($cat_model->getIsActive()==1) {
                        $cats_new[$cat_model->getId()] = $cat_model->getName();
                    }
                }
                //$cats_new[$c->getId()]=$c->getName();

            }
            //}
        }

        if (count($cats_new) > 0) {
            $yml .= "		<categories>\n";
            foreach ($cats_new as $key => $cat_name) {
                $yml .= '<category id="' . $key . '">' . $cat_name . '</category>' . "\n";
            }
            $yml .= "		</categories>\n";
        }
    }
    $yml .= "		<offers>\n";
    //$yml .= "		<offer id='" . $product_id . "' available='" . $available . "'>\n";


    foreach ($collection_product as $product) {

        //if ($product->getData('include_in_feed_rozetka') == '1') {
        //echo $product->getAttributeSetId();
        //die();
               $product_id = $product->getId();
        //$stockItem = $product->getStockItem();

        $product = Mage::getModel('catalog/product')->load($product_id);
        $stockItem = $product->getStockItem();




        if ($stockItem->getIsInStock())
            $available = 'true';
        else
            $available = 'false';


        $old_magento_id=$product->getOldMagentoId();
        echo '$old_magento_id = '.$old_magento_id."\r\n";
        if ($old_magento_id!='')
            $id_roz=$old_magento_id;
        else $id_roz=$product_id;

        $yml .= '		<offer id="' . $id_roz . '" available="' . $available . '">';
        $yml .="\n";
        $name_product = $product->getName();
        $code = $product->getSku();

        $price = $product->getPrice();
        $special_price = false;
        if (empty($product->getSpecialPrice())) {
            //$price = $product->getFinalPrice();//
            $price=Mage::helper('core')->currency($product->getFinalPrice(), false, false);
            //$price = str_replace('грн.', '', $price);

        } else {
            //$price = $product->getFinalPrice();//
            $price = Mage::helper('core')->currency($product->getFinalPrice(), false, false);
            //$price = str_replace('грн.', '', $price);

            //$special_price = $product->getSpecialPrice();//
            $special_price = Mage::helper('core')->currency($product->getSpecialPrice(), false, false);
            //$special_price = str_replace('грн.', '', $special_price);

        }

        $query = "SELECT * FROM core_url_rewrite WHERE product_id='" . $product_id . "' AND category_id IS NULL";
        $results = $readConnection->fetchAll($query);
        $urllink = $product->getProductUrl();
        foreach ($results as $res) {
            $urllink = 'https://ukrsnab.com.ua/' . $res['request_path'];
        }

        //$image = Mage::getBaseUrl('media') . 'catalog/product' . $product->getImage();
        $description = $product->getDescription();
        $paramCurrency = 'UAH';
        $name_product=htmlentities($name_product);


        $yml .= "<name>" . $name_product . "</name>\n";
        //$yml .= "<code>".$code."</code>\n";
        $special_date=strtotime($product->getSpecialToDate());
        $date=time();
        if ($special_price&&($special_date>=$date)) {
            $yml .= "<price_old>" . $price . "</price_old>\n";
            $yml .= "<price>" . $special_price . "</price>\n";
        } else
            $yml .= "<price>" . $price . "</price>\n";

        $yml .= "<description><![CDATA[" . $description . "]]></description>\n";
        $yml .= "<url>" . $urllink . "</url>\n";
        $yml .= "<currencyId>" . $paramCurrency . "</currencyId>\n";

        //categoryId

        $cats = $product->getCategoryIds();


        $_level = 1;
        $cat_finish = false;
        foreach ($cats as $c) {
            $cat_model = Mage::getModel('catalog/category')->load($c);
            $cat = $cat_model->getData();
            if ($cat['level'] > $_level) {
                $_level = $cat['level'];
                $cat_finish = $cat_model;
            }

        }
        if ($cat_finish) {
            $yml .= "<categoryId>" . $cat_finish->getId() . "</categoryId>\n";
        }


        //<picture>http://abc.ua/upload/iblock/a53/a5391cddb40be91705.jpg</picture>
        if ($available == 'true')
            $yml .= "<stock_quantity>100</stock_quantity>\n";
        else
            $yml .= "<stock_quantity>0</stock_quantity>\n";


        //$product_modal=Mage::getModel('catalog/product')->load($product->getId());

        $product->load('media_gallery');
        //if ($product_modal) {

            if (count($product->getMediaGalleryImages()) > 0) {

              //  $yml .= "<product_images>\n";
                foreach ($product->getMediaGalleryImages() as $_image) {

                    $file_img = $_image->getFile();//$_image->getData('file');
                    $img = $siteUrl . 'media/catalog/product';
                    $img .= $file_img;


                    $_img = Mage::helper('catalog/image')->init($product, 'thumbnail', $_image->getFile());//$file_img);


                    $mv_w_big = 600;

                    $_img_big = (string)Mage::helper('catalog/image')->init($product, 'image', $file_img)->resize($mv_w_big,500);//, $mv_h_big);

                    $yml .= "<picture>" .  $img . "</picture>\n";
                    //echo  $_img."\r\n".$_img_big;

                    //die();
                }
               // $yml .= "</product_images>\n";
            }




           /* foreach ($_product->getMediaGalleryImages() as $_image) {
                $file = $_image->getData('file');
                $img = $siteUrl . 'media/catalog/product';
                $img .= $file;
               // $yml .= "<picture>" . $img . "</picture>\n";
                $_img = Mage::helper('catalog/image')->init($product, 'thumbnail', $file);

                $mv_w_big = 600;
                $ratio_big = $mv_w_big / $_img->getOriginalWidth();
                $mv_h_big = $ratio_big * $_img->getOriginalHeight();


                $_img_big = (string)Mage::helper('catalog/image')->init($product, 'thumbnail', $file)->resize($mv_w_big, $mv_h_big);


                //$yml .= "<picture>" . $_img_big . "</picture>\n";
            }*/
       // }

        //<stock_quantity>100</stock_quantity>
        $vendorname = false;
        if ($product->getProizvoditel() !== false) {
            $vendorname =$product->getAttributeText('proizvoditel');// $product->getProizvoditel();
        }

        if ($vendorname)
            $yml .= "<vendor>" . $vendorname . "</vendor>\n";

        $entityTypeId = Mage::getResourceModel('catalog/product')->getTypeId();
        $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter($entityTypeId);

        $attributes = $product->getAttributes();
        $attributeSetId = $product->getAttributeSetId();


        $skus[]=$product->getSku();
        //echo $product->getName() . ' ' . $product->getId() . " ". $product->getSku()."\r\n";// . ' include_in_feed_rozetka' . $product->getData('include_in_feed_rozetka') . "\r\n";
        /*echo  '<pre>data';
        print_r($product->getData());
        echo '</pre>';*/

/*        echo '<pre>$attributes';
        print_r($attributes);
        echo '</pre>';
        die();*/
        foreach ($attributes as $attribute) {




            //echo $attribute->getAttributeGroupId() . ' ' . $attribute->getAttributeSetId() . ' ' . $attributeSetId . "\r\n";
$data_attr=$attribute->getData();
            /*echo '<pre>';
            print_r($attribute->getData());
            echo '</pre>';
*/
            $attr_group=$data_attr['attribute_set_info'];
            $attribute_group_id=$attribute->getAttributeGroupId();
            foreach ($attr_group as $att)
            {
                $attribute_group_id=$att['group_id'];
            }
            //echo  '$attribute_group_id'.$attribute_group_id."\r\n";
            if ($attribute_group_id!='') {
                $_query = "SELECT * FROM eav_attribute_group WHERE attribute_group_name='Особые характеристики'
AND attribute_group_id='" . $attribute_group_id. "' LIMIT 1";
                //echo $_query . "\r\n\r\n";
                //die();
                $results_attr_gr = $readConnection->fetchAll($_query);
                if (count($results_attr_gr) > 0) {
                    /*echo  $product->getName() . ' ' . $product->getId().'<pre>$results_attr_gr '.$attribute->getAttributeSetId().' '.$attributeSetId;
                    print_r($results_attr_gr);
                    echo '</pre>';*/
                    //if ($attribute->getAttributeSetId() == $attributeSetId) {
                    $attributeLabel = $attribute->getFrontendLabel();
                    $value = $attribute->getFrontend()->getValue($product);
                    // echo $product->getName() . ' ' . $product->getId().' '.$attributeLabel.' value='.$value."\r\n";
                    //echo '$attributeLabel'.$attributeLabel.' '.$value."\r\n";
                    if (($attributeLabel != '') && ($value != 'No') && ($value != '')) {//($attributeLabel!='Гарантия')&&
                        //echo $attributeLabel . '-' . $value;
                        //echo "<br />";//' . $label . '-
                        $yml .= "<param name=\"$attributeLabel\">" . $value . "</param>\n";
                        //die();
                    }
                    //}
                }
            }
        }

       //die();


        $yml .= "<param name=\"Доставка/Оплата\">Доставка: Самовывоз из магазина, Самовывоз из Новой Почты 2-5 дней, Самовывоз из отделений Интайм 3- 6 дней, Курьер по вашему адресу г.Киев 1-2 дня. Оплата: Предоплата, Наличными, Visa/MasterCard, Безналичным с НДС</param>\n";
        //$yml .= "<param name=\"Гарантия\">Гарантия 36 месяцев обмен возврат в течении 14 дней</param>\n";

        //die();


        /* <offer id="19305" available="true">
  <url>http://abc.ua/catalog/muzhskaya_odezhda/kurtki/kurtkabx.html</url>
  <price>4499</price>
  <currencyId>UAH</currencyId>
  <categoryId>391</categoryId>
  <picture>http://abc.ua/upload/iblock/a53/a5391cddb40be91705.jpg</picture>
  <picture>http://abc.ua/upload/iblock/9d0/9d06805d219fb525fc.jpg</picture>
  <picture>http://abc.ua/upload/iblock/93d/93de38537e1cc1f8f2.jpg</picture>
  <vendor>Abc clothes</vendor>
  <stock_quantity>100</stock_quantity>
  <name>Куртка Abc clothes Scoperandom-HH XL Черная (1323280942900)</name>
  <description><![CDATA[<p>Одежда<b>Abc clothes</b> способствует развитию функций головного мозга за счет поощрения мелкой моторики.</p><p>В Abc <b>New Collection</b> будет особенно удобно лазать, прыгать, бегать.</p><p>За счет своей универсальноcти и многофункциональности, <b>Abc clothes</b> отлично подходит:</p><ul><li><b>Для весны</b></li><li><b>Для лета</b></li><li><b>Для ранней осени</b> </li></ul><br><p><b>Состав:</b><br>• 92% полиэстер, 8% эластан, нетоксичность подтверждена лабораторно.</p><p><b>Вес:</b> 305 г</p>]]></description>
  <param name="Вид">Куртка</param>
  <param name="Размер">XL</param>
  <param name="Сезон">Весна-Осень</param>
  <param name="Категория">Мужская</param>
  <param name="Цвет">Черный</param>
  <param name="Длина">Средней длины</param>
  <param name="Стиль">Повседневный (casual)</param>
  <param name="Особенности">Модель с капюшоном</param>
  <param name="Состав">92% полиэстер, 8% эластан</param>
  <param name="Артикул">58265468</param>
  </offer>

         */


        $yml .= "		</offer>\n";
        //}
    }

    $yml .= "		</offers>\n";
    $yml .= "	</shop>\n";
    $yml .= "</yml_catalog>\n";

    /*
    <name>
code
stock
priceRUAH
guarantee
image*/


    /*echo '<pre>';
    print_r($skus);
    echo '</pre>';
    */$handle = fopen('/var/www/ukrsnab.com.ua/htdocs/'.$file, "w+");

    fwrite($handle, header('Content-Type: text/xml; charset=utf8'));
    fwrite($handle, $yml);
    fclose($handle);
}


$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$attributeSetId = 9;
//->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)); //STATUS_DISABLED
$resource = Mage::getSingleton('core/resource');

/**
 * Установка соединения для чтения
 */
$readConnection = $resource->getConnection('core_read');



/*
$query = "SELECT * FROM eav_attribute_set WHERE entity_type_id='10'";
$results_eav_attribute_set = $readConnection->fetchAll($query);
foreach ($results_eav_attribute_set as $d_eav_attribute_set) {
    $attributeSetId=$d_eav_attribute_set['attribute_set_id'];
    $installer->addAttributeToSet(10, $attributeSetId, 'General', 'include_in_feed_rozetka', 10);
    $installer->endSetup();
}*/

/*
 * $installer->addAttribute('catalog_product', 'include_in_feed_rozetka', array(
    'group' => 'General',
    'label' => 'Добавит в xml для розетки',
    'type' => 'int',
    'input' => 'checkbox',
    'required' => false,
    'visible_on_front' => false,
    'filterable' => 0,
    'searchable' => 0,
    'comparable' => 0,
    'user_defined' => 1,
    'is_configurable' => 0,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note' => '',
));

$installer->addAttribute('catalog_category', 'include_in_feed_rozetka', array(
    'group' => 'General',
    'label' => 'Добавит в xml для розетки',
    'type' => 'int',
    'input' => 'checkbox',
    'required' => false,
    'backend'       => '',
    'visible'       => 1,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,

));*/


generate_feed('feed_products.xml');