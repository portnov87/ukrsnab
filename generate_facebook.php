<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 23.07.2018
 * Time: 17:39
 */

date_default_timezone_set('Europe/Kiev');
require_once('app/Mage.php');
umask(0);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
ini_set('memory_limit', '2048M');

ob_implicit_flush(1);

echo str_pad('', 1024);
@ob_flush();
flush();


$date = date('Y-m-d');
$maps=[];

$uploadfile = 'sitemap_ukrsnab_1.csv';


$csv= "id,title,description,availability,condition,price,link,image_link,brand,additional_image_link,
age_group,color,gender,item_group_id,google_product_category,material,pattern,
product_type,sale_price,sale_price_effective_date,shipping,shipping_weight,size,custom_label_0,custom_label_1,
custom_label_2,custom_label_3,custom_label_4";


//FB_product_1234,Facebook T-Shirt (Unisex),A vibrant crewneck for all shapes and sizes. Made from 100% cotton.,in stock,new,9.99 USD,https://www.facebook.com/facebook_t_shirt,https://www.facebook.com/t_shirt_image_001.jpg,Facebook,https://www.facebook.com/t_shirt_image_002.jpg,adult,blue,unisex,FB1234_shirts,Apparel & Accessories > Clothing > Shirts,cotton,stripes1,Apparel & Accessories > Clothing > Shirts,4.99 USD,2017-12-01T0:00-23:59/2017-12-31T0:00-23:59,US:CA:Ground:9.99 USD,0.3 lb,small,,,,,



$collection_products = Mage::getModel('catalog/product')->getCollection()
    ->addUrlRewrite()
    ->addFieldToFIlter('status',1);

    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');



    foreach ($collection_products as $product)
    {
        $product_id=$product->getId();
        $title=$product->getName();
        $description=strip_tags($product->getShortDescription());
        $condition=$product->getCondition();
        $price=$product->getPrice();
        //$link=$product->getPrice();


        $query = "SELECT * FROM core_url_rewrite WHERE product_id='" . $product_id . "' AND category_id IS NULL";
        $results = $readConnection->fetchAll($query);
        $url = $product->getProductUrl();
        foreach ($results as $res) {
            $url = $url_domen . '/' . $res['request_path'];
        }

        $image=Mage::getBaseUrl('media') . 'catalog/product' . $product->getImage();
        $additional_image_link='';
        $csv.="$product_id,$title,$description,in stock,,$price UAH,$url,$image,$brand,$additional_image_link";
    }


    /*
    $size = count($collection_products);
    if ($size>10000)
        $count_maps = ceil($size / 10000);
    else $count_maps=1;
    $maps_products = [];
    for ($i = 1; $i <= $count_maps; $i++) {
        $limit = 10000;
        $offset = $i;

        $collection_products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addUrlRewrite()
            ->setPageSize($limit)
            ->setCurPage($offset);
        $new_sitemap = '';
        $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach ($collection_products as $product) {

            $product_id = $product->getId();
            $query = "SELECT * FROM core_url_rewrite WHERE product_id='" . $product_id . "' AND category_id IS NULL";
            $results = $readConnection->fetchAll($query);
            $url = $product->getProductUrl();
            foreach ($results as $res) {
                $url = $url_domen . '/' . $res['request_path'];
            }

            // $url = $product->getProductUrl();//UrlProduct();
            // echo 'url'.$url;die();
            $date = date('Y-m-d');
            $new_sitemap .= '<url>
                        <loc>' . $url . '</loc>
                        <lastmod>' . $date . '</lastmod>
                        <changefreq>weekly</changefreq>
                       </url>';
        }
        $new_sitemap .= '</urlset>';
        $name = 'sitemap_product_' . $i;
        if (save_file($new_sitemap, $name)) {
            $maps_products[] = $name;
        }
        @ob_flush();
        flush();
    }
*/





if (file_exists($uploadfile)) {
    $delimeter = ',';
    $handle_redirects = fopen($uploadfile, "r");

    $new_sitemap = '';
    $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    while (($csv = fgetcsv($handle_redirects, 0, $delimeter)) !== false) {
        $rows++;

            $url = $csv[0];

                 $new_sitemap .= '<url>
                        <loc>' . $url . '</loc>
                        <lastmod>' . $date . '</lastmod>
                        <changefreq>weekly</changefreq>
                       </url>';




    }

    $new_sitemap .= '</urlset>';
}


$name = 'sitemap_1';
if (save_file($new_sitemap, $name)) {
    $maps[] = $name;
}




$uploadfile = 'sitemap_ukrsnab_2.csv';
if (file_exists($uploadfile)) {
    $delimeter = ',';
    $handle_redirects = fopen($uploadfile, "r");

    $new_sitemap = '';
    $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    while (($csv = fgetcsv($handle_redirects, 0, $delimeter)) !== false) {
        $rows++;

            $url = $csv[0];

            $new_sitemap .= '<url>
                        <loc>' . $url . '</loc>
                        <lastmod>' . $date . '</lastmod>
                        <changefreq>weekly</changefreq>
                       </url>';




    }

    $new_sitemap .= '</urlset>';
}


$name = 'sitemap_2';
if (save_file($new_sitemap, $name)) {
    $maps[] = $name;
}



$uploadfile = 'sitemap_ukrsnab_3.csv';
if (file_exists($uploadfile)) {
    $delimeter = ',';
    $handle_redirects = fopen($uploadfile, "r");

    $new_sitemap = '';
    $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    while (($csv = fgetcsv($handle_redirects, 0, $delimeter)) !== false) {
        $rows++;

            $url = $csv[0];

            $new_sitemap .= '<url>
                        <loc>' . $url . '</loc>
                        <lastmod>' . $date . '</lastmod>
                        <changefreq>weekly</changefreq>
                       </url>';




    }

    $new_sitemap .= '</urlset>';
}


$name = 'sitemap_3';
if (save_file($new_sitemap, $name)) {
    $maps[] = $name;
}




$uploadfile = 'sitemap_ukrsnab_4.csv';
if (file_exists($uploadfile)) {
    $delimeter = ',';
    $handle_redirects = fopen($uploadfile, "r");

    $new_sitemap = '';
    $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    while (($csv = fgetcsv($handle_redirects, 0, $delimeter)) !== false) {
        $rows++;
        //if ($rows > 1) {
            $url = $csv[0];

            $new_sitemap .= '<url>
                        <loc>' . $url . '</loc>
                        <lastmod>' . $date . '</lastmod>
                        <changefreq>weekly</changefreq>
                       </url>';


//        }

    }

    $new_sitemap .= '</urlset>';
}


$name = 'sitemap_4';
if (save_file($new_sitemap, $name)) {
    $maps[] = $name;
}


$url_domen = 'https://ukrsnab.com.ua';
$sitemap = '<?xml version="1.0" encoding="UTF-8"?>
        <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
foreach ($maps as $map) {
    $date = date('Y-m-d');
    $sitemap .= ' <sitemap>
            <loc>' . $url_domen . '/sitemaps/' . $map . '.xml</loc>
            <lastmod>' . $date . '</lastmod>
           </sitemap>';
}
$sitemap .= '</sitemapindex>';

save_file($sitemap, 'sitemap');




die();
/*

//->getCollection()->addUrlRewriteToResult();
$new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
$new_sitemap .= categories(3);
$new_sitemap .= '</urlset>';

save_file($new_sitemap, 'category_sitemap');
die();*/
$array_files_sitemaps = [
    'static_sitemap' => 'static_sitemap.xml',
    'category_sitemap' => 'category_sitemap.xml',
    'filters_sitemap' => 'filters_sitemap.xml',
    'product_sitemap' => 'product_sitemap.xml',
];

$url_domen = 'https://ukrsnab.com.ua';
$sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
foreach ($array_files_sitemaps as $key => $map) {

    $date = date('Y-m-d');
    if ($generate_sotemap = generate_map($key)) {
        $sitemap .= ' <sitemap>
  	<loc>' . $url_domen . '/sitemaps/' . $key . '.xml</loc>
  	<lastmod>' . $date . '</lastmod>
   </sitemap>';
    }
}
$sitemap .= '</sitemapindex>';

if (save_file($sitemap, 'sitemap')) {
    return true;
}

function generate_map($key)
{
    global $url_domen;
    $date = date('Y-m-d');
    switch ($key) {
        case 'static_sitemap':
            $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            $url = $url_domen . '/';
            $new_sitemap .= '<url>
                        <loc>' . $url . '</loc>
                        <lastmod>' . $date . '</lastmod>
                        <changefreq>always</changefreq>
                       </url>';

            $collection = Mage::getModel('cms/page')->getCollection();//->addStoreFilter(Mage::app()->getStore()->getId());
            $collection->getSelect()->where('is_active = 1');
            foreach ($collection as $page):
                $PageData = $page->getData();

                if ($PageData['identifier'] != 'galaelectronues_home' && $PageData['identifier'] != 'mobile_home' && $PageData['identifier'] != 'no-route' && $PageData['identifier'] != 'enable-cookies' && $PageData['identifier'] != 'home2') {

                    $url = $url_domen . '/' . $PageData["identifier"];//$page->getUrl($PageData['identifier']);
                    $new_sitemap .= '<url>
                        <loc>' . $url . '</loc>
                        <lastmod>' . $date . '</lastmod>
                        <changefreq>daily</changefreq>
                       </url>';

                }
            endforeach;


            /*
            foreach ($categories as $cat) {
                if ($cat->getStatus() == 1) {
                    $url = $cat->getCategoryUrl();
                    $date = date('Y-m-d');
                    $new_sitemap .= '<url>
                        <loc>' . $url . '</loc>
                        <lastmod>' . $date . '</lastmod>
                        <changefreq>weekly</changefreq>
                       </url>';
                }
            }*/
            $new_sitemap .= '</urlset>';

            save_file($new_sitemap, $key);
            break;
        case 'category_sitemap':
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');

            //->getCollection()->addUrlRewriteToResult();
            $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            $new_sitemap .= categories(2);
            $new_sitemap .= '</urlset>';

            save_file($new_sitemap, $key);

            break;
        case 'filters_sitemap':

            $meta=Mage::getModel('seo/meta')->getCollection()
                ->addFieldToFilter('robots', array('eq' => 2))
                ->addFieldToFilter('is_active', 1);


                $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';


            foreach ($meta as $filter) {

                $url = $url_domen.$filter->getUrl();
                $date = date('Y-m-d');
                $new_sitemap .= '<url>
                        <loc>' . $url . '</loc>
                        <lastmod>' . $date . '</lastmod>
                        <changefreq>weekly</changefreq>
                       </url>';

            }
            $new_sitemap .= '</urlset>';

            save_file($new_sitemap, $key);
            break;
        case 'product_sitemap':
            productsmaps('product_sitemap');

            break;
    }
    return true;
}

function categories($category_id, $new_sitemap = '')
{
    global $url_domen;
    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');


    $query = "SELECT catalog_category_entity.entity_id FROM catalog_category_entity LEFT JOIN
catalog_category_entity_int ON (catalog_category_entity.entity_id = catalog_category_entity_int.entity_id AND catalog_category_entity_int.attribute_id=44 ) 
WHERE catalog_category_entity_int.value=1";

    $results = $readConnection->fetchAll($query);

    foreach ($results as $res){
        $category_id=$res['entity_id'];
        $query_url = "SELECT * FROM core_url_rewrite WHERE category_id='" . $category_id . "' AND product_id IS NULL";//$cat->getId()

        $results_url = $readConnection->fetchAll($query_url);
        $url = '';
        foreach ($results_url as $_res) {
            $url = $url_domen . '/' . $_res['request_path'];
        }

        if ($url != '') {

            if (
                (strpos($url,'-v-dnepre-dnepropetrovske')===false)
                &&(strpos($url,'-v-uzhgorode')===false)
                &&(strpos($url,'-v-vinnice')===false)
                &&(strpos($url,'-v-chernigove')===false)
                &&(strpos($url,'-v-zhitomire')===false)
                &&(strpos($url,'-v-sumah')===false)
                &&(strpos($url,'-v-poltave')===false)
                &&(strpos($url,'-v-har-kove')===false)
                &&(strpos($url,'-v-zaporozh-e')===false)
                &&(strpos($url,'-v-hersone')===false)
                &&(strpos($url,'-v-nikolaeve')===false)
                &&(strpos($url,'-v-kirovograde')===false)
                &&(strpos($url,'-v-cherkassah')===false)
                &&(strpos($url,'-v-hmel-nickom')===false)
                &&(strpos($url,'-v-ternopole')===false)
                &&(strpos($url,'-v-rovno')===false)
                &&(strpos($url,'-v-lucke')===false)
                &&(strpos($url,'-vo-l-vove')===false)
                &&(strpos($url,'-v-chernovcah')===false)
                &&(strpos($url,'-v-ivano-frankovske')===false)
                &&(strpos($url,'-v-odesse')===false)
            ) {

                $date = date('Y-m-d');
                $new_sitemap .= '<url>
                        <loc>' . $url . '</loc>
                        <lastmod>' . $date . '</lastmod>
                        <changefreq>weekly</changefreq>
                       </url>';
            }

        }

    }
    return $new_sitemap;
}

function productsmaps($name_map)
{
    global $url_domen;
    $collection_products = Mage::getModel('catalog/product')->getCollection();

    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');

    $size = count($collection_products);
    if ($size>10000)
    $count_maps = ceil($size / 10000);
    else $count_maps=1;
    $maps_products = [];
    for ($i = 1; $i <= $count_maps; $i++) {
        $limit = 10000;
        $offset = $i;

        $collection_products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addUrlRewrite()
            ->setPageSize($limit)
            ->setCurPage($offset);
        $new_sitemap = '';
        $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach ($collection_products as $product) {

            $product_id = $product->getId();
            $query = "SELECT * FROM core_url_rewrite WHERE product_id='" . $product_id . "' AND category_id IS NULL";
            $results = $readConnection->fetchAll($query);
            $url = $product->getProductUrl();
            foreach ($results as $res) {
                $url = $url_domen . '/' . $res['request_path'];
            }

            // $url = $product->getProductUrl();//UrlProduct();
            // echo 'url'.$url;die();
            $date = date('Y-m-d');
            $new_sitemap .= '<url>
                        <loc>' . $url . '</loc>
                        <lastmod>' . $date . '</lastmod>
                        <changefreq>weekly</changefreq>
                       </url>';
        }
        $new_sitemap .= '</urlset>';
        $name = 'sitemap_product_' . $i;
        if (save_file($new_sitemap, $name)) {
            $maps_products[] = $name;
        }
        @ob_flush();
        flush();
    }

    $sitemap_product_common = '<?xml version="1.0" encoding="UTF-8"?>
        <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    foreach ($maps_products as $map) {
        $date = date('Y-m-d');
        $sitemap_product_common .= ' <sitemap>
            <loc>' . $url_domen . '/sitemaps/' . $map . '.xml</loc>
            <lastmod>' . $date . '</lastmod>
           </sitemap>';
    }
    $sitemap_product_common .= '</sitemapindex>';
    if (save_file($sitemap_product_common, $name_map)) {
        return true;
    }

    return false;
}

function save_file($data, $name)
{
    global $url_domen;

    $file = $name . '.xml';
    if ($name != 'sitemap') {
        if (!file_exists('sitemaps/')) {
            umask(0);
            mkdir('sitemaps', 775);
        }
        $file = 'sitemaps/' . $file;

    }
    $handle = fopen('/home/ukrsnab_u/web/www/ukrsnab.com.ua/'.$file, "w+");

    fwrite($handle, header('Content-Type: text/html; charset=utf8'));
    if (fwrite($handle, $data)) {
        fclose($handle);
        return true;
    }
    fclose($handle);
    return false;

}
/*
   <sitemap>
  	<loc>http://optomarket.com.ua/static_sitemap.xml</loc>
  	<lastmod>(Дата последнего изменения файла в формате ГГГГ-ММ-ДД)</lastmod>
   </sitemap>
   <sitemap>
  	<loc>http://optomarket.com.ua/category_sitemap.xml</loc>
  	<lastmod>(Дата последнего изменения файла в формате ГГГГ-ММ-ДД)</lastmod>
  </sitemap>
   <sitemap>
  	<loc>http://optomarket.com.ua/filters_sitemap.xml</loc>
  	<lastmod>(Дата последнего изменения файла в формате ГГГГ-ММ-ДД)</lastmod>
   </sitemap>
 <sitemap>
   </sitemap>
   <sitemap>
  	<loc>http://optomarket.com.ua/product_sitemap.xml</loc>
  	<lastmod>(Дата последнего изменения файла в формате ГГГГ-ММ-ДД)</lastmod>
   </sitemap>
   </sitemapindex>';*/