<?php
/*------------------------------------------------------------------------------
  
  Import/Export from E-Trade soft products for Magento 1.3-1.9
  Version 1.91r
  
  based on: ElbuzGroup Solutions & E-Trade CSV file format (http://forum.elbuz.com/viewtopic.php?f=5&t=249)

  Copyright (c) 2006-2015 Hasegawa Makoto
  http://www.elbuz.com

  Released under the GNU General Public License
------------------------------------------------------------------------------*/
header('Content-Type: text/html; charset=utf-8');

////////////////
$login = "admin";
$password = "gU9gKwrG1sA";

// прописывать для новых товаров в поле SKU числовой ИД товара из ПЛИ
$save_product_id_for_sku=0; // 1 - да, 0 - нет
// артукул производителя из ПЛИ прописывать для определённого атрибута
$My_SKU_attribute_code=''; // символьный код атрибута, например 'article'
// флаг обновления наименования товара из ПЛИ
$product_name_update=0; // 1 - да, 0 - нет
// флаг обновления краткого описания товара из ПЛИ
$product_brief_desc_update=0; // 1 - да, 0 - нет
// флаг обновления полного описания товара из ПЛИ
$product_full_desc_update=0; // 1 - да, 0 - нет
// флаг обновления ЧПУ (SEO URL) ссылок на товары
$product_seo_url_update=0; // 1 - да, 0 - нет
// флаг обновления ЧПУ (SEO URL) ссылок на категории
$cat_seo_url_update=0; // 1 - да, 0 - нет
// флаг обновления артикула товара (SKU) из ПЛИ
$product_sku_update=0; // 1 - да, 0 - нет
////////////////

//error_reporting(E_ALL);
//ini_set('display_errors', 1);
error_reporting(E_ERROR | E_WARNING); // E_ERROR | E_WARNING | E_PARSE | E_NOTICE
ini_set('display_errors', 1);

function auth_send(){
    header('WWW-Authenticate: Basic realm="Closed Zone"');
    header('HTTP/1.0 401 Unauthorized');
    echo "<html><body bgcolor=white link=blue vlink=blue alink=red>"
    ,"<h1>Ошибка аутентификации!</h1>"
    ,"</body></html>";
    exit;
};

if (!isset($_SERVER['PHP_AUTH_USER'])) {
	auth_send();
} else {
	$auth_user = $_SERVER['PHP_AUTH_USER'];
	$auth_pass = $_SERVER['PHP_AUTH_PW'];
	
	if (($auth_user != $login) || ($auth_pass != $password)) {
		auth_send();
	};
};
////////////////

$ini_get_locale=setlocale(LC_ALL, array('ru_RU.CP1251', 'ru_RU.cp1251', 'ru_RU.UTF8', 'ru_RU.utf8', 'ru_ru.CP1251', 'ru_ru.cp1251', 'ru_ru.UTF8', 'ru_ru.utf8', 'ru_RU', 'ru_ru', 'Russian'));

// очистка данных каталога
// DELETE FROM `catalog_category_entity` WHERE `entity_id`>2
// TRUNCATE TABLE `catalog_product_entity` 
// TRUNCATE TABLE `catalogsearch_fulltext` 

ini_set("memory_limit", "1024M");
ini_set("post_max_size", "128M");
ini_set("upload_max_filesize", "128M");
ini_set("max_execution_time", "6000");
ini_set("max_input_time", "6000");
ini_set('auto_detect_line_endings', '1');
ini_set('magic_quotes_gpc', '0');
ini_set('magic_quotes_runtime', '0');
ini_set('magic_quotes_sybase', '0');

// чтение конфигурационного файла
$config_file = file_get_contents(dirname(__FILE__) . '/app/etc/local.xml');
$p = xml_parser_create();
xml_parse_into_struct($p, $config_file, $vals, $index);
xml_parser_free($p);

foreach($index['ACTIVE'] as $k => $ind) {
	if (intval($vals[$ind]['value']) == 1) {
		define('DB_SERVER', $vals[ $index['HOST'][$k] ]['value']);
		define('DB_SERVER_USERNAME', $vals[ $index['USERNAME'][$k] ]['value']);
		define('DB_SERVER_PASSWORD', $vals[ $index['PASSWORD'][$k] ]['value']);
		define('DB_DATABASE', $vals[ $index['DBNAME'][$k] ]['value']);
		define('TABLE_PREFIX', $vals[ $index['TABLE_PREFIX'][$k] ]['value']);
		break;
	}
}

/*
define('DB_SERVER', 'localhost');
define('DB_SERVER_USERNAME', 'root');
define('DB_SERVER_PASSWORD', '');
define('DB_DATABASE', 'dbdbdb');
define('TABLE_PREFIX', 'sm_');
*/

define("DB_CHARSET", "utf8"); //cp1251 or utf8

// Определяем названия таблиц
$m_core_url_rewrite=TABLE_PREFIX."core_url_rewrite";
$m_stock=TABLE_PREFIX."cataloginventory_stock_status";

$m_attribute=TABLE_PREFIX."eav_attribute";
$m_attribute_set=TABLE_PREFIX."eav_attribute_set";
$m_attribute_group=TABLE_PREFIX."eav_attribute_group";
$m_eav_entity_type=TABLE_PREFIX."eav_entity_type";
$m_entity_attribute=TABLE_PREFIX."eav_entity_attribute";
$m_catalog_attribute=TABLE_PREFIX."catalog_eav_attribute";

$m_attribute_option=TABLE_PREFIX."eav_attribute_option";
$m_attribute_option_value=TABLE_PREFIX."eav_attribute_option_value";

$m_category=TABLE_PREFIX."catalog_category_entity";
$m_category_int=TABLE_PREFIX."catalog_category_entity_int";
$m_category_text=TABLE_PREFIX."catalog_category_entity_text";
$m_category_varchar=TABLE_PREFIX."catalog_category_entity_varchar";
$m_category_product=TABLE_PREFIX."catalog_category_product";

$m_product=TABLE_PREFIX."catalog_product_entity";
$m_product_int=TABLE_PREFIX."catalog_product_entity_int";
$m_product_text=TABLE_PREFIX."catalog_product_entity_text";
$m_product_varchar=TABLE_PREFIX."catalog_product_entity_varchar";
$m_product_decimal=TABLE_PREFIX."catalog_product_entity_decimal";
$m_product_price=TABLE_PREFIX."catalog_product_index_price";
$m_product_website=TABLE_PREFIX."catalog_product_website";
$m_product_inventory_stock_status=TABLE_PREFIX."cataloginventory_stock_status";
$m_product_search_fulltext=TABLE_PREFIX."catalogsearch_fulltext";
$m_product_category_product_index=TABLE_PREFIX."catalog_category_product_index";
$m_product_inventory_stock_item=TABLE_PREFIX."cataloginventory_stock_item";
$m_product_catalog_flat_1=TABLE_PREFIX."catalog_product_flat_1";
$m_product_media_gallery=TABLE_PREFIX."catalog_product_entity_media_gallery";
$m_product_media_gallery_value=TABLE_PREFIX."catalog_product_entity_media_gallery_value";
$m_product_index_eav=TABLE_PREFIX."catalog_product_index_eav";

$manufacturer_attribute_code='manufacturer'; // символьный идентификатор атрибута "Производитель"

// директории для храниения картинок товаров
// эти директории должны быть созданы вручную, например по ftp
// ./media/catalog/product/big_img/

$ProductsBigImagePath='/big_img/';

$unpackzipdir='./media/tmp/';
if (is_dir($unpackzipdir)==false) { // // Create temp dir
	mkdir($unpackzipdir, 0777, true);
}

$unpackzipdir='./tmp/';
$unpack_zip_log=0;
$csv_delimiter_char="\t"; // TAB

// Подключаемся к БД
echo DB_SERVER.' '. DB_SERVER_USERNAME.' '. DB_SERVER_PASSWORD.' '.DB_DATABASE."<br/>";
$link = @mysqli_connect(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD) or @die("no connect to db !");
mysqli_select_db(DB_DATABASE, $link) or @die("select db error !");

mysql_query("set names '".DB_CHARSET."'") or die("Invalid query: " . mysqli_error());
mysql_query("set character_set_client='".DB_CHARSET."'") or die("Invalid query: " . mysqli_error());
mysql_query("set character_set_results='".DB_CHARSET."'") or die("Invalid query: " . mysqli_error());

//mysql_query("SET innodb_lock_wait_timeout = 120");	

// Настройка параметров движка
$store_id='0'; // ID вида магазина (Store View) for root admin access
$store_id_default='1'; // ID вида магазина (Store View) for default store
$website_id='1'; // ID вэб сайта (Website)
$website_id_default='1'; // ID вэб сайта (Website)
$stock_id='1'; // ID склада

// ID вида магазина (Store View) && ID вэб сайта (Website) для admin
$parameters_query = mysql_query("SELECT store_id, code, website_id, group_id, name FROM ".TABLE_PREFIX."core_store WHERE code='admin'", $link) or die("Invalid query: " . mysql_error());
if ($my_row = mysql_fetch_array($parameters_query)) {
	$store_id=$my_row['store_id'];
	//$website_id=$my_row['website_id'];
}

// ID вида магазина (Store View) && ID вэб сайта (Website) для default
$parameters_query = mysql_query("SELECT store_id, code, website_id, group_id, name FROM ".TABLE_PREFIX."core_store WHERE code='default'", $link) or die("Invalid query: " . mysql_error());
if ($my_row = mysql_fetch_array($parameters_query)) {
	$store_id_default=$my_row['store_id'];
	//$website_id_default=$my_row['website_id'];
}



// id сущности "категории товаров"
$parameters_query = mysql_query("SELECT entity_type_id FROM ".$m_eav_entity_type." WHERE entity_model='catalog/category'", $link) or die("Invalid query: " . mysql_error());
if ($my_row = mysql_fetch_array($parameters_query)) {
	$entity_cat_type_id=$my_row['entity_type_id'];
} else {
	$entity_cat_type_id=3; // по умолчанию = 3
}

// id сущности "товары"
$parameters_query = mysql_query("SELECT entity_type_id FROM ".$m_eav_entity_type." WHERE entity_model='catalog/product'", $link) or die("Invalid query: " . mysql_error());
if ($my_row = mysql_fetch_array($parameters_query)) {
	$entity_product_type_id=$my_row['entity_type_id'];
} else {
	$entity_product_type_id=4; // по умолчанию = 4
}

// id группы атрибутов "категории"
$parameters_query = mysql_query("SELECT attribute_set_id FROM ".$m_attribute_set." WHERE entity_type_id='".$entity_cat_type_id."' AND attribute_set_name='Default'", $link) or die("Invalid query: " . mysql_error());
if ($my_row = mysql_fetch_array($parameters_query)) {
	$attribute_cat_set_id=$my_row['attribute_set_id'];
} else {
	$attribute_cat_set_id=3; // по умолчанию = 3
}

// id группы атрибутов "товары"
$parameters_query = mysql_query("SELECT attribute_set_id FROM ".$m_attribute_set." WHERE entity_type_id='".$entity_product_type_id."' AND attribute_set_name='Default'", $link) or die("Invalid query: " . mysql_error());
if ($my_row = mysql_fetch_array($parameters_query)) {
	$attribute_product_set_id=$my_row['attribute_set_id'];
} else {
	$attribute_product_set_id=4; // по умолчанию = 4
}

// 26.09.2010 Импорт категорий и товаров
if (isset($_POST['action']) && $_POST['action']=='import_products_and_cats') {
  if (is_uploaded_file($_FILES['csv_file']['tmp_name'])) {

    switch ($_POST['arc']) {
	    case 'csv': 
			$import_file_name=$_FILES['csv_file']['tmp_name'];
			break;
	    case 'zip': 
			$import_file_name=unpackZip($unpackzipdir, $_FILES['csv_file']['tmp_name'], $unpack_zip_log);
			$import_file_name=$unpackzipdir.$import_file_name;
			break;
	    case 'gzip': 
			$import_file_name=UnpackGZip($unpackzipdir, $_FILES['csv_file']['tmp_name'], $unpack_zip_log);
			$import_file_name=$unpackzipdir.$import_file_name;
			break;
	}

	ini_set('auto_detect_line_endings', '1');
	//@setlocale(LC_ALL, 'ru_RU.CP1251');
	
	if(!$handle = fopen($import_file_name, "r"))
	{
	    die('Could not open file. Canceled...');
	}
	
	$row = 0;
    $count_cat_add = 0;
	$count_cat_upd = 0;
	$count_products_add = 0;
	$count_products_upd = 0;
	$count_mans_add = 0;

	// Узнаём ID атрибутов для категорий товаров
	$cat_active_attribute_id=0;
	$cat_include_in_menu_attribute_id=0;
	$cat_landing_page_attribute_id=0;
	$cat_is_anchor_attribute_id=0;
	$cat_custom_use_parent_settings_attribute_id=0;
	$cat_filter_price_range_attribute_id=0;
	$cat_custom_apply_to_products_attribute_id=0;
	$cat_custom_design_apply_attribute_id=0;
	$cat_description_attribute_id=0;
	$cat_meta_keywords_attribute_id=0;
	$cat_meta_description_attribute_id=0;
	$cat_custom_layout_update_attribute_id=0;
	$cat_available_sort_by_attribute_id=0;
	$cat_url_key_attribute_id=0;
	$cat_name_attribute_id=0;
	$cat_meta_title_attribute_id=0;
	$cat_display_mode_attribute_id=0;
	$cat_custom_design_attribute_id=0;
	$cat_page_layout_attribute_id=0;
	$cat_image_attribute_id=0;
	$cat_url_path_attribute_id=0;
	$main_cat_id=2; // Привязка всех категорий товаров к этому ID категории (ID базовой категории, Default Category)

	// root_id
	$parameters_query = mysql_query("SELECT value FROM ".TABLE_PREFIX."core_config_data WHERE path='catalog/category/root_id'", $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$main_cat_id=$my_row['value'];
	} else { // если в конфиге нет записи, тогда ищем по '1/2' в спр. категорий
		$parameters_query = mysql_query("SELECT entity_id  FROM ".$m_category." WHERE path='1/2' OR path='1/3'", $link) or die("Invalid query: " . mysql_error());
		if ($my_row = mysql_fetch_array($parameters_query)) {
			$main_cat_id=$my_row['entity_id'];
		}
	}
	
	// is_active
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='is_active' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_active_attribute_id=$my_row['attribute_id'];
	}

	// include_in_menu
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='include_in_menu' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_include_in_menu_attribute_id=$my_row['attribute_id'];
	}
	// landing_page
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='landing_page' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_landing_page_attribute_id=$my_row['attribute_id'];
	}

	// is_anchor
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='is_anchor' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_is_anchor_attribute_id=$my_row['attribute_id'];
	}
	
	// custom_use_parent_settings
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='custom_use_parent_settings' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_custom_use_parent_settings_attribute_id=$my_row['attribute_id'];
	}
	
	// filter_price_range
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='filter_price_range' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_filter_price_range_attribute_id=$my_row['attribute_id'];
	}
	
	// custom_apply_to_products
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='custom_apply_to_products' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_custom_apply_to_products_attribute_id=$my_row['attribute_id'];
	}
	
	// custom_design_apply
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='custom_design_apply' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_custom_design_apply_attribute_id=$my_row['attribute_id'];
	}
	
	// description
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='description' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_description_attribute_id=$my_row['attribute_id'];
	}
	
	// meta_keywords
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='meta_keywords' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_meta_keywords_attribute_id=$my_row['attribute_id'];
	}
	
	// meta_description
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='meta_description' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_meta_description_attribute_id=$my_row['attribute_id'];
	}
	
	// custom_layout_update
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='custom_layout_update' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_custom_layout_update_attribute_id=$my_row['attribute_id'];
	}
	
	// available_sort_by
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='available_sort_by' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_available_sort_by_attribute_id=$my_row['attribute_id'];
	}
	
	// url_key
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='url_key' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_url_key_attribute_id=$my_row['attribute_id'];
	}

	// name
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='name' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_name_attribute_id=$my_row['attribute_id'];
	}	
	
	// meta_title
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='meta_title' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_meta_title_attribute_id=$my_row['attribute_id'];
	}
	
	// display_mode
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='display_mode' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_display_mode_attribute_id=$my_row['attribute_id'];
	}
	
	// custom_design
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='custom_design' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_custom_design_attribute_id=$my_row['attribute_id'];
	}

	// page_layout
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='page_layout' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_page_layout_attribute_id=$my_row['attribute_id'];
	}
	
	// image_attribute
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='image_attribute' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_image_attribute_id=$my_row['attribute_id'];
	}

	// url_path
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='url_path' AND entity_type_id=".$entity_cat_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$cat_url_path_attribute_id=$my_row['attribute_id'];
	}
	
	//is_featured_category
	//is_featured_subcat



	// Узнаём ID атрибутов для товаров
	$product_weight_attribute_id=0;
	$product_price_attribute_id=0;
	$product_special_price_attribute_id=0;
	$product_cost_attribute_id=0;
	$product_status_attribute_id=0;
	$product_visibility_attribute_id=0;
	$product_tax_class_id_attribute_id=0;
	$product_enable_googlecheckout_attribute_id=0;
	$product_is_recurring_attribute_id=0;
	$product_description_attribute_id=0;
	$product_short_description_attribute_id=0;
	$product_InDepth_description_attribute_id=0;
	$product_meta_keyword_attribute_id=0;
	$product_custom_layout_update_attribute_id=0;
	$product_name_attribute_id=0;
	$product_model_attribute_id=0;
	$product_sku_attribute_id=0;
	$product_my_sku_attribute_id=0;
	$product_warranty_attribute_id=0;
	$product_url_key_attribute_id=0;
	$product_gift_message_available_attribute_id=0;
	$product_meta_title_attribute_id=0;
	$product_meta_description_attribute_id=0;
	$product_image_attribute_id=0;
	$product_small_image_attribute_id=0;
	$product_thumbnail_attribute_id=0;
	$product_custom_design_attribute_id=0;
	$product_page_layout_attribute_id=0;
	$product_options_container_attribute_id=0;
	$product_url_path_attribute_id=0;
	$product_small_image_label_attribute_id=0;
	$product_image_label_attribute_id=0;
	$product_manufacturer_attribute_id=0;
	
	
	// weight
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='weight' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_weight_attribute_id=$my_row['attribute_id'];
	}
	
	// price
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='price' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_price_attribute_id=$my_row['attribute_id'];
	}
	
	// special_price
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='special_price' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_special_price_attribute_id=$my_row['attribute_id'];
	}

	// cost
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='cost' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_cost_attribute_id=$my_row['attribute_id'];
	}	
	
	// status
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='status' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_status_attribute_id=$my_row['attribute_id'];
	}
	
	// visibility
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='visibility' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_visibility_attribute_id=$my_row['attribute_id'];
	}
	
	// tax_class_id
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='tax_class_id' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_tax_class_id_attribute_id=$my_row['attribute_id'];
	}
	
	// enable_googlecheckout
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='enable_googlecheckout' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_enable_googlecheckout_attribute_id=$my_row['attribute_id'];
	}
	
	// is_recurring
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='is_recurring' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_is_recurring_attribute_id=$my_row['attribute_id'];
	}
	
	// description
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='description' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_description_attribute_id=$my_row['attribute_id'];
	}
	
	// short_description
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='short_description' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_short_description_attribute_id=$my_row['attribute_id'];
	}
	
	// InDepth description
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='in_depth' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_InDepth_description_attribute_id=$my_row['attribute_id'];
	}
	
	// meta_keyword
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='meta_keyword' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_meta_keyword_attribute_id=$my_row['attribute_id'];
	}

	// custom_layout_update
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='custom_layout_update' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_custom_layout_update_attribute_id=$my_row['attribute_id'];
	}

	// name
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='name' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_name_attribute_id=$my_row['attribute_id'];
	}
	
	// model
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='model' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_model_attribute_id=$my_row['attribute_id'];
	}
	
	// sku
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='sku' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_sku_attribute_id=$my_row['attribute_id'];
	}
	
	// my sku
	if (!empty($My_SKU_attribute_code)) {
		$My_SKU_attribute_code=mysql_real_escape_string($My_SKU_attribute_code);
		
		$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='".$My_SKU_attribute_code."' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
		if ($my_row = mysql_fetch_array($parameters_query)) {
			$product_my_sku_attribute_id=$my_row['attribute_id'];
		}
	}
	
	// warranty
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='warranty' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_warranty_attribute_id=$my_row['attribute_id'];
	}
	
	// url_key
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='url_key' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_url_key_attribute_id=$my_row['attribute_id'];
	}

	// gift_message_available
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='gift_message_available' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_gift_message_available_attribute_id=$my_row['attribute_id'];
	}
	
	// meta_title
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='meta_title' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_meta_title_attribute_id=$my_row['attribute_id'];
	}
	
	// meta_description
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='meta_description' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_meta_description_attribute_id=$my_row['attribute_id'];
	}

	// image
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='image' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_image_attribute_id=$my_row['attribute_id'];
	}

	// small_image
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='small_image' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_small_image_attribute_id=$my_row['attribute_id'];
	}

	// thumbnail
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='thumbnail' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_thumbnail_attribute_id=$my_row['attribute_id'];
	}
	
	// custom_design
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='custom_design' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_custom_design_attribute_id=$my_row['attribute_id'];
	}

	// page_layout
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='page_layout' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_page_layout_attribute_id=$my_row['attribute_id'];
	}

	// options_container
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='options_container' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_options_container_attribute_id=$my_row['attribute_id'];
	}
	
	// url_path
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='url_path' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_url_path_attribute_id=$my_row['attribute_id'];
	}	
	
	// image_label
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='image_label' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_image_label_attribute_id=$my_row['attribute_id'];
	}	

	// small_image_label
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='small_image_label' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_small_image_label_attribute_id=$my_row['attribute_id'];
	}

	// manufacturer
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='".$manufacturer_attribute_code."' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_manufacturer_attribute_id=$my_row['attribute_id'];
		
		if ($product_manufacturer_attribute_id>0) { // активируем атрибут в стандартном наборе атрибутов для товара, если он не активирован
			$parameters_query = mysql_query("SELECT attribute_set_id FROM ".$m_attribute_set." WHERE entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());

			$parameters_query = mysql_query("SELECT ".$m_attribute_group.".attribute_set_id, ".$m_entity_attribute.".attribute_id, ".$m_attribute_group.".attribute_group_id 
				FROM ".$m_attribute_set." 
				INNER JOIN ".$m_attribute_group." ON ".$m_attribute_set.".attribute_set_id=".$m_attribute_group.".attribute_set_id 
				INNER JOIN ".$m_entity_attribute." ON ".$m_attribute_group.".attribute_group_id=".$m_entity_attribute.".attribute_group_id 
				WHERE ".$m_attribute_set.".entity_type_id=".$entity_product_type_id." AND 
				".$m_attribute_group.".attribute_group_name='General' AND 
				".$m_entity_attribute.".attribute_id=".$product_manufacturer_attribute_id, $link) or die("Invalid query: " . mysql_error());
			
			if ($my_row = mysql_fetch_array($parameters_query)) {
			} else { // активируем атрибут
				$parameters_query = mysql_query("SELECT ".$m_attribute_group.".attribute_set_id, ".$m_attribute_group.".attribute_group_id 
					FROM ".$m_attribute_set." 
					INNER JOIN ".$m_attribute_group." ON ".$m_attribute_set.".attribute_set_id=".$m_attribute_group.".attribute_set_id 
					WHERE ".$m_attribute_set.".entity_type_id=".$entity_product_type_id." AND ".$m_attribute_group.".attribute_group_name='General'", $link) or die("Invalid query: " . mysql_error());
				
				if ($my_row = mysql_fetch_array($parameters_query)) {
					$parameters_query = mysql_query("INSERT INTO ".$m_entity_attribute." (entity_type_id, attribute_set_id, attribute_group_id, attribute_id, sort_order) VALUES (".$entity_product_type_id.", ".$my_row['attribute_set_id'].", ".$my_row['attribute_group_id'].", ".$product_manufacturer_attribute_id.", 100)", $link) or die("Invalid query: " . mysql_error());
				}
			}
		}
	}

	mysql_query("CREATE TEMPORARY TABLE ".$m_category_product."_temp LIKE ".$m_category_product, $link) or die("Invalid query: " . mysql_error());

	while (($data = fgetcsv($handle, filesize($import_file_name), $csv_delimiter_char)) !== FALSE) {
		$num = count($data);
		$row++;

		$record_type = $data[0]; // 1
		$parameter1 = $data[1];  // 2
		$parameter2 = $data[2];  // 3
		$parameter3 = $data[3];  // 4
		$parameter4 = $data[4];  // 5
		$parameter5 = $data[5];  // 6
		$parameter6 = $data[6];  // 7
		$parameter7 = $data[7];  // 8
		$parameter8 = $data[8];  // 9
		$parameter9 = $data[9];  // 10
		$parameter10 = $data[10];// 11
		$parameter11 = $data[11];// 12
		$parameter12 = $data[12];// 13
		$parameter13 = $data[13];// 14
		$parameter14 = $data[14];// 15
		$parameter15 = $data[15];// 16
		$parameter16 = $data[16];// 17
		$parameter17 = $data[17];// 18
		$parameter18 = $data[18];// 19
		$parameter19 = $data[19];// 20
		$parameter20 = $data[20];// 21
		$parameter21 = $data[21];// 22
		$parameter22 = $data[22];// 23
		$parameter23 = $data[23];// 24
		$parameter24 = $data[24];// 25
		$parameter25 = $data[25];// 26
		$parameter26 = $data[26];// 27
		$parameter27 = $data[27];// 28
		$parameter28 = $data[28];// 29
		$parameter29 = $data[29];// 30
		$parameter30 = $data[30];// 31
		$parameter31 = $data[31];// 32
		$parameter32 = $data[32];// 33
		$parameter33 = $data[33];// 34
		$parameter34 = $data[34];// 35
		$parameter35 = $data[35];// 36
		$parameter36 = $data[36];// 37
		$parameter37 = $data[36];// 38
		$parameter38 = $data[36];// 39
		$parameter39 = $data[36];// 40

		// Категории товара
		if ($record_type=='g') {
			
			// удаляем переходы на новую строку
			$parameter3=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter3);
			$parameter9=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter9);
			$parameter4=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter4);
			$parameter7=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter7);
			$parameter8=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter8);

			$cat_url=cyr_to_translit_utf8($parameter3);
			
			$parameter3=mysql_real_escape_string($parameter3);
			$parameter9=mysql_real_escape_string($parameter9);
			$parameter4=mysql_real_escape_string($parameter4);
			$parameter7=mysql_real_escape_string($parameter7);
			$parameter8=mysql_real_escape_string($parameter8);
			
			if ($parameter2==0 && $parameter1<>1 && $parameter1<>$main_cat_id) {
				$parameter2=$main_cat_id;
			}

			if ($parameter1==$main_cat_id) {
				$parameter2='1';
			}
			
			if ($parameter1<>1 && $parameter1<>$main_cat_id) {
				if (strpos($parameter10, "1/".$main_cat_id."/")==0) {
					$parameter10=str_replace("/0/", "1/".$main_cat_id."/", $parameter10);
				} else {
					$parameter10=str_replace("/0/", "", $parameter10);
				}
			} else {
				if ($parameter1==1) {
					$parameter10='1';
				}
				
				if ($parameter1==$main_cat_id) {
					$parameter10='1/'.$main_cat_id;
				}
			}
			
			// Проверяем есть ли такая категория, если нету добавляем новую, если есть тогда обновляем 
			$parameters_query = mysql_query("SELECT entity_id FROM ".$m_category." WHERE entity_id = '" . $parameter1 . "' limit 1", $link) or die("Invalid query: " . mysql_error());
			
			if ($my_row = mysql_fetch_row($parameters_query)) { //есть такая категория, с таким кодом, тогда обновляем данные
				$cat_position=$parameter5=='0' ? 'position' : $parameter5;
				
				mysql_query("UPDATE ".$m_category." SET parent_id='".$parameter2."', path='".$parameter10."', updated_at=now(), position=".$cat_position." WHERE entity_id = '" . $parameter1 . "' limit 1", $link) or die("Invalid query: $m_category - " . mysql_error());
				
				$cat_position='';
				
				if ($cat_name_attribute_id>0) { // обновление наименования категории
					mysql_query("UPDATE ".$m_category_varchar." SET value='".$parameter3."' WHERE attribute_id=".$cat_name_attribute_id." AND entity_type_id=".$entity_cat_type_id." AND store_id=".$store_id." AND  entity_id=".$parameter1, $link) or die("Invalid query: m_category_varchar - " . mysql_error());
				}
				
				if ($cat_seo_url_update==1) {
					mysql_query("UPDATE ".$m_core_url_rewrite." SET request_path='".$product_seo_url."' WHERE category_id=".$parameter1." AND store_id=".$stock_id, $link) or die("Invalid query: $m_core_url_rewrite - " . mysql_error());
				}
				
				// Изменяем значение счетчика количества обновленных позиций
				$count_cat_upd++;
				
			} else { // добавляем новую категорию
				
				$cat_level=$parameter11>0 ? (int)$parameter11-1 : 0;
				
				mysql_query("INSERT INTO ".$m_category." (entity_id, parent_id, entity_type_id, attribute_set_id, created_at, updated_at, path, level, position, children_count) VALUES(".$parameter1.",'".$parameter2."', '".$entity_cat_type_id."', '".$attribute_cat_set_id."', now(), now(), '".$parameter10."', '".$cat_level."', '".$parameter5."', 1)", $link) or die("Invalid query: $m_category - " . mysql_error());

				mysql_query("INSERT INTO ".$m_core_url_rewrite." (store_id, category_id, id_path, request_path, target_path, is_system) VALUES('".$stock_id."','".$parameter1."','category/".$parameter1."','".$cat_url."_".$parameter1.".html','catalog/category/view/id/".$parameter1."',1)", $link) or die("Invalid query: $m_core_url_rewrite - " . mysql_error());
								

				
				//  Атрибуты категории - int
				if ($cat_active_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_active_attribute_id.", ".$store_id.",".$parameter1.",1)", $link) or die("Invalid query: $m_category_int - " . mysql_error());
				}
				
				if ($cat_include_in_menu_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_include_in_menu_attribute_id.", ".$store_id.",".$parameter1.",1)", $link) or die("Invalid query: $m_category_int - " . mysql_error());
				}

				if ($cat_landing_page_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_landing_page_attribute_id.", ".$store_id.",".$parameter1.",NULL)", $link) or die("Invalid query: $m_category_int - " . mysql_error());
				}
				
				if ($cat_is_anchor_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_is_anchor_attribute_id.", ".$store_id.",".$parameter1.",0)", $link) or die("Invalid query: $m_category_int - " . mysql_error());
				}
				
				if ($cat_custom_use_parent_settings_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_custom_use_parent_settings_attribute_id.", ".$store_id.",".$parameter1.",0)", $link) or die("Invalid query: $m_category_int - " . mysql_error());
				}
				
				if ($cat_filter_price_range_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_filter_price_range_attribute_id.", ".$store_id.",".$parameter1.",NULL)", $link) or die("Invalid query: $m_category_int - " . mysql_error());
				}
				
				if ($cat_custom_apply_to_products_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_custom_apply_to_products_attribute_id.", ".$store_id.",".$parameter1.",0)", $link) or die("Invalid query: $m_category_int - " . mysql_error());
				}

				if ($cat_custom_design_apply_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_custom_design_apply_attribute_id.", ".$store_id.",".$parameter1.",1)", $link) or die("Invalid query: $m_category_int - " . mysql_error());
				}

				//  Атрибуты категории - text
				if ($cat_description_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_text." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_description_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter4."')", $link) or die("Invalid query: $m_category_text - " . mysql_error());
				}

				if ($cat_meta_keywords_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_text." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_meta_keywords_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter8."')", $link) or die("Invalid query: $m_category_text - " . mysql_error());
				}

				if ($cat_meta_description_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_text." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_meta_description_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter7."')", $link) or die("Invalid query: $m_category_text - " . mysql_error());
				}

				if ($cat_custom_layout_update_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_text." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_custom_layout_update_attribute_id.", ".$store_id.",".$parameter1.",'')", $link) or die("Invalid query: $m_category_text - " . mysql_error());
				}
				
				if ($cat_available_sort_by_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_text." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_available_sort_by_attribute_id.", ".$store_id.",".$parameter1.",'')", $link) or die("Invalid query: $m_category_text - " . mysql_error());
				}


				//  Атрибуты категории - varchar
				if ($cat_name_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_name_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter3."')", $link) or die("Invalid query: $m_category_varchar - " . mysql_error());
				}
				
				if ($cat_url_key_attribute_id>0) {
					$cat_seo_url=$parameter15;
					if (empty($cat_seo_url)) $cat_seo_url=$cat_url."_".$parameter1;
					
					mysql_query("INSERT INTO ".$m_category_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_url_key_attribute_id.", ".$store_id.",".$parameter1.",'".$cat_seo_url."')", $link) or die("Invalid query: $m_category_varchar - " . mysql_error());
				}
				
				if ($cat_meta_title_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_meta_title_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter6."')", $link) or die("Invalid query: $m_category_varchar - " . mysql_error());
				}
				
				if ($cat_display_mode_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_display_mode_attribute_id.", ".$store_id.",".$parameter1.",'PRODUCTS')", $link) or die("Invalid query: $m_category_varchar - " . mysql_error());
				}

				if ($cat_custom_design_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_custom_design_attribute_id.", ".$store_id.",".$parameter1.",'')", $link) or die("Invalid query: $m_category_varchar - " . mysql_error());
				}
				
				if ($cat_page_layout_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_page_layout_attribute_id.", ".$store_id.",".$parameter1.",'')", $link) or die("Invalid query: $m_category_varchar - " . mysql_error());
				}				
				
				if ($cat_image_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_category_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_image_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter9."')", $link) or die("Invalid query: $m_category_varchar - " . mysql_error());
				}

				if ($cat_url_path_attribute_id>0) {
					$cat_seo_url=$parameter15;
					if (empty($cat_seo_url)) $cat_seo_url=$cat_url."_".$parameter1.".html";
					if (strpos($cat_seo_url, ".html") === false) $cat_seo_url=$cat_seo_url.".html";
					
					mysql_query("INSERT INTO ".$m_category_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_url_path_attribute_id.", ".$store_id_default.",".$parameter1.",'".$cat_seo_url."')", $link) or die("Invalid query: $m_category_varchar - " . mysql_error());

					//mysql_query("INSERT INTO ".$m_category_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_cat_type_id.",".$cat_url_path_attribute_id.", 1,".$parameter1.",'".$cat_seo_url."')", $link) or die("Invalid query: $m_category_varchar - " . mysql_error());
				}
				
				$count_cat_add++;
			}
		}

		
		// Производители товара
		if ($record_type=='m') {
			
			// прослешивание данных 
			$parameter2=mysql_real_escape_string($parameter2);
			// удаляем переходы на новую строку
			$parameter2=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter2);
			
			$qm = mysql_query("SELECT ".$m_attribute_option.".option_id, ".$m_attribute_option_value.".value FROM ".$m_attribute_option." INNER JOIN ".$m_attribute_option_value." ON ".$m_attribute_option.".option_id=".$m_attribute_option_value.".option_id WHERE ".$m_attribute_option.".attribute_id = '".$product_manufacturer_attribute_id."' AND ".$m_attribute_option_value.".value='".mysql_real_escape_string($parameter2)."'", $link) or die("Invalid query: ".mysql_error());
			
			if ($my_row = mysql_fetch_array($qm)) {
			} else {
				mysql_query("INSERT INTO ".$m_attribute_option." (attribute_id) VALUES(".$product_manufacturer_attribute_id.")", $link) or die("Invalid query: $m_attribute_option - " . mysql_error());
				
				$attribute_option_id=mysql_insert_id($link) or die("Invalid query: mysql_insert_id() - " . mysql_error());				
				
				mysql_query("INSERT INTO ".$m_attribute_option_value." (option_id, store_id, value) VALUES(".$attribute_option_id.", ".$store_id.", '".mysql_real_escape_string($parameter2)."')", $link) or die("Invalid query: $m_attribute_option_value - " . mysql_error());
				
				// Изменяем значение счетчика количества обновленных позиций
				$count_mans_add++;
			}
		}

		// Категории товаров (1 товар в нескольких категориях)
		if ($record_type=='pg') {
			mysql_query("INSERT INTO ".$m_category_product."_temp (product_id, category_id) VALUES(".$parameter1.", ".$parameter2.")", $link) or die("Invalid query: - " . mysql_error());
		}

		// Товары
		if ($record_type=='p') {
			
			// удаляем переходы на новую строку
			$parameter3=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter3);
			$parameter4=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter4);
			$parameter8=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter8);
			$parameter9=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter9);
			$parameter10=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter10);
			$parameter12=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter12);
			$parameter15=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter15);
			$parameter16=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter16);
			$parameter17=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter17);
			$parameter27=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter27);
			$parameter28=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter28);
			$parameter30=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter30);
			
			$product_url=cyr_to_translit_utf8($parameter4);
			
			// прослешивание данных 
			$parameter3=mysql_real_escape_string($parameter3);
			$parameter4=mysql_real_escape_string($parameter4);
			$parameter8=mysql_real_escape_string($parameter8);
			$parameter9=mysql_real_escape_string($parameter9);
			$parameter10=mysql_real_escape_string($parameter10);
			$parameter12=mysql_real_escape_string($parameter12);
			$parameter15=mysql_real_escape_string($parameter15);
			$parameter16=mysql_real_escape_string($parameter16);
			$parameter17=mysql_real_escape_string($parameter17);
			$parameter28=mysql_real_escape_string($parameter28);
			$parameter30=mysql_real_escape_string($parameter30);
			
			// Перекодировка из win1251 в utf8
			if (DB_CHARSET=='utf8') {
				// $parameter3=iconv("windows-1251", 'utf-8', $parameter3);
				// $parameter4=iconv("windows-1251", 'utf-8', $parameter4);
				// $parameter8=iconv("windows-1251", 'utf-8', $parameter8);
				// $parameter9=iconv("windows-1251", 'utf-8', $parameter9);
				// $parameter10=iconv("windows-1251", 'utf-8', $parameter10);
				// $parameter12=iconv("windows-1251", 'utf-8', $parameter12);
			}

			// SKU
			$product_sku=$parameter3;
			if ($save_product_id_for_sku==1) $product_sku=$parameter1;

			$product_seo_url=$parameter24;
			if (empty($product_seo_url)) $product_seo_url=$product_url."_".$parameter1.".html";
			if (strpos($product_seo_url, ".html") === false) $product_seo_url=$product_seo_url.".html";
				
			// Проверяем есть ли такой товар, если нет добавляем новый, если есть тогда обновляем 
			$parameters_query = mysql_query("SELECT entity_id FROM ".$m_product." WHERE entity_id = '" . $parameter1 . "' limit 1", $link) or die("Invalid query: " . mysql_error());
			
			if ($my_row = mysql_fetch_row($parameters_query)) { //есть такой товар, с таким кодом, тогда обновляем данные
				$parameters_query2 = mysql_query("SELECT category_id, product_id, store_id FROM ".$m_product_category_product_index." WHERE category_id='".$parameter2."' AND product_id = '" . $parameter1 . "' AND store_id=".$store_id_default."  limit 1", $link) or die("Invalid query: " . mysql_error());
				if ($my_row = mysql_fetch_row($parameters_query2)) {
				} else {
					mysql_query("UPDATE ".$m_product_category_product_index." SET category_id='".$parameter2."' WHERE product_id = '" . $parameter1 . "' AND store_id=".$store_id_default." limit 1", $link) or die("Invalid UPDATE query: $m_product_category_product_index - " . mysql_error());
				}
				
				//mysql_query("UPDATE ".$m_category_product." SET category_id='".$parameter2."' WHERE product_id = '" . $parameter1 . "' limit 1", $link) or die("Invalid UPDATE query: $m_category_product - " . mysql_error());
	
				mysql_query("UPDATE ".$m_product_catalog_flat_1." SET price='".$parameter5."' WHERE entity_id='".$parameter1."' limit 1", $link) or die("Invalid UPDATE query: $m_product_catalog_flat_1 - " . mysql_error());
				
				// customer_group_id=0 - NOT LOGGED IN
				mysql_query("UPDATE ".$m_product_price." SET tax_class_id=0, price='".$parameter5."', final_price='".$parameter5."', min_price='".$parameter5."', max_price='".$parameter5."', tier_price=NULL WHERE entity_id='".$parameter1."' AND website_id=".$website_id." AND customer_group_id=0 limit 1", $link) or die("Invalid query: $m_product_price - " . mysql_error());

				// customer_group_id=1 - General
				mysql_query("UPDATE ".$m_product_price." SET tax_class_id=0, price='".$parameter5."', final_price='".$parameter5."', min_price='".$parameter5."', max_price='".$parameter5."', tier_price=NULL WHERE entity_id='".$parameter1."' AND website_id=".$website_id." AND customer_group_id=1 limit 1", $link) or die("Invalid query: $m_product_price - " . mysql_error());
				
				// customer_group_id=2 - Wholesale
				mysql_query("UPDATE ".$m_product_price." SET tax_class_id=0, price='".$parameter5."', final_price='".$parameter5."', min_price='".$parameter5."', max_price='".$parameter5."', tier_price=NULL WHERE entity_id='".$parameter1."' AND website_id=".$website_id." AND customer_group_id=2 limit 1", $link) or die("Invalid query: $m_product_price - " . mysql_error());
				
				// customer_group_id=3 - Retailer
				mysql_query("UPDATE ".$m_product_price." SET tax_class_id=0, price='".$parameter5."', final_price='".$parameter5."', min_price='".$parameter5."', max_price='".$parameter5."', tier_price=NULL WHERE entity_id='".$parameter1."' AND website_id=".$website_id." AND customer_group_id=".$entity_cat_type_id." limit 1", $link) or die("Invalid query: $m_product_price - " . mysql_error());
				
				if ($product_price_attribute_id>0) {
					mysql_query("UPDATE ".$m_product_decimal." SET value='".$parameter5."' WHERE entity_type_id=".$entity_product_type_id." AND attribute_id=".$product_price_attribute_id." AND store_id=".$store_id." AND entity_id=".$parameter1, $link) or die("Invalid query: $m_product_decimal - " . mysql_error());
				}
				
				if ($product_sku_attribute_id>0 && $product_sku_update==1) {
					mysql_query("UPDATE ".$m_product_text." SET value='".$product_sku."' WHERE entity_type_id=".$entity_product_type_id." AND attribute_id=".$product_sku_attribute_id." AND store_id=".$store_id." AND entity_id=".$parameter1, $link) or die("Invalid query: $m_product_text - " . mysql_error());
				}
				
				if ($product_name_attribute_id>0 && $product_name_update==1) {
					mysql_query("UPDATE ".$m_product_varchar." SET value='".$parameter4."' WHERE entity_type_id=".$entity_product_type_id." AND attribute_id=".$product_name_attribute_id." AND store_id=".$store_id." AND entity_id=".$parameter1, $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
					
					mysql_query("UPDATE ".$m_product_search_fulltext." SET data_index='".$product_sku." ".$parameter4." ".$parameter17." ".$parameter12."' WHERE product_id=".$parameter1." AND store_id=".$store_id, $link) or die("Invalid query: $m_product_search_fulltext - " . mysql_error());
					
					mysql_query("UPDATE ".$m_product_catalog_flat_1." SET name='".$parameter4."' WHERE entity_id=".$parameter1, $link) or die("Invalid query: $m_product_decimal - " . mysql_error());
				}
				
				if ($product_description_attribute_id>0 && $product_full_desc_update==1) {
					$product_description=$parameter17;
					if (empty($product_description)) {
						$product_description='A description of the process of forming...';
					}
					
					mysql_query("UPDATE ".$m_product_text." SET value='".$product_description."' WHERE entity_type_id=".$entity_product_type_id." AND attribute_id=".$product_description_attribute_id." AND store_id=".$store_id." AND entity_id=".$parameter1, $link) or die("Invalid query: $m_product_text - " . mysql_error());
				}
				
				if ($product_short_description_attribute_id>0 && $product_brief_desc_update==1) {
					$product_short_description=$parameter12;
					if (empty($product_short_description)) {
						$product_short_description='A description of the process of forming...';
						//$product_short_description=$parameter17; // get full desc for short desc
					}
					
					mysql_query("UPDATE ".$m_product_text." SET value='".$product_short_description."' WHERE entity_type_id=".$entity_product_type_id." AND attribute_id=".$product_short_description_attribute_id." AND store_id=".$store_id." AND entity_id=".$parameter1, $link) or die("Invalid query: $m_product_decimal - " . mysql_error());
				}
				
				if ($product_seo_url_update==1) {
					mysql_query("UPDATE ".$m_core_url_rewrite." SET request_path='".$product_seo_url."' WHERE product_id=".$parameter1." AND store_id=".$stock_id, $link) or die("Invalid query: $m_core_url_rewrite - " . mysql_error());
				}
				
				// Обновление кол-ва товара на складе
				if ($parameter25=="0") {
					$parameter13="0"; // если товар деактивирован, тогда кол-во 0
					$visibility="3"; // "Search" - будет доступен через поиск
					
				} else {
					$visibility="4"; // "Catalog, Search" - отображается в каталоге
					$stock_status="1";
				}
				
				if ($parameter13=="0") $stock_status="0";
				
				mysql_query("UPDATE ".$m_product_inventory_stock_item." SET qty=".$parameter13.", is_in_stock=".$stock_status." WHERE product_id=".$parameter1, $link) or die("Invalid query: $m_product_inventory_stock_item - " . mysql_error());

				mysql_query("UPDATE ".$m_product_inventory_stock_status." SET qty=".$parameter13.", stock_status=".$stock_status." WHERE product_id=".$parameter1, $link) or die("Invalid query: $m_product_inventory_stock_status - " . mysql_error());
				
				if ($product_visibility_attribute_id>0) {
					mysql_query("UPDATE ".$m_product_int." SET value=".$visibility." WHERE entity_type_id=".$entity_product_type_id." AND attribute_id=".$product_visibility_attribute_id." AND store_id=".$store_id." AND entity_id=".$parameter1, $link) or die("Invalid query: $m_product_int - " . mysql_error());
				}

				
				//  обновление производителя для товара
				if ($product_manufacturer_attribute_id>0) {
					// поиск производителя по названию
					$qm = mysql_query("SELECT ".$m_attribute_option.".option_id, ".$m_attribute_option_value.".value FROM ".$m_attribute_option." INNER JOIN ".$m_attribute_option_value." ON ".$m_attribute_option.".option_id=".$m_attribute_option_value.".option_id WHERE ".$m_attribute_option.".attribute_id = '".$product_manufacturer_attribute_id."' AND ".$m_attribute_option_value.".value='".mysql_real_escape_string($parameter15)."'", $link) or die("Invalid query: ".mysql_error());
			
					if ($my_row = mysql_fetch_array($qm)) {
						mysql_query("UPDATE ".$m_product_int." SET value=".$my_row['option_id']." WHERE entity_type_id=".$entity_product_type_id." AND attribute_id=".$product_manufacturer_attribute_id." AND store_id=".$store_id." AND entity_id=".$parameter1, $link) or die("Invalid query: $m_product_int - " . mysql_error());
					}
				}
				
				// mysql_query("UPDATE ".$m_product." SET sku='".$parameter3."', updated_at=now() WHERE entity_id = '" . $parameter1 . "' limit 1", $link) or die("Invalid query: $m_product - " . mysql_error());

				//if ($cat_name_attribute_id>0) { // обновление наименования товара
					//mysql_query("UPDATE ".$m_category_varchar." SET value='".$parameter3."' WHERE attribute_id=".$cat_name_attribute_id." AND entity_type_id=".$entity_cat_type_id." AND store_id=".$store_id." AND  entity_id=".$parameter1, $link) or die("Invalid query: m_category_varchar - " . mysql_error());
				//}

				// Изменяем значение счетчика количества обновленных позиций
				$count_products_upd++;
				
			} else { // добавляем новый товар
				// default attribute set
				$product_attribute_set_id=$attribute_product_set_id;
				if (!empty($parameter30)) { // получаем id наборов атрибутов товаров
					$parameters_query = mysql_query("SELECT attribute_set_id FROM ".$m_attribute_set." WHERE entity_type_id='".$entity_product_type_id."' AND attribute_set_name='".$parameter30."'", $link) or die("Invalid query: " . mysql_error());
					if ($my_row = mysql_fetch_array($parameters_query)) {
						$product_attribute_set_id=$my_row['attribute_set_id'];
					}
				}
				
				mysql_query("INSERT INTO ".$m_product." (entity_id, entity_type_id, attribute_set_id, type_id, sku, created_at, updated_at, has_options, required_options) VALUES(".$parameter1.", ".$entity_product_type_id.", ".$product_attribute_set_id.", 'simple', '".$product_sku."', now(), now(), 0, 0)", $link) or die("Invalid query: $m_product - " . mysql_error());
				
				//mysql_query("INSERT INTO ".$m_category_product." (category_id, product_id, position) VALUES(".$parameter2.", ".$parameter1.", 1)", $link) or die("Invalid query: $m_category_product - " . mysql_error());
				
				$product_category_path_query = mysql_query("SELECT path FROM ".$m_category." WHERE entity_id=".$parameter2, $link) or die("Invalid query: $m_category - " . mysql_error());
				if ($my_row = mysql_fetch_array($product_category_path_query)) {				
					$cat_path=$my_row['path'];
					$cat_path=explode('/',$cat_path);

					foreach ($cat_path as $path_value) {
						if ($path_value<>'1') {
							if ($parameter2==$path_value) {
								$is_parent='1';
								$position='1';
							} else {
								$position='0';
								$is_parent='0';
							}
							
							// Проверяем есть ли запись о товаре, если нет добавляем
							$parameters_query = mysql_query("SELECT category_id, product_id, store_id FROM ".$m_product_category_product_index." WHERE category_id='".$path_value."' AND product_id='".$parameter1."' AND store_id=".$store_id_default." LIMIT 1", $link) or die("Invalid query: " . mysql_error());
							
							if ($my_row = mysql_fetch_row($parameters_query)) {
							} else {
								mysql_query("INSERT INTO ".$m_product_category_product_index." (category_id, product_id, position, is_parent, store_id, visibility) VALUES(".$path_value.", ".$parameter1.", ".$position.", ".$is_parent.", ".$store_id_default.", 4)", $link) or die("Invalid INSERT query: $m_product_category_product_index - " . mysql_error());
							}
						}
					}
				}

				// customer_group_id=0 - NOT LOGGED IN
				mysql_query("INSERT INTO ".$m_product_price." (entity_id, customer_group_id, website_id, tax_class_id, price, final_price, min_price, max_price, tier_price) VALUES(".$parameter1.", 0, ".$website_id.", 0, '".$parameter5."', '".$parameter5."', '".$parameter5."', '".$parameter5."', NULL)", $link) or die("Invalid query: $m_product_price - " . mysql_error());
				
				// customer_group_id=1 - General
				mysql_query("INSERT INTO ".$m_product_price." (entity_id, customer_group_id, website_id, tax_class_id, price, final_price, min_price, max_price, tier_price) VALUES(".$parameter1.", 1, ".$website_id.", 0, '".$parameter5."', '".$parameter5."', '".$parameter5."', '".$parameter5."', NULL)", $link) or die("Invalid query: $m_product_price - " . mysql_error());
				
				// customer_group_id=2 - Wholesale
				mysql_query("INSERT INTO ".$m_product_price." (entity_id, customer_group_id, website_id, tax_class_id, price, final_price, min_price, max_price, tier_price) VALUES(".$parameter1.", 2, ".$website_id.", 0, '".$parameter5."', '".$parameter5."', '".$parameter5."', '".$parameter5."', NULL)", $link) or die("Invalid query: $m_product_price - " . mysql_error());
				
				// customer_group_id=3 - Retailer
				mysql_query("INSERT INTO ".$m_product_price." (entity_id, customer_group_id, website_id, tax_class_id, price, final_price, min_price, max_price, tier_price) VALUES(".$parameter1.", 3, ".$website_id.", 0, '".$parameter5."', '".$parameter5."', '".$parameter5."', '".$parameter5."', NULL)", $link) or die("Invalid query: $m_product_price - " . mysql_error());
				
				mysql_query("INSERT INTO ".$m_product_website." (product_id, website_id) VALUES(".$parameter1.", ".$website_id.")", $link) or die("Invalid query: $m_product_website - " . mysql_error());				
				
				mysql_query("INSERT INTO ".$m_core_url_rewrite." (store_id, category_id, product_id, id_path, request_path, target_path, is_system) VALUES('".$stock_id."',NULL, '".$parameter1."','product/".$parameter1."','".$product_seo_url."','catalog/product/view/id/".$parameter1."',1)", $link) or die("Invalid query: $m_core_url_rewrite - " . mysql_error());

				//stock_status_changed_automatically, use_config_enable_qty_increments

				mysql_query("INSERT INTO ".$m_product_inventory_stock_item." (product_id, stock_id, qty, min_qty, use_config_min_qty, is_qty_decimal, backorders, use_config_backorders, min_sale_qty, use_config_min_sale_qty, max_sale_qty, use_config_max_sale_qty, is_in_stock, low_stock_date, notify_stock_qty, use_config_notify_stock_qty, manage_stock, use_config_manage_stock, use_config_qty_increments, qty_increments, enable_qty_increments) VALUES(".$parameter1.", ".$stock_id.", ".$parameter13.", 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, NULL, NULL, 1, 0, 1, 1, 0.0000, 0)", $link) or die("Invalid query: $m_product_inventory_stock_item - " . mysql_error());	

				mysql_query("INSERT INTO ".$m_product_inventory_stock_status." (product_id, website_id, stock_id, qty, stock_status) VALUES(".$parameter1.", ".$website_id.", ".$stock_id.", ".$parameter13.", 1)", $link) or die("Invalid query: $m_product_inventory_stock_status - " . mysql_error());	
				
				mysql_query("INSERT INTO ".$m_product_search_fulltext." (product_id, store_id, data_index) VALUES(".$parameter1.", 1, '".$product_sku." ".$parameter4." ".$parameter17." ".$parameter12."')", $link) or die("Invalid query: $m_product_search_fulltext - " . mysql_error());

				//mysql_query("INSERT INTO ".$m_product_catalog_flat_1." (entity_id, attribute_set_id, type_id, cost, created_at, has_options, image_label, is_recurring, links_exist, links_purchased_separately, links_title, name, news_from_date, news_to_date, price, price_type, price_view, recurring_profile, required_options, shipment_type, short_description, sku, sku_type, small_image, small_image_label, special_from_date, special_price, special_to_date, tax_class_id, thumbnail, thumbnail_label, updated_at, url_key, url_path, visibility, weight, weight_type) VALUES(".$parameter1.", ".$product_attribute_set_id.", 'simple', NULL, now(), 0, NULL, 0, NULL, NULL, NULL, '".$parameter4."', NULL, NULL, ".$parameter5.", NULL, NULL, NULL, 0, NULL, '".$parameter12."', '".$product_sku."', NULL, 'no_selection', NULL, NULL, NULL, NULL, 0, 'no_selection', NULL, now(), '".$product_url."_".$parameter1."', '".$product_seo_url."', 4, 0.0000, NULL)", $link) or die("Invalid query: $m_product_catalog_flat_1 - " . mysql_error());
				
				
				
				//  Атрибуты товаров - decimal
				if ($product_weight_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_decimal." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_weight_attribute_id.", ".$store_id.",".$parameter1.",".(int)$parameter27.")", $link) or die("Invalid query: $m_product_decimal - " . mysql_error());
				}

				if ($product_price_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_decimal." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_price_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter5."')", $link) or die("Invalid query: $m_product_decimal - " . mysql_error());
				}

				if ($product_special_price_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_decimal." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_special_price_attribute_id.", ".$store_id.",".$parameter1.",NULL)", $link) or die("Invalid query: $m_product_decimal - " . mysql_error());
				}
				
				if ($product_cost_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_decimal." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_cost_attribute_id.", ".$store_id.",".$parameter1.",NULL)", $link) or die("Invalid query: $m_product_decimal - " . mysql_error());
				}
				
				//  Атрибуты товаров - int
				if ($product_status_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_status_attribute_id.", ".$store_id.",".$parameter1.",".$parameter25.")", $link) or die("Invalid query: $m_product_int - " . mysql_error());
				}
				
				if ($product_visibility_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_visibility_attribute_id.", ".$store_id.",".$parameter1.",4)", $link) or die("Invalid query: $m_product_int - " . mysql_error());
				}
				
				if ($product_tax_class_id_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_tax_class_id_attribute_id.", ".$store_id.",".$parameter1.",0)", $link) or die("Invalid query: $m_product_int - " . mysql_error());
				}
				
				if ($product_enable_googlecheckout_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_enable_googlecheckout_attribute_id.", ".$store_id.",".$parameter1.",1)", $link) or die("Invalid query: $m_product_int - " . mysql_error());
				}

				if ($product_is_recurring_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_is_recurring_attribute_id.", ".$store_id.",".$parameter1.",0)", $link) or die("Invalid query: $m_product_int - " . mysql_error());
				}

				if ($product_manufacturer_attribute_id>0) {
					// поиск производителя по названию
					$qm = mysql_query("SELECT ".$m_attribute_option.".option_id, ".$m_attribute_option_value.".value FROM ".$m_attribute_option." INNER JOIN ".$m_attribute_option_value." ON ".$m_attribute_option.".option_id=".$m_attribute_option_value.".option_id WHERE ".$m_attribute_option.".attribute_id = '".$product_manufacturer_attribute_id."' AND ".$m_attribute_option_value.".value='".mysql_real_escape_string($parameter15)."'", $link) or die("Invalid query: ".mysql_error());
			
					if ($my_row = mysql_fetch_array($qm)) {
						mysql_query("INSERT INTO ".$m_product_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_manufacturer_attribute_id.", ".$store_id.",".$parameter1.",".$my_row['option_id'].")", $link) or die("Invalid query: $m_product_int - " . mysql_error());
					}
				}
	
				//  Атрибуты товаров - text
				if ($product_description_attribute_id>0) {
					$product_description=$parameter17;
					if (empty($product_description)) {
						$product_description='A description of the process of forming...';
					}
					
					mysql_query("INSERT INTO ".$m_product_text." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_description_attribute_id.", ".$store_id.",".$parameter1.",'".$product_description."')", $link) or die("Invalid query: $m_product_text - " . mysql_error());
				}	

				if ($product_InDepth_description_attribute_id>0) {
					$product_description=$parameter17;
					if (empty($product_description)) {
						$product_description='A description of the process of forming...';
					}
					
					mysql_query("INSERT INTO ".$m_product_text." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_InDepth_description_attribute_id.", ".$store_id.",".$parameter1.",'".$product_description."')", $link) or die("Invalid query: $m_product_text - " . mysql_error());
				}
				
				if ($product_short_description_attribute_id>0) {
					$product_short_description=$parameter12;
					if (empty($product_short_description)) {
						$product_short_description='A description of the process of forming...';
						//$product_short_description=$parameter17; // get full desc for short desc
					}
					
					mysql_query("INSERT INTO ".$m_product_text." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_short_description_attribute_id.", ".$store_id.",".$parameter1.",'".$product_short_description."')", $link) or die("Invalid query: $m_product_text - " . mysql_error());
				}
				
				
				
				if ($product_meta_keyword_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_text." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_meta_keyword_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter10."')", $link) or die("Invalid query: $m_product_text - " . mysql_error());
				}
				
				if ($product_custom_layout_update_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_text." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_custom_layout_update_attribute_id.", ".$store_id.",".$parameter1.",'')", $link) or die("Invalid query: $m_product_text - " . mysql_error());
				}
	
				if ($product_small_image_label_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_text." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_small_image_label_attribute_id.", ".$store_id.",".$parameter1.",'')", $link) or die("Invalid query: $m_product_text - " . mysql_error());
				}
				
				if ($product_image_label_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_text." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_image_label_attribute_id.", ".$store_id.",".$parameter1.",'')", $link) or die("Invalid query: $m_product_text - " . mysql_error());
				}
	
	
				//  Атрибуты товаров - varchar
				if ($product_name_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_name_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter4."')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_model_attribute_id>0) {
					$product_model=$parameter16;
					if (empty($product_model)) $product_model=$parameter4;
					
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_model_attribute_id.", ".$store_id.",".$parameter1.",'".$product_model."')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_sku_attribute_id>0) {
					$product_sku=$product_sku;
					if (empty($product_sku)) $product_sku=$parameter1;
					
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_sku_attribute_id.", ".$store_id.",".$parameter1.",'".$product_sku."')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_my_sku_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_my_sku_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter3."')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_warranty_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_warranty_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter28."')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}

				if ($product_url_key_attribute_id>0) {
					$product_seo_url=$parameter24;
					if (empty($product_seo_url)) $product_seo_url=$product_url."_".$parameter1;
					
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_url_key_attribute_id.", ".$store_id.",".$parameter1.",'".$product_seo_url."')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_gift_message_available_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_gift_message_available_attribute_id.", ".$store_id.",".$parameter1.",'2')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}

				if ($product_meta_title_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_meta_title_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter8."')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_meta_description_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_meta_description_attribute_id.", ".$store_id.",".$parameter1.",'".$parameter9."')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_image_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_image_attribute_id.", ".$store_id.",".$parameter1.",'no_selection')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_small_image_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_small_image_attribute_id.", ".$store_id.",".$parameter1.",'no_selection')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_thumbnail_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_thumbnail_attribute_id.", ".$store_id.",".$parameter1.",'no_selection')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_custom_design_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_custom_design_attribute_id.", ".$store_id.",".$parameter1.",'')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_page_layout_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_page_layout_attribute_id.", ".$store_id.",".$parameter1.",'')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_options_container_attribute_id>0) {
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_options_container_attribute_id.", ".$store_id.",".$parameter1.",'container2')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
				
				if ($product_url_path_attribute_id>0) {
					$product_seo_url=$parameter24;
					if (empty($product_seo_url)) $product_seo_url=$product_url."_".$parameter1.".html";
					if (strpos($product_seo_url, ".html") === false) $product_seo_url=$product_seo_url.".html";
					
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_url_path_attribute_id.", ".$store_id.",".$parameter1.",'".$product_seo_url."')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
					
					mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_url_path_attribute_id.", ".$store_id_default.",".$parameter1.",'".$product_seo_url."')", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
				}
		
	
				$count_products_add++;
			}
		}
	}
	
	fclose($handle);
	
	// Категории товаров (1 товар в нескольких категориях)		
	mysql_query("DELETE FROM ".$m_category_product." WHERE product_id IN (SELECT product_id FROM ".$m_category_product."_temp GROUP BY product_id)", $link) or die("Invalid query: " . mysql_error());
	mysql_query("INSERT INTO ".$m_category_product." (product_id, category_id) SELECT product_id, category_id FROM ".$m_category_product."_temp", $link) or die("Invalid query: " . mysql_error());
	
	// update cats children count
	mysql_query("CREATE TEMPORARY TABLE catalog_category_entity_tmp LIKE ".$m_category, $link) or die("Invalid query: update cats children count - " . mysql_error());
	mysql_query("INSERT INTO catalog_category_entity_tmp SELECT * FROM ".$m_category, $link) or die("Invalid query: update cats children count - " . mysql_error());
	mysql_query("UPDATE ".$m_category." cce
				SET children_count =
				(
					SELECT count(cce2.entity_id) - 1 as children_county
					FROM catalog_category_entity_tmp cce2
					WHERE PATH LIKE CONCAT(cce.path,'%')
				)", $link) or die("Invalid query: update cats children count - " . mysql_error());

	echo 'Всего записей: '.$row.'<br />';
	echo 'Категорий добавлено: '.$count_cat_add.'<br />';
	echo 'Категорий обновлено: '.$count_cat_upd.'<br />';
	echo 'Производителей добавлено: '.$count_mans_add.'<br />';
	echo 'Товаров добавлено: '.$count_products_add.'<br />';
	echo 'Товаров обновлено: '.$count_products_upd.'<br />';
	
	//@setlocale(LC_ALL, 'ru_RU.UTF8');
  }
}





// 26.09.2010 Импорт - Техн. характеристики товара для добавления в поле Описание товара
if (isset($_POST['action']) && $_POST['action']=='import_products_descriptions') {
  if (is_uploaded_file($_FILES['csv_file']['tmp_name'])) {

    switch ($_POST['arc']) {
	    case 'csv': 
			$import_file_name=$_FILES['csv_file']['tmp_name'];
			break;
	    case 'zip': 
			$import_file_name=unpackZip($unpackzipdir, $_FILES['csv_file']['tmp_name'], $unpack_zip_log);
			$import_file_name=$unpackzipdir.$import_file_name;
			break;
	    case 'gzip': 
			$import_file_name=UnpackGZip($unpackzipdir, $_FILES['csv_file']['tmp_name'], $unpack_zip_log);
			$import_file_name=$unpackzipdir.$import_file_name;
			break;
	}

	

	ini_set('auto_detect_line_endings', '1');
	//@setlocale(LC_ALL, 'ru_RU.CP1251');
	
	if(!$handle = fopen($import_file_name, "r"))
	{
	    echo('Could not open file. Canceled...');
	    die('Could not open file. Canceled...');
	}
	
	$row = 0;
	$count_products_upd = 0;
	
	$product_description_attribute_id=0;
	$product_short_description_attribute_id=0;
	$product_InDepth_description_attribute_id=0;
	$product_media_gallery_attribute_id=0;
	$product_image_attribute_id=0;
	$product_small_image_attribute_id=0;
	$product_thumbnail_attribute_id=0;
	$product_small_image_label_attribute_id=0;
	$product_image_label_attribute_id=0;
	
	// description
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='description' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_description_attribute_id=$my_row['attribute_id'];
	}

	// short_description
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='short_description' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_short_description_attribute_id=$my_row['attribute_id'];
	}
	
	// InDepth description
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='in_depth' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_InDepth_description_attribute_id=$my_row['attribute_id'];
	}
	
	// media_gallery
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='media_gallery' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_media_gallery_attribute_id=$my_row['attribute_id'];
	}

	// image
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='image' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_image_attribute_id=$my_row['attribute_id'];
	}

	// small_image
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='small_image' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_small_image_attribute_id=$my_row['attribute_id'];
	}

	// thumbnail
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='thumbnail' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_thumbnail_attribute_id=$my_row['attribute_id'];
	}
	
	// image_label
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='image_label' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_image_label_attribute_id=$my_row['attribute_id'];
	}	

	// small_image_label
	$parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code='small_image_label' AND entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: " . mysql_error());
	if ($my_row = mysql_fetch_array($parameters_query)) {
		$product_small_image_label_attribute_id=$my_row['attribute_id'];
	}

	mysql_query("DROP TABLE IF EXISTS `etrade_cc_pics`", $link) or die("Invalid query: $m_product_text - " . mysql_error());
	mysql_query("CREATE TABLE IF NOT EXISTS `etrade_cc_pics` (
		  `row_id` int(11) NOT NULL AUTO_INCREMENT,
		  `tov_id` int(11) NOT NULL,
		  `tov_guid` varchar(80) NOT NULL,
		  `pic_name` varchar(240) NOT NULL,
		  `pic_type` varchar(3) NOT NULL,
		  `pic_file_prefix` varchar(120) NOT NULL,
		  `pic_order` int(11) NOT NULL,
		  `tov_name` varchar(240) NOT NULL,
		  `pic_sub_dir` varchar(240) NOT NULL,
		  PRIMARY KEY (`row_id`),
		  KEY `tov_id` (`tov_id`),
		  KEY `tov_guid` (`tov_guid`),
		  KEY `pic_name` (`pic_name`),
		  KEY `pic_type` (`pic_type`),
		  KEY `pic_file_prefix` (`pic_file_prefix`),
		  KEY `pic_order` (`pic_order`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8", $link) or die("Invalid query: $m_product_text - " . mysql_error());
				
	while (($data = fgetcsv($handle, filesize($import_file_name), $csv_delimiter_char)) !== FALSE) {
		$num = count($data);
		$row++;

		$record_type = $data[0]; // 1
		$parameter1 = $data[1];  // 2
		$parameter2 = $data[2];  // 3
		$parameter3 = $data[3];  // 4
		$parameter4 = $data[4];  // 5
		$parameter5 = $data[5];  // 6
		$parameter6 = $data[6];  // 7
		$parameter7 = $data[7];  // 8
		$parameter8 = $data[8];  // 9
		$parameter9 = $data[9];  // 10
		$parameter10 = $data[10];  // 11
		
		// удаляем переходы на новую строку
		$parameter4=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter4);
		$parameter7=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter7);
		$parameter8=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter8);
		$parameter4=mysql_real_escape_string($parameter4);
		$parameter7=mysql_real_escape_string($parameter7);
		$parameter8=mysql_real_escape_string($parameter8);
		
		// Перекодировка из win1251 в utf8
		// if (DB_CHARSET=='utf8') {
			// $parameter4=iconv("windows-1251", 'utf-8', $parameter4);
		// }

		// фото товаров
		if ($record_type=='pics2') {
			mysql_query("INSERT INTO etrade_cc_pics(tov_id, tov_guid, pic_name, pic_type, pic_file_prefix, pic_order, tov_name, pic_sub_dir) 
				VALUES('".$parameter1."', '".$parameter2."', '".$parameter3."', '".$parameter4."', '".$parameter5."', '".$parameter6."', '".$parameter7."', '".$parameter8."')", $link) or die("Invalid query: etrade_cc_pics - " . mysql_error());
		}
		
		// Техн. характеристики товара для добавления в поле Описание товара
		if ($record_type=='fs') {
			if ($product_description_attribute_id>0 && !empty($parameter4)) {
				mysql_query("INSERT INTO ".$m_product_text." (attribute_id, entity_id, entity_type_id, store_id, value) VALUES(".$product_description_attribute_id.",".$parameter1.", '".$entity_product_type_id."','".$store_id."', value='".$parameter4."') ON DUPLICATE KEY UPDATE value='".$parameter4."'", $link) or die("Invalid query: $m_product_text - " . mysql_error());
			
				mysql_query("UPDATE ".$m_product_text." SET value='".$parameter4."' WHERE attribute_id=".$product_description_attribute_id." AND entity_id=".$parameter1." AND entity_type_id=".$entity_product_type_id." AND store_id=".$store_id, $link) or die("Invalid query: $m_product_text - " . mysql_error());
				
				$count_products_upd++;
			}

			if ($product_InDepth_description_attribute_id>0 && !empty($parameter4)) {
				mysql_query("INSERT INTO ".$m_product_text." (attribute_id, entity_id, entity_type_id, store_id, value) VALUES(".$product_InDepth_description_attribute_id.",".$parameter1.", '".$entity_product_type_id."','".$store_id."', value='".$parameter4."') ON DUPLICATE KEY UPDATE value='".$parameter4."'", $link) or die("Invalid query: $m_product_text - " . mysql_error());
				
				mysql_query("UPDATE ".$m_product_text." SET value='".$parameter4."' WHERE attribute_id=".$product_InDepth_description_attribute_id." AND entity_id=".$parameter1." AND entity_type_id=".$entity_product_type_id." AND store_id=".$store_id, $link) or die("Invalid query: $m_product_text - " . mysql_error());
			}
			
			if ($product_short_description_attribute_id>0 && !empty($parameter8)) {
				mysql_query("INSERT INTO ".$m_product_text." (attribute_id, entity_id, entity_type_id, store_id, value) VALUES(".$product_short_description_attribute_id.",".$parameter1.", '".$entity_product_type_id."','".$store_id."', value='".$parameter4."') ON DUPLICATE KEY UPDATE value='".$parameter4."'", $link) or die("Invalid query: $m_product_text - " . mysql_error());
				
				mysql_query("UPDATE ".$m_product_text." SET value='".$parameter8."' WHERE attribute_id=".$product_short_description_attribute_id." AND entity_id=".$parameter1." AND entity_type_id=".$entity_product_type_id." AND store_id=".$store_id, $link) or die("Invalid query: $m_product_text - " . mysql_error());
			}
			
			// фото
			/*
			if ($product_media_gallery_attribute_id>0 && (!empty($parameter2) or !empty($parameter3))) {
				if (!empty($parameter2)) {
					$photo_file=$ProductsBigImagePath.$parameter2;
				} else {
					$photo_file=$ProductsBigImagePath.$parameter3;
				}
				
				// if ($check_exist_pic==1) {
					// $parameters_query = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE entity_id=".$parameter1." AND attribute_id=".$product_media_gallery_attribute_id, $link) or die("Invalid query: " . mysql_error());
					// if ($my_row = mysql_fetch_array($parameters_query)) {
						// continue;
					// }
				// }
	
				// большая картинка
				mysql_query("INSERT INTO ".$m_product_media_gallery." (attribute_id, entity_id, value) VALUES (".$product_media_gallery_attribute_id.",".$parameter1.",'".$photo_file."') ON DUPLICATE KEY UPDATE value='".$photo_file."'", $link) or die("Table: ".$m_product_media_gallery." | entity_id: ".$parameter1." | Invalid query (error): " . mysql_error());
				$big_pic_id=mysql_insert_id($link) or die("Invalid query: mysql_insert_id() - " . mysql_error());
				
				mysql_query("INSERT INTO ".$m_product_media_gallery_value." (value_id, store_id, label, position, disabled) VALUES (".$big_pic_id.", ".$store_id.", '".$parameter7."', 1, 0)", $link) or die("Invalid query: $m_product_media_gallery_value - " . mysql_error());

				mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_image_attribute_id.", ".$store_id.",".$parameter1.",'".$photo_file."') ON DUPLICATE KEY UPDATE value='".$photo_file."'", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
								
				mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_small_image_attribute_id.", ".$store_id.",".$parameter1.",'".$photo_file."') ON DUPLICATE KEY UPDATE value='".$photo_file."'", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());
			
				mysql_query("INSERT INTO ".$m_product_varchar." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES(".$entity_product_type_id.",".$product_thumbnail_attribute_id.", ".$store_id.",".$parameter1.",'".$photo_file."') ON DUPLICATE KEY UPDATE value='".$photo_file."'", $link) or die("Invalid query: $m_product_varchar - " . mysql_error());

			
				// product_catalog_flat_1
				mysql_query("UPDATE ".$m_product_catalog_flat_1." SET image_label='".$parameter7."', small_image='".$photo_file."' WHERE entity_id='".$parameter1."' limit 1", $link) or die("Invalid query: $m_product_catalog_flat_1 - " . mysql_error());
			}
			*/
		}
	}

	fclose($handle);
	
	// обновление фото у товаров
	UpdateProductPics();
	
	echo 'Всего записей: '.$row.'<br />';
	echo 'Описание товаров обновлено: '.$count_products_upd.'<br />';
  }
}

function UpdateProductPics () {
	global $link, $store_id, $entity_product_type_id, $product_media_gallery_attribute_id, $product_image_attribute_id, $product_small_image_attribute_id, $product_thumbnail_attribute_id, $ProductsBigImagePath;

	// обновление фото у товаров
	if ($product_media_gallery_attribute_id>0) {
		// удаляем картинки, если таких товаров нет в БД сайта
		mysql_query("DELETE FROM etrade_cc_pics WHERE tov_id NOT IN (SELECT entity_id FROM ".TABLE_PREFIX."catalog_product_entity_int)", $link) or die("Invalid query: etrade_cc_pics_products_list - " . mysql_error());
		
		// удаляем старые фото
		mysql_query("DROP TABLE IF EXISTS `etrade_cc_pics_products_list`", $link) or die("Invalid query: etrade_cc_pics_products_list - " . mysql_error());
		mysql_query("CREATE TABLE etrade_cc_pics_products_list
		  SELECT tov_id FROM etrade_cc_pics GROUP BY tov_id", $link) or die("Invalid query: etrade_cc_pics_products_list - " . mysql_error());

		mysql_query("DELETE ".TABLE_PREFIX."catalog_product_entity_media_gallery FROM ".TABLE_PREFIX."catalog_product_entity_media_gallery INNER JOIN etrade_cc_pics_products_list ON ".TABLE_PREFIX."catalog_product_entity_media_gallery.entity_id=etrade_cc_pics_products_list.tov_id", $link) or die("Invalid query: ".TABLE_PREFIX."catalog_product_entity_media_gallery_value - " . mysql_error());
		mysql_query("DELETE FROM ".TABLE_PREFIX."catalog_product_entity_media_gallery_value 
			WHERE value_id NOT IN (SELECT value_id FROM ".TABLE_PREFIX."catalog_product_entity_media_gallery)", $link) or die("Invalid query: ".TABLE_PREFIX."catalog_product_entity_media_gallery_value - " . mysql_error());
		mysql_query("DELETE ".TABLE_PREFIX."catalog_product_entity_varchar FROM ".TABLE_PREFIX."catalog_product_entity_varchar INNER JOIN etrade_cc_pics_products_list 
			ON ".TABLE_PREFIX."catalog_product_entity_varchar.entity_id=etrade_cc_pics_products_list.tov_id AND ".TABLE_PREFIX."catalog_product_entity_varchar.attribute_id IN(".$product_image_attribute_id.", ".$product_small_image_attribute_id.", ".$product_thumbnail_attribute_id.")", $link) or die("Invalid query: ".TABLE_PREFIX."catalog_product_entity_varchar - " . mysql_error());
		
		// добавляем инфу об фото
		mysql_query("INSERT INTO ".TABLE_PREFIX."catalog_product_entity_media_gallery (attribute_id, entity_id, value) 
			SELECT ".$product_media_gallery_attribute_id." as attribute_id, tov_id, CONCAT('".$ProductsBigImagePath."', etrade_cc_pics.pic_name) AS value 
			FROM etrade_cc_pics", $link) or die("Invalid query (error): " . mysql_error());
		
		mysql_query("INSERT INTO ".TABLE_PREFIX."catalog_product_entity_media_gallery_value (value_id, store_id, label, position) 
			SELECT ".TABLE_PREFIX."catalog_product_entity_media_gallery.value_id, ".$store_id." as store_id, etrade_cc_pics.tov_name, etrade_cc_pics.pic_order 
			FROM ".TABLE_PREFIX."catalog_product_entity_media_gallery
			INNER JOIN etrade_cc_pics ON etrade_cc_pics.tov_id=".TABLE_PREFIX."catalog_product_entity_media_gallery.entity_id 
			WHERE ".TABLE_PREFIX."catalog_product_entity_media_gallery.attribute_id=".$product_media_gallery_attribute_id." AND 
				".TABLE_PREFIX."catalog_product_entity_media_gallery.value=CONCAT('".$ProductsBigImagePath."', etrade_cc_pics.pic_name) 
			ORDER BY etrade_cc_pics.tov_id, etrade_cc_pics.pic_order 
			", $link) or die("Invalid query: ".TABLE_PREFIX."catalog_product_entity_media_gallery_value - " . mysql_error());

		mysql_query("INSERT INTO ".TABLE_PREFIX."catalog_product_entity_varchar (entity_type_id, attribute_id, store_id, entity_id, value) 
			SELECT ".$entity_product_type_id." as entity_type_id, ".$product_image_attribute_id." as attribute_id, ".$store_id." as store_id, tov_id, CONCAT('".$ProductsBigImagePath."', pic_name) AS value 
			FROM etrade_cc_pics 
			WHERE pic_type='M' AND tov_id NOT IN (SELECT entity_id FROM ".TABLE_PREFIX."catalog_product_entity_varchar WHERE attribute_id=".$product_image_attribute_id." AND store_id=".$store_id.")", $link) or die("Invalid query (error): " . mysql_error());		
		
		mysql_query("INSERT INTO ".TABLE_PREFIX."catalog_product_entity_varchar (entity_type_id, attribute_id, store_id, entity_id, value) 
			SELECT ".$entity_product_type_id." as entity_type_id, ".$product_small_image_attribute_id." as attribute_id, ".$store_id." as store_id, tov_id, CONCAT('".$ProductsBigImagePath."', pic_name) AS value 
			FROM etrade_cc_pics 
			WHERE pic_type='M' AND tov_id NOT IN (SELECT entity_id FROM ".TABLE_PREFIX."catalog_product_entity_varchar WHERE attribute_id=".$product_small_image_attribute_id." AND store_id=".$store_id.")
			", $link) or die("Invalid query (error): " . mysql_error());	
			
		mysql_query("INSERT INTO ".TABLE_PREFIX."catalog_product_entity_varchar (entity_type_id, attribute_id, store_id, entity_id, value) 
			SELECT ".$entity_product_type_id." as entity_type_id, ".$product_thumbnail_attribute_id." as attribute_id, ".$store_id." as store_id, tov_id, CONCAT('".$ProductsBigImagePath."', pic_name) AS value 
			FROM etrade_cc_pics 
			WHERE pic_type='M' AND tov_id NOT IN (SELECT entity_id FROM ".TABLE_PREFIX."catalog_product_entity_varchar WHERE attribute_id=".$product_thumbnail_attribute_id." AND store_id=".$store_id.")
			", $link) or die("Invalid query (error): " . mysql_error());	
	
		/*
		// product_catalog_flat_1
		mysql_query("UPDATE ".TABLE_PREFIX."catalog_product_flat_1, etrade_cc_pics 
			SET image_label=etrade_cc_pics.tov_name, small_image=CONCAT('".$ProductsBigImagePath."', etrade_cc_pics.pic_name) 
			WHERE entity_id=etrade_cc_pics.tov_id AND etrade_cc_pics.pic_type='M'", $link) or die("Invalid query: ".TABLE_PREFIX."catalog_product_flat_1 - " . mysql_error());
		*/
		mysql_query("DROP TABLE IF EXISTS `etrade_cc_pics`", $link) or die("Invalid query: $m_product_text - " . mysql_error());
	}
}



// 26.09.2010 Импорт - Техн. характеристики товара для фильтрации товаров
if (isset($_POST['action']) && $_POST['action']=='import_products_parametrs_filter') {
  if (is_uploaded_file($_FILES['csv_file']['tmp_name'])) {

    switch ($_POST['arc']) {
	    case 'csv': 
			$import_file_name=$_FILES['csv_file']['tmp_name'];
			break;
	    case 'zip': 
			$import_file_name=unpackZip($unpackzipdir, $_FILES['csv_file']['tmp_name'], $unpack_zip_log);
			$import_file_name=$unpackzipdir.$import_file_name;
			break;
	    case 'gzip': 
			$import_file_name=UnpackGZip($unpackzipdir, $_FILES['csv_file']['tmp_name'], $unpack_zip_log);
			$import_file_name=$unpackzipdir.$import_file_name;
			break;
	}

	

	ini_set('auto_detect_line_endings', '1');
	//@setlocale(LC_ALL, 'ru_RU.CP1251');
	
	if(!$handle = fopen($import_file_name, "r"))
	{
	    echo('Could not open file. Canceled...');
	    die('Could not open file. Canceled...');
	}
	
	$row = 0;
	
	$count_cps_add=0;
	$count_cps_upd=0;
	$count_cpsg_add=0;
	$count_cpsg_upd=0;
	$count_cpa_add=0;
	$count_cpa_upd=0;
	$count_products_upd=0;
	
	mysql_query("DROP TABLE IF EXISTS `etrade_cc_pics`", $link) or die("Invalid query: $m_product_text - " . mysql_error());
	mysql_query("CREATE TABLE IF NOT EXISTS `etrade_cc_pics` (
		  `row_id` int(11) NOT NULL AUTO_INCREMENT,
		  `tov_id` int(11) NOT NULL,
		  `tov_guid` varchar(80) NOT NULL,
		  `pic_name` varchar(240) NOT NULL,
		  `pic_type` varchar(3) NOT NULL,
		  `pic_file_prefix` varchar(120) NOT NULL,
		  `pic_order` int(11) NOT NULL,
		  `tov_name` varchar(240) NOT NULL,
		  `pic_sub_dir` varchar(240) NOT NULL,
		  PRIMARY KEY (`row_id`),
		  KEY `tov_id` (`tov_id`),
		  KEY `tov_guid` (`tov_guid`),
		  KEY `pic_name` (`pic_name`),
		  KEY `pic_type` (`pic_type`),
		  KEY `pic_file_prefix` (`pic_file_prefix`),
		  KEY `pic_order` (`pic_order`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8", $link) or die("Invalid query: $m_product_text - " . mysql_error());
		
	// cascade delete
	while (($data = fgetcsv($handle, filesize($import_file_name), $csv_delimiter_char)) !== FALSE) {
		$record_type = $data[0]; // 1
		$parameter1 = $data[1];  // 2
		$parameter2 = $data[2];  // 3
		$parameter3 = $data[3];  // 4
		$parameter4 = $data[4];  // 5
		$parameter5 = $data[5];  // 6
		
		// cascade delete old attributes and values
		if ($record_type=='cpa') { // our attribute list from cc
			mysql_query("DELETE FROM ".$m_attribute." WHERE attribute_code LIKE 'f".$parameter1."_%'", $link) or die("Invalid query: $m_attribute - ".mysql_error());
		}
		
		// фото товаров
		if ($record_type=='pics2') {
			mysql_query("INSERT INTO etrade_cc_pics(tov_id, tov_guid, pic_name, pic_type, pic_file_prefix, pic_order, tov_name, pic_sub_dir) 
				VALUES('".$parameter1."', '".$parameter2."', '".$parameter3."', '".$parameter4."', '".$parameter5."', '".$parameter6."', '".$parameter7."', '".$parameter8."')", $link) or die("Invalid query: etrade_cc_pics - " . mysql_error());
		}
		
		// cascade delete old attributes sets
		// if ($record_type=='cpa') {
			// mysql_query("DELETE FROM ".$m_attribute_set." WHERE attribute_code LIKE '%cps_%'", $link) or die("Invalid query: $m_attribute_set - ".mysql_error());
		// }
	}
	
	fclose($handle);
	
	
	// 
	if(!$handle = fopen($import_file_name, "r"))
	{
	    echo('Could not open file. Canceled...');
	    die('Could not open file. Canceled...');
	}
	
	while (($data = fgetcsv($handle, filesize($import_file_name), $csv_delimiter_char)) !== FALSE) {
		$num = count($data);
		$row++;

		$record_type = $data[0]; // 1
		$parameter1 = $data[1];  // 2
		$parameter2 = $data[2];  // 3
		$parameter3 = $data[3];  // 4
		$parameter4 = $data[4];  // 5
		$parameter5 = $data[5];  // 6


		// удаляем переходы на новую строку
		// $parameter1=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter4);
		// $parameter2=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter4);
		// $parameter3=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter4);
		// $parameter4=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter4);
		// $parameter5=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $parameter5);
		
		// мнемонизируем специальные символы в строке
		// $parameter1=mysql_real_escape_string($parameter1);
		// $parameter2=mysql_real_escape_string($parameter2);
		// $parameter3=mysql_real_escape_string($parameter3);
		// $parameter4=mysql_real_escape_string($parameter4);
		// $parameter5=mysql_real_escape_string($parameter5);
		

		if ($record_type=='cps') { // catalog_product_set
			$result1 = mysql_query("SELECT attribute_set_id, attribute_set_name FROM ".$m_attribute_set." WHERE attribute_set_name LIKE 'cps_".$parameter1."_%' LIMIT 1", $link) or die("Invalid query: $m_attribute_set - ".mysql_error());
		
			if (mysql_num_rows($result1)==0) {
				mysql_query("INSERT INTO ".$m_attribute_set." (entity_type_id, attribute_set_name, sort_order) VALUES (".$entity_product_type_id.", 'cps_".$parameter1."_".$parameter2."',0)", $link) or die("Invalid query: $m_attribute_set - " . mysql_error());
				
				$attribute_set_id=mysql_insert_id($link) or die("Invalid query: mysql_insert_id() - " . mysql_error());
				
				// список системных групп
				$result_group = mysql_query("SELECT attribute_group_id, attribute_set_id, attribute_group_name, sort_order, default_id FROM ".$m_attribute_group." WHERE attribute_set_id=".$entity_product_type_id, $link) or die("Invalid query: $m_attribute_group - ".mysql_error());
				
				while ($result_row = mysql_fetch_array($result_group)) {
					mysql_query("INSERT INTO ".$m_attribute_group." (attribute_set_id, attribute_group_name, sort_order, default_id) VALUES (".$attribute_set_id.", '".$result_row['attribute_group_name']."',".$result_row['sort_order'].",".$result_row['sort_order'].")", $link) or die("Invalid query: $m_attribute_group - " . mysql_error());
					
					$attribute_group_id=mysql_insert_id($link) or die("Invalid query: mysql_insert_id() - " . mysql_error());
					
					// список атрибутов системной группы
					$result2 = mysql_query("SELECT entity_type_id, attribute_set_id, attribute_group_id, attribute_id, sort_order FROM ".$m_entity_attribute." WHERE attribute_group_id=".$result_row['attribute_group_id'], $link) or die("Invalid query: $m_entity_attribute - ".mysql_error());
				
					while ($result_row = mysql_fetch_array($result2)) {
						mysql_query("INSERT INTO ".$m_entity_attribute." (entity_type_id, attribute_set_id, attribute_group_id, attribute_id, sort_order) VALUES (".$entity_product_type_id.", ".$attribute_set_id.", '".$attribute_group_id."',".$result_row['attribute_id'].",".$result_row['sort_order'].")", $link) or die("Invalid query: $m_entity_attribute - " . mysql_error());
					}
				}
				
				$count_cps_add++;
			} else {
				$result_row = mysql_fetch_assoc($result1);
				$attribute_set_id=$result_row['attribute_set_id'];
				$count_cps_upd++;
				
				// change attribute set name
				mysql_query("UPDATE ".$m_attribute_set." SET attribute_set_name='cps_".$parameter1."_".$parameter2."' WHERE attribute_set_name LIKE '%cps_".$parameter1."_%'", $link) or die("Invalid query: $m_product - " . mysql_error());
			}
		}
		
		if ($record_type=='v') { // cpsg - our catalog_product_set_group
			$result = mysql_query("SELECT attribute_group_id, attribute_group_name FROM ".$m_attribute_group." WHERE attribute_group_name LIKE 'cpsg_".$parameter1."_%' LIMIT 1", $link) or die("Invalid query: $m_attribute_group - ".mysql_error());
			
			if (mysql_num_rows($result)==0) {
				mysql_query("INSERT INTO ".$m_attribute_group." (attribute_set_id, attribute_group_name, sort_order, default_id) VALUES (".$attribute_set_id.", 'cpsg_".$parameter1."_".$parameter2."',100,0)", $link) or die("Invalid query: $m_attribute_group - " . mysql_error());
				
				$attribute_group_id=mysql_insert_id($link) or die("Invalid query: mysql_insert_id() - " . mysql_error());
				
				$count_cpsg_add++;
			} else {
				$result_row = mysql_fetch_assoc($result);
				$attribute_group_id=$result_row['attribute_group_id'];
				
				$count_cpsg_upd++;
			}
		}
		
		if ($record_type=='cpa') { // our attribute list from cc
			$result = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code LIKE 'f".$parameter1."_%' LIMIT 1", $link) or die("Invalid query: $m_attribute - ".mysql_error());
			
			if (mysql_num_rows($result)==0) {
				// For internal use. Must be unique with no spaces. Maximum length of attribute code must be less then 30 symbols
				$attribute_code="f".$parameter1."_".str_replace("-","_",cyr_to_translit_utf8($parameter5));
				$attribute_code=substr($attribute_code, 0, 29);
				
				mysql_query("INSERT INTO ".$m_attribute." (entity_type_id, attribute_code, attribute_model, backend_model, backend_type, backend_table, frontend_model, frontend_input, frontend_label, frontend_class, source_model, is_required, is_user_defined, default_value, is_unique, note) VALUES (".$entity_product_type_id.", '".$attribute_code."', NULL, NULL, 'int', NULL, NULL, 'select', '".$parameter5."', NULL, 'eav/entity_attribute_source_table', 0, 1, '', 0, '')", $link) or die("Invalid query: $m_attribute - " . mysql_error());

				$attribute_id=mysql_insert_id($link) or die("Invalid query: mysql_insert_id() - " . mysql_error());
				
				mysql_query("INSERT INTO ".$m_entity_attribute." (entity_type_id, attribute_set_id, attribute_group_id, attribute_id, sort_order) VALUES (".$entity_product_type_id.", ".$attribute_set_id.", '".$attribute_group_id."',".$attribute_id.",'".$parameter4."')", $link) or die("Invalid query: $m_entity_attribute - " . mysql_error());				
				
				$is_visible_on_front=0; // показывать атрибут и значение в карточке товара для посетителя
				
				mysql_query("INSERT INTO ".$m_catalog_attribute." (attribute_id, frontend_input_renderer, is_global, is_visible, is_searchable, is_filterable, is_comparable, is_visible_on_front, is_html_allowed_on_front, is_used_for_price_rules, is_filterable_in_search, used_in_product_listing, used_for_sort_by, is_configurable, apply_to, is_visible_in_advanced_search, position, is_wysiwyg_enabled, is_used_for_promo_rules) VALUES (".$attribute_id.", NULL, 0, 1, 0, 1, 1, ".$is_visible_on_front.", 1, 0, 0, 1, 0, 0, '', 0, 0, 0, 0)", $link) or die("Invalid query: $m_catalog_attribute - " . mysql_error());		
								
				$count_cpa_add++;
			} else { // attribute update
				$result_row = mysql_fetch_assoc($result);
				$attribute_id=$result_row['attribute_id'];
				
				$count_cpa_upd++;
			}
		}
		
		if ($record_type=='gv') { // our attributes values list for attribute from cc
			mysql_query("INSERT INTO ".$m_attribute_option." (attribute_id, sort_order) VALUES (".$attribute_id.", 0)", $link) or die("Invalid query: $m_attribute_option - " . mysql_error());	
			
			$option_id=mysql_insert_id($link) or die("Invalid query: mysql_insert_id() - " . mysql_error());
			
			mysql_query("INSERT INTO ".$m_attribute_option_value." (option_id, store_id, value) VALUES (".$option_id.", ".$store_id.", '".mysql_real_escape_string($parameter3)."')", $link) or die("Invalid query: $m_attribute_option_value - " . mysql_error());
			
			$option_value_id=mysql_insert_id($link) or die("Invalid query: mysql_insert_id() - " . mysql_error());
			
		}

		if ($record_type=='pid') { // products
			mysql_query("INSERT INTO ".$m_product_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES (".$entity_product_type_id.", ".$attribute_id.", ".$store_id.", ".$parameter1.", ".$option_id.") ON DUPLICATE KEY UPDATE value='".$option_id."'", $link) or die("Invalid query: $m_product_int - " . mysql_error());

			mysql_query("INSERT INTO ".$m_product_index_eav." (entity_id, attribute_id, store_id, value) VALUES (".$parameter1.", ".$attribute_id.",".$store_id_default.",".$option_id.")", $link) or die("Invalid query: $m_product_index_eav - " . mysql_error());
			
			
			// change attribute_set_id for product
			mysql_query("UPDATE ".$m_product." SET attribute_set_id=".$attribute_set_id." WHERE entity_id=".$parameter1, $link) or die("Invalid query: $m_product - " . mysql_error());
			
			$count_products_upd++;
		}
	}
	
	fclose($handle);
	
	
	
	// NULL
	$result_attr_set = mysql_query("SELECT attribute_set_id, attribute_set_name FROM ".$m_attribute_set." WHERE attribute_set_name LIKE 'cps_%'", $link) or die("Invalid query: $m_attribute_set - ".mysql_error());
	
	// список наборов атрибутов
	while ($result_row = mysql_fetch_array($result_attr_set)) {
		$product_cat_id=$result_row['attribute_set_name'];

		$begin_text='cps_';
		$end_text='_';
		$whereis_start=strpos($product_cat_id, $begin_text,1);
		$whereis_end=strpos($product_cat_id, $end_text,4);
		$product_cat_id=trim(substr($product_cat_id, $whereis_start+strlen($begin_text), $whereis_end-$whereis_start-strlen($begin_text)));

		$result_attr_group = mysql_query("SELECT attribute_set_id, attribute_group_id FROM ".$m_attribute_group." WHERE attribute_set_id=".$result_row['attribute_set_id']." AND sort_order=100 LIMIT 1", $link) or die("Invalid query: $m_attribute_group - ".mysql_error());
		
		// список групп атрибутов
		while ($result_row2 = mysql_fetch_array($result_attr_group)) {
			$result_attr_entity = mysql_query("SELECT attribute_id FROM ".$m_entity_attribute." WHERE attribute_set_id=".$result_row2['attribute_set_id']." AND attribute_group_id=".$result_row2['attribute_group_id'], $link) or die("Invalid query: $m_entity_attribute - ".mysql_error());
			
			// список атрибутов группы
			while ($result_row3 = mysql_fetch_array($result_attr_entity)) {
				// список всех товаров в категории
				$result_products_list = mysql_query("SELECT product_id FROM ".$m_product_category_product_index." WHERE category_id=".$product_cat_id." AND is_parent=1 AND store_id=".$store_id_default, $link) or die("Invalid query: $m_product_category_product_index - ".mysql_error());
				
				// проверка у каждого товара наличие всех атрибутов
				while ($result_row4 = mysql_fetch_array($result_products_list)) {
					$result_product_entity_int = mysql_query("SELECT entity_id FROM ".$m_product_int." WHERE entity_id=".$result_row4['product_id']." AND attribute_id=".$result_row3['attribute_id'], $link) or die("Invalid query: $m_product_int - ".mysql_error());
					
					// если нет атрибута у товара, тогда добавляем NULL
					if (mysql_num_rows($result_product_entity_int)==0) {
						 mysql_query("INSERT INTO ".$m_product_int." (entity_type_id, attribute_id, store_id, entity_id, value) VALUES (".$entity_product_type_id.", ".$result_row3['attribute_id'].", ".$store_id.", ".$result_row4['product_id'].", NULL)", $link) or die("Invalid query: $m_product_int - ".mysql_error());
					}
				}
			}
		}
	}
	
	// обновление фото у товаров
	//UpdateProductPics();
	
	echo 'Загруженный файл: '.$import_file_name.'<br />';
	echo 'Всего записей: '.$row.'<br />';
	echo 'Наборов атрибутов добавлено: '.$count_cps_add.'<br />';
	echo 'Наборов атрибутов обновлено: '.$count_cps_upd.'<br />';
	echo 'Групп атрибутов добавлено: '.$count_cpsg_add.'<br />';
	echo 'Групп атрибутов обновлено: '.$count_cpsg_upd.'<br />';
	echo 'Атрибутов добавлено: '.$count_cpa_add.'<br />';
	echo 'Атрибутов обновлено: '.$count_cpa_upd.'<br />';
	echo 'Товаров обновлено: '.$count_products_upd.'<br />';

  }
}






// 26.09.2010 Экспорт каталога товаров в формат E-Trade
if (isset($_POST['action']) && $_POST['action']=='export_products') {

	
	@setlocale(LC_ALL, 'ru_RU.CP1251');
	//@setlocale(LC_ALL, 'ru_RU');
	
	$csv_file='';
	
	// search default categories for products
	mysql_query("DROP TABLE IF EXISTS `etrade_products_cats_tmp`", $link) or die("Invalid query: ".mysql_error());
	mysql_query("CREATE TABLE etrade_products_cats_tmp LIKE ".$m_category_product, $link) or die("Invalid query: " . mysql_error());
	mysql_query("INSERT INTO etrade_products_cats_tmp (product_id, category_id) SELECT product_id, category_id FROM ".$m_category_product, $link) or die("Invalid query: - " . mysql_error());
	mysql_query("UPDATE etrade_products_cats_tmp, ".$m_category." SET etrade_products_cats_tmp.position=".$m_category.".level WHERE etrade_products_cats_tmp.category_id=".$m_category.".entity_id", $link) or die("Invalid query: ".mysql_error());
		
	// Категории товаров
	$q1 = mysql_query("SELECT entity_id, entity_type_id, attribute_set_id, parent_id, created_at, updated_at, path, position, level, children_count FROM ".$m_category." WHERE entity_type_id=".$entity_cat_type_id." ORDER BY level ASC", $link) or die("Invalid query: ".mysql_error());

	while ($result_row = mysql_fetch_array($q1)) {
		
		$cat_id=$result_row['entity_id'];
		$cat_parent_id=$result_row['parent_id'];
		$cat_active='';
		$cat_name='';
		$cat_description='';
		$cat_image='';
		$cat_meta_title='';
		$cat_meta_keywords='';
		$cat_meta_description='';
		$position=$result_row['position'];
		
		// category_varchar
		$q2 = mysql_query("SELECT ".$m_category_varchar.".entity_id, ".$m_category_varchar.".entity_type_id, ".$m_category_varchar.".attribute_id, ".$m_category_varchar.".store_id, ".$m_category_varchar.".value, ".$m_attribute.".attribute_code FROM ".$m_category_varchar." INNER JOIN ".$m_attribute." ON ".$m_category_varchar.".attribute_id=".$m_attribute.".attribute_id WHERE entity_id=".$cat_id." AND ".$m_category_varchar.".store_id=".$store_id, $link) or die("Invalid query: ".mysql_error());

		while ($result_row = mysql_fetch_array($q2)) {
			switch ($result_row['attribute_code']) {
				case 'name': // название категории
					$cat_name=$result_row['value'];
					break;
				case 'meta_title':
					$cat_meta_title=$result_row['value'];
					break;
				case 'image':
					$cat_image=$result_row['value'];
					break;
				default:
					//exit;
			}
		}

		
		// category_text
		$q2 = mysql_query("SELECT ".$m_category_text.".entity_id, ".$m_category_text.".entity_type_id, ".$m_category_text.".attribute_id, ".$m_category_text.".store_id, ".$m_category_text.".value, ".$m_attribute.".attribute_code FROM ".$m_category_text." INNER JOIN ".$m_attribute." ON ".$m_category_text.".attribute_id=".$m_attribute.".attribute_id WHERE entity_id=".$cat_id." AND ".$m_category_text.".store_id=".$store_id, $link) or die("Invalid query: ".mysql_error());

		while ($result_row = mysql_fetch_array($q2)) {
			switch ($result_row['attribute_code']) {
				case 'description':
					$cat_description=$result_row['value'];
					break;
				case 'meta_keywords':
					$cat_meta_keywords=$result_row['value'];
					break;
				case 'meta_description':
					$cat_meta_description=$result_row['value'];
					break;	
				default:
					//exit;
			}
		}

		// category_int
		$q2 = mysql_query("SELECT ".$m_category_int.".entity_id, ".$m_category_int.".entity_type_id, ".$m_category_int.".attribute_id, ".$m_category_int.".store_id, ".$m_category_int.".value, ".$m_attribute.".attribute_code FROM ".$m_category_int." INNER JOIN ".$m_attribute." ON ".$m_category_int.".attribute_id=".$m_attribute.".attribute_id WHERE entity_id=".$cat_id." AND ".$m_category_int.".store_id=".$store_id, $link) or die("Invalid query: ".mysql_error());

		while ($result_row = mysql_fetch_array($q2)) {
			switch ($result_row['attribute_code']) {
				case 'is_active':
					$cat_active=$result_row['value'];
					break;
				default:
					//exit;
			}
		}
		
		// удаляем переходы на новую строку
		$cat_name=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $cat_name);
		$cat_description=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $cat_description);
		$cat_meta_title=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $cat_meta_title);
		$cat_meta_keywords=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $cat_meta_keywords);
		$cat_meta_description=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $cat_meta_description);
	
		if ($cat_id==1 && $cat_name=='') {
			$cat_name='Top';
		}

		if ($cat_id<>1 && $cat_name=='') {
			$cat_name='Cat_'.$cat_id;
		}
		
		
		$cat_url='';
		$cat_url_answer = mysql_query("SELECT request_path FROM ".$m_core_url_rewrite." WHERE category_id=".$cat_id." AND store_id=".$stock_id." GROUP BY category_id", $link);
		while ($result_row = mysql_fetch_array($cat_url_answer)) {
			$cat_url=$result_row['request_path'];
		}
		
		$csv_file.='g'.$csv_delimiter_char; // 1
		$csv_file.=$cat_id.$csv_delimiter_char; // 2
		$csv_file.=$cat_parent_id.$csv_delimiter_char; // 3
		$csv_file.=$cat_name.$csv_delimiter_char; // 4
		$csv_file.=$cat_description.$csv_delimiter_char; // 5
		$csv_file.=$position.$csv_delimiter_char; // 6  category_order
		$csv_file.=$cat_meta_title.$csv_delimiter_char; // 7 - virtual "Meta Title"
		$csv_file.=$cat_meta_keywords.$csv_delimiter_char; // 8
		$csv_file.=$cat_meta_description.$csv_delimiter_char; // 9
		$csv_file.=$cat_image.$csv_delimiter_char; // 10
		$csv_file.=''.$csv_delimiter_char; // 11 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 12 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 13 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 14 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 15 - virtual 
		$csv_file.=$cat_url.$csv_delimiter_char; // 16 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 17 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 18 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 19 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 20 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 21 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 22 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 23 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 24 - virtual
		$csv_file.=''.$csv_delimiter_char; // 25 - virtual
		$csv_file.=''.$csv_delimiter_char; // 26 - virtual
		$csv_file.=''.$csv_delimiter_char; // 27 - virtual
		$csv_file.=''.$csv_delimiter_char; // 28 - virtual
		$csv_file.="\n";
	}
	
	// Производители
	$q4 = mysql_query("SELECT attribute_id FROM ".$m_attribute." WHERE attribute_code = '".$manufacturer_attribute_code."'", $link) or die("Invalid query: ".mysql_error());
	
	$result_row4 = mysql_fetch_array($q4);
	$manufacturer_attribute_id=$result_row4['attribute_id'];
	
	$q5 = mysql_query("SELECT ".$m_attribute_option.".option_id, ".$m_attribute_option_value.".value FROM ".$m_attribute_option." INNER JOIN ".$m_attribute_option_value." ON ".$m_attribute_option.".option_id=".$m_attribute_option_value.".option_id WHERE ".$m_attribute_option.".attribute_id = '".$manufacturer_attribute_id."'", $link) or die("Invalid query: ".mysql_error());
	while ($result_row5 = mysql_fetch_array($q5)) {
		$brand_name=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $result_row5['value']);
		
		$csv_file.='m'.$csv_delimiter_char; // 1
		$csv_file.=$result_row5['option_id'].$csv_delimiter_char; // 2
		$csv_file.=$brand_name.$csv_delimiter_char; // 3
		$csv_file.=''.$csv_delimiter_char; // 4
		$csv_file.=''.$csv_delimiter_char; // 5 meta_title
		$csv_file.=''.$csv_delimiter_char; // 6 meta_description
		$csv_file.=''.$csv_delimiter_char; // 7 meta_keywords
		$csv_file.=''.$csv_delimiter_char; // 8 url
		$csv_file.=''.$csv_delimiter_char; // 9 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 10 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 11 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 12 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 13 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 14 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 15 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 16 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 17 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 18 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 19 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 20 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 21 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 22 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 23 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 24 - virtual
		$csv_file.=''.$csv_delimiter_char; // 25 - virtual
		$csv_file.=''.$csv_delimiter_char; // 26 - virtual
		$csv_file.=''.$csv_delimiter_char; // 27 - virtual
		$csv_file.=''.$csv_delimiter_char; // 28 - virtual
		$csv_file.="\n";
	}
	
	// Товары
	$q1 = mysql_query("SELECT entity_id, entity_type_id, attribute_set_id, type_id, sku, has_options, required_options, created_at, updated_at FROM ".$m_product." WHERE entity_type_id=".$entity_product_type_id, $link) or die("Invalid query: ".mysql_error());

	while ($result_row = mysql_fetch_array($q1)) {
		
		$product_id=$result_row['entity_id'];
		$product_SKU=$result_row['sku'];
		$product_cat_id='0';
		$product_name='';
		$product_description='';
		$product_short_description='';
		$product_image='';
		$product_small_image='';
		$product_thumbnail_image='';
		$product_meta_title='';
		$product_meta_keywords='';
		$product_meta_description='';
		$product_status='';
		$product_price='';
		$product_stock='1';
		$manufacturer_id='0';
		$manufacturer_name='';

		// product_varchar
		$q2 = mysql_query("SELECT ".$m_product_varchar.".entity_id, ".$m_product_varchar.".entity_type_id, ".$m_product_varchar.".attribute_id, ".$m_product_varchar.".store_id, ".$m_product_varchar.".value, ".$m_attribute.".attribute_code FROM ".$m_product_varchar." INNER JOIN ".$m_attribute." ON ".$m_product_varchar.".attribute_id=".$m_attribute.".attribute_id WHERE entity_id=".$product_id." AND ".$m_product_varchar.".store_id=".$store_id, $link) or die("Invalid query: ".mysql_error());

		while ($result_row = mysql_fetch_array($q2)) {
			switch ($result_row['attribute_code']) {
				case 'name': // название товара
					$product_name=$result_row['value'];
					break;
				case 'meta_title':
					$product_meta_title=$result_row['value'];
					break;
				case 'meta_description':
					$product_meta_description=$result_row['value'];
					break;
				case 'image': // Base Image 
					$product_image=$result_row['value'];
					if ($product_image=='no_selection') {
						$product_image='';
					}
					break;
				case 'small_image': // small image
					$product_small_image=$result_row['value'];
					if ($product_small_image=='no_selection') {
						$product_small_image='';
					}
					break;
				case 'thumbnail': // thumbnail
					$product_thumbnail_image=$result_row['value'];
					if ($product_thumbnail_image=='no_selection') {
						$product_thumbnail_image='';
					}
					break;
				default:
					//exit;
			}
		}

		
		// product_text
		$q2 = mysql_query("SELECT ".$m_product_text.".entity_id, ".$m_product_text.".entity_type_id, ".$m_product_text.".attribute_id, ".$m_product_text.".store_id, ".$m_product_text.".value, ".$m_attribute.".attribute_code FROM ".$m_product_text." INNER JOIN ".$m_attribute." ON ".$m_product_text.".attribute_id=".$m_attribute.".attribute_id WHERE entity_id=".$product_id." AND ".$m_product_text.".store_id=".$store_id, $link) or die("Invalid query: ".mysql_error());

		while ($result_row = mysql_fetch_array($q2)) {
			switch ($result_row['attribute_code']) {
				case 'description':
					$product_description=$result_row['value'];
					break;
				case 'short_description':
					$product_short_description=$result_row['value'];
					break;
				case 'meta_keyword':
					$product_meta_keywords=$result_row['value'];
					break;	
				default:
					//exit;
			}
		}

		// product_int
		$q2 = mysql_query("SELECT ".$m_product_int.".entity_id, ".$m_product_int.".entity_type_id, ".$m_product_int.".attribute_id, ".$m_product_int.".store_id, ".$m_product_int.".value, ".$m_attribute.".attribute_code FROM ".$m_product_int." INNER JOIN ".$m_attribute." ON ".$m_product_int.".attribute_id=".$m_attribute.".attribute_id WHERE entity_id=".$product_id." AND ".$m_product_int
.".store_id=".$store_id, $link) or die("Invalid query: ".mysql_error());


		while ($result_row = mysql_fetch_array($q2)) {
			switch ($result_row['attribute_code']) {
				case 'status':
					$product_status=$result_row['value'];
					break;
				
				case $manufacturer_attribute_code:
					$manufacturer_id=$result_row['value'];
					if (strlen($manufacturer_id)>0) {
						$q3 = mysql_query("SELECT value FROM ".$m_attribute_option_value." WHERE option_id=".$manufacturer_id, $link) or die("Invalid query: ".mysql_error());
						$result_row2 = mysql_fetch_array($q3);
						$manufacturer_name=$result_row2['value'];
					}
				default:
					//exit;
			}
		}

		// catalog_product_index_price
		$q2 = mysql_query("SELECT price FROM ".$m_product_price." WHERE entity_id=".$product_id." AND website_id=".$website_id." AND customer_group_id=0", $link) or die("Invalid query: ".mysql_error());

		while ($result_row = mysql_fetch_array($q2)) {
			$product_price=$result_row['price'];
		}
		
		// stock
		$q2 = mysql_query("SELECT qty, product_id, website_id, stock_id, stock_status FROM ".$m_stock." WHERE product_id=".$product_id." AND website_id=".$website_id, $link) or die("Invalid query: ".mysql_error());

		while ($result_row = mysql_fetch_array($q2)) {
			$product_stock=$result_row['qty'];
		}

		// catalog_category_product
		//$q2 = mysql_query("SELECT category_id, product_id, position FROM ".$m_category_product." WHERE product_id=".$product_id." GROUP BY product_id ORDER BY position", $link) or die("Invalid query: ".mysql_error());
		
		$q2 = mysql_query("SELECT category_id, product_id, position
			FROM etrade_products_cats_tmp
			WHERE product_id = ".$product_id." AND
				position IN (
					SELECT MAX( position )
					FROM etrade_products_cats_tmp
					WHERE product_id = ".$product_id."
				)", $link) or die("Invalid query: ".mysql_error());

		while ($result_row = mysql_fetch_array($q2)) {
			$product_cat_id=$result_row['category_id'];
		}
		
		$product_url='';
		$product_url_answer = mysql_query("SELECT request_path FROM ".$m_core_url_rewrite." WHERE product_id=".$product_id." AND store_id=".$stock_id." GROUP BY product_id", $link);
		while ($result_row = mysql_fetch_array($product_url_answer)) {
			$product_url=$result_row['request_path'];
		}
		
		// удаляем переходы на новую строку
		$product_name=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $product_name);
		$product_meta_title=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $product_meta_title);
		$product_meta_keywords=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $product_meta_keywords);
		$product_meta_description=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '' , $product_meta_description);
		$product_short_description=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '<BR />' , $product_short_description);
		$product_description=str_replace(array(chr(13).chr(10), chr(13), chr(10), chr(9)), '<BR />' , $product_description);
		
		$csv_file.='p'.$csv_delimiter_char; // 1
		$csv_file.=$product_id.$csv_delimiter_char; // 2
		$csv_file.=$product_cat_id.$csv_delimiter_char; // 3
		$csv_file.=$product_SKU.$csv_delimiter_char; // 4
		$csv_file.=$product_name.$csv_delimiter_char; // 5
		$csv_file.=$product_price.$csv_delimiter_char; // 6 - price
		$csv_file.=''.$csv_delimiter_char; // 7 - virtual SupplyID // $row1['supplier_id']
		$csv_file.=$manufacturer_id.$csv_delimiter_char; // 8 - virtual ManufactureID
		$csv_file.=$product_meta_title.$csv_delimiter_char; // 9 - virtual Head Title
		$csv_file.=$product_meta_keywords.$csv_delimiter_char; // 10
		$csv_file.=$product_meta_description.$csv_delimiter_char; // 11
		$csv_file.='0'.$csv_delimiter_char; // 12 - virtual (best_sale)
		$csv_file.=$product_short_description.$csv_delimiter_char; // 13
		$csv_file.=$product_stock.$csv_delimiter_char; // 14 stock
		$csv_file.='0'.$csv_delimiter_char; // 15 - virtual (content_tov_id)
		$csv_file.=$manufacturer_name.$csv_delimiter_char; // 16 - manufac_name
		$csv_file.=''.$csv_delimiter_char; // 17 - model
		$csv_file.=$product_description.$csv_delimiter_char; // 18 - opis2
		$csv_file.=$product_small_image.$csv_delimiter_char; // 19 - img1sm
		$csv_file.=$product_image.$csv_delimiter_char; // 20 - img2big
		$csv_file.='item_order'.$csv_delimiter_char; // 21 - sort_order
		$csv_file.=''.$csv_delimiter_char; // 22 - virtual
		$csv_file.=''.$csv_delimiter_char; // 23 - virtual 
		$csv_file.=''.$csv_delimiter_char; // 24 - virtual
		$csv_file.=$product_url.$csv_delimiter_char; // 25 - virtual
		$csv_file.=''.$csv_delimiter_char; // 26 - virtual
		$csv_file.=''.$csv_delimiter_char; // 27 - virtual
		$csv_file.=''.$csv_delimiter_char; // 28 - virtual
		$csv_file.=''.$csv_delimiter_char; // 29 - virtual
		$csv_file.=''.$csv_delimiter_char; // 30 - virtual
		$csv_file.=''.$csv_delimiter_char; // 31 - virtual
		$csv_file.=''.$csv_delimiter_char; // 32 - virtual
		$csv_file.=''.$csv_delimiter_char; // 33 - virtual
		$csv_file.=''.$csv_delimiter_char; // 34 - virtual
		$csv_file.=''.$csv_delimiter_char; // 35 - virtual
		$csv_file.=''.$csv_delimiter_char; // 36 - virtual
		$csv_file.=''.$csv_delimiter_char; // 37 - virtual
		$csv_file.=''.$csv_delimiter_char; // 38 - virtual
		$csv_file.=''.$csv_delimiter_char; // 39 - virtual
		$csv_file.=''.$csv_delimiter_char; // 40 - virtual
		$csv_file.=''.$csv_delimiter_char; // 41 - virtual
		$csv_file.=''.$csv_delimiter_char; // 42 - virtual
		$csv_file.=''.$csv_delimiter_char; // 43 - virtual
		$csv_file.=''.$csv_delimiter_char; // 44 - virtual
		$csv_file.=''.$csv_delimiter_char; // 45 - virtual
		$csv_file.=''.$csv_delimiter_char; // 46 - virtual
		$csv_file.=''.$csv_delimiter_char; // 47 - virtual
		$csv_file.=''.$csv_delimiter_char; // 48 - virtual
		$csv_file.=''.$csv_delimiter_char; // 49 - virtual
		$csv_file.=''.$csv_delimiter_char; // 50 - virtual
		$csv_file.=''.$csv_delimiter_char; // 51 - virtual
		$csv_file.=''.$csv_delimiter_char; // 52 - virtual
				
		
		$csv_file.="\n";
		
	}
	
	// Товары в категориях
	$q7 = mysql_query("SELECT product_id, category_id, position FROM ".$m_category_product." ORDER BY product_id, category_id", $link) or die("Invalid query: ".mysql_error());

	while ($result_row = mysql_fetch_array($q7)) {
		$csv_file.='pg'.$csv_delimiter_char; // 1
		$csv_file.=$result_row['product_id'].$csv_delimiter_char; // 2
		$csv_file.=$result_row['category_id'].$csv_delimiter_char; // 3
		$csv_file.=''.$csv_delimiter_char; // 4 
		$csv_file.=''.$csv_delimiter_char; // 5 
		$csv_file.="\n";
	}
	
	mysql_query("DROP TABLE IF EXISTS `etrade_products_cats_tmp`", $link) or die("Invalid query: ".mysql_error());
	
	// Преобразуем в кодировку win-1251
	if (DB_CHARSET=="utf8") {
		//$csv_file=iconv("utf-8", "windows-1251//TRANSLIT", $csv_file);
	}
	
  switch ($_POST['arc']) {
    case 'csv': $ext='_products.csv';
                break;
    case 'zip': $ext='.csv.zip';
                $zipfile = new zipfile();
                $zipfile->addFile($csv_file, 'magento_products.csv');
                $csv_file=$zipfile->file();
                break;
    case 'gzip':$ext='_products.csv.gz';
                $csv_file=gzencode($csv_file);
                break;
  }
  
  header("Content-Type: application/octetstream");
  header("Content-Disposition: attachment; filename=".'magento'.$ext);
  header("Content-Length: ".strlen($csv_file));
  header("Expires: 0");
  echo $csv_file;
  exit();
}


function Transliterate($string){
  $cyr=array(
     "Щ", "Ш", "Ч","Ц", "Ю", "Я", "Ж","А","Б","В",
     "Г","Д","Е","Ё","З","И","Й","К","Л","М","Н",
     "О","П","Р","С","Т","У","Ф","Х","Ь","Ы","Ъ",
     "Э","Є", "Ї","І",
     "щ", "ш", "ч","ц", "ю", "я", "ж","а","б","в",
     "г","д","е","ё","з","и","й","к","л","м","н",
     "о","п","р","с","т","у","ф","х","ь","ы","ъ",
     "э","є", "ї","і"
  );
  $lat=array(
     "Shch","Sh","Ch","C","Yu","Ya","J","A","B","V",
     "G","D","e","e","Z","I","y","K","L","M","N",
     "O","P","R","S","T","U","F","H","", 
     "Y","" ,"E","E","Yi","I",
     "shch","sh","ch","c","Yu","Ya","j","a","b","v",
     "g","d","e","e","z","i","y","k","l","m","n",
     "o","p","r","s","t","u","f","h",
     "", "y","" ,"e","e","yi","i"
  );
  for($i=0; $i<count($cyr); $i++)  {
     $c_cyr = $cyr[$i];
     $c_lat = $lat[$i];
     $string = str_replace($c_cyr, $c_lat, $string);
  }
  $string = 
  	preg_replace(
  		"/([qwrtpsdfghklzxcvbnmQWRTPSDFGHKLZXCVBNM]+)[jJ]e/", 
  		"\${1}e", $string);
  $string = 
  	preg_replace(
  		"/([qwrtpsdfghklzxcvbnmQWRTPSDFGHKLZXCVBNM]+)[jJ]/", 
  		"\${1}'", $string);
  $string = preg_replace("/([eyuioaEYUIOA]+)[Kk]h/", "\${1}h", $string);
  $string = preg_replace("/^kh/", "h", $string);
  $string = preg_replace("/^Kh/", "H", $string);
  return $string;
}


function cyr_to_translit_utf8($string){
  $string = str_replace(array(" ",'"',"&","<",">"), array(" "), $string);
  $string = preg_replace("/[_\s\.,?!\[\](){}]+/", "_", $string);
  $string = preg_replace("/-{2,}/", "--", $string);
  $string = preg_replace("/_-+_/", "--", $string);
  $string = preg_replace("/[_\-]+$/", "", $string);
  $string = Transliterate($string);
  $string = strtolower($string);
  $string = preg_replace("/j{2,}/", "j", $string);
  $string = preg_replace("/[^0-9a-z_\-]+/", "", $string);
  return $string;
}


function unhtmlentities($string) {
  $trans_tbl = get_html_translation_table (HTML_ENTITIES);
  $trans_tbl = array_flip ($trans_tbl);
  return strtr ($string, $trans_tbl);
}




// Append associative array elements
function array_push_associative(&$arr) {
   $args = func_get_args();
   foreach ($args as $arg) {
       if (is_array($arg)) {
           foreach ($arg as $key => $value) {
               $arr[$key] = $value;
               $ret++;
           }
       }else{
           $arr[$arg] = "";
       }
   }
   return $ret;
}




function UnpackGZip($dir, $pack_filename, $log=0) {
	
	$unpack_filename = 'unpacked_file.csv';
	
	// Узнаем размер распакованного файла
	$FileOpen = fopen($pack_filename, "rb");
	fseek($FileOpen, -4, SEEK_END);
	
	$buf = fread($FileOpen, 4);
	$GZFileSize = end(unpack("V", $buf));
	fclose($FileOpen);
		
        //$HandleRead = gzopen($unpack_filename, "rb");
        //$ContentRead = gzread($HandleRead, $GZFileSize);
		
	

	if($zd = gzopen($pack_filename, "rb")) {
		//filesize($pack_filename)*5
		$GZipUnpackContent = gzread($zd, $GZFileSize);
		gzclose($zd);
			
		// Создаем новый файл
		if ($fs = fopen($dir . $unpack_filename,"w")) {
			// Сохраняем новый файл
			if (fwrite($fs, $GZipUnpackContent) === FALSE) {
				echo "Не могу произвести запись в файл ($dir . $filename)";
				exit;
			} else {
				//echo "файл ($filename) удачно распакован !";
			}

			fclose($fs);
		}
	}

	return $unpack_filename;
}


/**
 * Распаковывает zip-архив в указанный каталог.
 * $dir  - полный путь к каталогу.
 * $file - полный путь к zip-архиву.
 * $log  - флаг вывода диагностики.
 */

function unpackZip($dir, $file, $log) {
    $name="";
	
	if ($log) echo "Start unpack [$file] into [$dir] directory... <br>";
	
    if ($zip = zip_open($file)) {
        if ($zip) {
            // создать каталог, если он не существует
            //if (!file_exists($dir)) mkdir($dir);
			if (is_dir($dir)==false) mkdir($dir);

            while ($zip_entry = zip_read($zip)) {
                $name = zip_entry_name($zip_entry);
                if ($log) echo "   <b>unpack </b>: $name <br>";

                // последний символ имени
                $last = substr($name, strlen($name)-1);

                // если это каталог
                if ($last == "/" || $last == "\\") {
                    $subdir = $dir."/".$name;
                    // создать каталог, если он не существует
                    if (! file_exists($subdir)) mkdir ($subdir);
                } else {
                    // распаковать файл
                    if (zip_entry_open($zip,$zip_entry,"r")) {
                        $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                        $fp=fopen($dir."/".zip_entry_name($zip_entry),"w");
                        fwrite($fp,$buf);
                        zip_entry_close($zip_entry);
                    } else {
                        return false;
                    }
                }
            }
            zip_close($zip);
        }
    } else {
        return $name;
    }

    return $name ;
}




/**
 * Zip file creation class.
 * Makes zip files.
 *
 * Based on :
 *
 *  http://www.zend.com/codex.php?id=535&single=1
 *  By Eric Mueller <eric@themepark.com>
 *
 *  http://www.zend.com/codex.php?id=470&single=1
 *  by Denis125 <webmaster@atlant.ru>
 *
 *  a patch from Peter Listiak <mlady@users.sourceforge.net> for last modified
 *  date and time of the compressed file
 *
 * Official ZIP file format: http://www.pkware.com/appnote.txt
 *
 * @access  public
 */
class zipfile
{
    /**
     * Array to store compressed data
     *
     * @var  array    $datasec
     */
    var $datasec      = array();

    /**
     * Central directory
     *
     * @var  array    $ctrl_dir
     */
    var $ctrl_dir     = array();

    /**
     * End of central directory record
     *
     * @var  string   $eof_ctrl_dir
     */
    var $eof_ctrl_dir = "\x50\x4b\x05\x06\x00\x00\x00\x00";

    /**
     * Last offset position
     *
     * @var  integer  $old_offset
     */
    var $old_offset   = 0;


    /**
     * Converts an Unix timestamp to a four byte DOS date and time format (date
     * in high two bytes, time in low two bytes allowing magnitude comparison).
     *
     * @param  integer  the current Unix timestamp
     *
     * @return integer  the current date in a four byte DOS format
     *
     * @access private
     */
    function unix2DosTime($unixtime = 0) {
        $timearray = ($unixtime == 0) ? getdate() : getdate($unixtime);

        if ($timearray['year'] < 1980) {
          $timearray['year']    = 1980;
          $timearray['mon']     = 1;
          $timearray['mday']    = 1;
          $timearray['hours']   = 0;
          $timearray['minutes'] = 0;
          $timearray['seconds'] = 0;
        } // end if

        return (($timearray['year'] - 1980) << 25) | ($timearray['mon'] << 21) | ($timearray['mday'] << 16) |
                ($timearray['hours'] << 11) | ($timearray['minutes'] << 5) | ($timearray['seconds'] >> 1);
    } // end of the 'unix2DosTime()' method


    /**
     * Adds "file" to archive
     *
     * @param  string   file contents
     * @param  string   name of the file in the archive (may contains the path)
     * @param  integer  the current timestamp
     *
     * @access public
     */
    function addFile($data, $name, $time = 0)
    {
        $name     = str_replace('\\', '/', $name);

        $dtime    = dechex($this->unix2DosTime($time));
        $hexdtime = '\x' . $dtime[6] . $dtime[7]
                  . '\x' . $dtime[4] . $dtime[5]
                  . '\x' . $dtime[2] . $dtime[3]
                  . '\x' . $dtime[0] . $dtime[1];
        eval('$hexdtime = "' . $hexdtime . '";');

        $fr   = "\x50\x4b\x03\x04";
        $fr   .= "\x14\x00";            // ver needed to extract
        $fr   .= "\x00\x00";            // gen purpose bit flag
        $fr   .= "\x08\x00";            // compression method
        $fr   .= $hexdtime;             // last mod time and date

        // "local file header" segment
        $unc_len = strlen($data);
        $crc     = crc32($data);
        $zdata   = gzcompress($data);
        $zdata   = substr(substr($zdata, 0, strlen($zdata) - 4), 2); // fix crc bug
        $c_len   = strlen($zdata);
        $fr      .= pack('V', $crc);             // crc32
        $fr      .= pack('V', $c_len);           // compressed filesize
        $fr      .= pack('V', $unc_len);         // uncompressed filesize
        $fr      .= pack('v', strlen($name));    // length of filename
        $fr      .= pack('v', 0);                // extra field length
        $fr      .= $name;

        // "file data" segment
        $fr .= $zdata;

        // "data descriptor" segment (optional but necessary if archive is not
        // served as file)
        $fr .= pack('V', $crc);                 // crc32
        $fr .= pack('V', $c_len);               // compressed filesize
        $fr .= pack('V', $unc_len);             // uncompressed filesize

        // add this entry to array
        $this -> datasec[] = $fr;
        $new_offset        = strlen(implode('', $this->datasec));

        // now add to central directory record
        $cdrec = "\x50\x4b\x01\x02";
        $cdrec .= "\x00\x00";                // version made by
        $cdrec .= "\x14\x00";                // version needed to extract
        $cdrec .= "\x00\x00";                // gen purpose bit flag
        $cdrec .= "\x08\x00";                // compression method
        $cdrec .= $hexdtime;                 // last mod time & date
        $cdrec .= pack('V', $crc);           // crc32
        $cdrec .= pack('V', $c_len);         // compressed filesize
        $cdrec .= pack('V', $unc_len);       // uncompressed filesize
        $cdrec .= pack('v', strlen($name) ); // length of filename
        $cdrec .= pack('v', 0 );             // extra field length
        $cdrec .= pack('v', 0 );             // file comment length
        $cdrec .= pack('v', 0 );             // disk number start
        $cdrec .= pack('v', 0 );             // internal file attributes
        $cdrec .= pack('V', 32 );            // external file attributes - 'archive' bit set

        $cdrec .= pack('V', $this -> old_offset ); // relative offset of local header
        $this -> old_offset = $new_offset;

        $cdrec .= $name;

        // optional extra field, file comment goes here
        // save to central directory
        $this -> ctrl_dir[] = $cdrec;
    } // end of the 'addFile()' method


    /**
     * Dumps out file
     *
     * @return  string  the zipped file
     *
     * @access public
     */
    function file()
    {
        $data    = implode('', $this -> datasec);
        $ctrldir = implode('', $this -> ctrl_dir);

        return
            $data .
            $ctrldir .
            $this -> eof_ctrl_dir .
            pack('v', sizeof($this -> ctrl_dir)) .  // total # of entries "on this disk"
            pack('v', sizeof($this -> ctrl_dir)) .  // total # of entries overall
            pack('V', strlen($ctrldir)) .           // size of central dir
            pack('V', strlen($data)) .              // offset to start of central dir
            "\x00\x00";                             // .zip file comment length
    } // end of the 'file()' method

} // end of the 'zipfile' class

?>


<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title>E-Trade - Export & Import .csv v.1.2 for Magento 1.5   |   www.elbuz.com</title>
<style type="text/css">

.contentBoxHeading {
	font-family: Georgia, Arial,Tahoma,Verdana,sans-serif;
	font-size: 24px;
	font-weight: normal;
	font-style: italic;
}

.main {
	font-family: Arial,Tahoma,Verdana,sans-serif;
	font-size: 12px;
	font-weight: normal;
}

</style>

</head>

<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">

<table border="0" width="50%" cellspacing="2" cellpadding="2" align="center">
  <tr>
    <td class="boxCenter" width="100%" valign="top" align="center"><br />
    <h1 class="contentBoxHeading">Импорт / Экспорт данных в формате E-Trade CSV <br />для интернет-магазина Magento</h1>
		<img src="http://www.magentocommerce.com/img/logo.gif" width="171" height="47" /><br /><br />
	  <table border="1" bordercolor="#C0C0C0" style="border-collapse: collapse" width="100%" cellspacing="0" cellpadding="2">

	  <tr>
        <td class="main" width="50%" align="center"><b><em>Экспорт данных с сайта в файл CSV.</em></b></td>
		<td class="main" width="50%" align="center"><b><em>Импорт данных из файла CSV на сайт.</em></b></td>
	  </tr>
	  
      <tr>
        <td class="main" width="50%">
		<br><p><b>Экспорт категорий и товаров для программ<br /><a href="http://elbuz.com" target="_blank">E-Trade PriceList Importer, E-Trade Content Creator.</a></b><br>
          <form id="csv_export" action="<?php echo basename($_SERVER['PHP_SELF'])?>" method="post">

          <input type="hidden" name="action" value="export_products">
          <br>
          <select name="arc">
            <option value="csv">csv</option>
            <option value="zip">csv.zip</option>
            <option value="gzip">csv.gzip</option>
          </select>
          <input type="submit" value="Экспортировать">
          </form>
        </td>
		
        <td class="main">
		
		<br><b>КАТЕГОРИИ И ТОВАРЫ.<br />Импорт категорий и товаров из <a href="http://elbuz.com" target="_blank">E-Trade PriceList Importer.</a></b><br>
		
          <form id="csv_import" action="<?php echo basename($_SERVER['PHP_SELF'])?>" method="post" enctype="multipart/form-data">
		  <input type="file" name="csv_file">
          <input type="hidden" name="action" value="import_products_and_cats">
          <br>
          Макс. размер файла: <b><?php echo ini_get('upload_max_filesize'); ?></b>
          <br>
          <select name="arc">
            <option value="csv">file.csv</option>
			<option value="zip">file.zip</option>
			<option value="gzip">file.gzip</option>
          </select>			  
          <input type="submit" value="Импортировать">
          </form>
        </td>
      </tr>


	  
	  <tr>
        <td class="main" width="50%"><?php echo 'db_name: '.DB_SERVER.'<br/>'.'db_host: '.DB_DATABASE.'<br/>memory_limit: '.ini_get('memory_limit').'<br/>'.'post_max_size: '.ini_get('post_max_size').'<br/>'.'max_execution_time: '.ini_get('max_execution_time').'<br/>'.'max_input_time: '.ini_get('max_input_time').'<br/>locale: '.$ini_get_locale ?></td>

		<td class="main">
		
		<br><b>ОПИСАНИЕ ТОВАРОВ.<br />Импорт технических характеристик товаров из <a href="http://elbuz.com" target="_blank">E-Trade Content Creator</a></b> для вставки в поле "Описание".<br>
		
          <form id="csv_import" action="<?php echo basename($_SERVER['PHP_SELF'])?>" method="post" enctype="multipart/form-data">
<input type="file" name="csv_file">
          <input type="hidden" name="action" value="import_products_descriptions">
          <br>
          Макс. размер файла: <b><?php echo ini_get('upload_max_filesize'); ?></b>
          <br>
          <select name="arc">
			<option value="csv">file.csv</option>
            <option value="zip">file.zip</option>
			<option value="gzip">file.gzip</option>
          </select>		  
          <input type="submit" value="Импортировать">
          </form>
        </td>
		</tr>
		
		<tr>
        <td class="main" width="50%"></td>

		<td class="main">
		
		<br><b>ФИЛЬТРАЦИЯ.<br />Импорт технических характеристик товаров из <a href="http://elbuz.com" target="_blank">E-Trade Content Creator</a></b> для фильтрации товаров по характеристикам.<br>
		
          <form id="csv_import" action="<?php echo basename($_SERVER['PHP_SELF'])?>" method="post" enctype="multipart/form-data">
<input type="file" name="csv_file">
          <input type="hidden" name="action" value="import_products_parametrs_filter">
          <br>
          Макс. размер файла: <b><?php echo ini_get('upload_max_filesize'); ?></b>
          <br>
          <select name="arc">
			<option value="csv">file.csv</option>
            <option value="zip">file.zip</option>
			<option value="gzip">file.gzip</option>
          </select>		  
          <input type="submit" value="Импортировать">
          </form>
        </td>
      </tr>
 
		</table></td>
	</tr>
	
	<tr>
		<td class="main"><a href="http://elbuz.com" target="_blank"><img src="http://elbuz.com/images/my/elbuzgroup_logo.png" width="125" height="28" border="0" /><a href="http://elbuz.com" target="_blank">© 2006-<?php echo date('Y'); ?> ElbuzGroup</a> Модуль интеграции продуктов серии E-Trade с интернет-магазином на базе Magento.</td>
	</tr>
	<tr>
		<td class="main"><a href="http://elbuz.com/e-trade-pricelist-importer-prays-analizator/opisanie-programmi-analizatora-prays-listov-praysov-e-trade-pricelist-importer.html" target="_blank"><img src="http://elbuz.com/images/my/pli_green_logo_32x32.png" width="32" height="32" border="0" /> <a href="http://elbuz.com/e-trade-content-creator-formirovanie-kontenta/opisanie-programmi-e-trade-content-creator.html" target="_blank"><img src="http://elbuz.com/images/my/cc_purple_logo_32x32.png" width="32" height="32" border="0" /></td>
	</tr>
</table>

</body></html>