
function sendLoadMoreProductsRequest(url) {

    new Ajax.Request(url, {
        onSuccess: function (response) {
            //Create dummy element
            var div = document.createElement('div');
            json = response.responseText.evalJSON(true);
            //div.innerHTML
            html = json.listing;//.listing;//responseText;//.listing;
            // html=div.innerHTML;
            div.innerHTML=html;
            //refresh the page class element
            //$$('.pages')[0].innerHTML = $(div).select('.pages')[0].innerHTML;
            //append the list to the existing product list
            var cataloglist = document.getElementById('category-main--products_list');

            $$('#toolbar-bottom').forEach(function (element) {
                //console.log(element);
                //element.remove();
            });

            var toolbarbottom = document.getElementById('toolbar-bottom');
            //toolbarbottom.
            toolbarbottom.innerHTML=$(div).select('#toolbar-bottom')[0].innerHTML;



            //each()[0].style.display = "none";
            //.remove();
            //$$('#catalog-listing')[0].innerHTML += $(div).select('#catalog-listing')[0].innerHTML;
            cataloglist.innerHTML += $(div).select('#category-main--products_list')[0].innerHTML;//html;//$(div).select('#catalog-listing')[0].innerHTML;
            //check if there are more products to be loaded or not

            if (!$$('a.pag-item.pag-item--next')[0]) {
            //if (!jQuery(html).select('a.pag-item.pag-item--next')[0]) {



                jQuery('#load-more-products').hide();//style.display = "none";
            }

            var placeholder = '/media/lazyload/default/Spinner-1s-200px_2_.gif';
            var img_threshold = '';
            jQuery("img.cwslazyloader").lazyload({
                effect : "fadeIn",
                data_attribute  : "src",
                offset: img_threshold,
                placeholder : placeholder,
                load : function(elements_left, settings) {
                    this.style.width = "auto";
                }
            });
        }
    });
}

function callbackFunc(e) {

    if ($$('a.pag-item.pag-item--next')[0]) {
        var nextPageUrl = $$('a.pag-item.pag-item--next')[0].readAttribute('href');


        sendLoadMoreProductsRequest(nextPageUrl);
    }
    else {

        //hide button
        jQuery('#load-more-products').hide();
        //$$(e.currentTarget).hide();
    }
}

//Assigning click event to the button which triggers the "next" link
//$('load-more-products').observe('click', callbackFunc);

if (!jQuery('a.pag-item.pag-item--next')[0]) {
    jQuery('#load-more-products').hide();//style.display = "none";
}
jQuery(document).ready(function(){

    console.log('catalog custome');
    jQuery('.faq-block__item .faq-block__question').on('click', function () {
        console.log('.faq-block__item .faq-block__question click on');
        jQuery(this).parent().find('.faq-block__answer').toggle('fast');// "slow");//,slideDown();
    });

    jQuery(document).on('click', '#filter_mobile_popup .show_all', function () {

        if (jQuery(this).parent().parent().find('.box_all_links').hasClass('height_little')) {
            console.log('скрыть');
            jQuery(this).parent().parent().find('.box_all_links').removeClass('height_little');
            jQuery(this).text('Скрыть');
        } else {
            console.log('показать');
            jQuery(this).parent().parent().find('.box_all_links').addClass('height_little');
            jQuery(this).text('Показать все');
        }

        return false;
    });
    jQuery(document).on('click', '.sidebar-filter--pin .show_all', function () {

        if (jQuery(this).parent().parent().find('.box_all_links').hasClass('height_little')) {
            console.log('скрыть');
            jQuery(this).parent().parent().find('.box_all_links').removeClass('height_little');
            jQuery(this).text('Скрыть');
        } else {
            console.log('показать');
            jQuery(this).parent().parent().find('.box_all_links').addClass('height_little');
            jQuery(this).text('Показать все');
        }

        return false;
    });




    jQuery(document).on('click', '.cart a.cart-inner', function () {

        var id = jQuery(this).data('id');
        var name = jQuery(this).data('name');
        var brand = jQuery(this).data('brand');
        var price = jQuery(this).data('price');
        var category = jQuery(this).data('category');
        var position = jQuery(this).data('position');
        console.log(name);
        window.dataLayer = window.dataLayer || [];


        dataLayer.push({
            'ecommerce': {
                'currencyCode': 'UAH',
                'click': {
                    'products': [{
                        'name': name,
                        'id': id,
                        'price': price,
                        'brand': brand,
                        'category': category,
                        'position': position
                    }]
                }
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Product Clicks',
            'gtm-ee-event-non-interaction': 'False',
        });

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

            // console.log('smart');
            // $('#category-main--products_list .cart').on('click', function () {
            //jQuery('#category-main--products_list .cart').removeClass('dbclick');
            if (!jQuery(this).parent().hasClass('dbclick'))
            {
                jQuery('#category-main--products_list .cart').removeClass('dbclick');
                jQuery(this).parent().addClass('dbclick');
                return false;
            }

            //});

        }

    });

    jQuery(document).on('click', '#load-more-products', function (e) {
        callbackFunc(e);
    });
    jQuery('.footer').on('click', '.product-popup .close, .product-popup .product-popup--bg, .product-popup .popup-continue',function(){

        jQuery(".product-popup").fadeToggle();
        jQuery("body").toggleClass("popup-act");
    });
    //, '#category-main--products_list',
    jQuery(document).on('click', '.buy_product_list',function () {

        var url = jQuery(this).attr('href');

        var price=jQuery(this).data('price');

        jQuery.ajax({
            url: url,
            type: "POST",
            dataType: 'json',
            data: [],
            success: function (data) {
                /*jQuery('#modalcart').html(data.modal);// .pop-up-wrap
                jQuery('#modalcart').modal('show');//.open();
                */
                jQuery('.minicart_head').html(data.minicart_head);
                jQuery('#popup_cart_add').html(data.modal);// .pop-up-wrap

                gtag('event','add_to_cart', {
                    'send_to': 'AW-10775240954',
                    'value': data.value,
                    'items': data.gtm_items
                });

                jQuery('#popup_cart_add').fadeToggle();
                jQuery('body').toggleClass('popup-act');

                var products_enhecom=data.products;

                dataLayer.push({
                    'ecommerce': {
                        'currencyCode': 'UAH',
                        'add': {
                            'products': products_enhecom
                        }
                    },
                    'event': 'gtm-ee-event',
                    'gtm-ee-event-category': 'Enhanced Ecommerce',
                    'gtm-ee-event-action': 'Adding a Product to a Shopping Cart',
                    'gtm-ee-event-non-interaction': 'False',
                });

                if (data.facebook) {
                    var facebook = data.facebook;
                    fbq('track', 'AddToCart', {
                        content_type: 'product',
                        content_ids: [facebook.id],
                        value: facebook.price,
                        content_name: facebook.content_name,
                        currency: "UAH",
                        content_category: facebook.content_category
                    });

                }

            }
        });
        return false;

    });




    jQuery(document).on('submit', '.product_addtocart_form', function () {

        var url = jQuery(this).attr('action');


        var data = jQuery(this).serialize();
        jQuery.ajax({
            url: url,
            type: "POST",
            dataType: 'json',
            data: data,
            success: function (data) {

                jQuery('#popup_cart_add').html(data.modal);// .pop-up-wrap

                jQuery('.minicart_head').html(data.minicart_head);

                jQuery('#popup_cart_add').fadeToggle();
                jQuery('body').toggleClass('popup-act');

                var products_enhecom=data.products;

                dataLayer.push({
                    'ecommerce': {
                        'currencyCode': 'UAH',
                        'add': {
                            'products': products_enhecom
                        }
                    },
                    'event': 'gtm-ee-event',
                    'gtm-ee-event-category': 'Enhanced Ecommerce',
                    'gtm-ee-event-action': 'Adding a Product to a Shopping Cart',
                    'gtm-ee-event-non-interaction': 'False',
                });

                if (data.facebook) {
                    var facebook = data.facebook;
                    fbq('track', 'AddToCart', {
                        content_type: 'product',
                        content_ids: [facebook.id],
                        value: facebook.price,
                        content_name: facebook.content_name,
                        currency: "UAH",
                        content_category: facebook.content_category
                    });

                }
                //  window.location='/checkout/cart/';
            }
        });
        return false;

    });
});
