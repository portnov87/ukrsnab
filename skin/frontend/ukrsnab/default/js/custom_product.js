jQuery(document).ready(function(){

    if (jQuery(window).width() > 768) {
        if (jQuery('.MagicZoom').length) {


            jQuery('.MagicZoom').jqZoomIt({
                zoomPosition: 'right',
                zoomDistance: 17,
                multiplierX: 1,
                multiplierY: 1,
            });
        }

        jQuery(document).on('click','.viewer-small--image_inner',function(){
            setTimeout(function(){
                jQuery('.MagicZoom').jqZoomIt({
                    zoomPosition: 'right',
                    zoomDistance: 17,
                    multiplierX: 1,
                    multiplierY: 1,
                });
            });
        });
    }
    jQuery('.owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        navText:['',''],
        responsiveClass:true,
        responsive:{
            0:{
                items:4,
                nav:true
            },
            600:{
                items:2,
                nav:false
            },
            1000:{
                items:4,
                nav:true,
                loop:false
            }
        }
    });
});