var lifetime = 3600;
var expireAt = Mage.Cookies.expires;
if (lifetime > 0) {
    expireAt = new Date();
    expireAt.setTime(expireAt.getTime() + lifetime * 1000);
}
Mage.Cookies.set('external_no_cache', 1, expireAt);

jQuery(document).ready(function(){




    jQuery('a.hero-items--table, a.capability-item--inner').each(function(index){
        var src=jQuery(this).data('src');
        console.log('src = ' +src);
        if (src!='') {

            jQuery(this).css('background-image', "url(" + src + ")");
        }
    });
        var placeholder = '/media/lazyload/default/Spinner-1s-200px_2_.gif';
        var img_threshold = '';
        jQuery("img.cwslazyloader").lazyload({
            effect : "fadeIn",
            data_attribute  : "src",
            offset: img_threshold,
            placeholder : placeholder,
            load : function(elements_left, settings) {
                this.style.width = "auto";
            }
        });
    jQuery('.rate_footer label').on('click', function () {
            if (jQuery.cookie(rateCookie)) {

            } else {

                var value = jQuery(this).data('value');

                jQuery.ajax({
                    url: '/ukrsnab/ajax/voterating',
                    data: 'value=' + value + '&url='+cookiepath,
                    dataType: 'json',
                    success: function (data) {

                        //console.log(data);
                        jQuery('.raitingVote').html(data.votes);
                        jQuery('.raitingValue').html(data.raiting);

                        jQuery.cookie("vote_"+cookiepath, '12345679', {expires: 7});
                        jQuery('.rate_footer').removeClass('can_vote');

                    }
                });
            }

            return false;
        });

        jQuery(window).scroll(function () {

            var stickyHeader = jQuery('header'), scrollheader = jQuery(window).scrollTop();

            if (scrollheader >= 160){
                stickyHeader.addClass('zindexHeader');
            } else {
                stickyHeader.removeClass('zindexHeader');
            }
        });

    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() != 0) {
            jQuery('#topNubex').fadeIn();
        } else {
            jQuery('#topNubex').fadeOut();
        }
    });
    jQuery('#topNubex').click(function () {
        jQuery('body,html').animate({scrollTop: 0}, 700);
    });


    $(window).scroll(function () {

        var stickyHeader = $('header'), scrollheader = $(window).scrollTop();

        if (scrollheader >= 160){
            stickyHeader.addClass('zindexHeader');//.slideDown();
        } else {
            stickyHeader.removeClass('zindexHeader');
        }
    });

    jQuery(document).on('click','[data-quantity="plus"]',function (e) {
        //jQuery('[data-quantity="plus"]').click(function (e) {
        // Stop acting like a button

        e.preventDefault();
        // Get the field name
        var fieldName = jQuery(this).attr('data-field');
        // Get its current value
        // var currentVal = parseInt($('input[name=' + fieldName + ']').val());
        var currentVal = parseInt(jQuery(this).parent().parent().find('.input-group-field').val());

        // If is not undefined
        //alert(fieldName+' '+currentVal);
        if (!isNaN(currentVal)) {
            // Increment
            //jQuery(this).parent().parent().find('input[name=' + fieldName + ']').val(currentVal + 1);
            jQuery(this).parent().parent().find('.input-group-field').val(currentVal + 1);


        } else {
            // Otherwise put a 0 there
            jQuery(this).parent().parent().find('.input-group-field').val(0);
            //jQuery(this).parent().parent().find('input[name=' + fieldName + ']').val(0);
        }


        var form=jQuery('.wrapper-basket--page.wrapper-grid form');


        $j.ajax({
            url: form.attr('action'),
            data: form.serialize(),
            dataType: 'json',
            success: function(data) {

                cart_checkout=data.cart_checkout;

                //basket-grid--content
                //page-grid--main
                html=jQuery(cart_checkout).find('.wrapper-basket--page.wrapper-grid > .page-grid--main');//wrapper-basket--page.wrapper-grid .page-grid--main');
                // .basket-grid--content
                //'.wrapper-basket--page.wrapper-grid  .page-grid--main'
                cart_checkout=jQuery('.wrapper-basket--page.wrapper-grid > .page-grid--main').html(html);//data.cart_checkout);
            }
        });


    });
    // This button will decrement the value till 0
    //'.basket-grid'
    jQuery(document).on('click','[data-quantity="minus"]',function (e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var fieldName = jQuery(this).attr('data-field');
        // Get its current value
        var currentVal = parseInt(jQuery(this).parent().parent().find('.input-group-field').val());
        // If it isn't undefined or its greater than 0

        if (!isNaN(currentVal) && currentVal > 1) {
            // Decrement one
            jQuery(this).parent().parent().find('.input-group-field').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            jQuery(this).parent().parent().find('.input-group-field').val(1);
        }

        var form=jQuery('.wrapper-basket--page.wrapper-grid form');


        $j.ajax({
            url: form.attr('action'),
            data: form.serialize(),
            dataType: 'json',
            success: function(data) {

                var cart_checkout=data.cart_checkout;

                //basket-grid--content
                //page-grid--main
                var html=jQuery(cart_checkout).find('.wrapper-basket--page.wrapper-grid > .page-grid--main');//wrapper-basket--page.wrapper-grid .page-grid--main');
                // .basket-grid--content
                //'.wrapper-basket--page.wrapper-grid  .page-grid--main'
                var cart_checkout=jQuery('.wrapper-basket--page.wrapper-grid > .page-grid--main').html(html);//data.cart_checkout);
            }
        });

    });


    jQuery('.oneclick-buy--form, #oneclickorder-form').on('submit',function(){

        var url=jQuery(this).attr('action');

        var phone=jQuery(this).find('input[name="oneclick_buy_phone"]').val();
        var data=jQuery(this).serialize();
        jQuery.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (result) {
                if (result.success){
                    location.href=result.redirect;
                }else{

                }
            }
        });
        return false;

    });

    if (jQuery(window).width() < 768) {

        jQuery('.catalog-menu--category .catalog-menu--category_name').on('click', function () {
            event.preventDefault();

            var subcategories = jQuery(this).parent().find('.catalog-menu--category_subcategories');
            if (subcategories.hasClass('active_menu')) {
                jQuery('.catalog-menu--category_subcategories').removeClass('active_menu');
            } else {
                jQuery('.catalog-menu--category_subcategories').removeClass('active_menu');
                subcategories.addClass('active_menu');
            }
            return false;
        });
    }else{
        jQuery(document).on('mouseenter', '.wrapper-hero--catalog, .catalog-menu--categories.catalog-menu--categories_show, .wrapper-hero--catalog .container.container-sticky--header',function () {
            jQuery('html').append('<div class="overload" style="' +
                'z-index:99;position:fixed;top:0;left:0;width:100%;height:100%;background:rgba(0,0,0,.45)' +
                '"></div>');
        });
        jQuery(document).on('mouseleave', '.wrapper-hero--catalog, .catalog-menu--categories.catalog-menu--categories_show, .wrapper-hero--catalog .container.container-sticky--header', function () {
            jQuery('.overload').remove();
        });
    }

    jQuery('.load_products_category_home').on('click',function(){

        var url='/ukrsnab/ajax/productscategory';//jQuery(this).attr('action');
        jQuery('.load_products_category_home').removeClass('active');
        jQuery(this).addClass('active');
        var parent=jQuery(this).parent().parent().parent().parent();
        var category_id_home=jQuery(this).data('category_id');
        //console.log(parent.html());

        ajax_update=parent.find('.ajax_update-container');
        ajax_update.addClass('loading');
        jQuery.ajax({
            type: 'GET',
            url: url,
            data: 'category_id='+category_id_home,
            dataType: 'json',
            success: function (result) {
                //if (result.content){
                    parent.find('.ajax_update-container').html(result.content);
                ajax_update.removeClass('loading');


                var placeholder = '/media/lazyload/default/Spinner-1s-200px_2_.gif';
                var img_threshold = '';
                jQuery("img.cwslazyloader").lazyload({
                    effect : "fadeIn",
                    data_attribute  : "src",
                    offset: img_threshold,
                    placeholder : placeholder,
                    load : function(elements_left, settings) {
                        this.style.width = "auto";
                    }
                });
            }
        });
        return false;
    });
});